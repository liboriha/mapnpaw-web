<?php

namespace App;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ViewErrorBag;

class BladeHelper
{
    /**
     * Retrieve an old input item.
     * Return old input item only if form is currently processed form.
     * Processed form is defined by $popup.
     *
     * @param  string $key     Item key
     * @param  mixed  $default Default value
     * @param  string $popup   Form js-pop-up data-popup attribute
     * @return mixed
     */
    public static function old($key, $default = null, $popup = null)
    {
        if (($popup == null || session('show-js-popup') == $popup)) {
            return old($key, $default);
        } else {
            return $default;
        }
    }

    /**
     * Checks it out checkbox according to an old input item.
     *
     * @param  string     $key     Item key
     * @param  mixed      $default Default value
     * @param  string     $popup   Form js-pop-up data-popup attribute
     * @return string
     */
    public static function checkingCheckbox(
        $key,
        $default = null,
        $page = null,
        $popup = null,
        $section = null
    ) {
        $errors = self::getErrors();

        // Use old value if forms is processed and has error.
        if (($popup == null || session('show-js-popup') == $popup) &&
            $errors->count() > 0
        ) {
            $checked = (old($key, null) !== null);
        } else {
            $checked = ($default == true);
        }

        if ($checked) {
            return 'checked';
        } else {
            return '';
        }
    }

    /**
     * Returns fail CSS class if item key is in form errors.
     *
     * @param  string     $key     Item key
     * @param  string     $popup   Form js-pop-up data-popup attribute
     * @return string
     */
    public static function failClass($key, $popup = null)
    {
        $errors = self::getErrors();

        if (($popup == null || session('show-js-popup') == $popup) &&
            ($errors->has($key))
        ) {
            return 'fail';
        } else {
            return '';
        }
    }

    /**
     * Returns fail paragraph if item key is in form errors.
     * Add this by {!! \App\BladeHelper::addFailParagraph(...) !!} - unescaped data.
     *
     * @param  string     $key     Item key
     * @param  string     $popup   Form js-pop-up data-popup attribute
     * @return string
     */
    public static function addFailParagraph($key, $popup = null)
    {
        $errors = self::getErrors();

        if (($popup == null || session('show-js-popup') == $popup) &&
            ($errors->has($key))
        ) {
            return '<p class="error-text">' . implode("</br>", $errors->get($key)) . '</p>';
        } else {
            return '';
        }
    }

    /**
     * Get errors messages.
     *
     * @return MessageBag Errors messages
     */
    private static function getErrors()
    {
        return session()->get('errors') ?: new ViewErrorBag;
    }
}
