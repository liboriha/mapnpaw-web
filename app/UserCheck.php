<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class UserCheck extends Model
{
    protected $fillable = ['place_id', 'user_id'];

    /**
     * Get the place that owns the user check.
     */
    public function place()
    {
        return $this->belongsTo(\App\Place::class);
    }

    /**
     * Get the user that owns the user check.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}