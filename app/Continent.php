<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Translatable;

class Continent extends Model
{
    use Translatable;

    protected $fillable = ['name', 'medal_image_locked', 'medal_image_unlocked'];

    protected $translatable = ['name'];

    /**
     * Get the categories for the continent.
     */
    public function categories()
    {
        return $this->hasMany(\App\Category::class);
    }

    /**
     * Get the places for the continent.
     */
    public function places()
    {
        return $this->hasMany(\App\Place::class);
    }
}
