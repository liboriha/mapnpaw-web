<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use TCG\Voyager\Models\Translation;
use TCG\Voyager\Translator;
use TCG\Voyager\Traits\Translatable as VoyagerTranslatable;

/**
 * Fix scopeWithTranslation and scopeWithTranslations methods of TCG\Voyager\Traits\Translatable trait.
 * If Voyager is updated, this trait may not work.
 */
trait Translatable
{
    // Use Voyager Translatable because Voyager testing, that classes use their trait.
    use VoyagerTranslatable {
        // Marks Voyager trait methods as bad methods.
        scopeWithTranslation as bad_scopeWithTranslation;
        scopeWithTranslations as bad_scopeWithTranslations;
    }

    /**
     * Where scope for tranlatable column.
     * Multilingual must be enabled, otherwise search is only in base table.
     * Column must be translatable, otherwise search is only in base table.
     * App language must be supported, otherwise search is only in base table.
     * Using app()-getLocale() to get actual language setting.
     *
     * @param  [type] $query      Database query
     * @param  [type] $columnName Column to search in
     * @param  [type] $value      Value to search
     * @return [type]             Database query
     */
    public function scopeWhereTranslation($query, $columnName, $value)
    {
        if (!is_string($columnName)) {
            throw new InvalidArgumentException('Column name must be a string.');
        }

        if (config('voyager.multilingual.enabled') == true &&
            in_array($columnName, $this->getTranslatableAttributes()) &&
            app()->getLocale() != config('voyager.multilingual.default') &&
            in_array(app()->getLocale(), config('voyager.multilingual.locales'))
        ) {
            // Search in translation table.
            
            return $query->whereHas('translations', function ($query) use ($columnName, $value) {
                $query->where('column_name', $columnName);
                $query->where('value', $value);
                $query->where('locale', app()->getLocale());
            });

            // Extension adds search in base table for value if translation NOT exists (NOT have record). (bit slower)
            
            /*return $query->where(function ($query) use ($columnName, $value) {
                $query->where(function ($query) use ($columnName, $value) {
                    $query->where($columnName, $value);
                    $query->whereDoesntHave('translations', function ($query) use ($columnName, $value) {
                        $query->where('column_name', $columnName);
                        $query->where('locale', app()->getLocale());
                    });
                });
                $query->orWhereHas('translations', function ($query) use ($columnName, $value) {
                    $query->where('column_name', $columnName);
                    $query->where('value', $value);
                    $query->where('locale', app()->getLocale());
                });
            });*/
        } else {
            // Search in base table.
            
            return $query->where($columnName, $value);
        }
    }

    /**
     * This scope eager loads the translations for the default and the fallback locale only.
     * We can use this as a shortcut to improve performance in our application.
     *
     * @param Builder     $query
     * @param string|null $locale
     * @param string|bool $fallback
     */
    public function scopeWithTranslation(Builder $query, $locale = null, $fallback = true)
    {
        if (is_null($locale)) {
            $locale = app()->getLocale();
        }

        if ($fallback === true) {
            $fallback = config('app.fallback_locale', 'en');
        }

        $query->with(['translations' => function (Relation $query) use ($locale, $fallback) {
            $query->where(function ($query) use ($locale, $fallback) {
                $query->where('locale', $locale);

                if ($fallback !== false) {
                    $query->orWhere('locale', $fallback);
                }
            });
        }]);
    }

    /**
     * This scope eager loads the translations for the default and the fallback locale only.
     * We can use this as a shortcut to improve performance in our application.
     *
     * @param Builder           $query
     * @param string|null|array $locales
     * @param string|bool       $fallback
     */
    public function scopeWithTranslations(Builder $query, $locales = null, $fallback = true)
    {
        if (is_null($locales)) {
            $locales = app()->getLocale();
        }

        if ($fallback === true) {
            $fallback = config('app.fallback_locale', 'en');
        }

        $query->with(['translations' => function (Relation $query) use ($locales, $fallback) {
            if (is_null($locales)) {
                return;
            }

            $query->where(function ($query) use ($locales, $fallback) {
                if (is_array($locales)) {
                    $query->whereIn('locale', $locales);
                } else {
                    $query->where('locale', $locales);
                }

                if ($fallback !== false) {
                    $query->orWhere('locale', $fallback);
                }
            });
        }]);
    }
}
