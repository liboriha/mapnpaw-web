<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;
use App\Http\Notifications\ResetPassword as ResetPasswordNotification;

class User extends \TCG\Voyager\Models\User implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'nickname', 'email', 'password', 'visibility',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The default values of attributes.
     *
     * @var array
     */
    protected $attributes = [
        'visibility' => self::VISIBILITY_NAME,
    ];

    /**
     * Visibility of name. (Name or nickname.)
     */
    const VISIBILITY_NAME = 'NAME';
    const VISIBILITY_NICKNAME = 'NICKNAME';

    /**
     * List of visibilities of name.
     *
     * @var array
     */
    public static $visibilities = [self::VISIBILITY_NAME, self::VISIBILITY_NICKNAME];

    public static function boot()
    {
        parent::boot();

        self::deleting(function (User $user) {
            // Before delete, delete attached user photos.
            foreach ($user->userPhotos as $userPhoto) {
                $userPhoto->delete();
            }

            // Before delete, delete user avatar.
            if (config('voyager.user.default_avatar') != $user->avatar &&
                Storage::disk(config('voyager.storage.disk'))->exists($user->avatar)
            ) {
                Storage::disk(config('voyager.storage.disk'))->delete($user->avatar);
            }
        });
    }


    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Get the user checks for the user.
     */
    public function userChecks()
    {
        return $this->hasMany(\App\UserCheck::class);
    }

    /**
     * Get the user photos for the user.
     */
    public function userPhotos()
    {
        return $this->hasMany(\App\UserPhoto::class);
    }

    /**
     * Get the user ratings for the user.
     */
    public function userRatings()
    {
        return $this->hasMany(\App\UserRating::class, 'user_id', 'id');
    }

    /**
     * Get the user points for the user.
     */
    public function userPoints()
    {
        return $this->hasMany(\App\UserPoint::class);
    }

    /**
     * Get the user medals for the user.
     */
    public function userMedals()
    {
        return $this->hasMany(\App\UserMedal::class);
    }

    /**
     * Get the user flags for the user.
     */
    public function userFlags()
    {
        return $this->hasMany(\App\UserFlag::class);
    }

    /**
     * Get the points sum for the user.
     */
    public function pointsSum()
    {
        return $this->hasOne(\App\UserPoint::class)
            ->selectRaw('sum(points) as points_sum, user_id')
            ->groupBy('user_id');
    }

    /**
     * Get attribute pointsSum.
     */
    public function getPointsSumAttribute()
    {
        if (!array_key_exists('pointsSum', $this->relations)) {
            $this->load('pointsSum');
        }

        $relation = $this->getRelation('pointsSum');

        return ($relation) ? $relation->points_sum : 0;
    }

    /**
     * Get attribute name.
     */
    public function getNameAttribute()
    {
        if ($this->visibility == self::VISIBILITY_NAME) {
            return trim($this->firstname . " " . $this->lastname);
        } else {
            return $this->nickname;
        }
    }

    /**
     * Determine if user is registered by email or facebook.
     */
    public function isFacebookUser()
    {
        return $this->facebook_id != null;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
}
