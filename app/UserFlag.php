<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class UserFlag extends Model
{
    protected $fillable = ['user_id', 'continent_id', 'category_id', 'place_id'];

    /**
     * Get the continent that owns the user flag.
     */
    public function continent()
    {
        return $this->belongsTo(\App\Continent::class);
    }

    /**
     * Get the category that owns the user flag.
     */
    public function category()
    {
        return $this->belongsTo(\App\Category::class);
    }

    /**
     * Get the category that owns the user flag.
     */
    public function place()
    {
        return $this->belongsTo(\App\Place::class);
    }
}