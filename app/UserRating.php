<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class UserRating extends Model
{
    protected $fillable = ['place_id', 'user_id', 'stars', 'rating'];

    /**
     * Get the place that owns the user rating.
     */
    public function place()
    {
        return $this->belongsTo(\App\Place::class);
    }

    /**
     * Get the user that owns the user rating.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}