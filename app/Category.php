<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Traits\Translatable;

class Category extends Model
{
    use Translatable;

    protected $fillable = [
        'continent_id',
        'name',
        'medal_name',
        'medal_image_locked',
        'medal_image_unlocked',
        'flag_name',
        'flag_image_locked',
        'flag_image_unlocked',
        'radius'
    ];

    protected $translatable = [
        'name',
        'medal_name',
        'flag_name'
    ];

    protected $table = 'categories';

    // Variables to store medal and flag user state. Need set by processUserInfo method.
    public $achieved_medal;
    public $achieved_flag;

    /**
     * Get the continent that owns the category.
     */
    public function continent()
    {
        return $this->belongsTo(\App\Continent::class);
    }

    /**
     * Get the places for the category.
     */
    public function places()
    {
        return $this->hasMany(\App\Place::class);
    }

    /**
     * Get the user medals for the category.
     */
    public function userMedals()
    {
        return $this->hasMany(\App\UserMedal::class);
    }

    /**
     * Get the user flags for the category.
     */
    public function userFlags()
    {
        return $this->hasMany(\App\UserFlag::class);
    }

    /**
     * Get the flag info to category (flag place count).
     */
    public function flagInfo()
    {
        return $this->hasOne(\App\Place::class)
            ->select(DB::raw('category_id, COUNT(*) as flag_place_count'))
            ->where('is_flag', true)
            ->groupBy('category_id');
    }

    /**
     * Get the medal info to category (top place count).
     */
    public function medalInfo()
    {
        return $this->hasOne(\App\Place::class)
            ->select(DB::raw('category_id, COUNT(*) as top_place_count'))
            ->where('is_top', true)
            ->groupBy('category_id');
    }

    /**
     * Scope a query to only include categories with at least one top place.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereHasMedal($query)
    {
        return $query->whereHas('places', function ($q) {
            $q->where('is_top', true);
        });
    }

    /**
     * Returns if category has medal - has at least one top place.
     *
     * @return boolean
     */
    public function hasMedal()
    {
        // Need loaded medalInfo relation.
        if (!array_key_exists('medalInfo', $this->relations)) {
            $this->load('medalInfo');
        }

        $relation = $this->getRelation('medalInfo');

        if ($relation && $relation->top_place_count > 0) {
            return true;
        }

        return false;
    }

    /**
     * Scope a query to only include categories with flag place.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereHasFlag($query)
    {
        return $query->whereHas('places', function ($q) {
            $q->where('is_flag', true);
        });
    }

    /**
     * Returns if category has flag - has flag place.
     *
     * @return boolean
     */
    public function hasFlag()
    {
        // Need loaded flagInfo relation.
        if (!array_key_exists('flagInfo', $this->relations)) {
            $this->load('flagInfo');
        }

        $relation = $this->getRelation('flagInfo');

        if ($relation && $relation->flag_place_count > 0) {
            return true;
        }

        return false;
    }

    /**
     * Get category name with continent name.
     */
    public function getNameWithContinentAttribute()
    {
        return $this->name . " (" . $this->continent->name . ")";
    }

    /**
     * Get real radius (in meters). If radius is not set, use default radius.
     */
    public function getRealRadiusAttribute()
    {
        return $this->radius ?? config('awapp.places.defaults.radius');
    }

    /**
     * Method to process user info to set information about achieved medal or flag by that user.
     * Can call with user_id - useful for single category (not need whole user model).
     * Can call with user model - useful for process list of categories (use cached data).
     */
    public function processUserInfo($user)
    {
        if (is_numeric($user)) {
            $user_id = $user;

            $this->achieved_medal = false;
            $userMedal = $this->userMedals()->where('user_id', $user_id)->first();
            if ($userMedal != null) {
                $this->achieved_medal = true;
            }

            $this->achieved_flag = false;
            $userFlag = $this->userFlags()->where('user_id', $user_id)->first();
            if ($userFlag != null) {
                $this->achieved_flag = true;
            }
        } else {
            $this->achieved_medal = false;
            // Use cached data on user model if exists.
            $userMedal = $user->userMedals->where('category_id', $this->id)->first();
            if ($userMedal != null) {
                $this->achieved_medal = true;
            }

            $this->achieved_flag = false;
            // Use cached data on user model if exists.
            $userFlag = $user->userFlags->where('category_id', $this->id)->first();
            if ($userFlag != null) {
                $this->achieved_flag = true;
            }
        }
    }
}
