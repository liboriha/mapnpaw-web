<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class UserPoint extends Model
{
    protected $fillable = ['place_id', 'user_id', 'type', 'points'];

    /**
     * Types.
     */
    const TYPE_CHECKIN = 'CHECKIN';
    const TYPE_FIRST_PHOTO = 'FIRST_PHOTO';

    /**
     * List of types.
     *
     * @var array
     */
    public static $types = [self::TYPE_CHECKIN, self::TYPE_FIRST_PHOTO];


    /**
     * Get the place that owns the user point.
     */
    public function place()
    {
        return $this->belongsTo(\App\Place::class);
    }

    /**
     * Get the user that owns the user point.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}