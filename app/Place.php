<?php

namespace App;

use App\Http\Helpers\FileHelper;
use App\Http\Helpers\MapHelper;
use App\Services\PlaceCluster\UpdatePlaceClustersService;
use Carbon\Carbon;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Place extends Model
{
    use SpatialTrait;

    protected $fillable = [
        'continent_id',
        'category_id',
        'name',
        'image',
        'description',
        'address',
        'gps',
        'radius',
        'is_top',
        'is_flag'
    ];

    protected $spatialFields = [
        'gps',
    ];

    // Variables to store place user state. Need set by processUserInfo method.
    public $checkedIn;
    public $lastCheckIn;
    public $canCheckIn;
    public $inRange;

    // Variables to store checkin state. Need set by processCheckIn method.
    public $achieved_points;
    public $top_points_remaining;

    public static function boot()
    {
        parent::boot();

        self::saving(function (Place $place) {
            // Before saving, set continent_id attribute according to setted category.
            $place->continent_id = $place->category->continent_id;
        });

        // Update clusters of place - new, update
        self::saved(function (Place $place) {
            $updateSvr = app(UpdatePlaceClustersService::class);

            if (empty($place->getOriginal())) {
                $place = $place->fresh();
                $gps = $place->gps;
                $categoryId = $place->category_id;

                // Update assigned clusters - new place
                $updateSvr->updateClusters(null, null, $gps, $categoryId);
            } else {
                $origGps = $place->getOriginal()['gps'];
                $origCategoryId = $place->getOriginal()['category_id'];
                $place = $place->fresh();
                $gps = $place->gps;
                $categoryId = $place->category_id;

                // Update assigned clusters - update place
                $updateSvr->updateClusters($origGps, $origCategoryId, $gps, $categoryId);
            }
        });

        // Update clusters of place - delete
        self::deleted(function (Place $place) {
            $updateSvr = app(UpdatePlaceClustersService::class);

            $origGps = $place->getOriginal()['gps'];
            $origCategoryId = $place->getOriginal()['category_id'];

            // Update assigned clusters - delete place
            $updateSvr->updateClusters($origGps, $origCategoryId, null, null);
        });

        self::deleting(function (Place $place) {
            if (Storage::disk(config('voyager.storage.disk'))->exists($place->image)) {
                Storage::disk(config('voyager.storage.disk'))->delete($place->image);
            }
            foreach ($place->userPhotos()->get() as $userPhoto) {
                $userPhoto->delete();
            }
        });
    }

    /**
     * Get the continent that owns the place.
     */
    public function continent()
    {
        return $this->belongsTo(\App\Continent::class);
    }

    /**
     * Get the category that owns the place.
     */
    public function category()
    {
        return $this->belongsTo(\App\Category::class);
    }

    /**
     * Get the user photos for the place.
     */
    public function userPhotos()
    {
        return $this->hasMany(\App\UserPhoto::class)->orderBy('created_at');
    }

    /**
     * Get the user ratings for the place.
     */
    public function userRatings()
    {
        return $this->hasMany(\App\UserRating::class)->orderBy('created_at');
    }

    /**
     * Get the user checks for the place.
     */
    public function userChecks()
    {
        return $this->hasMany(\App\UserCheck::class);
    }

    /**
     * Get the average stars rating for the place.
     */
    public function averageStarsRatings()
    {
        return $this->hasOne(\App\UserRating::class)
            ->selectRaw('avg(stars) as average_stars, place_id')
            ->groupBy('place_id');
    }

    /**
     * Scope a query to only include top places.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsTop($query)
    {
        return $query->where('is_top', true);
    }

    /**
     * Scope a query to only include flag places.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeIsFlag($query)
    {
        return $query->where('is_flag', true);
    }

    /**
     * Get attribute averageStarsRatings.
     */
    public function getAverageStarsRatingsAttribute()
    {
        if (!array_key_exists('averageStarsRatings', $this->relations)) {
            $this->load('averageStarsRatings');
        }

        $relation = $this->getRelation('averageStarsRatings');

        return ($relation) ? $relation->average_stars : 0;
    }

    /**
     * Get image of place. If image is null, get first image from user.
     */
    public function getImageAttribute($value)
    {
        if (empty($value) && $this->userPhotos->isNotEmpty()) {
            return $this->userPhotos->first()->image;
        }

        return $value;
    }

    /**
     * Get real radius (in meters). If radius is not set, use default radius.
     */
    public function getRealRadiusAttribute()
    {
        if ($this->radius != null) {
            return $this->radius;
        } else {
            return $this->category->real_radius;
        }
    }

    /**
     * Method to process user info to set information about if this place is checked or can be checked by that user.
     * If you process more places, is better to eager load userChecks with corresponding user ID.
     */
    public function processUserInfo($user_id, $user_lat = null, $user_lng = null)
    {
        if (array_key_exists('userChecks', $this->relations)) {
            // Used with eager loaded userChecks data - must be loaded with data of user.
            $lastCheck = $this->userChecks->where('user_id', $user_id)->sortByDesc('created_at')->first();
        } else {
            $lastCheck = $this->userChecks()->where('user_id', $user_id)->orderBy('created_at', 'desc')->first();
        }

        $needLastCheckCreatedBefore = Carbon::now()->subDays(config('awapp.places.defaults.wait-for-next'));

        // Determine if last checkin is checked before time that user need to wait to can do next checkin.
        // Or last checkin not exists.
        $lastCheckOK = true;

        if ($lastCheck != null) {
            $this->checkedIn = true;
            $this->lastCheckIn = $lastCheck->created_at->timestamp;

            if ($lastCheck->created_at->gt($needLastCheckCreatedBefore)) {
                // Can't checkin because user checkin this place in close time.
                $lastCheckOK = false;
            }
        } else {
            $this->checkedIn = false;
            $this->lastCheckIn = 0;
        }

        if ($user_lat != null && $user_lng != null) {
            $distanceFromPlace = MapHelper::getDistance($user_lat, $user_lng, $this->gps->getLat(), $this->gps->getLng());
            // Check radius and time of last checkin.
            if ($distanceFromPlace <= $this->real_radius) {
                $this->inRange = true;

                if ($lastCheckOK) {
                    $this->canCheckIn = true;
                } else {
                    $this->canCheckIn = false;
                }
            } else {
                $this->inRange = false;
                $this->canCheckIn = false;
            }
        }
    }

    /**
     * Format and return array of (lat,lng) pairs of points fetched from the database.
     *
     * @return array $coords
     */
    public function getCoordinates()
    {
        // If gps field is null, return empty array.
        if ($this->gps == null) {
            return [];
        }

        return [
            [
                'lat' => $this->gps->getLat(),
                'lng' => $this->gps->getLng(),
            ]
        ];
    }

    /**
     * User check-in on place.
     * In attribute achieved_points is points that user achieve by check-in this place.
     * Also check if user win medal (if soo, create record about winning medal).
     * Also check if user win flag (if soo, create record about winning medal).
     */
    public function processCheckIn($user_id)
    {
        // No check for GPS coordinates even no check last checkin.

        $userChecks = $this->userChecks()->first();
        if ($userChecks == null) {
            // First check-in
            $this->achieved_points = config('awapp.places.points.first-checkin');
        } else {
            // Next check-in
            $this->achieved_points = config('awapp.places.points.next-checkin');
        }

        // Add record about checkin
        UserCheck::create([
            'user_id' => $user_id,
            'place_id' => $this->id
        ]);

        // Add record with points
        UserPoint::create([
            'user_id' => $user_id,
            'place_id' => $this->id,
            'type' => UserPoint::TYPE_CHECKIN,
            'points' => $this->achieved_points
        ]);

        // Check if place is flag place, if so then create record about winning flag (if user doesn't have it).

        if ($this->is_flag) {
            // Get log about achieving flag by user in place category.
            // Condition on category_id not place_id. Flag belongs to category, place_id is only for logging.
            $userFlagRecord = UserFlag::where('user_id', $user_id)->where('category_id', $this->category_id)->first();

            if ($userFlagRecord == null) {
                // Create log about achieving flag by user in this place category.
                UserFlag::create([
                    'user_id' => $user_id,
                    'place_id' => $this->id,
                    'continent_id' => $this->continent_id,
                    'category_id' => $this->category_id
                ]);
            }
        }

        // Check if place is top place, if so then check it. And if user checkin all top places (count of top places
        // is defined in config 'awapp.places.medals.TOP-get-medal-count') then create record about winning medal
        // (if user doesn't have it).

        if ($this->is_top) {
            $userMedalRecord = UserMedal::where('user_id', $user_id)
                ->where('continent_id', $this->continent_id)
                ->where('category_id', $this->category_id)
                ->first();

            // Get already checked top places by user in place category.
            $category_id = $this->category_id;
            $checkedTopPlaces = UserCheck::select('place_id')
                ->where('user_id', $user_id)
                ->whereHas('place', function ($query) use ($category_id) {
                    $query->where('category_id', $category_id);
                    $query->where('is_top', true);
                })
                ->groupBy('place_id')
                ->get();

            // Count remaining top places needs check-in to achieving medal.
            $this->top_points_remaining = config('awapp.places.medals.TOP-get-medal-count') - $checkedTopPlaces->count();
            if ($this->top_points_remaining < 0) {
                $this->top_points_remaining = 0;
            }

            if ($userMedalRecord == null && $checkedTopPlaces->count() >= config('awapp.places.medals.TOP-get-medal-count')) {
                // Create log about achieving medal by user in this place category. Added actual checked places.
                $userMedal = UserMedal::create([
                    'user_id' => $user_id,
                    'continent_id' => $this->continent_id,
                    'category_id' => $this->category_id
                ]);

                $placesIDs = $checkedTopPlaces->pluck('place_id');

                $userMedal->places()->attach($placesIDs);
            }
        }

        if ($this->is_flag || $this->is_top) {
            // Set user info to place category to show info about state of achieving medal and flag of place category.
            $this->category->processUserInfo($user_id);
        }
    }

    /**
     * Adds user photo of user to this place.
     * In attribute achieved_points is points that user achieve by adding photo.
     *
     * @return boolean If photo is added or not (reached limit)
     */
    public function addUserPhotoBase64($user_id, $base64Image)
    {
        // Save image
        $path = config('awapp.images.user-photo.directory') . '/' . date('FY') . '/'; // e.g.: tablename/November2017/

        $savedFileName = FileHelper::saveBase64Image(
            $base64Image,
            config('voyager.storage.disk'),
            $path,
            config('awapp.images.user-photo.prefix')
        );

        // Determine if it's first photo of user to this place and not reach limit
        $userPhotoCount = $this->userPhotos->where('user_id', $user_id)->count();

        if ($userPhotoCount >= config('awapp.places.photos.user-limit-count')) {
            return false;
        }

        if ($userPhotoCount == 0) {
            // First photo
            $this->achieved_points = config('awapp.places.points.first-photo');

            UserPoint::create([
                'place_id' => $this->id,
                'user_id' => $user_id,
                'type' => UserPoint::TYPE_FIRST_PHOTO,
                'points' => config('awapp.places.points.first-photo')
            ]);
        } else {
            // Next photo
            $this->achieved_points = config('awapp.places.points.next-photo');
        }

        UserPhoto::create([
            'place_id' => $this->id,
            'user_id' => $user_id,
            'image' => $path . $savedFileName
        ]);

        return true;
    }

    /**
     * Adds user rating of user to this place.
     * If user rating exists then rewrite it.
     *
     * @return boolean If photo is added or not (reached limit)
     */
    public function addUserRating($user_id, $stars, $text)
    {
        // Get user rating of this place if exists.
        $userRating = $this->userRatings->where('user_id', $user_id)->first();

        if ($userRating == null) {
            $userRating = new UserRating();
            $userRating->place_id = $this->id;
            $userRating->user_id = $user_id;
        }

        $userRating->stars = $stars;
        $userRating->rating = $text;

        $userRating->save();
    }
}
