<?php

namespace App;

use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Model;

class PlaceCluster extends Model
{
    use SpatialTrait;
    
    protected $spatialFields = [
        'gps',
        'avg_gps',
    ];
}
