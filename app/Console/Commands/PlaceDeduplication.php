<?php

namespace App\Console\Commands;

use App\Category;
use App\Services\Deduplication\DeduplicationClosePointsService;
use App\Services\Deduplication\DeduplicationLogService;
use App\Services\Deduplication\DeduplicationSameNamesService;
use Illuminate\Console\Command;

class PlaceDeduplication extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'places:deduplication
        {--pretend : Define if only do log without really deleting places}
        {--noClosePlaces : Define if not to deduplicate close places}
        {--noSameNames : Define if not to deduplicate places with same names and distance}
        {--samePlaceCategories=3,13,23,33,43 : Selected categories IDs to deduplicate same names}
        {--closePlacesDistance=0.00005 : Distance in sphere degrees to determine close points}
        {--sameNamesDistance=4000 : Distance in meters to determine close points with same names}
        {--latDivide=6 : Divide process of close places - divide latitude axis to X parts}
        {--lngDivide=12 : Divide process of close places - divide longitude axis to X parts}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deduplication of places. (Use --help to show options.)';

    /**
     * Deduplication log service.
     *
     * @var DeduplicationLogService
     */
    protected $logSvr;

    /**
     * Deduplication close points service.
     *
     * @var DeduplicationClosePointsService
     */
    protected $closePointsSvr;

    /**
     * Deduplication same names service.
     *
     * @var DeduplicationSameNamesService
     */
    protected $sameNamesSvr;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        DeduplicationLogService $logSvr,
        DeduplicationClosePointsService $closePointsSvr,
        DeduplicationSameNamesService $sameNamesSvr
    ) {
        parent::__construct();

        $this->logSvr = $logSvr;
        $this->closePointsSvr = $closePointsSvr;
        $this->sameNamesSvr = $sameNamesSvr;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "\n";
        echo "Deduplication of places\n";
        echo "-----------------------\n";

        $pretend = $this->option('pretend');
        $noClosePlaces = $this->option('noClosePlaces');
        $noSameNames = $this->option('noSameNames');
        $samePlaceCategories = $this->option('samePlaceCategories');

        // Check and parse command options.
        if ($noClosePlaces && $noSameNames) {
            throw new \InvalidArgumentException("Can't use both --noClosePlaces and --noSameNames");
        }
        if (!is_numeric($this->option('closePlacesDistance')) || $this->option('closePlacesDistance') < 0) {
            throw new \InvalidArgumentException("Option --closePlacesDistance must be numeric and greater or equal 0.");
        }
        if (!is_numeric($this->option('sameNamesDistance')) || $this->option('sameNamesDistance') < 0) {
            throw new \InvalidArgumentException("Option --sameNamesDistance must be numeric and greater or equal 0.");
        }
        // Use ctype_digit to check if variable is integer value.
        if (!ctype_digit($this->option('latDivide')) || $this->option('latDivide') <= 0) {
            throw new \InvalidArgumentException("Option --latDivide must be integer and greater than 0.");
        }
        // Use ctype_digit to check if variable is integer value.
        if (!ctype_digit($this->option('lngDivide')) || $this->option('lngDivide') <= 0) {
            throw new \InvalidArgumentException("Option --lngDivide must be integer and greater than 0.");
        }
        if ($samePlaceCategories == "") {
            $samePlaceCategories = [];
        } else {
            $samePlaceCategories = explode(",", $samePlaceCategories);
        }
        foreach ($samePlaceCategories as $samePlaceCategory) {
            if (!ctype_digit($samePlaceCategory)) {
                throw new \InvalidArgumentException("Option --samePlaceCategories must have integer values.");
            }
        }
        // Get categories names.
        $categories = Category::whereIn('id', $samePlaceCategories)->get();
        $samePlaceCategoriesNames = [];
        $validAllCategories = true;
        foreach ($samePlaceCategories as $samePlaceCategory) {
            if (($category = $categories->first(function ($value, $key) use ($samePlaceCategory) {
                return $value->id == $samePlaceCategory;
            })) != null) {
                $samePlaceCategoriesNames[$samePlaceCategory] = $category->name;
            } else {
                $samePlaceCategoriesNames[$samePlaceCategory] = null;
                $validAllCategories = false;
            }
        }
        if (!$validAllCategories) {
            throw new \InvalidArgumentException("Option --samePlaceCategories must have IDs of categories that exists.");
        }

        // Set variables.
        $closePlacesDistance = $this->option('closePlacesDistance');
        $sameNamesDistance = $this->option('sameNamesDistance');
        if ($sameNamesDistance < 0.1) {
            $sameNamesDistance = 0.1; // Because of distance formula.
        }
        $latDivide = $this->option('latDivide');
        $lngDivide = $this->option('lngDivide');

        $timeStart = microtime(true);

        // ---------------------------------------------------------
        // Deduplication too close places.
        // ---------------------------------------------------------

        if (!$noClosePlaces) {
            echo "\nStart deduplication too close places (distance " . $closePlacesDistance . " degree):\n";

            $timePart = microtime(true);

            $this->logSvr->writeLog("-----------------------------------------------------------------\n");
            $this->logSvr->writeLog("Deduplication too close places (distance " . $closePlacesDistance . " degree):\n");
            $this->logSvr->writeLog("-----------------------------------------------------------------\n");

            // Process boxes of deduplication - because of demanding process.
            $latStep = (double)180 / $latDivide;
            $lngStep = (double)360 / $lngDivide;
            $maxBoxesCount = $lngDivide * $latDivide;
            for ($lngIndex = 0; $lngIndex < $lngDivide; $lngIndex++) {
                for ($latIndex = 0; $latIndex < $latDivide; $latIndex++) {
                    echo "\r" . ($lngIndex * $latDivide + $latIndex + 1) . " / " . $maxBoxesCount;

                    $this->closePointsSvr->deduplicateClosePoints(
                        $closePlacesDistance,
                        ($latIndex * $latStep - 90),
                        (($latIndex + 1) * $latStep - 90),
                        ($lngIndex * $lngStep - 180),
                        (($lngIndex + 1) * $lngStep - 180),
                        $this->logSvr,
                        $pretend
                    );
                }
            }

            echo "\rDeduplication too close places time: " . (microtime(true) - $timePart) . " sec.\n";
            echo "\n-----------------------\n";
        }

        // ---------------------------------------------------------
        // Deduplication places with same name and defined distance.
        // ---------------------------------------------------------

        if (!$noSameNames) {
            echo "\nStart deduplication places with same name and distance " . $sameNamesDistance . " m):\n";

            foreach ($samePlaceCategoriesNames as $categoryId => $categoryName) {
                echo "Deduplication accross category ID " . $categoryId . " (" . $categoryName . ")\n";
            }

            if (empty($samePlaceCategoriesNames)) {
                echo "Deduplication accross all categories\n";
            }

            $timePart = microtime(true);

            $this->logSvr->writeLog("-----------------------------------------------------------------\n");
            $this->logSvr->writeLog("Deduplication places with same name and distance " . $sameNamesDistance . " m):\n");
            $this->logSvr->writeLog("-----------------------------------------------------------------\n");
            $this->sameNamesSvr->deduplicateSameNames($sameNamesDistance, $samePlaceCategories, $this->logSvr, $pretend);

            echo "Deduplication places with same name and distance time: " . (microtime(true) - $timePart) . " sec.\n";
            echo "\n-----------------------\n";
        }

        // -----------------------------

        // Summary
        echo "\nTotal deduplication time: " . (microtime(true) - $timeStart) . " sec.\n";
        echo "Log file name: " . $this->logSvr->getFilePath() . "\n";
        echo "\n";
    }
}
