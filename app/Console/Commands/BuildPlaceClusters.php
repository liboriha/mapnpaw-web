<?php

namespace App\Console\Commands;

use App\Services\PlaceCluster\BuildClusterService;
use Illuminate\Console\Command;

class BuildPlaceClusters extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'placeClusters:build {--showLevelProgress : Show detailed level progress}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Build place clusters for all categories.'
        . ' (Use --showLevelProgress to show detailed level progress.)';

    /**
     * Build cluster service.
     *
     * @var BuildClusterService
     */
    protected $buildClustersSvr;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(BuildClusterService $buildClustersSvr)
    {
        parent::__construct();

        $this->buildClustersSvr = $buildClustersSvr;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->buildClustersSvr->buildClusters($this->option('showLevelProgress'));
    }
}
