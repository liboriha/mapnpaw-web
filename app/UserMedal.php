<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Contains user medails for collect TOP 10 places. 
 * Also contain record for traveller of the continent (here as continent medal - without specify category).
 */
class UserMedal extends Model
{
    protected $fillable = ['user_id', 'continent_id', 'category_id'];

    /**
     * Get the continent that owns the user medal.
     */
    public function continent()
    {
        return $this->belongsTo(\App\Continent::class);
    }

    /**
     * Get the category that owns the user medal.
     */
    public function category()
    {
        return $this->belongsTo(\App\Category::class);
    }

    /**
     * Get the category that owns the user medal.
     */
    public function places()
    {
        return $this->belongsToMany(\App\Place::class, 'user_medals_places');
    }

    /**
     * Return true if medal is continent medal.
     */
    public function is_continent_medal()
    {
        return $category_id == null ? true : false;
    }

    /**
     * Scope a query to only include continent medal.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeContinentMedal($query)
    {
        return $query->whereNull('category_id');
    }

    /**
     * Scope a query to only include continent medal.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNonContinentMedal($query)
    {
        return $query->whereNotNull('category_id');
    }
}