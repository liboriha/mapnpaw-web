<?php

namespace App\Services\Deduplication;

use App\Http\Helpers\MapHelper;
use App\Place;
use App\Services\Deduplication\DeduplicationLogService;
use Grimzy\LaravelMysqlSpatial\Types\Geometry;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Support\Facades\DB;

class DeduplicationSameNamesService
{
    /**
     * Remove close points with same name. Close points is defined by $closeDistance (in meters).
     *
     * @param  float            $closeDistance Distance defines close points with same name (distance is in meters)
     * @param  array            $categories    Categories to deduplicate in
     * @param  DeduplicationLog $log           Deduplication log service
     * @param  boolean          $pretend       If true, only write log and not really delete places.
     * @return void
     */
    public function deduplicateSameNames($closeDistance, $categories = [], DeduplicationLogService $log = null, $pretend = false)
    {
        // Get close points with same names.
        // First get places with same names, than filter it by distance.
        if (empty($categories)) {
            $queryFrom = Place::select('places.id as id', 'places.name AS name', 'places.gps AS gps', 'B.id AS close_id', 'B.name AS close_name', 'B.gps AS close_gps')
                ->crossJoin('places AS B')
                ->whereRaw('`places`.`id` < `B`.`id` AND `places`.`name` = `B`.`name`');

            $query = Place::select('*')->selectRaw("(? * acos(cos(radians(ST_Y(T.gps))) * cos(radians(ST_Y(T.close_gps))) * cos(radians(ST_X(T.close_gps)) - radians(ST_X(T.gps))) + sin(radians(ST_Y(T.gps))) * sin(radians(ST_Y(T.close_gps))))) AS distance", [MapHelper::EARTH_RADIUS])
                ->from(DB::raw("({$queryFrom->toSql()}) as T"))
                // Remove group by because of performance
                //->groupBy('id', 'name', 'close_id', 'close_name', 'distance')
                ->having('distance', '<', $closeDistance);
        } else {
            $queryFrom = Place::select('places.id as id', 'places.name AS name', 'places.gps AS gps', 'B.id AS close_id', 'B.name AS close_name', 'B.gps AS close_gps')
                ->crossJoin('places AS B')
                ->whereRaw('`places`.`id` < `B`.`id` AND `places`.`name` = `B`.`name`')
                ->whereIn('places.category_id', $categories)
                ->whereIn('B.category_id', $categories);

            $query = Place::select('*')->selectRaw("(? * acos(cos(radians(ST_Y(T.gps))) * cos(radians(ST_Y(T.close_gps))) * cos(radians(ST_X(T.close_gps)) - radians(ST_X(T.gps))) + sin(radians(ST_Y(T.gps))) * sin(radians(ST_Y(T.close_gps))))) AS distance", [MapHelper::EARTH_RADIUS])
                ->from(DB::raw("({$queryFrom->toSql()}) as T"))
                ->mergeBindings($queryFrom->getQuery())
                // Remove group by because of performance
                //->groupBy('id', 'name', 'close_id', 'close_name', 'distance')
                ->having('distance', '<', $closeDistance);
        }

        echo "Loading closes places from DB\n";

        $closes = [];
        $cursor = $query->cursor();
        // Fetch first data - determine time that data already fetched from DB.
        $current = $cursor->current();

        echo "Start parsing data from DB\n";

        echo "Fetched: 0";
        $count = 0;
        // Create structured array with data
        if ($current != null) {
            foreach ($cursor as $value) {
                $count++;
                if (!array_key_exists($value->id, $closes)) {
                    $closes[$value->id] = [
                        'id' => $value->id,
                        'name' => $value->name,
                        // No need convert because of place model
                        'gps' => $value->gps,
                        'closes' => [],
                    ];
                }
                if (!array_key_exists($value->close_id, $closes)) {
                    $closes[$value->close_id] = [
                        'id' => $value->close_id,
                        'name' => $value->close_name,
                        // Must parse, not converted
                        'gps' => Geometry::fromWKB($value->close_gps),
                        'closes' => [],
                    ];
                }
                $closes[$value->id]['closes'][] = $value->close_id;
                $closes[$value->close_id]['closes'][] = $value->id;

                if ($count % 1000 == 0) {
                    echo "\rFetched: $count";
                }
            }
        }

        echo "\rTotal loaded rows from DB: $count\n";

        // Sort data by source ID
        ksort($closes);

        echo "Creating groups of closes places\n";

        // Create groups
        echo "Processed: 0";
        $count = 0;
        $groups = [];
        foreach ($closes as $key => $value) {
            $count++;

            $groupIDs = $this->createGroup($closes, $key);
            if (!empty($groupIDs)) {
                $groups[] = [
                    'ids' => $groupIDs
                ];
            }
            if ($count % 1000 == 0) {
                echo "\rProcessed: $count";
            }
        }

        echo "\rComputing average GPS coordinates for groups\n";

        echo "Processed: 0";
        $count = 0;
        // Compute average gps - compute average gps with cartesian coordinates (great precision).
        // Not use average gps as in building cluster (must use different because of google maps geometry).
        foreach ($groups as $key => $value) {
            $count++;

            $x = 0;
            $y = 0;
            $z = 0;

            foreach ($value['ids'] as $id) {
                $lngRad = deg2rad($closes[$id]['gps']->getLng());
                $latRad = deg2rad($closes[$id]['gps']->getLat());

                $x += cos($latRad) * cos($lngRad);
                $y += cos($latRad) * sin($lngRad);
                $z += sin($latRad);
            }

            $x = $x / count($value['ids']);
            $y = $y / count($value['ids']);
            $z = $z / count($value['ids']);

            $r = sqrt($x**2 + $y**2 + $z**2);
            if ($r != 0) {
                $groups[$key]['avg_gps'] = new Point(rad2deg(asin($z / $r)), rad2deg(atan2($y, $x)));
            } else {
                // Can't convert back, becouse average point in the center of earth.
                // Only happens when you have two point on the opposite side of the earth.
                // (Must have $closeDistance bigger than half of the circumference of the earth.)
                $groups[$key]['avg_gps'] = new Point(0, 0);
            }

            if ($count % 500 == 0) {
                echo "\rProcessed: $count";
            }
        }

        echo "\rDetermine which point to preserve according to average GPS coordinates\n";

        echo "Processed: 0";
        $count = 0;
        // Compute delete ID by distance (preserve point with minimum distance).
        $deleteIDs = [];
        foreach ($groups as $key => $value) {
            $count++;

            $minDistance = null;
            $minId = null;

            foreach ($value['ids'] as $id) {
                $distance = MapHelper::getDistance(
                    $closes[$id]['gps']->getLat(),
                    $closes[$id]['gps']->getLng(),
                    $value['avg_gps']->getLat(),
                    $value['avg_gps']->getLng()
                );
                if ($minDistance === null || $minDistance > $distance) {
                    $minDistance = $distance;
                    $minId = $id;
                }
            }

            $groups[$key]['deleteIDs'] = array_diff($value['ids'], [$minId]);
            $groups[$key]['preserveID'] = $minId;

            $deleteIDs = array_merge($deleteIDs, $groups[$key]['deleteIDs']);

            if ($count % 500 == 0) {
                echo "\rProcessed: $count";
            }
        }

        echo "\rLogging info to log file\n";

        echo "Processed: 0";
        $count = 0;
        // Log places IDs with names.
        if ($log != null) {
            $logText = "";
            foreach ($groups as $key => $value) {
                $count++;

                foreach ($value['deleteIDs'] as $id) {
                    $logText .= "Same name and too close, delete point ID "
                        . $id . " (" . $closes[$id]['name'] . "), because of point ID "
                        . $value['preserveID'] . " (" . $closes[$value['preserveID']]['name'] . ").\n";
                }

                if ($count % 500 == 0) {
                    $log->writeLog($logText);
                    $logText = "";

                    echo "\rProcessed: $count";
                }
            }
            $log->writeLog($logText);
        }

        echo "\rTotal places to delete: " . count($deleteIDs) . "\n";

        echo "Processed: 0";
        $count = 0;
        // Delete places.
        if (!$pretend) {
            foreach (array_chunk($deleteIDs, 1000) as $deleteChunk) {
                $count += count($deleteChunk);

                Place::whereIn('id', $deleteChunk)->delete();
                
                echo "\rProcessed: $count";
            }
        }

        echo "\rFinished deduplication\n";
    }

    /**
     * Helper method to create groups of places.
     * @param  array &$closesPlaces Array with places (with closes places IDs)
     * @param  int   $id            Processed place ID
     * @return array                Array with ID of places in group
     */
    protected function createGroup(&$closesPlaces, $id)
    {
        // Not process place that is already processed
        if (array_key_exists('processed', $closesPlaces[$id])) {
            return [];
        }

        // Add processed place to group and mark it as processed.
        $groupIDs = [$id];
        $closesPlaces[$id]['processed'] = true;

        // Process closes places to add to group.
        foreach ($closesPlaces[$id]['closes'] as $key => $value) {
            $groupIDs = array_merge($groupIDs, $this->createGroup($closesPlaces, $value));
        }

        return $groupIDs;
    }
}
