<?php

namespace App\Services\Deduplication;

use Carbon\Carbon;
use Illuminate\Support\Facades\File;

class DeduplicationLogService
{
    /**
     * File path to log file (defined in constructor).
     * @var string
     */
    protected $filePath = null;

    /**
     * If log file was created.
     * @var boolean
     */
    protected $created = false;

    public function __construct()
    {
        $logPath = config('awapp.logs.deduplication-path');
        if (!File::exists($logPath)) {
            File::makeDirectory($logPath, 0777, true);
        }
        $date = new Carbon();
        $this->filePath = $logPath . '/' . $date->format('Ymd_His') . '_' . str_random(5) . '.log';
    }

    /**
     * Method to write text to log file. (Append text.)
     * @param  string $text Text to write
     */
    public function writeLog($text)
    {
        File::append($this->filePath, $text);

        // Set file was created (write some text to file).
        $this->created = true;
    }

    /**
     * Get file path of log file
     * @return string File path
     */
    public function getFilePath()
    {
        if ($this->created == false) {
            return null;
        }
        return basename($this->filePath);
    }
}
