<?php

namespace App\Services\Deduplication;

use App\Place;
use App\Services\Deduplication\DeduplicationLogService;

class DeduplicationClosePointsService
{
    /**
     * Remove too close points defined by $distanceCoord (distance is in coords degrees).
     * Deduplication is processed on coordinated box, can specify latitude and longitude to process deduplication.
     * If not specify latituda and longitude, processes all earth.
     *
     * @param  float            $distanceCoord Distance defines close points (distance is in coords degrees)
     * @param  float            $minLat        Min latitude to process
     * @param  float            $maxLat        Max latitude to process
     * @param  float            $minLng        Mix longitude to process
     * @param  float            $maxLng        Max longitude to process
     * @param  DeduplicationLog $log           Deduplication log service (if null, no log)
     * @param  boolean          $pretend       If true, only write log and not really delete places.
     * @return void
     */
    public function deduplicateClosePoints(
        $distanceCoord,
        $minLat = -90,
        $maxLat = 90,
        $minLng = -180,
        $maxLng = 180,
        DeduplicationLogService $log = null,
        $pretend = false
    ) {
        // Get close points.
        $query = Place::select('places.id', 'places.name', 'B.id AS close_id', 'B.name AS close_name')->crossJoin('places AS B')->whereRaw('`places`.`id` < `B`.`id` AND ST_X(`places`.`gps`) >= ? AND ST_X(`places`.`gps`) <= ? AND ST_Y(`places`.`gps`) >= ? AND ST_Y(`places`.`gps`) <= ? AND ST_X(`B`.`gps`) >= ? AND ST_X(`B`.`gps`) <= ? AND ST_Y(`B`.`gps`) >= ? AND ST_Y(`B`.`gps`) <= ? AND ST_WITHIN(`B`.`gps`, ST_Buffer(`places`.`gps`, ?))', [$minLng, $maxLng, $minLat, $maxLat, $minLng, $maxLng, $minLat, $maxLat, $distanceCoord]);

        $data = $query->get();

        // Create array with data sorted by source ID.
        $closes = [];
        foreach ($data as $key => $value) {
            if (!array_key_exists($value->id, $closes)) {
                $closes[$value->id] = [];
            }
            $closes[$value->id][] = $value;
        }
        ksort($closes);

        // Get ID of places to delete.
        $toDelete = [];
        foreach ($closes as $key => $values) {
            // Skip items of deleted item, their items is not too close.
            if (array_key_exists($key, $toDelete)) {
                continue;
            }
            foreach ($values as $value) {
                // Also remove duplications. (Override same value.)
                $toDelete[$value->close_id] = $value;
            }
        }

        // Log places IDs with names.
        if ($log != null) {
            $logText = "";
            foreach ($toDelete as $key => $value) {
                $logText .= "Too close, delete point ID " . $value->close_id . " (" . $value->close_name . ")"
                    . ", because of point ID " . $value->id . " (" . $value->name . ").\n";
            }
            $log->writeLog($logText);
        }

        // Delete places.
        if (!$pretend) {
            $deleteIDs = array_keys($toDelete);
            foreach (array_chunk($deleteIDs, 1000) as $deleteChunk) {
                Place::whereIn('id', $deleteChunk)->delete();
            }
        }
    }
}
