<?php

namespace App\Services\PlaceCluster;

use App\Category;
use App\Place;
use App\PlaceCluster;
use App\UserPhoto;
use Carbon\Carbon;
use Grimzy\LaravelMysqlSpatial\Types\MultiPolygon;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Grimzy\LaravelMysqlSpatial\Types\Polygon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BuildClusterService
{
    /**
     * Method to build places clusters.
     */
    public function buildClusters($showLevelProgress = null)
    {
        \DB::disableQueryLog();
        \Debugbar::disable();

        echo "\nBuild clusters for places\n";
        echo "-------------------------\n";

        $time_start = microtime(true);

        $config = config('awapp.map.clustering.build');

        $categories = Category::get();

        $categories = $categories->pluck('id');

        // Add processing all categories.
        $categories[] = null;
        // For testing purposes
        //$categories = array_intersect($categories->toArray(), [1, null]);

        $lastClusterValidIndex = PlaceCluster::select('id')->orderBy('id', 'desc')->first();
        $lastClusterValidIndex = $lastClusterValidIndex != null ? $lastClusterValidIndex->id : -1;

        Cache::put('build-cluster-last-valid-id', $lastClusterValidIndex, 24 * 60); // One day.

        foreach ($config as $zoomLevel => $configLayer) {
            $clustersCount[$zoomLevel] = 0;
        }

        try {
            // Process all categories clusters
            foreach ($categories as $categoryId) {
                if ($categoryId != null) {
                    echo "Build clusters for category ID " . $categoryId . ".\n";
                } else {
                    echo "Build clusters for all categories.\n";
                }

                $clustersTotal = [];

                $sumClusters = 0;
                $category_time = 0;

                // Process all zoom level of clusters
                foreach ($config as $zoomLevel => $configLayer) {
                    // Prepare processed IDs array.
                    $layer_time_start = microtime(true);

                    $processedId = [];
                    if ($configLayer['query'] == 'place') {
                        $maxId = Place::select('id')->orderBy('id', 'desc')->first();
                    } else {
                        $maxId = PlaceCluster::select('id')->orderBy('id', 'desc')->first();
                    }
                    $this->clearArray($processedId, ($maxId->id ?? 0), false);

                    // Get lat/lng boxes sizes.
                    $latAngle = 180 / $configLayer['mapDivide']['lat'];
                    $lngAngle = 360 / $configLayer['mapDivide']['lng'];

                    $latBoxSize = $configLayer['boxDivide']['lat'];
                    $lngBoxSize = $configLayer['boxDivide']['lng'];

                    $margin = ((float)$configLayer['clusterRadius'] + 1000) / 40030000 * 360;

                    $maxBoxesCount = $configLayer['mapDivide']['lng'] * $configLayer['mapDivide']['lat'];

                    // Process all boxes. (world divided to boxes for memory and performance reason)
                    for ($lngStep = 0; $lngStep < $configLayer['mapDivide']['lng']; $lngStep++) {
                        $minLng = $lngStep * $lngAngle - 180;
                        $maxLng = ($lngStep + 1) * $lngAngle - 180;

                        for ($latStep = 0; $latStep < $configLayer['mapDivide']['lat']; $latStep++) {
                            $minLat = $latStep * $latAngle - 90;
                            $maxLat = ($latStep + 1) * $latAngle - 90;

                            if ($showLevelProgress != null) {
                                echo "\r" . ($lngStep * $configLayer['mapDivide']['lat'] + $latStep + 1) . " / " . $maxBoxesCount;
                            }

                            $processBoxes = [];
                            $this->clearArray($processBoxes, $latBoxSize * $lngBoxSize, []);

                            // Get processed boxes. Divide each box to subboxes.
                            $this->getDataToCluster($processBoxes, $configLayer, $categoryId, $lngStep, $minLat, $maxLat, $minLng, $maxLng, $latAngle, $lngAngle, $latBoxSize, $lngBoxSize, $margin);

                            // Create clusters for boxes.
                            $this->createClusters($processBoxes, $processedId, $categoryId, $configLayer, $clustersTotal);
                        }
                    }

                    $time = (microtime(true) - $layer_time_start);
                    $category_time += $time;
                    $countClustersTotal = count($clustersTotal);

                    echo "\r";
                    echo $countClustersTotal . " clusters for zoom level " . $zoomLevel . " (" . $time . " sec).\n";

                    $sumClusters += $countClustersTotal;
                    $clustersCount[$zoomLevel] += $countClustersTotal;

                    /*$sum = 0;
                    foreach ($clustersTotal as $key => $cluster) {
                        $sum += $cluster['count'];
                    }*/

                    $clustersTotal = [];
                }

                /*$sum = 0;
                foreach ($processedId as $id => $processId) {
                    if ($processId == true) {
                        $sum++;
                    }
                }*/

                echo "Total " . $sumClusters . " clusters (" . $category_time . " sec).\n";

                echo "-------------------------\n";
            }

            if ($lastClusterValidIndex != null) {
                PlaceCluster::where('id', '<=', $lastClusterValidIndex)->delete();
            }
        } catch (\Exception $e) {
            if ($lastClusterValidIndex != null) {
                PlaceCluster::where('id', '>', $lastClusterValidIndex)->delete();
            }

            Cache::forget('build-cluster-last-valid-id');

            throw $e;
        }

        Cache::forget('build-cluster-last-valid-id');

        echo "Result:\n";
        $sum = 0;
        foreach ($clustersCount as $zoomLevel => $count) {
            $sum += $count;
            echo $count . " clusters for zoom level " . $zoomLevel . ".\n";
        }
        echo "Total " . $sum . " clusters.\n";

        echo "Total build clusters time: " . (microtime(true) - $time_start) . " sec.\n";

        \Debugbar::enable();
    }

    /**
     * Helper method to get distance of two gps points, for google maps purposes. (Not real distance.)
     *
     * Distance formula is also in UpdatePlaceClustersService file.
     */
    protected function distance($lat1, $lng1, $lat2, $lng2)
    {
        return 6371000 * sqrt(pow((180 - abs(fmod($lng1 - $lng2, 360) + 180)) * 0.0174532925199433, 2) + pow(((log((1 + sin($lat1 * 0.0174532925199433)) / (1 - sin($lat1 * 0.0174532925199433))) * 0.5) - (log((1 + sin($lat2 * 0.0174532925199433)) / (1 - sin($lat2 * 0.0174532925199433))) * 0.5)), 2));
    }

    /**
     * Helper method to get data for creating clusters, load part of world places and divide it to subboxes.
     * (For memory and performance reason.)
     */
    protected function getDataToCluster(&$processBoxes, $configLayer, $categoryId, $lngStep, $minLat, $maxLat, $minLng, $maxLng, $latAngle, $lngAngle, $latBoxSize, $lngBoxSize, $margin)
    {
        $minMLng = $minLng - $margin;
        $maxMLng = $maxLng + $margin;

        $minMLat = $minLat - $margin;
        $maxMLat = $maxLat + $margin;

        // Prepare area to get from database. Special case for marginal area.
        if ($lngStep != 0 && $lngStep != $configLayer['mapDivide']['lng']) {
            $searchedArea = Polygon::fromString("($minMLng $minMLat,$maxMLng $minMLat,$maxMLng $maxMLat,$minMLng $maxMLat,$minMLng $minMLat)");
        } elseif ($lngStep != 0) {
            $lngMargin = -180 + $margin;
            $searchedArea = new MultiPolygon([
                Polygon::fromString("($minMLng $minMLat,$maxLng $minMLat,$maxLng $maxMLat,$minMLng $maxMLat,$minMLng $minMLat)"),
                Polygon::fromString("(-180 $minMLat,$lngMargin $minMLat,$lngMargin $maxMLat,-180 $maxMLat,-180 $minMLat)")
            ]);
        } else {
            $lngMargin = 180 - $margin;
            $searchedArea = new MultiPolygon([
                Polygon::fromString("($minLng $minMLat,$maxMLng $minMLat,$maxMLng $maxMLat,$minLng $maxMLat,$minLng $minMLat)"),
                Polygon::fromString("($lngMargin $minMLat,180 $minMLat,180 $maxMLat, $lngMargin $maxMLat,$lngMargin $minMLat)")
            ]);
        }

        // Specific query for places or next level of clusters.
        if ($configLayer['query'] == 'place') {
            $query = Place::select('id', 'gps');
            $query = $query->within('gps', $searchedArea);

            if ($categoryId != 0 && $categoryId != null) {
                $query = $query->where('category_id', $categoryId);
            }
        } else {
            $query = PlaceCluster::select('id', 'gps', 'avg_gps', 'count');
            
            $lastValidId = Cache::get('build-cluster-last-valid-id', null);

            if ($lastValidId != null) {
                $query = $query->where('id', '>', $lastValidId);
            }

            $query = $query->where('zoom_level', $configLayer['queryZoom']);
            $query = $query->within('gps', $searchedArea);

            $query = $query->where('category_id', $categoryId);
        }

        $latBoxAngleInv = (float)1 / ($latAngle / $latBoxSize);
        $lngBoxAngleInv = (float)1 / ($lngAngle / $lngBoxSize);

        // Load point from database by chunks.
        $query->chunk(1000, function ($places) use (&$processBoxes, $minLat, $minLng, $latBoxSize, $lngBoxSize, $latBoxAngleInv, $lngBoxAngleInv, $configLayer, $margin) {
            foreach ($places as $place) {
                $lat = $place->gps->getLat();
                $lng = $place->gps->getLng();

                $diffLat = $lat - $minLat;
                $diffLng = $lng - $minLng;

                $latIndex = floor($diffLat * $latBoxAngleInv);
                $lngIndex = floor($diffLng * $lngBoxAngleInv);

                $processBoxIndex = $latIndex * $latBoxSize + $lngIndex;

                if ($configLayer['query'] == 'place') {
                    $placeArr = [
                        'id' => $place->id,
                        'lat' => $lat,
                        'lng' => $lng,
                        'ownBoxIndex' => $processBoxIndex,
                    ];
                } else {
                    $placeArr = [
                        'id' => $place->id,
                        'lat' => $lat,
                        'lng' => $lng,
                        'count' => $place->count,
                        'avgLat' => $place->avg_gps->getLat(),
                        'avgLng' => $place->avg_gps->getLng(),
                        'ownBoxIndex' => $processBoxIndex,
                    ];
                }

                // Add point to default process box.
                $this->addToProcessBoxes($processBoxes, $placeArr, $latIndex, $lngIndex, $latBoxSize, $lngBoxSize);

                // Add point to adjacent process box.
                $latIndex = floor(($diffLat - $margin) * $latBoxAngleInv);
                $lngIndex = floor(($diffLng - $margin) * $lngBoxAngleInv);

                $this->addToProcessBoxes($processBoxes, $placeArr, $latIndex, $lngIndex, $latBoxSize, $lngBoxSize);

                $latIndex = floor(($diffLat - $margin) * $latBoxAngleInv);
                $lngIndex = floor(($diffLng + $margin) * $lngBoxAngleInv);

                $this->addToProcessBoxes($processBoxes, $placeArr, $latIndex, $lngIndex, $latBoxSize, $lngBoxSize);

                $latIndex = floor(($diffLat + $margin) * $latBoxAngleInv);
                $lngIndex = floor(($diffLng - $margin) * $lngBoxAngleInv);

                $this->addToProcessBoxes($processBoxes, $placeArr, $latIndex, $lngIndex, $latBoxSize, $lngBoxSize);

                $latIndex = floor(($diffLat + $margin) * $latBoxAngleInv);
                $lngIndex = floor(($diffLng + $margin) * $lngBoxAngleInv);

                $this->addToProcessBoxes($processBoxes, $placeArr, $latIndex, $lngIndex, $latBoxSize, $lngBoxSize);

                unset($placeArr);
            }
        });
    }

    /**
     * Helper method to initialize and clear array.
     */
    protected function clearArray(&$array, $maxIndex, $defaultValue)
    {
        for ($i = 1; $i <= $maxIndex; $i++) {
            $array[$i] = $defaultValue;
        }
    }

    /**
     * Helper method to add place to process box.
     */
    protected function addToProcessBoxes(&$processBoxes, &$place, $latIndex, $lngIndex, $latBoxSize, $lngBoxSize)
    {
        if ($latIndex >= 0 && $latIndex <= $latBoxSize && $lngIndex >= 0 && $lngIndex <= $lngBoxSize) {
            $processBoxIndex = $latIndex * $latBoxSize + $lngIndex;

            $processBoxes[$processBoxIndex][$place['id']] = &$place;
        }
    }

    /**
     * Helper method for creating cluster.
     */
    protected function createClusters(&$processBoxes, &$processedId, $categoryId, $configLayer, &$clustersTotal)
    {
        $notProcessedPlaces = [];

        foreach ($processBoxes as $processBoxIndex => $processBox) {
            $clusters = [];

            // Process first
            $notProcessPlaces = $this->createClustersMatching($processBoxIndex, $processBox, $clusters, $processedId, $configLayer, $clustersTotal);
            
            // Process second with not processed places - place in margin.
            $this->createClustersMatching($processBoxIndex, $processBox, $clusters, $processedId, $configLayer, $clustersTotal);

            foreach ($clusters as $clusterKey => $cluster) {
                $clusters[$clusterKey]['avgLat'] = (float)$cluster['sumLat'] / $cluster['count'];
                $clusters[$clusterKey]['avgLng'] = fmod((float)$cluster['sumLng'] / $cluster['count'] + 180 + 360, 360) - 180;
            }

            $data = [];
            foreach ($clusters as $cluster) {
                $data[] = [
                    'category_id' => $categoryId,
                    'zoom_level' => $configLayer['zoomLevel'],
                    'count' => $cluster['count'],
                    'gps' => DB::raw(sprintf("ST_GeomFromText('%s')", (new Point($cluster['lat'], $cluster['lng']))->toWKT())),
                    'avg_gps' => DB::raw(sprintf("ST_GeomFromText('%s')", (new Point($cluster['avgLat'], $cluster['avgLng']))->toWKT())),
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                ];
            }

            if (count($data) > 0) {
                PlaceCluster::insert($data);
            }
        }
    }

    /**
     * Helper method to add place to cluster.
     */
    protected function addPlaceToCluster(&$cluster, &$place)
    {
        if (array_key_exists('count', $place)) {
            $cluster['count'] += $place['count'];
            // add cluster point to upper cluster
            $cluster['sumLat'] += $place['avgLat'] * $place['count'];
            if (abs($cluster['lng'] - $place['avgLng']) < 180) {
                $cluster['sumLng'] += $place['avgLng'] * $place['count'];
            } else {
                if ($cluster['lng'] > $place['avgLng']) {
                    $cluster['sumLng'] += ($place['avgLng'] + 360) * $place['count'];
                } else {
                    $cluster['sumLng'] += ($place['avgLng'] - 360) * $place['count'];
                }
            }
        } else {
            $cluster['count']++;
            // add place point to cluster
            $cluster['sumLat'] += $place['lat'];
            if (abs($cluster['lng'] - $place['lng']) < 180) {
                $cluster['sumLng'] += $place['lng'];
            } else {
                if ($cluster['lng'] > $place['lng']) {
                    $cluster['sumLng'] += $place['lng'] + 360;
                } else {
                    $cluster['sumLng'] += $place['lng'] - 360;
                }
            }
        }
    }

    /**
     * Helper method to create clusters - matching point with cluster by distance.
     */
    protected function createClustersMatching($processBoxIndex, &$processBox, &$clusters, &$processedId, $configLayer, &$clustersTotal)
    {
        $notProcessPlaces = [];

        foreach ($processBox as $placeId => $place) {
            if ($processedId[$placeId] == true) {
                continue;
            }

            foreach ($clusters as $clusterKey => $cluster) {
                $dist =  $this->distance($cluster['lat'], $cluster['lng'], $place['lat'], $place['lng']);

                if ($this->distance($cluster['lat'], $cluster['lng'], $place['lat'], $place['lng']) <= $configLayer['clusterRadius']) {
                    $this->addPlaceToCluster($clusters[$clusterKey], $place);

                    $processedId[$placeId] = true;

                    break;
                }
            }

            if ($processedId[$placeId] == false && $place['ownBoxIndex'] == $processBoxIndex) {
                // Not processed place, but processed box place (not margin place)
                $count = 1;

                if (array_key_exists('count', $place)) {
                    $newCluster = [
                        'count' => $place['count'],
                        'lat' => $place['lat'],
                        'lng' => $place['lng'],
                        'sumLat' => $place['avgLat'] * $place['count'],
                        'sumLng' => $place['avgLng'] * $place['count'],
                    ];
                } else {
                    $newCluster = [
                        'count' => 1,
                        'lat' => $place['lat'],
                        'lng' => $place['lng'],
                        'sumLat' => $place['lat'],
                        'sumLng' => $place['lng'],
                    ];
                }

                $clusters[$placeId] = &$newCluster;

                $clustersTotal[] = &$newCluster;

                $processedId[$placeId] = true;

                unset($newCluster);
            } else {
                $notProcessPlaces[] = &$place;
            }
        }

        return $notProcessPlaces;
    }
}
