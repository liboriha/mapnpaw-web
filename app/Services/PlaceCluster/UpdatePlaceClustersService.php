<?php

namespace App\Services\PlaceCluster;

use App\PlaceCluster;
use App\UserPhoto;
use Grimzy\LaravelMysqlSpatial\Types\MultiPolygon;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Grimzy\LaravelMysqlSpatial\Types\Polygon;
use Illuminate\Support\Facades\Storage;

class UpdatePlaceClustersService
{
    /**
     * Method to update place clusters.
     */
    public function updateClusters(
        Point $oldGps = null,
        $oldCategoryId = null,
        Point $newGps = null,
        $newCategoryId = null
    ) {
        $config = config('awapp.map.clustering.build');

        foreach ($config as $zoomLevel => $configLayer) {
            $radius = $configLayer['clusterRadius'];

            // By place category

            // First fetch then update
            $oldCluster = null;
            if ($oldGps != null) {
                $oldCluster = $this->getRelatedCluster($zoomLevel, $radius, $oldGps, $oldCategoryId);
            }

            if ($oldGps != null && $oldCluster != null) {
                $newCount = $oldCluster->count - 1;

                if ($newCount == 0) {
                    $oldCluster->delete();
                } else {
                    $oldCluster->avg_gps = new Point(
                        ($oldCluster->avg_gps->getLat() * $oldCluster->count - $oldGps->getLat()) / ($newCount),
                        ($oldCluster->avg_gps->getLng() * $oldCluster->count - $oldGps->getLng()) / ($newCount)
                    );
                    $oldCluster->count = $newCount;
                        
                    $oldCluster->save();
                }
            }

            $newCluster = null;
            if ($newGps != null) {
                $newCluster = $this->getRelatedCluster($zoomLevel, $radius, $newGps, $newCategoryId);
            }

            if ($newGps != null) {
                if ($newCluster == null) {
                    $newCluster = new PlaceCluster();
                    $newCluster->category_id = $newCategoryId;
                    $newCluster->zoom_level = $zoomLevel;
                    $newCluster->count = 0;
                    $newCluster->gps = new Point($newGps->getLat(), $newGps->getLng());
                    $newCluster->avg_gps = new Point(0, 0);
                }

                $newCount = $newCluster->count + 1;

                $newCluster->avg_gps = new Point(
                    ($newCluster->avg_gps->getLat() * $newCluster->count + $newGps->getLat()) / ($newCount),
                    ($newCluster->avg_gps->getLng() * $newCluster->count + $newGps->getLng()) / ($newCount)
                );
                $newCluster->count = $newCount;

                $newCluster->save();
            }

            // By all category
            
            // First fetch then update
            if ($oldGps != null) {
                $oldCluster = $this->getRelatedCluster($zoomLevel, $radius, $oldGps, null);
            }

            if ($oldGps != null && $oldCluster != null) {
                $newCount = $oldCluster->count - 1;

                if ($newCount == 0) {
                    $oldCluster->delete();
                } else {
                    $oldCluster->avg_gps = new Point(
                        ($oldCluster->avg_gps->getLat() * $oldCluster->count - $oldGps->getLat()) / ($newCount),
                        ($oldCluster->avg_gps->getLng() * $oldCluster->count - $oldGps->getLng()) / ($newCount)
                    );
                    $oldCluster->count = $newCount;
                        
                    $oldCluster->save();
                }
            }

            if ($newGps != null) {
                $newCluster = $this->getRelatedCluster($zoomLevel, $radius, $newGps, null);
            }

            if ($newGps != null) {
                if ($newCluster == null) {
                    $newCluster = new PlaceCluster();
                    $newCluster->category_id = null;
                    $newCluster->zoom_level = $zoomLevel;
                    $newCluster->count = 0;
                    $newCluster->gps = new Point($newGps->getLat(), $newGps->getLng());
                    $newCluster->avg_gps = new Point(0, 0);
                }

                $newCount = $newCluster->count + 1;

                $newCluster->avg_gps = new Point(
                    ($newCluster->avg_gps->getLat() * $newCluster->count + $newGps->getLat()) / ($newCount),
                    ($newCluster->avg_gps->getLng() * $newCluster->count + $newGps->getLng()) / ($newCount)
                );
                $newCluster->count = $newCount;

                $newCluster->save();
            }
        }
    }

    /**
     * Helper method to get related cluster to place.
     * Get certain zoomlevel and category cluster.
     *
     * Distance formula is also in BuildClusterService file.
     */
    public function getRelatedCluster($zoomLevel, $radius, Point $gps, $categoryId = null)
    {
        $exp = exp($radius / 6371000 * 2);
        $part = (2 * $exp / (sin(deg2rad(abs($gps->getLat()))) + 1) - $exp);
        $step = (abs($gps->getLat()) - rad2deg(asin((1 - $part)/(1 + $part))));

        $maxLat = (float) $gps->getLat() + $step;
        $minLat = (float) $gps->getLat() - $step;

        $maxLng = (float) $gps->getLng() + rad2deg($radius / 6371000);
        $minLng = (float) $gps->getLng() - rad2deg($radius / 6371000);

        if ($minLng >= -180 && $maxLng <= 180) {
            $searchedArea = Polygon::fromString(
                "($minLng $minLat,$maxLng $minLat,$maxLng $maxLat,$minLng $maxLat,$minLng $minLat)"
            );
        } else {
            if ($minLng < -180) {
                $minLng = $minLng + 360;
            } else {
                $maxLng = $maxLng - 360;
            }

            $searchedArea = new MultiPolygon([
                Polygon::fromString("(-180 $minLat,$maxLng $minLat,$maxLng $maxLat,-180 $maxLat,-180 $minLat)"),
                Polygon::fromString("($minLng $minLat,180 $minLat,180 $maxLat,$minLng $maxLat,$minLng $minLat)"),
            ]);
        }

        $query = PlaceCluster::select('id', 'count', 'gps', 'avg_gps')
            ->where('category_id', $categoryId)
            ->where('zoom_level', $zoomLevel);

        $query = $query->selectRaw("(6371000 * sqrt(pow((180 - abs(mod(ST_X(gps) - ?, 360) + 180)) * 0.0174532925199433, 2) + pow(((log((1 + sin(ST_Y(gps) * 0.0174532925199433)) / (1 - sin(ST_Y(gps) * 0.0174532925199433))) * 0.5) - (log((1 + sin(? * 0.0174532925199433)) / (1 - sin(? * 0.0174532925199433))) * 0.5)), 2))) AS distance", [$gps->getLng(), $gps->getLat(), $gps->getLat()]);

        $query = $query->within('gps', $searchedArea);

        $query = $query->groupBy('id', 'count', 'gps', 'avg_gps', 'distance');

        $query = $query->having('distance', '<=', $radius);

        // Place is in first cluster in DB. (if match more clusters)
        $query = $query->orderBy('id', 'asc');

        return $query->first();
    }
}
