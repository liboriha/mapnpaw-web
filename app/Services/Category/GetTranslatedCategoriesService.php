<?php

namespace App\Services\Category;

use App\Category;
use Illuminate\Support\Collection;

class GetTranslatedCategoriesService
{
    /**
     * Get unique translated categories names in collection.
     * Can be restricted by $continentId.
     *
     * @param  integer|null  $continentId Restrict results on continent
     * @return Collection                 Collection of translated categories names
     */
    public function getTranslatedCategories($continentId = null) : Collection
    {
        $query = Category::select(
            'id',
            'name',
            'medal_image_locked',
            'medal_image_unlocked',
            'flag_image_locked',
            'flag_image_unlocked'
        );

        // Adds continent condition, if continent parameter exists.
        if (!empty($continentId)) {
            $query = $query->where('continent_id', $continentId);
        }

        // Eager load translations.
        $query = $query->withTranslation();

        // Gets all categories - we can't distinct on server because of translations.
        // Translated names can be different than not translated names.
        $categories = $query->get();

        // Translates categories models.
        $transCategories = $categories->translate();

        // Gets collection of unique translated category names.
        return $transCategories->unique('name')->values();
    }
}
