<?php

namespace App\Services\Place;

use App\UserPhoto;
use Illuminate\Support\Facades\Storage;

class DeleteUserPhotoService
{
    /**
     * Remove user photo.
     * User must be owner of photo.
     * @param  integer           $userId     User ID
     * @param  UserPhoto|integer $userPhoto  User photo ID or UserPhoto model
     * @return boolean                       If user photo was deleted.
     */
    public function deletePhoto($userId, $userPhoto)
    {
        // If $userPhoto is number, load user photo model.
        if (is_numeric($userPhoto)) {
            $userPhoto = UserPhoto::find($userPhoto);
        }

        if ($userPhoto == null) {
            // Photo not found.
            abort(404);
        }

        if ($userPhoto->user_id != $userId) {
            // Not currently logged user photo - can't delete.
            abort(404);
        }

        // Delete file.
        Storage::disk(config('voyager.storage.disk'))->delete($userPhoto->image);

        return $userPhoto->delete();
    }
}
