<?php

namespace App\Services\User;

use App\User;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;

class SendResetLinkService
{
    /**
     * Send reset email with URL link for resseting user password.
     * Only one of parameter can be null. If both filled, then e-mail is preferred parameter.
     *
     * @param  string  $email    User e-mail
     * @return boolean                If email was send
     */
    public function sendResetLinkEmail($email)
    {
        // Load user by email or username
        $user = User::where('email', $email)->first();

        $credentials = ['email' => $email];

        // Check if user is registered as regular user. (Not by facebook account.)
        if ($user != null && $user->facebook_id != null) {
            throw ValidationException::withMessages([
                'email' => __('awapp.reset-password.error-messages.facebook-cant-reset-password')
            ]);
        }

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then return result.
        $response = Password::broker()->sendResetLink(
            $credentials
        );

        if ($response == Password::RESET_LINK_SENT) {
            return true;
        } else {
            return false;
        }
    }
}
