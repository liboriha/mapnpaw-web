<?php

namespace App\Services\User;

use App\Http\Helpers\FileHelper;
use App\User;
use Illuminate\Support\Facades\Hash;

class UpdateProfileService
{
    /**
     * Update facebook profile.
     * User must be facebook user or it throws 500 error.
     *
     * @param  User   $user       User model to update
     * @param  string $nickname   Nickname of user
     * @param  enum   $visibility User visibility enumeration
     * @return User               Updated user model
     */
    public function updateFacebookProfile(User $user, $nickname, $visibility) : User
    {
        if (!$user->isFacebookUser()) {
            throw new \LogicException('Updated profile if normal profile, can\'t update it as facebook profile');
        }

        $user->nickname = $nickname ?? '';
        $user->visibility = $visibility;

        $user->save();

        return $user;
    }

    /**
     * Update normal profile.
     * User must be normal user or it throws 500 error.
     * It saving avatar file image.
     * If password is not filled, new password not setted.
     * If avatarFile is not filled, new avatar not setted.
     *
     * @param  User                              $user       User model to update
     * @param  string                            $email      [description]
     * @param  string|null                       $password   [description]
     * @param  string                            $firstname  [description]
     * @param  string                            $lastname   [description]
     * @param  string                            $nickname   Nickname of user
     * @param  enum                              $visibility User visibility enumeration
     * @param  Illuminate\Http\UploadedFile|null $avatarFile Avatar image file to upload
     * @return User                                          Updated user model
     */
    public function updateProfile(
        User $user,
        $email,
        $password,
        $firstname,
        $lastname,
        $nickname,
        $visibility,
        $avatarFile
    ) : User {
        if ($user->isFacebookUser()) {
            throw new \LogicException('Updated profile if facebook profile, can\'t update it as normal profile');
        }

        $user->email = $email;

        if ($password != null) {
            $user->password = Hash::make($password);
        }

        $user->firstname = $firstname ?? '';
        $user->lastname = $lastname ?? '';
        $user->nickname = $nickname ?? '';
        $user->visibility = $visibility;

        if ($avatarFile != null) {
            // Saving avatar image file if exists.

            // e.g.: users/November2017/ as in BREAD editor
            $path = config('awapp.images.profile-image.directory') . '/' . date('FY') . '/';

            $savedFileName = FileHelper::saveFormImage(
                $avatarFile,
                config('voyager.storage.disk'),
                $path,
                config('awapp.images.profile-image.prefix')
            );

            $user->avatar = $path . $savedFileName;
        }

        $user->save();

        return $user;
    }
}
