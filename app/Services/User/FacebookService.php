<?php

namespace App\Services\User;

use App\Http\Helpers\FileHelper;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Laravel\Socialite\Contracts\User as SocialiteUser;

class FacebookService
{
    /**
     * Create or get facebook user model.
     *
     * @param  SocialiteUser $socialiteUser Socialite user
     * @return Uesr                         User model
     */
    public function createOrGetUser(SocialiteUser $socialiteUser) : User
    {
        if ($socialiteUser->email == null) {
            // Not have permission to get email from facebook, can't sign up facebook user.
            
            throw ValidationException::withMessages([
                'login' => __('awapp.login.error-messages.facebook-not-have-email')
            ])->redirectTo(route('home'));
        }

        // Get facebook user if exists.
        $user = User::where('facebook_id', $socialiteUser->id)->first();

        if ($user == null) {
            // Facebook user not registered - register facebook user

            $user = User::where('email', $socialiteUser->email)->first();

            if ($user != null) {
                // Conflict, user email exists, can't register facebook user
                if ($user->facebook_id == null) {
                    throw ValidationException::withMessages([
                        'login' => __('awapp.login.error-messages.normal-login-as-facebook')
                    ])->redirectTo(route('home'));
                } else {
                    throw ValidationException::withMessages([
                        'login' => __('awapp.login.error-messages.facebook-cant-register')
                    ])->redirectTo(route('home'));
                }
            }

            $user = new User();
            $user->facebook_id = $socialiteUser->id;
            $user->email = $socialiteUser->email;
            $user->password = Hash::make(str_random(40));
        }

        // Create new user
        // Create image
        $avatarPath = null;

        if ($socialiteUser->avatar != null) {
            $path = config('awapp.images.profile-image.directory') . '/' . date('FY') . '/';

            $savedFileName = FileHelper::saveURLImage(
                $socialiteUser->avatar,
                config('voyager.storage.disk'),
                $path,
                config('awapp.images.profile-image.prefix')
            );

            $avatarPath = $path . $savedFileName;
        }

        // Create user - not add nickname because facebook don't send it.
        $user->firstname = $socialiteUser->user['first_name'];
        $user->lastname = $socialiteUser->user['last_name'];
        // Generate random password to can't login by password, login by facebook token.
        $user->avatar = $avatarPath;

        // Default facebook user role is USER. (setted as default role.)

        $user->save();

        // Reload model
        $user = User::find($user->id);

        return $user;
    }
}
