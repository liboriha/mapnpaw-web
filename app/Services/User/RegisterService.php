<?php

namespace App\Services\User;

use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;

class RegisterService
{
    /**
     * Register (create) new user.
     *
     * @param  string  $email    User email to register
     * @param  string  $password New password
     * @return User              User model
     */
    public function registerUser($email, $password) : User
    {
        $user = new User();

        $user->email = $email;

        // Insert password
        $user->password = Hash::make($password);

        $user->save();

        event(new Registered($user));

        return $user;
    }
}
