<?php

namespace App\Services\Map;

use App\Category;
use App\Http\Helpers\MapHelper;
use App\Http\Resources\Place as PlaceResource;
use App\Http\Resources\PlaceCluster as PlaceClusterResource;
use App\Place;
use App\PlaceCluster;
use App\User;
use App\UserPhoto;
use Grimzy\LaravelMysqlSpatial\Types\Polygon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class GetPlacesOrClustersService
{
    /**
     *
     * @param  integer           $userId     User ID
     * @param  UserPhoto|integer $userPhoto  User photo ID or UserPhoto model
     * @return boolean                       If user photo was deleted.
     */
    /**
     * Return places or clusters according to zoom level.
     * Must set GPS position and radius.
     * Can set category or zoom level (without zoom, returns places).
     * Can specify page do get data (get 100 places or clusters).
     * When using pages on places, not use harvesine formula (extremly expensive when show all earth).
     *
     * @param  User|null $user                   User to process checks
     * @param  float $lat                   Latitude
     * @param  float $lng                   Longitude
     * @param  float|null $radius           Radius in meters
     * @param  string|null $category        Category name to get places or clusters
     * @param  int|null $zoom               Zoom level
     * @param  int|null $page               Page number
     * @return AnonymousResourceCollection  Resouce collection
     */
    public function getPlacesOrClusters($user, $lat, $lng, $radius = null, $category = null, $zoom = null, $page = null)
    {
        // If radius is not defined use default map radius
        if (empty($radius)) {
            $radius = config('awapp.map.defaults.radius');
        }

        $pageCount = config('awapp.map.defaults.page-count');

        // Resolve categories by name, categories with same name accross different continents.
        $categoryName = $category;
        $categoryIDs = null;
        if (!empty($categoryName)) {
            // Need to use special where, because column is translated.
            $categoryIDs = Category::select('id')->whereTranslation('name', $categoryName)->get()->pluck('id');
            if ($categoryIDs->isEmpty()) {
                return response('', 404);
            }
        }

        // Compute rectangle of area to search to limit query results.
        $earthRadius = MapHelper::EARTH_RADIUS; // Radius of Earth in meters

        // latitude
        $maxLat = (float) $lat + rad2deg($radius / $earthRadius);
        $minLat = (float) $lat - rad2deg($radius / $earthRadius);

        // longitude (longitude is smaller when latitude increases)
        $maxLng = (float) $lng + rad2deg($radius / $earthRadius) / cos(deg2rad((float) $lat));
        $minLng = (float) $lng - rad2deg($radius / $earthRadius) / cos(deg2rad((float) $lat));

        $searchedArea = Polygon::fromString("($minLng $minLat,$maxLng $minLat,$maxLng $maxLat,$minLng $maxLat,$minLng $minLat)");

        if ($page != null) {
            $page = intval($page);
            if ($page < 1) {
                $page = 1;
            }
        }
        
        $zoomTranslation = config('awapp.map.clustering.zoomTranslation');
        if (array_key_exists($zoom, $zoomTranslation) && $zoomTranslation[$zoom] != null) {
            $query = PlaceCluster::select('id', 'gps', 'avg_gps', 'count');

            if (Cache::has('build-cluster-last-valid-id')) {
                $query = $query->where('id', '<=', Cache::get('build-cluster-last-valid-id', null));
            }

            // If category name is setted, than limit query to categories
            if (!empty($categoryName)) {
                $query = $query->whereIn('category_id', $categoryIDs);
            } else {
                $query = $query->whereNull('category_id');
            }

            // Set required zoom level
            $query = $query->where('zoom_level', $zoomTranslation[$zoom]);

            //Limit query to rectangle area
            $query = $query->within('avg_gps', $searchedArea);

            $query = $query->orderBy('id');

            if ($page != null) {
                $query = $query->limit($pageCount)->offset(($page - 1) * $pageCount);
            }

            return PlaceClusterResource::collection($query->get());
        }


        // Select basic columns
        $query = Place::select('id', 'gps', 'name', 'image', 'category_id');

        // Insert with('userPhotos') - one query for multiple results without image
        $query = $query->with('userPhotos');

        // When using pages on places, not use harvesine formula (extremly expensive when show all earth).
        if ($page == null) {
            // Build special select for getting distance from input GPS location
            $query = $query->selectRaw("(? * acos(cos(radians(?)) * cos(radians(ST_Y(gps))) * cos(radians(ST_X(gps)) - radians(?)) + sin(radians(?)) * sin(radians(ST_Y(gps))))) AS distance", [$earthRadius, $lat, $lng, $lat]);
        }

        // If category name is setted, than limit query to categories
        if (!empty($categoryName)) {
            $query = $query->whereIn('category_id', $categoryIDs);
        }

        //Limit query to rectangle area
        $query = $query->within('gps', $searchedArea);

        // When using pages on places, not use harvesine formula (extremly expensive when show all earth).
        if ($page == null) {
            // Need to be here because of Eloquent, Without it is valid SQL, but Eloquent need it here because of having
            $query = $query->groupBy('id', 'gps', 'name', 'image', 'category_id', 'distance');

            // Limit to radius
            $query = $query->having('distance', '<', $radius);

            // Order by distance from input GPS location
            $query = $query->orderBy('distance');
        } else {
            $query = $query->limit($pageCount)->offset(($page - 1) * $pageCount);
        }

        if ($user != null) {
            $query = $query->with(['userChecks' => function ($q) use ($user) {
                $q->where('user_id', $user->id);
            }]);
        }

        return PlaceResource::collection($query->get());
    }
}
