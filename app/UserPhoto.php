<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class UserPhoto extends Model
{
    protected $fillable = ['place_id', 'user_id', 'image'];

    public static function boot()
    {
        parent::boot();

        self::deleting(function (UserPhoto $userPhoto) {
            // Before delete, delete image.
            if (Storage::disk(config('voyager.storage.disk'))->exists($userPhoto->image)) {
                Storage::disk(config('voyager.storage.disk'))->delete($userPhoto->image);
            }
        });
    }

    /**
     * Get the place that owns the user photo.
     */
    public function place()
    {
        return $this->belongsTo(\App\Place::class);
    }

    /**
     * Get the user that owns the user photo.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}