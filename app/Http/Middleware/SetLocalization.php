<?php

namespace App\Http\Middleware;

use Closure;

class SetLocalization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $localization = $request->header('X-Localization');

        // set laravel localization
        if ($localization != null) {
            app()->setLocale($localization);
        }

        return $next($request);
    }
}
