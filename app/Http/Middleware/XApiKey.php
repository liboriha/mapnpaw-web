<?php

namespace App\Http\Middleware;

use Closure;

class XApiKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('X-API-KEY');

        if(!$token || $token != config('awapp.X-API-Key')) {
            abort(401);
        }

        return $next($request);
    }
}