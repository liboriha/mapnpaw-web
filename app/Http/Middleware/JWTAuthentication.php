<?php

namespace App\Http\Middleware;

use App\Jobs\InvalidateJWTToken;
use Closure;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Facades\JWTAuth;

class JWTAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = null;

        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response(null, 101);
            }
        } catch (TokenExpiredException $e) {
            try {
                $jwtToken = JWTAuth::getToken();
                // Get payload for refresh
                $payload = JWTAuth::manager()->setRefreshFlow()->decode($jwtToken);
                // Get user from token data
                $userId = $payload->get('sub');
                Auth::byId($userId);
                $user = Auth::user();

                // Refresh token with new refresh TTL.
                $refreshedToken = JWTAuth::fromUser($user);
                $token = 'Bearer ' . $refreshedToken;

                // Invalidate in 2 minutes - can be asynchronous calling - multiple use old token to refresh.
                InvalidateJWTToken::dispatch($jwtToken)->delay(now()->addMinutes(2));
            } catch (JWTException $e) {
                return response(null, 103);
            }
        } catch (JWTException $e) {
            return response($e, 101);
        }

        Auth::login($user, false);

        $response = $next($request);
        $response->headers->set('Authorization', $token);

        return $response;
    }
}