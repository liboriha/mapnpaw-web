<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\GetCategories;
use App\Http\Resources\CategorySummary;
use App\Services\Category\GetTranslatedCategoriesService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    // URL: GET /api/v1/categories
    public function getCategories(GetCategories $request, GetTranslatedCategoriesService $service)
    {
        $uniqueTransCats = $service->getTranslatedCategories($request->input('continentId'));

        foreach ($uniqueTransCats as $uniqueTransCat) {
            $uniqueTransCat->getModel()->processUserInfo(Auth::user());
        }

        return CategorySummary::collection($uniqueTransCats);
    }
}
