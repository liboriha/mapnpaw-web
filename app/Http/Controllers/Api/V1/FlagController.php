<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Resources\ContinentFlags;
use App\Http\Resources\PlaceDetail;
use App\Continent;
use App\Category;

class FlagController extends Controller
{
    // URL: GET /api/v1/flags
    public function getFlags()
    {
        // Use with methods to eager loading data from database which we use later.
        // Use whereHasFlag() to limit to categories with flags.
        // Use with('flagInfo') to process with ContinentFlags (filter categories).
        // Use with('continent') to get info about category continent.
        // Use withTranslation() to get translations for category and continent.
        return ContinentFlags::collection(
            Continent::with(['categories' => function ($q) {
                $q->whereHasFlag()
                    ->with('flagInfo')
                    ->with(['continent' => function ($q) {
                        $q->withTranslation();
                    }])
                    ->withTranslation();
            }])
            ->withTranslation()
            ->get()
        );
    }

    // URL: GET /api/v1/flag/{id}
    public function getPlaceForFlag($id)
    {
        $user = Auth::user();

        // Comment whereHasMedal, we want empty collection of places, if no top place exists in category.
        // // Finds category with flag place -> flag exists, otherwise not exists.
        // Flag belongs to category -> flag ID = category ID
        $category = Category::find($id); // Category::whereHasFlag()->find($id);
        if ($category == null) {
            return response('', 404);
        }

        // Use with methods to eager loading data from database which we use later.
        // Category might have only one flag place.
        $place = $category->places()
            ->isFlag()
            ->with(['userPhotos' => function ($query) {
                $query->with('user');
            }])
            ->with(['userRatings' => function ($query) {
                $query->with('user');
            }])
            ->first();
        
        if ($place == null) {
            return response()->json(['data' => []]);
        }

        $place->processUserInfo($user->id);

        return PlaceDetail::collection(collect([$place]));
    }
}
