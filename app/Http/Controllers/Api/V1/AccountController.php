<?php
namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\Api\V1\ChangeUserProfileRequest;
use App\Http\Requests\Api\V1\ChangePasswordRequest;
use App\Http\Requests\Api\V1\LoginRequest;
use App\Http\Requests\Api\V1\RegisterRequest;
use App\Http\Requests\Api\V1\ResetPasswordRequest;
use App\Http\Requests\Api\V1\FacebookLoginRequest;
use Illuminate\HTTP\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserToken;
use App\Http\Resources\UserProfile as UserResource;
use Illuminate\Support\Facades\Auth;
use App\Http\Helpers\FileHelper;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use App\User;
use Socialite;

class AccountController extends Controller
{
    // URL: POST /api/v1/account
    public function register(RegisterRequest $request)
    {
        $user = User::create([
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
        ]);

        return new UserToken($user, 200);
    }

    // URL: POST /api/v1/token
    public function login(LoginRequest $request)
    {
        Auth::attempt([
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ]);
        
        if (Auth::check()) {
            // Login success
            
            return new UserToken(Auth::user(), 200);
        } else {
            // Login fail

            return abort(401, __('auth.failed'));
        }
    }

    // URL: GET /api/v1/profile
    public function getProfile()
    {
        return new UserResource(Auth::user(), 200);
    }

    // URL: POST /api/v1/profile
    public function changeProfile(ChangeUserProfileRequest $request)
    {
        $user = Auth::user();

        $user->email = $request->input('email');
        $user->firstname = $request->input('firstname');
        $user->lastname = $request->input('lastname');
        $user->nickname = $request->input('nickname');
        $user->visibility = strtoupper($request->input('display'));

        $oldUserImage = $user->avatar;

        // Save new profile image if exists
        if (!empty($request->input('profilePicture'))) {
            // e.g.: users/November2017/ as in BREAD editor
            $path = config('awapp.images.profile-image.directory') . '/' . date('FY') . '/';
            $prefix = config('awapp.images.profile-image.prefix');

            $savedFileName = FileHelper::saveBase64Image(
                $request->input('profilePicture'),
                config('voyager.storage.disk'),
                $path,
                $prefix
            );

            $user->avatar = $path . $savedFileName;
        }

        // Save new data to user profile
        $user->save();

        // Remove old profile image if it's not default
        if (!empty($request->input('profilePicture'))) {
            if ($oldUserImage != config('voyager.user.default_avatar', 'users/default.png')) {
                Storage::disk(config('voyager.storage.disk'))->delete($oldUserImage);
            }
        }

        return response('', 200);
    }

    // URL: POST /api/v1/profile/password
    public function changePassword(ChangePasswordRequest $request)
    {
        // Change user password
        $user = Auth::user();
        $user->password = bcrypt($request->input('newPassword'));
        $user->save();

        return response('', 204);
    }

    // URL: POST /api/v1/account/forgotten-password
    public function sendResetLinkEmail(ResetPasswordRequest $request)
    {
        $user = User::where('email', $request->input('email'))->first();

        if ($user != null && $user->facebook_id != null) {
            throw ValidationException::withMessages(['email' => __('validation.custom.email.facebook-cant-reset')]);
        }

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message.
        // Finally, we'll send out a proper response.
        $response = Password::broker()->sendResetLink(
            $request->only('email')
        );

        if ($response == Password::RESET_LINK_SENT) {
            return response('', 204);
        } else {
            return abort(404, __('auth.reset-failed'));
        }
    }

    // URL: POST /api/v1/token/facebook
    public function facebookLogin(FacebookLoginRequest $request)
    {
        // Get info from facebook
        try {
            $facebookUser = Socialite::driver('facebook')
                ->fields([
                    'first_name', 'last_name', 'name', 'email'
                ])
                ->userFromToken($request->input('token'));
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $exceptionData = json_decode($e->getResponse()->getBody()->getContents());
            throw ValidationException::withMessages(['facebook' => $exceptionData->error->message]);
        }

        // Test if we have email from facebook. (Have permission to get email.)
        Validator::validate((array)$facebookUser, ['email' => 'required']);

        // Get facebook user if exists.
        $user = User::where('facebook_id', $facebookUser->id)->first();

        if ($user == null) {
            // Facebook user not registered - register facebook user

            $user = User::where('email', $facebookUser->email)->first();

            if ($user != null) {
                // Conflict, user email exists, can't register facebook user
                throw ValidationException::withMessages(['email' => __('validation.custom.facebook.cant-register')]);
            }

            // Create new user
            // Create image
            $avatarPath = null;

            if ($facebookUser->avatar != null) {
                $path = config('awapp.images.profile-image.directory') . '/' . date('FY') . '/';
                $prefix = config('awapp.images.profile-image.prefix');

                $savedFileName = FileHelper::saveURLImage(
                    $facebookUser->avatar,
                    config('voyager.storage.disk'),
                    $path,
                    $prefix
                );

                $avatarPath = $path . $savedFileName;
            }
            
            // Create user - not add nickname because facebook don't send it.
            $user = new User();
            $user->facebook_id = $facebookUser->id;
            $user->firstname = $facebookUser->user['first_name'];
            $user->lastname = $facebookUser->user['last_name'];
            $user->email = $facebookUser->email;
            $user->password = bcrypt(str_random(40)); // Generate random password, login by facebook token.
            $user->visibility = User::VISIBILITY_NAME;
            $user->avatar = $avatarPath;

            $user->save();
        }

        return new UserToken($user, 200);
    }
}
