<?php

namespace App\Http\Controllers\Api\V1;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Helpers\MapHelper;
use App\Http\Requests\Api\V1\AddUserPhotoRequest;
use App\Http\Requests\Api\V1\AddUserRatingRequest;
use App\Http\Requests\Api\V1\GetClustersRequest;
use App\Http\Requests\Api\V1\GetPlaceDetailRequest;
use App\Http\Requests\Api\V1\GetPlacesPagedRequest;
use App\Http\Requests\Api\V1\GetPlacesRequest;
use App\Http\Resources\AchievedPoints;
use App\Http\Resources\CheckInOnPlace;
use App\Http\Resources\Place as PlaceResource;
use App\Http\Resources\PlaceCluster as PlaceClusterResource;
use App\Http\Resources\PlaceDetail;
use App\Place;
use App\PlaceCluster;
use App\Services\Map\GetPlacesOrClustersService;
use App\Services\PlaceCluster\BuildClusterService;
use Carbon\Carbon;
use Grimzy\LaravelMysqlSpatial\Types\LineString;
use Grimzy\LaravelMysqlSpatial\Types\MultiPolygon;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Grimzy\LaravelMysqlSpatial\Types\Polygon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class MapController extends Controller
{
    // URL: GET /api/v1/map
    public function getPlaces(GetPlacesRequest $request, GetPlacesOrClustersService $service)
    {
        return $service->getPlacesOrClusters(
            Auth::user(),
            $request->input('lat'),
            $request->input('lng'),
            $request->input('radius'),
            $request->input('category'),
            $request->input('zoom')
        );
    }

    // URL: GET /api/v1/map/paged
    public function getPlacesPaged(GetPlacesPagedRequest $request, GetPlacesOrClustersService $service)
    {
        return $service->getPlacesOrClusters(
            Auth::user(),
            $request->input('lat'),
            $request->input('lng'),
            $request->input('radius'),
            $request->input('category'),
            null,
            $request->input('page')
        );
    }

    // URL: GET /api/v1/map/{pointId}
    public function getPlaceDetail(GetPlaceDetailRequest $request, $pointId)
    {
        $user = Auth::user();

        // Use with method to eager loading data from database which we use later.
        $place = Place::with(['userPhotos' => function ($query) {
            $query->with('user');
        }])->with(['userRatings' => function ($query) {
            $query->with('user');
        }])->find($pointId);

        if ($place == null) {
            return response('', 404);
        }

        $place->processUserInfo($user->id, $request->input('lat'), $request->input('lng'));

        return new PlaceDetail($place);
    }

    // URL: POST /api/v1/map/{pointId}/check-in
    public function checkIn(Request $request, $pointId)
    {
        $user = Auth::user();

        // Use with method to eager loading data from database which we use later.
        $place = Place::with(['category' => function ($query) {
            $query->with('continent');
        }])->find($pointId);

        if ($place == null) {
            return response('', 404);
        }

        $place->processCheckIn($user->id);

        return new CheckInOnPlace($place);
    }

    // URL: POST /api/v1/map/{pointId}/photo
    public function addUserPhoto(AddUserPhotoRequest $request, $pointId)
    {
        $user = Auth::user();

        // Use with method to eager loading data from database which we use later.
        // Load user photos of place.
        $place = Place::with(['userPhotos' => function ($query) use ($user) {
            $query->where('user_id', $user->id);
        }])->find($pointId);

        if ($place == null) {
            return response('', 404);
        }

        $place->addUserPhotoBase64($user->id, $request->input('image'));

        return new AchievedPoints($place);
    }

    // URL: POST /api/v1/map/{pointId}/rate
    public function addUserRating(AddUserRatingRequest $request, $pointId)
    {
        $user = Auth::user();

        // Use with method to eager loading data from database which we use later.
        // Load user ratings of place.
        $place = Place::with(['userRatings' => function ($query) use ($user) {
            $query->where('user_id', $user->id);
        }])->find($pointId);

        if ($place == null) {
            return response('', 404);
        }

        $place->addUserRating($user->id, $request->input('stars'), $request->input('text'));

        return response('', 204);
    }
}
