<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Achievements;
use App\Http\Resources\CheckInPlace;
use Illuminate\Database\Eloquent\Collection;

class AchievementsController extends Controller
{
    // URL: GET /api/v1/achievements
    public function getAchievements()
    {
        $user = Auth::user();

        return new Achievements($user);
    }

    // URL: GET /api/v1/check-ins
    public function getCheckInPlaces()
    {
        $user = Auth::user();

        // Gets user checks ordered by creation date descending  - from newest.
        $userChecks = $user->userChecks()->with('place')->orderBy('created_at', 'desc')->get();

        // Group by place_id and takes first item from that group - we want only one record for place_id.
        $userChecks = $userChecks->groupBy('place_id');

        $userChecks = $userChecks->map(function ($item) {
            return $item->first();
        });

        // Need values to resource collection to work right (not added ID before item).
        $userChecks = $userChecks->values();

        // Prevent more SQL queries, because Place resource needs userChecks relation on place model.
        foreach ($userChecks as $userCheck) {
            $userCheck->place->setRelation('userChecks', new Collection([$userCheck]));
        }

        return CheckInPlace::collection($userChecks);
    }
}
