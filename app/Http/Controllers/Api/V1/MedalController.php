<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\ContinentMedals;
use App\Http\Resources\PlaceDetail;
use App\Continent;
use App\Category;

class MedalController extends Controller
{
    // URL: GET /api/v1/medals
    public function getMedals()
    {
        // Use with methods to eager loading data from database which we use later.
        // Use whereHasMedal() to limit to categories with medals.
        // Use with('medalInfo') to process with ContinentMedals (filter categories).
        // Use with('continent') to get info about category continent.
        // Use withTranslation() to get translations for category and continent.
        return ContinentMedals::collection(
            Continent::with(['categories' => function ($q) {
                $q->whereHasMedal()
                    ->with('medalInfo')
                    ->with(['continent' => function ($q) {
                        $q->withTranslation();
                    }])
                    ->withTranslation();
            }])
            ->withTranslation()
            ->get()
        );
    }

    // URL: GET /api/v1/medal/{id}
    public function getPlacesForMedal($id)
    {
        $user = Auth::user();

        // Comment whereHasMedal, we want empty collection of places, if no top place exists in category.
        // // Finds category with top place -> medal exists, otherwise not exists.
        // Medal belongs to category -> medal ID = category ID
        $category = Category::find($id); // Category::whereHasMedal()->find($id);
        if ($category == null) {
            return response('', 404);
        }

        // Use with methods to eager loading data from database which we use later.
        $places = $category->places()
            ->isTop()
            ->with(['userPhotos' => function ($query) {
                $query->with('user');
            }])
            ->with(['userRatings' => function ($query) {
                $query->with('user');
            }])
            // Used for preloading data for processUserInfo method.
            // Prevents many SQL query to database.
            ->with(['userChecks' => function ($query) use ($user) {
                $query->where('user_id', $user->id);
            }])
            ->get();

        foreach ($places as $place) {
            $place->processUserInfo($user->id);
        }

        return PlaceDetail::collection($places);
    }
}
