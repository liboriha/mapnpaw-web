<?php

namespace App\Http\Controllers\Browser;

use App\Http\Controllers\Controller;
use App\Http\Requests\Browser\RegisterRequest;
use App\Services\User\RegisterService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /**
     * User registration
     * POST /register
     */
    public function register(RegisterRequest $request, RegisterService $registerService)
    {
        $user = $registerService->registerUser($request->input('email'), $request->input('password'));

        Auth::login($user);

        return redirect()
            ->route("map")
            ->with([
                'message'    => __('awapp.register.success-messages.registered'),
                'alert-type' => 'success',
            ]);
    }
}
