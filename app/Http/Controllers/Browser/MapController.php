<?php

namespace App\Http\Controllers\Browser;

use App\Http\Controllers\Controller;
use App\Services\Category\GetTranslatedCategoriesService;
use Illuminate\Http\Request;

class MapController extends Controller
{
    /**
     * Show map
     * GET /map
     */
    public function index(GetTranslatedCategoriesService $service)
    {
        return view('map.index', [
            'categories' => $service->getTranslatedCategories(),
        ]);
    }

    /**
     * Show visited places by user.
     * GET /map/visited-places
     */
    public function showVisitedPlaces()
    {
        return view('visited-places.index');
    }
}
