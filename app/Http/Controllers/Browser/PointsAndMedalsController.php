<?php

namespace App\Http\Controllers\Browser;

use App\Http\Controllers\Controller;
use App\UserPoint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PointsAndMedalsController extends Controller
{
    /**
     * Show signpost of points, medals and flags
     * POST /points-and-medals
     */
    public function index()
    {
        $userPoints = Auth::user()->userPoints;

        $checkinPoints = $userPoints->where('type', UserPoint::TYPE_CHECKIN)->sum('points');
        $photoPoints = $userPoints->where('type', UserPoint::TYPE_FIRST_PHOTO)->sum('points');

        return view('points-and-medals.index', [
            'pointsPhotos' => $photoPoints,
            'pointsCheckins' => $checkinPoints,
        ]);
    }
}
