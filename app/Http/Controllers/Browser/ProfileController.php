<?php

namespace App\Http\Controllers\Browser;

use App\Http\Controllers\Controller;
use App\Http\Requests\Browser\EditFacebookProfileRequest;
use App\Http\Requests\Browser\EditProfileRequest;
use App\Services\User\UpdateProfileService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Show user profile
     * GET /profile
     */
    public function index()
    {
        return view('profile.index', [
            'user' => Auth::user(),
        ]);
    }

    /**
     * User show edit profile
     * Determine if user is facebook user (facebook user cant edit all fields)
     * GET /profile/edit
     */
    public function editProfile()
    {
        return view('profile-edit.index', [
            'user' => Auth::user(),
            'isFacebookUser' => Auth::user()->isFacebookUser(),
        ]);
    }

    /**
     * User update profile
     * Determine if user is facebook user (facebook user cant edit all fields)
     * POST /profile/edit
     */
    public function updateProfile(UpdateProfileService $service)
    {
        if (Auth::user()->isFacebookUser()) {
            $request = app(EditFacebookProfileRequest::class);

            $service->updateFacebookProfile(
                Auth::user(),
                $request->input('nickname'),
                $request->input('name-visibility')
            );
        } else {
            $request = app(EditProfileRequest::class);

            $service->updateProfile(
                Auth::user(),
                $request->input('email'),
                $request->input('password'),
                $request->input('name'),
                $request->input('last-name'),
                $request->input('nickname'),
                $request->input('name-visibility'),
                $request->file('photo-file')
            );
        }

        return redirect()
            ->route("profile")
            ->with([
                'message'    => __('awapp.profile.success-messages.updated'),
                'alert-type' => 'success',
            ]);
    }
}
