<?php

namespace App\Http\Controllers\Browser;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show home page
     * GET /
     */
    public function index()
    {
        return view('home.index');
    }
}
