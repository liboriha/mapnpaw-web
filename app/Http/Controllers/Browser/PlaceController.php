<?php

namespace App\Http\Controllers\Browser;

use App\Http\Controllers\Controller;
use App\Place;
use App\Services\Place\DeleteUserPhotoService;
use App\UserPhoto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PlaceController extends Controller
{
    /**
     * Show place detail
     * GET /place/{placeId}/detail
     */
    public function detail($placeId)
    {
        // Preload data to use in place detail.
        $place = Place::with('averageStarsRatings', 'userPhotos.user')->find($placeId);

        if ($place == null) {
            // Place not found.
            abort(404);
        }

        // Preload data about checkin to use in place detail.
        // If user is not logged, pass null (set false to processed items).
        $place->processUserInfo(Auth::user()->id ?? null);

        // Group user photos by users.
        $groupedUserPhotos = $place->userPhotos->groupBy('user_id');
        if (Auth::check()) {
            // Get owner photos.
            $ownerUserPhotos = $groupedUserPhotos->get(Auth::user()->id);
            // Remove owner photos from group photos.
            $groupedUserPhotos->forget(Auth::user()->id);
        } else {
            $ownerUserPhotos = [];
        }

        $place->category = $place->category->translate();
        
        return view('place-detail.index', [
            'place' => $place,
            'ownerUserPhotos' => $ownerUserPhotos, // Only owner photos or null
            'groupedUserPhotos' => $groupedUserPhotos, // Without owner photos
            'continentName' => $place->category->continent->name,
            'continentId' => $place->category->continent_id,
            'categoryName' => $place->category->name,
            'categoryId' => $place->category_id,
            'placeName' => $place->name,
            'placeId' => $place->id,
        ]);
    }

    /**
     * Show medal/flag place detail
     * GET /medals/continent/{continentId}/category/{categoryId}/place/{placeId}/detail
     * GET /flags/continent/{continentId}/category/{categoryId}/place/{placeId}/detail
     */
    public function medalFlagPlaceDetail($continentId, $categoryId, $placeId)
    {
        return $this->detail($placeId);
    }

    /**
     * Show place ratings
     * GET /place/{placeId}/rating
     */
    public function showRatings($placeId)
    {
        // Preload data to use in place detail.
        $place = Place::with('userRatings.user')->find($placeId);

        if ($place == null) {
            // Place not found.
            abort(404);
        }

        $place->category = $place->category->translate();

        return view('place-rating.index', [
            'place' => $place,
            'continentName' => $place->category->continent->name,
            'continentId' => $place->category->continent_id,
            'categoryName' => $place->category->name,
            'categoryId' => $place->category_id,
            'placeName' => $place->name,
            'placeId' => $place->id,
        ]);
    }

    /**
     * Show medal/flag place rating
     * GET /medals/continent/{continentId}/category/{categoryId}/place/{placeId}/rating
     * GET /flags/continent/{continentId}/category/{categoryId}/place/{placeId}/rating
     */
    public function medalFlagPlaceRating($continentId, $categoryId, $placeId)
    {
        return $this->showRatings($placeId);
    }

    /**
     * Delete user photo, only can delete own photo.
     * GET /place/delete-photo/{photoId}
     */
    public function deletePhoto($photoId, DeleteUserPhotoService $service)
    {
        $photo = UserPhoto::find($photoId);

        $placeId = $photo->place_id ?? null;

        $service->deletePhoto(Auth::user()->id, $photo);

        return redirect()
            ->route("place.detail", ['placeId' => $placeId])
            ->with([
                'message'    => __('awapp.place.success-messages.photo-deleted'),
                'alert-type' => 'success',
            ]);
    }
}
