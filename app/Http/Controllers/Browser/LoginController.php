<?php

namespace App\Http\Controllers\Browser;

use App\Exceptions\ValidationExceptionExtension;
use App\Http\Controllers\Controller;
use App\Http\Requests\Browser\LoginRequest;
use App\Services\User\FacebookService;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * POST /login
     * Need register show login popup, if login failed to show login popup again.
     *
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function postLogin(LoginRequest $request)
    {
        ValidationExceptionExtension::setPageProcessing('log-in');

        // Check if user is facebook user - error.
        $user = User::where($this->username(), $request->input($this->username()))->first();
        if ($user != null && $user->facebook_id != null) {
            // User is facebook user - can't login as normal user.
            
            return redirect()
                ->route("home")
                ->with([
                    'show-js-popup' => 'log-in',
                ])
                ->withInput(Input::except('password'))
                ->withErrors([
                    'login' => __('awapp.login.error-messages.facebook-login-as-normal')
                ]);
        }

        return $this->login($request);
    }

    /**
     * Preempts $redirectTo member variable (from RedirectsUsers trait)
     */
    public function redirectTo()
    {
        if (Auth::user()->hasRole('user')) {
            return route('map');
        } elseif (Auth::user()->hasRole('admin') || Auth::user()->hasRole('superadmin')) {
            return route('voyager.dashboard');
        } else {
            return route('home');
        }
    }

    /**
     * GET /facebook/redirect
     * Redirect to facebook login.
     *
     * Need permissions for email and birthdate.
     */
    public function facebookRedirect()
    {
        return Socialite::driver('facebook')->scopes(['email'])->reRequest()->redirect();
    }

    /**
     * GET /facebook/callback
     * Register or login facebook user.
     */
    public function facebookCallback(FacebookService $facebookService)
    {
        ValidationExceptionExtension::setPageProcessing('log-in');
        
        $socialiteUser = Socialite::driver('facebook')->fields([
            'first_name', 'last_name', 'name', 'email'
        ])->user();

        $user = $facebookService->createOrGetUser($socialiteUser);

        Auth::login($user);

        return redirect()->to($this->redirectTo());
    }
}
