<?php

namespace App\Http\Controllers\Browser;

use App\Http\Controllers\Controller;
use App\Http\Requests\Browser\ResetPasswordRequest;
use App\Services\User\SendResetLinkService;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Where to redirect users after resetting their password.
     * Preempts $redirectTo member variable (from RedirectsUsers trait)
     */
    public function redirectTo()
    {
        if (Auth::user()->hasRole('user')) {
            return route('map');
        } elseif (Auth::user()->hasRole('admin') || Auth::user()->hasRole('superadmin')) {
            return route('voyager.dashboard');
        } else {
            return route('home');
        }
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm(Request $request, $token = null)
    {
        // Show reset password popup form.
        session()->now('show-js-popup', 'reset-password');

        return view('home.index', [
            'restoreToken' => $token,
        ]);
    }

    /**
     * Validate the given request with the given rules.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array  $rules
     * @param  array  $messages
     * @param  array  $customAttributes
     * @return array
     */
    public function validate(Request $request, array $rules, array $messages = [], array $customAttributes = [])
    {
        return parent::validate($request, $rules, $messages, array_merge($customAttributes, $this->validationAttr()));
    }

    /**
     * Get the password reset validation attributes names.
     *
     * @return array
     */
    protected function validationAttr()
    {
        return [
            'email' => __('form.form.inputs.placeholders.mail'),
            'password' => __('form.form.inputs.placeholders.password'),
            'password_confirmation' => __('form.form.inputs.placeholders.password_confirmation'),
        ];
    }

    /**
     * Get the response for a successful password reset.
     *
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetResponse($response)
    {
        return redirect($this->redirectPath())
            ->with([
                'message'    => trans($response),
                'alert-type' => 'success',
            ]);
    }

    /**
     * Searches user by email and then send email with URL for resetting password.
     * URL: POST /password-reset
     */
    public function sendResetLinkEmail(ResetPasswordRequest $request, SendResetLinkService $service)
    {
        $status = $service->sendResetLinkEmail($request->input('email'), $request->input('username'));

        if ($status == true) {
            return redirect()
                ->route("home")
                ->with([
                    'message'    => __('awapp.reset-password.success-messages.send-email'),
                    'alert-type' => 'success',
                ]);
        } else {
            return redirect()
                ->route("home")
                ->with([
                    'message'    => __('awapp.reset-password.error-messages.failed'),
                    'alert-type' => 'fail',
                ]);
        }
    }
}
