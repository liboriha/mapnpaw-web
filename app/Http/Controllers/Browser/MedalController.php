<?php

namespace App\Http\Controllers\Browser;

use App\Category;
use App\Continent;
use App\Http\Controllers\Controller;
use App\Http\Helpers\CategoryHelper;
use App\Http\Helpers\ContinentHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MedalController extends Controller
{
    /**
     * Show summary of medals of user on continents.
     * POST /medals
     */
    public function index()
    {
        // Get medals counts.
        $medals = Auth::user()->userMedals;

        $groupedMedals = $medals->groupBy('continent_id');

        $contMedalsCount = $groupedMedals->map(function ($item, $key) {
            return $item->count();
        });

        // Get continents names
        $continents = Continent::withTranslation()->get();

        // Get continents image CSS classes
        $arrOfContImgClasses = ContinentHelper::arrayOfIdToImageClass();

        // Add data - need medals count and image CSS class for each continent.
        foreach ($continents as $continent) {
            $continent->medalsCount = $contMedalsCount[$continent->id] ?? 0;
            $continent->imageClass = $arrOfContImgClasses[$continent->id] ?? '';
            $continent->hasContinentTravelerIcon = $continent->medalsCount >= 10;
        }

        // Translates continents models.
        $continents = $continents->translate();

        return view('medals-detail.index', [
            'continents' => $continents,
        ]);
    }

    /**
     * Show summary of medals of user on specific continent.
     * POST /medals/continent/{continentId}
     */
    public function showCategoriesSummary($continentId)
    {
        $user = Auth::user();

        $continent = Continent::find($continentId);
        if ($continent == null) {
            // Not found continent.
            return abort(404);
        }

        // Get categories with medals data.
        $categories = $continent->categories()
            ->with(['userMedals' => function ($query) use ($user) {
                $query->where('user_id', $user->id);
            }])
            ->get();

        if ($categories == null) {
            // Continent or categories not found.
            abort(404);
        }

        // Get continents image CSS classes
        $arrOfContImgClasses = CategoryHelper::arrayOfIdToImageClass();

        // Add data - need image CSS class for each continent.
        foreach ($categories as $category) {
            $category->imageClass = $arrOfContImgClasses[$category->id] ?? '';
            if ($category->userMedals->isEmpty()) {
                $category->iconImage = $category->medal_image_locked;
            } else {
                $category->iconImage = $category->medal_image_unlocked;
            }
        }
        
        // Translates categories models.
        $categories = $categories->translate();

        // Translates continent model.
        $continent = $continent->translate();

        return view('continents-detail.index', [
            'continentName' => $continent->name,
            'continentId' => $continentId,
            'categories' => $categories,
        ]);
    }

    /**
     * Show summary of medals of user on specific category on specific continent.
     * POST /medals/continent/{continentId}/category/{categoryId}
     */
    public function showPlacesSummary($continentId, $categoryId)
    {
        $user = Auth::user();
        
        // Medal belongs to category -> medal ID = category ID
        $category = Category::where('continent_id', $continentId)->with('continent')->find($categoryId);
        if ($category == null) {
            // Not found category.
            return abort(404);
        }

        // Use with methods to eager loading data from database which we use later.
        $places = $category->places()
            ->isTop()
            // Used for preloading data for processUserInfo method.
            // Prevents many SQL query to database.
            ->with(['userChecks' => function ($query) use ($user) {
                $query->where('user_id', $user->id);
            }])
            ->get();

        foreach ($places as $place) {
            $place->processUserInfo($user->id);
        }

        // Translates continent model.
        $category->continent = $category->continent->translate();

        // Translates category model.
        $category = $category->translate();

        return view('place-list.index', [
            'continentName' => $category->continent->name,
            'continentId' => $continentId,
            'categoryName' => $category->name,
            'categoryId' => $categoryId,
            'places' => $places,
        ]);
    }
}
