<?php

namespace App\Http\Requests\Browser;

use App\Http\Requests\Traits\ErrorAdditionalMessagesTrait;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ResetPasswordRequest extends FormRequest
{
    use ErrorAdditionalMessagesTrait;

    /**
     * Set popup information to show right form when ValidationException throwed.
     * @var string
     */
    protected $showPopup = 'forgot-password';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email|max:191|exists:users,email',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'email' => __('form.form.inputs.placeholders.mail'),
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.exists' => __('auth.failed'),
        ];
    }
}
