<?php

namespace App\Http\Requests\Browser;

use App\Http\Requests\Traits\ErrorAdditionalMessagesTrait;
use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    use ErrorAdditionalMessagesTrait;

    /**
     * Set popup information to show right form when ValidationException throwed.
     * @var string
     */
    protected $showPopup = 'log-in';
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|min:3|max:191',
            'password' => 'required|string|min:6',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'email' => __('form.form.inputs.placeholders.mail'),
            'password' => __('form.form.inputs.placeholders.password'),
        ];
    }
}
