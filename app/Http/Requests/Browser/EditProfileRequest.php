<?php

namespace App\Http\Requests\Browser;

use App\Http\Requests\Traits\ErrorAdditionalMessagesTrait;
use App\User;
use Illuminate\Contracts\Validation\Factory as ValidationFactory;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class EditProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nickname' => 'nullable|required_if:name-visibility,' . User::VISIBILITY_NICKNAME . '|string|max:191',
            'name-visibility' => [
                'required',
                Rule::in(User::$visibilities)
            ],
            'name' => 'nullable|required_if:name-visibility,' . User::VISIBILITY_NAME . '|string|max:191',
            'last-name' => 'nullable|required_if:name-visibility,' . User::VISIBILITY_NAME . '|string|max:191',
            'email' => [
                'required',
                'string',
                'email',
                'max:191',
                Rule::unique('users', 'email')->ignore(Auth::user()->id),
            ],
            'password' => 'nullable|string|min:6',
            'photo-file' => 'nullable|image',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'nickname' => __('form.form.labels.nickname'),
            'name-visibility' => __('form.form.labels.visible'),
            'name' => __('form.form.labels.name'),
            'last-name' => __('form.form.labels.last-name'),
            'email' => __('form.form.labels.mail'),
            'password' => __('form.form.labels.change-password'),
            'photo-file' => __('form.form.labels.photo'),
        ];
    }

    /**
     * Get custom values for validator errors.
     *
     * @return array
     */
    public function values()
    {
        return [
            'name-visibility' => [
                User::VISIBILITY_NICKNAME => __('form.form.labels.nickname-visible'),
                User::VISIBILITY_NAME => __('form.form.labels.name-visible'),
            ],
        ];
    }

    /**
     * Create the default validator instance.
     *
     * @param  \Illuminate\Contracts\Validation\Factory  $factory
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function createDefaultValidator(ValidationFactory $factory)
    {
        $validator = parent::createDefaultValidator($factory);

        return $validator->setValueNames($this->values());
    }
}
