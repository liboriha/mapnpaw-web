<?php

namespace App\Http\Requests\Traits;

use App\Exceptions\ValidationExceptionExtension;
use App\Http\Requests\AbstractRequests\AbstractCreateIssueRequest;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Trait for sending flash data defined in property $flashData.
 */
trait ErrorAdditionalMessagesTrait
{
    /**
     * Set page information to show right form when ValidationException throwed.
     * @var string
     */
    // protected $showPage = 'page_name';

    /**
     * Set popup information to show right form when ValidationException throwed.
     * @var string
     */
    // protected $showPopup = 'popup_name';

    /**
     * Set section information to show right form when ValidationException throwed.
     * @var string
     */
    // protected $showSection = 'section_name';

    /**
     * Array to set additional data to session only for next request.
     * Only when ValidationException throwed
     * @var array
     */
    // protected $additionalMessages = ['key' => 'value'];

    /**
     * Sets the parameters for this request.
     *
     * This method also set additional messages to show right form if validation failed
     * (ValidationException throwed).
     *
     * This method also re-initializes all properties.
     *
     * @param array           $query      The GET parameters
     * @param array           $request    The POST parameters
     * @param array           $attributes The request attributes (parameters parsed from the PATH_INFO, ...)
     * @param array           $cookies    The COOKIE parameters
     * @param array           $files      The FILES parameters
     * @param array           $server     The SERVER parameters
     * @param string|resource $content    The raw body data
     */
    public function initialize(
        array $query = array(),
        array $request = array(),
        array $attributes = array(),
        array $cookies = array(),
        array $files = array(),
        array $server = array(),
        $content = null
    ) {
        parent::initialize($query, $request, $attributes, $cookies, $files, $server, $content);

        $this->setErrorAdditionalMessages();
    }

    /**
     * Set data to session only for next request.
     * Intended for multi-form page validation.
     */
    protected function setErrorAdditionalMessages()
    {
        ValidationExceptionExtension::setPageProcessing(
            property_exists($this, 'showPopup') ? $this->showPopup : null
        );

        if (property_exists($this, 'additionalMessages')) {
            ValidationExceptionExtension::addAdditionalMessages($this->additionalMessages);
        }
    }
}
