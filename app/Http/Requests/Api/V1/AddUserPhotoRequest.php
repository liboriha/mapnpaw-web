<?php

namespace App\Http\Requests\Api\V1;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use App\UserPhoto;

class AddUserPhotoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required|string'
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            // Check if user reached limit of number of user photos for one place.
            $user = Auth::user();
            $place_id = $this->pointId;

            $userPhotosCount = UserPhoto::select('id')->where('user_id', $user->id)->where('place_id', $place_id)->count();
            $limitCount = config('awapp.places.photos.user-limit-count');

            if ($userPhotosCount >= $limitCount) {
                $validator->errors()->add('image', __('validation.custom.user-photos.limit-reached', ['limit' => $limitCount]));
            }
        });
    }
}
