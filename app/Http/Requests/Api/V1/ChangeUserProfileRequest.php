<?php

namespace App\Http\Requests\Api\V1;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use App\User;

class ChangeUserProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email|max:191',
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'nickname' => 'required|string',
            'display' => 'required|string|in:name,nickname',
            'profilePicture' => '',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            // Get current user
            $user = Auth::user();

            if ($user != null) {
                if ($user->isFacebookUser()) {
                    // Facebook user profile

                    if ($user->email != $this->input('email')) {
                        // Can't change email in facebook user profile

                        $validator->errors()->add('email', __('validation.custom.email.facebook-user-cant-change'));
                    }
                } else {
                    // Normal user profile

                    if ($user->email != $this->input('email')) {
                        // It's not email of current user

                        $userByEmail = User::where('email', $this->input('email'))->first();
                        if ($userByEmail != null) {
                            // Can't change user profile email on email that use another user

                            $validator->errors()->add('email', __('validation.custom.email.change-email-already-exist'));
                        }
                    }
                }
            }
        });
    }
}
