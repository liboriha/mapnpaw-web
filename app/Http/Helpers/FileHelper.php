<?php

namespace App\Http\Helpers;

use Illuminate\Http\Testing\MimeType;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Exception\ImageException;
use Intervention\Image\Facades\Image;

class FileHelper
{
    /**
     * Find file name that is not used. Added index to filename.
     *
     * @var unique filename
     */
    public static function getFreeFilename($disk, $path, $filename, $extension)
    {
        $filenameWithoutExt = basename($filename, '.' . $extension);
        $index = 1;

        $tempFilename = $filenameWithoutExt;

        // Make sure the filename does not exist, if it does make sure to add a number to the end 1, 2, 3, etc...
        while (Storage::disk($disk)->exists($path . $tempFilename . '.' . $extension)) {
            $tempFilename = $filenameWithoutExt . (string) ($index++);
        }

        return $tempFilename . '.' . $extension;
    }

    /**
     * Validates image if it's base64 encoded image, if ok, then return null value.
     *
     * @var string|null validation result
     */
    public static function validateBase64Image($data)
    {
        // Check if data contain string "data:"
        if (substr($data, 0, strlen("data:")) !== "data:") {
            $data = "data:" . $data;
        }

        try {
            Image::make($data);
        } catch (ImageException $e) {
            return $e->getMessage();
        }
    }

    /**
     * Save image from string to file. Return filename used for save image.
     *
     * @var saved filename
     */
    public static function saveBase64Image($data, $disk, $path, $filePrefix = '')
    {
        // Check if data contain string "data:"
        if (substr($data, 0, strlen("data:")) !== "data:") {
            $data = "data:" . $data;
        }

        $image = Image::make($data);

        // Get file extension
        $extension = MimeType::search($image->mime);

        if ($extension == null) {
            throw new \UnexpectedValueException('Image has unknown image mime type: ' . $image->mime);
        }

        // Get unique file name
        $fileName = $filePrefix . self::getHash(32) . '.' . $extension;

        $fileName = self::getFreeFilename($disk, $path, $fileName, $extension);

        // Saving file to storage
        $image->encode($extension, null);

        Storage::disk($disk)->put(trim($path, '/') . '/' . $fileName, $image, 'public');

        return $fileName;
    }

    /**
     * Save image from URL to file. Return filename used for save image.
     *
     * @var saved filename
     */
    public static function saveURLImage($url, $disk, $path, $filePrefix = '')
    {
        $image = Image::make($url);

        // Get file extension
        $extension = MimeType::search($image->mime);

        if ($extension == null) {
            throw new \UnexpectedValueException('Image has unknown image mime type: ' . $image->mime);
        }

        // Get unique file name
        $fileName = $filePrefix . self::getHash(32) . '.' . $extension;

        $fileName = self::getFreeFilename($disk, $path, $fileName, $extension);

        // Saving file to storage
        $image->encode($extension, null);

        Storage::disk($disk)->put(trim($path, '/') . '/' . $fileName, $image, 'public');

        return $fileName;
    }

    /**
     * Save image from form input to file. Return filename used for save image.
     *
     * @var saved filename
     */
    public static function saveFormImage(UploadedFile $file, $disk, $path, $filePrefix = '')
    {
        // Get file extension
        $extension = $file->getClientOriginalExtension();

        // Get unique file name
        $fileName = $filePrefix . self::getHash(32) . '.' . $extension;

        $fileName = self::getFreeFilename($disk, $path, $fileName, $extension);

        // Saving file to storage
        $fileName = $file->storeAs($path, $fileName, $disk);

        return basename($fileName);
    }

    /**
     * Generate random hash from time.
     *
     * @var hash
     */
    public static function getHash($len = 64)
    {
        return substr(hash('sha256', strval(time())), 0, $len);
    }

    /**
     * Return URL of image path.
     *
     * @return URL
     */
    public static function getURL($imagePath)
    {
        // Encode image path, if contains spaces or something else.
        $encodePath = implode('/', array_map('rawurlencode', explode('/', $imagePath)));

        return $imagePath != null ? Storage::disk(config('voyager.storage.disk'))->url($encodePath) : null;
    }
}
