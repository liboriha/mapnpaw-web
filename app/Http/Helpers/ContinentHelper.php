<?php

namespace App\Http\Helpers;

class ContinentHelper
{
    /**
     * Get array of image CSS classes by continent IDs.
     *
     * @return array
     */
    public static function arrayOfIdToImageClass()
    {
        return [
            '1' => 'asia',
            '2' => 'africa',
            '3' => 'america',
            '4' => 'europe',
            '5' => 'australia',
        ];
    }
}
