<?php

namespace App\Http\Helpers;

class MapHelper
{
    const EARTH_RADIUS = 6371000; // Earth radius in meters.
    
    /**
     * Calculates the distance between two GPS coordinations.
     *
     * @return distance in km
     */
    public static function getDistance($lat1, $lng1, $lat2, $lng2) 
    {
        // Need restrict value, without restriction if lat and lng is same or opposite on the sphere, then result is NaN!
        return self::EARTH_RADIUS * acos(self::clamp(cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($lng2) - deg2rad($lng1)) + sin(deg2rad($lat1)) * sin(deg2rad($lat2)), -1, 1));
    }

    /**
     * Restrict value to be between min and max.
     *
     * @return restricted value
     */
    protected static function clamp($current, $min, $max) 
    {
        return max($min, min($max, $current));
    }
}
