<?php

namespace App\Http\Helpers;

class CategoryHelper
{
    /**
     * Get array of image CSS classes by categories IDs.
     *
     * @return array
     */
    public static function arrayOfIdToImageClass()
    {
        return [
            '1'  => 'taj-mahal',         // unesco
            '2'  => 'blue-country',      // nature
            '3'  => 'city',              // town
            '4'  => 'egypt-ruin',        // historic
            '5'  => 'glass-pyramid',     // museum
            '6'  => 'construction',      // factory
            '7'  => 'view',              // view
            '8'  => 'man-on-skate',      // sport
            '9'  => 'building-in-fog',   // culture
            '10' => 'park',              // park

            '11' => 'taj-mahal',         // unesco
            '12' => 'blue-country',      // nature
            '13' => 'city',              // town
            '14' => 'egypt-ruin',        // historic
            '15' => 'glass-pyramid',     // museum
            '16' => 'construction',      // factory
            '17' => 'view',              // view
            '18' => 'man-on-skate',      // sport
            '19' => 'building-in-fog',   // culture
            '20' => 'park',              // park

            '21' => 'taj-mahal',         // unesco
            '22' => 'blue-country',      // nature
            '23' => 'city',              // town
            '24' => 'egypt-ruin',        // historic
            '25' => 'glass-pyramid',     // museum
            '26' => 'construction',      // factory
            '27' => 'view',              // view
            '28' => 'man-on-skate',      // sport
            '29' => 'building-in-fog',   // culture
            '30' => 'park',              // park

            '31' => 'taj-mahal',         // unesco
            '32' => 'blue-country',      // nature
            '33' => 'city',              // town
            '34' => 'egypt-ruin',        // historic
            '35' => 'glass-pyramid',     // museum
            '36' => 'construction',      // factory
            '37' => 'view',              // view
            '38' => 'man-on-skate',      // sport
            '39' => 'building-in-fog',   // culture
            '40' => 'park',              // park

            '41' => 'taj-mahal',         // unesco
            '42' => 'blue-country',      // nature
            '43' => 'city',              // town
            '44' => 'egypt-ruin',        // historic
            '45' => 'glass-pyramid',     // museum
            '46' => 'construction',      // factory
            '47' => 'view',              // view
            '48' => 'man-on-skate',      // sport
            '49' => 'building-in-fog',   // culture
            '50' => 'park',              // park
        ];
    }
}
