<?php

namespace App\Http\Resources;

use App\Http\Helpers\FileHelper;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Expected resource is Place model with processed processUserInfo method.
 * (Need for used attributes: checkedIn, lastCheckIn, canCheckIn)  
 */
class PlaceDetail extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $placeImage = FileHelper::getURL($this->image);

        $userPictures = [];
        // Reverse user photos - we want newest be first (sorted by created_at asc).
        foreach ($this->userPhotos->reverse() as $userPhoto) {
            $profilePicture = FileHelper::getURL($userPhoto->user->avatar);
            $userPhotoImage = FileHelper::getURL($userPhoto->image);

            $userPictures[] = [
                'userId' => $userPhoto->user_id,
                'displayName' => $userPhoto->user->name,
                'profilePicture' => $profilePicture,
                'image' => $userPhotoImage
            ];
        }

        $rating = [];
        // Reverse user ratings - we want newest be first (sorted by created_at asc).
        foreach ($this->userRatings->reverse() as $userRating) {
            $profilePicture = FileHelper::getURL($userRating->user->avatar);

            $rating[] = [
                'userId' => $userRating->user_id,
                'displayName' => $userRating->user->name,
                'profilePicture' => $profilePicture,
                'stars' => $userRating->stars ? (float) $userRating->stars : null,
                'text' => $userRating->rating,
                'timestamp' => $userRating->created_at->timestamp
            ];
        }

        return [
            'id' => $this->id,
            'lat' => $this->gps->getLat(),
            'lng' => $this->gps->getLng(),
            'name' => $this->name,
            'image' => $placeImage,
            'description' => $this->description,
            'address' => $this->address,
            'checkedIn' => $this->checkedIn,
            'lastCheckIn' => $this->lastCheckIn,
            'canCheckIn' => $this->canCheckIn,
            'inRange' => $this->inRange,
            'range' => $this->real_radius,
            'userPictures' => $userPictures,
            'rating' => $rating,
        ];
    }
}
