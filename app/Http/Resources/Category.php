<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Translator;

/**
 * Expected resource is Category model.
 */
class Category extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        if (!($this->resource instanceof Translator) && Voyager::translatable($this->resource)) {
            // If resource is not translated and can be translated, than translate it.
            $this->resource = $this->resource->translate();
        }

        return [
            'id' => $this->id,
            'label' => $this->name,
        ];
    }
}
