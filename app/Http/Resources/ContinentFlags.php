<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Translator;

/**
 * Expected resource is Continent model.
 */
class ContinentFlags extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $user = Auth::user();

        $flagCategories = $this->categories->filter(function ($value, $key) {
            return $value->hasFlag();
        });

        // User is logged - process if user have flag.
        foreach ($flagCategories as $category) {
            // Process with user model (not userID) to better process all categories (cached data on user model).
            $category->processUserInfo($user);
        }
        
        if (!($this->resource instanceof Translator) && Voyager::translatable($this->resource)) {
            // If resource is not translated and can be translated, than translate it.
            $this->resource = $this->resource->translate();
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'flags' => Flag::collection($flagCategories)
        ];
    }
}
