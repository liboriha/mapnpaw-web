<?php

namespace App\Http\Resources;

use App\Http\Helpers\FileHelper;
use App\User;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserProfile extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $profilePicture = FileHelper::getURL($this->avatar);

        return [
            'id' => $this->id,
            'email' => $this->email,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'nickname' => $this->nickname,
            'display' => strtolower($this->visibility),
            'profilePicture' => $profilePicture,
        ];
    }
}
