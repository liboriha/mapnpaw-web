<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PlaceCluster extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {        
        return [
            'id' => $this->id,
            'lat' => $this->avg_gps->getLat(),
            'lng' => $this->avg_gps->getLng(),
            'checkedIn' => false,
            'lastCheckIn' => 0,
            'name' => (string)$this->count,
            'count' => $this->count,
            'image' => null,
        ];
    }
}
