<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Symfony\Component\HttpFoundation\Response;

class BaseResource extends Resource 
{

    /**
     * The status code for the resource response. If null, use default behaviour.
     *
     * @var integer
     */
    public $status_code;

    /**
     * The status code text for the resource response. If null, use default behaviour.
     *
     * @var string
     */
    public $status_code_text;

    /**
     * Create a new resource instance.
     *
     * @param  mixed  $resource
     * @return void
     */
    public function __construct($resource, $code = null, $text = null)
    {
        parent::__construct($resource);

        $this->status_code = $code;
        $this->status_code_text = $text;
    }

    /**
     * Create an HTTP response that represents the object.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function toResponse($request) 
    {
        $response = parent::toResponse($request);

        if ($this->status_code != null && $response instanceof Response) {
            return $response->setStatusCode($this->status_code, $this->status_code_text);
        } else {
            return $response;
        }
    }
}