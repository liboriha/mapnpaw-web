<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\UserPoint;
use App\Category;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Facades\Voyager;

/**
 * Expected resource is User model.
 */
class Achievements extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        // Get points
        $allPoints = $this->userPoints()->get(['type', 'points']);

        $checkinPoints = $allPoints->where('type', UserPoint::TYPE_CHECKIN)->sum('points');
        $photoPoints = $allPoints->where('type', UserPoint::TYPE_FIRST_PHOTO)->sum('points');

        // Determine if user have category medals and flags.
        
        // Use whereHasFlag() to limit to categories with flags.
        // Use with('continent') to get info about category continent to later use.
        // Use withTranslation() to get translations for category and continent.
        $flagCategories = Category::whereHasFlag()
            ->with(['continent' => function ($q) {
                $q->withTranslation();
            }])
            ->withTranslation()
            ->get();

        // Determine if user have category medals and flags.
        foreach ($flagCategories as $category) {
            // Process with user model (not userID) to better process all categories (cached data on user model).
            $category->processUserInfo($this->resource);
        }

        // Use whereHasMedal() to limit to categories with medals.
        // Use with('continent') to get info about category continent to later use.
        // Use wwithTranslation() to get translations for category and continent.
        $medalCategories = Category::whereHasMedal()
            ->with(['continent' => function ($q) {
                $q->withTranslation();
            }])
            ->withTranslation()
            ->get();

        foreach ($medalCategories as $category) {
            // Process with user model (not userID) to better process all categories (cached data on user model).
            $category->processUserInfo($this->resource);
        }

        return [
            'checkinPoints' => $checkinPoints,
            'photoPoints' => $photoPoints,
            'medals' => Medal::collection($medalCategories),
            'flags' => Flag::collection($flagCategories)
        ];
    }
}
