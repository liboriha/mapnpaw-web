<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

/**
 * Expected resource is Place model with processed processCheckIn method. 
 * (Need for used attributes: top_points_remaining, achieved_points)   
 */
class CheckInOnPlace extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $top = null;
        if ($this->is_top) {
            $top = [
                'topPointsRemaining' => $this->top_points_remaining,
                'medal' => Medal::make($this->category)
            ];
        }

        $flag = null;
        if ($this->is_flag) {
            $flag = Flag::make($this->category);
        }

        $message = $this->getMessage();

        return [
            'points' => $this->achieved_points,
            'top' => $top,
            'flag' => $flag,
            'message' => $message
        ];
    }

    protected function getMessage() {
        $pts = trans_choice(
            'checkin-messages.points',
            $this->achieved_points,
            ['num' => $this->achieved_points]
        );

        if($this->is_top) {
            if($this->top_points_remaining) {
                $rem = trans_choice(
                    'checkin-messages.top-points',
                    $this->top_points_remaining,
                    ['num' => $this->top_points_remaining]
                );

                return [
                    'flag' => 'check',
                    'title' => __('checkin-messages.normal', ['points' => $pts]),
                    'subtitle' => __('checkin-messages.top-points-remaining', [
                        'cat' => $this->category->name,
                        'cont' => $this->category->continent->name,
                        'remaining' => $rem
                    ])
                ];
            } else {
                return [
                    'flag' => 'medal',
                    'title' => __('checkin-messages.normal', ['points' => $pts]),
                    'subtitle' => __('checkin-messages.top', [
                        'cat' => $this->category->name,
                        'cont' => $this->category->continent->name,
                    ])
                ];
            }
        }

        if($this->is_flag) {
            return [
                'flag' => 'flag',
                'title' => __('checkin-messages.normal', ['points' => $pts]),
                'subtitle' => __('checkin-messages.flag', [
                    'cat' => $this->category->name,
                    'cont' => $this->category->continent->name,
                ])
            ];
        }

        return [
            'flag' => 'check',
            'title' => __('checkin-messages.normal', ['points' => $pts]),
            'subtitle' => ''
        ];
    }
}
