<?php

namespace App\Http\Resources;

use App\Http\Helpers\FileHelper;
use Illuminate\Http\Resources\Json\Resource;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Translator;

/**
 * Expected resource is Category model with processed processUserInfo method (need for achieved_flag attribute).
 */
class Flag extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $flag_image = null;
        if ($this->achieved_flag) {
            if ($this->flag_image_unlocked == null) {
                $flag_image = url('/images/icons/flag-gold.png');
            } else {
                $flag_image = FileHelper::getURL($this->flag_image_unlocked);
            }
        } else {
            if ($this->flag_image_locked == null) {
                $flag_image = url('/images/icons/flag-grey.png');
            } else {
                $flag_image = FileHelper::getURL($this->flag_image_locked);
            }
        }
        
        if (!($this->resource instanceof Translator) && Voyager::translatable($this->resource)) {
            // If resource is not translated and can be translated, than translate it.
            $this->resource = $this->resource->translate();
        }

        return [
            // ID of flag is ID of category because flag belongs to category.
            'id' => $this->id,
            'image' => $flag_image,
            'name' => $this->flag_name,
            'achieved' => $this->achieved_flag,
            'continent' => Continent::make($this->continent),
            'category' => Category::make($this->resource),
        ];
    }
}
