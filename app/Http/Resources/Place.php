<?php

namespace App\Http\Resources;

use App\Http\Helpers\FileHelper;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Auth;

class Place extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $image = FileHelper::getURL($this->image);
        
        $user = Auth::user();

        if ($this->checkedIn == null && $user != null) {
            $this->resource->processUserInfo($user->id);
        } else {
            $this->checkedIn = false;
            $this->lastCheckIn = 0;
        }

        return [
            'id' => $this->id,
            'lat' => $this->gps->getLat(),
            'lng' => $this->gps->getLng(),
            'checkedIn' => $this->checkedIn,
            'lastCheckIn' => $this->lastCheckIn,
            'name' => $this->name,
            'image' => $image,
        ];
    }
}
