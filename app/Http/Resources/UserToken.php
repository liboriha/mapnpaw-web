<?php

namespace App\Http\Resources;

use Tymon\JWTAuth\Facades\JWTAuth;

class UserToken extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'token' => JWTAuth::fromUser($this->resource)
        ];
    }
}
