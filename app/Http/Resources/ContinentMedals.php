<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Translator;

/**
 * Expected resource is Continent model.
 */
class ContinentMedals extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $user = Auth::user();

        $medalCategories = $this->categories->filter(function ($value, $key) {
            return $value->hasMedal();
        });

        // User is logged - process if user have medal.
        foreach ($medalCategories as $category) {
            // Process with user model (not userID) to better process all categories (cached data on user model).
            $category->processUserInfo($user);
        }
        
        if (!($this->resource instanceof Translator) && Voyager::translatable($this->resource)) {
            // If resource is not translated and can be translated, than translate it.
            $this->resource = $this->resource->translate();
        }

        $hasContinentIcon = false;
        if ($medalCategories->where('achieved_medal', true)->count() >= 10) {
            $hasContinentIcon = true;
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'has_continent_icon' => $hasContinentIcon,
            'medals' => Medal::collection($medalCategories)
        ];
    }
}
