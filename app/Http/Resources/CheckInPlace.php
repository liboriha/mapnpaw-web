<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

/**
 * Expected resource is UserCheck model.
 */
class CheckInPlace extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'timestamp' => $this->created_at->timestamp,
            'point' => Place::make($this->place)
        ];
    }
}
