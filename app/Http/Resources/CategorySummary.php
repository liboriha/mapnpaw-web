<?php

namespace App\Http\Resources;

use App\Http\Helpers\FileHelper;
use Illuminate\Http\Resources\Json\Resource;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Translator;

/**
 * Expected resource is Category model.
 */
class CategorySummary extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        if (!($this->resource instanceof Translator) && Voyager::translatable($this->resource)) {
            // If resource is not translated and can be translated, than translate it.
            $this->resource = $this->resource->translate();
        }
        
        if ($this->achieved_medal == true) {
            if ($this->medal_image_unlocked == null) {
                $medalIcon = url('/images/icons/gold-medal.png');
            } else {
                $medalIcon = FileHelper::getURL($this->medal_image_unlocked);
            }
        } else {
            if ($this->medal_image_locked == null) {
                $medalIcon = url('/images/icons/silver-medal.png');
            } else {
                $medalIcon = FileHelper::getURL($this->medal_image_locked);
            }
        }

        if ($this->achieved_flag == true) {
            if ($this->flag_image_unlocked == null) {
                $flagIcon = url('/images/icons/flag-gold.png');
            } else {
                $flagIcon = FileHelper::getURL($this->flag_image_unlocked);
            }
        } else {
            if ($this->flag_image_locked == null) {
                $flagIcon = url('/images/icons/flag-grey.png');
            } else {
                $flagIcon = FileHelper::getURL($this->flag_image_locked);
            }
        }

        return [
            'id' => $this->id,
            'label' => $this->name,
            'iconFlag' => $flagIcon,
            'iconMedal' => $medalIcon,
        ];
    }
}
