<?php

namespace App\Http\Resources;

use App\Http\Helpers\FileHelper;
use Illuminate\Http\Resources\Json\Resource;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Translator;

/**
 * Expected resource is Category model with processed processUserInfo method (need for achieved_medal attribute).
 */
class Medal extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $medal_image = null;
        if ($this->achieved_medal) {
            if ($this->medal_image_unlocked == null) {
                $medal_image = url('/images/icons/gold-medal.png');
            } else {
                $medal_image = FileHelper::getURL($this->medal_image_unlocked);
            }
        } else {
            if ($this->medal_image_locked == null) {
                $medal_image = url('/images/icons/silver-medal.png');
            } else {
                $medal_image = FileHelper::getURL($this->medal_image_locked);
            }
        }

        if (!($this->resource instanceof Translator) && Voyager::translatable($this->resource)) {
            // If resource is not translated and can be translated, than translate it.
            $this->resource = $this->resource->translate();
        }

        return [
            // ID of medal is ID of category because medal belongs to category.
            'id' => $this->id,
            'image' => $medal_image,
            'name' => $this->medal_name,
            'achieved' => $this->achieved_medal,
            'continent' => Continent::make($this->continent),
            'category' => Category::make($this->resource),
        ];
    }
}
