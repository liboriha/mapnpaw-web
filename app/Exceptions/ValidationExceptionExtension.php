<?php

namespace App\Exceptions;

class ValidationExceptionExtension
{
    private static $additionalMessages = [];

    /**
     * Returns additional messages to ValidationException.
     * @return array Additional messages
     */
    public static function getAdditionalMessages()
    {
        return self::$additionalMessages;
    }

    /**
     * Sets additional messages array.
     * @param array $additionalMessages Additional messages
     */
    public static function setAdditionalMessages(array $additionalMessages)
    {
        self::$additionalMessages = $additionalMessages;
    }

    /**
     * Adds additional messages
     * @param array $additionalMessages Array with additional messages to add.
     */
    public static function addAdditionalMessages(array $additionalMessages)
    {
        self::$additionalMessages = array_merge(self::$additionalMessages, $additionalMessages);
    }

    /**
     * Adds additional messages about page processing (eg. processing forms).
     * @param string $popup   [description]
     */
    public static function setPageProcessing($popup = null)
    {
        if ($popup != null) {
            self::addAdditionalMessages(['show-js-popup' => $popup]);
        }
    }
}
