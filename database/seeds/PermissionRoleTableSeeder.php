<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Models\Role;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        // Superadmin role

        $role = Role::where('name', 'superadmin')->firstOrFail();

        $permissions = Permission::all();

        $role->permissions()->sync(
            $permissions->pluck('id')->all()
        );

        // Admin role

        $role = Role::where('name', 'admin')->firstOrFail();

        $admin_added_permissions_groups = [
            'users',
            'categories',
            'continents',
            'places'
        ];

        $admin_added_permissions = [
            'browse_admin',
        ];

        $admin_exclude_permissions = [
            'delete_categories',
            'add_continents',
            'delete_continents',
        ];

        $permissions = Permission::where(function ($query) use ($admin_added_permissions, $admin_added_permissions_groups) {
            $query->whereIn('table_name', $admin_added_permissions_groups)
                ->orWhereIn('key', $admin_added_permissions);
        })->whereNotIn('key', $admin_exclude_permissions)->get();

        $role->permissions()->sync(
            $permissions->pluck('id')->all()
        );
    }
}
