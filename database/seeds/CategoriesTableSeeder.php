<?php

use Illuminate\Database\Seeder;
use App\Continent;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $continents = Continent::all();

        foreach ($continents as $continent) {
            $category = Category::firstOrNew(['name' => 'Unesco', 'continent_id' => $continent->id]);
            if (!$category->exists) {
                $category->fill([
                    'name' => 'Unesco',
                    'continent_id' => $continent->id
                ])->save();

                $category = $category->translate('en');
                $category->name = 'Unesco';
                $category->save();
            }

            $category = Category::firstOrNew(['name' => 'Příroda', 'continent_id' => $continent->id]);
            if (!$category->exists) {
                $category->fill([
                    'name' => 'Příroda',
                    'continent_id' => $continent->id
                ])->save();

                $category = $category->translate('en');
                $category->name = 'Nature';
                $category->save();
            }

            $category = Category::firstOrNew(['name' => 'Město', 'continent_id' => $continent->id]);
            if (!$category->exists) {
                $category->fill([
                    'name' => 'Město',
                    'continent_id' => $continent->id
                ])->save();

                $category = $category->translate('en');
                $category->name = 'Town';
                $category->save();
            }

            $category = Category::firstOrNew(['name' => 'Historické', 'continent_id' => $continent->id]);
            if (!$category->exists) {
                $category->fill([
                    'name' => 'Historické',
                    'continent_id' => $continent->id
                ])->save();

                $category = $category->translate('en');
                $category->name = 'Historic';
                $category->save();
            }

            $category = Category::firstOrNew(['name' => 'Muzeum', 'continent_id' => $continent->id]);
            if (!$category->exists) {
                $category->fill([
                    'name' => 'Muzeum',
                    'continent_id' => $continent->id
                ])->save();

                $category = $category->translate('en');
                $category->name = 'Museum';
                $category->save();
            }

            $category = Category::firstOrNew(['name' => 'Továrna', 'continent_id' => $continent->id]);
            if (!$category->exists) {
                $category->fill([
                    'name' => 'Továrna',
                    'continent_id' => $continent->id
                ])->save();

                $category = $category->translate('en');
                $category->name = 'Factory';
                $category->save();
            }

            $category = Category::firstOrNew(['name' => 'Výhled', 'continent_id' => $continent->id]);
            if (!$category->exists) {
                $category->fill([
                    'name' => 'Výhled',
                    'continent_id' => $continent->id
                ])->save();

                $category = $category->translate('en');
                $category->name = 'View';
                $category->save();
            }

            $category = Category::firstOrNew(['name' => 'Sport', 'continent_id' => $continent->id]);
            if (!$category->exists) {
                $category->fill([
                    'name' => 'Sport',
                    'continent_id' => $continent->id
                ])->save();

                $category = $category->translate('en');
                $category->name = 'Sport';
                $category->save();
            }

            $category = Category::firstOrNew(['name' => 'Kultura', 'continent_id' => $continent->id]);
            if (!$category->exists) {
                $category->fill([
                    'name' => 'Kultura',
                    'continent_id' => $continent->id
                ])->save();

                $category = $category->translate('en');
                $category->name = 'Culture';
                $category->save();
            }

            $category = Category::firstOrNew(['name' => 'Park', 'continent_id' => $continent->id]);
            if (!$category->exists) {
                $category->fill([
                    'name' => 'Park',
                    'continent_id' => $continent->id
                ])->save();

                $category = $category->translate('en');
                $category->name = 'Park';
                $category->save();
            }
        }
    }
}
