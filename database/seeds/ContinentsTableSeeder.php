<?php

use Illuminate\Database\Seeder;
use App\Continent;

class ContinentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $continent = Continent::firstOrNew(['name' => 'Asie']);
        if (!$continent->exists) {
            $continent->fill([
                'name' => 'Asie',
            ])->save();

            $continent = $continent->translate('en');
            $continent->name = 'Asia';
            $continent->save();
        }

        $continent = Continent::firstOrNew(['name' => 'Afrika']);
        if (!$continent->exists) {
            $continent->fill([
                'name' => 'Afrika',
            ])->save();

            $continent = $continent->translate('en');
            $continent->name = 'Africa';
            $continent->save();
        }

        $continent = Continent::firstOrNew(['name' => 'Amerika']);
        if (!$continent->exists) {
            $continent->fill([
                'name' => 'Amerika',
            ])->save();

            $continent = $continent->translate('en');
            $continent->name = 'America';
            $continent->save();
        }

        $continent = Continent::firstOrNew(['name' => 'Evropa']);
        if (!$continent->exists) {
            $continent->fill([
                'name' => 'Evropa',
            ])->save();

            $continent = $continent->translate('en');
            $continent->name = 'Europe';
            $continent->save();
        }

        $continent = Continent::firstOrNew(['name' => 'Austrálie']);
        if (!$continent->exists) {
            $continent->fill([
                'name' => 'Austrálie',
            ])->save();

            $continent = $continent->translate('en');
            $continent->name = 'Australia';
            $continent->save();
        }
    }
}
