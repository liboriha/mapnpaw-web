<?php

use Illuminate\Database\Seeder;
use VoyagerBread\Traits\BreadSeeder;

class CategoriesBreadSeeder extends Seeder
{
    use BreadSeeder;

    public function bread()
    {
        return [
            // usually the name of the table
            'name'                  => 'categories',
            'display_name_singular' => 'Kategorie',
            'display_name_plural'   => 'Kategorie',
            'icon'                  => 'voyager-categories',
            'model_name'            => 'App\Category',
            'controller'            => '',
            'generate_permissions'  => 1,
            'description'           => '',
            'server_side'           => 1,
        ];
    }

    public function inputFields()
    {
        return [
            'id' => [
                'type'         => 'number',
                'display_name' => 'ID',
                'required'     => 1,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'details'      => null,
                'order'        => 1,
            ],
            'continent_id' => [
                'type'         => 'number',
                'display_name' => 'ID kontinentu',
                'required'     => 1,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'details'      => null,
                'order'        => 2,
            ],
            'name' => [
                'type'         => 'text',
                'display_name' => 'Název',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 0,
                'details'      => null,
                'order'        => 3,
            ],
            'medal_name' => [
                'type'         => 'text',
                'display_name' => 'Název medaile',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 0,
                'details'      => null,
                'order'        => 4,
            ],
            'medal_image_locked' => [
                'type'         => 'image',
                'display_name' => 'Obrázek zamčené medaile',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 0,
                'details'      => null,
                'order'        => 5,
            ],
            'medal_image_unlocked' => [
                'type'         => 'image',
                'display_name' => 'Obrázek odemčené medaile',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 0,
                'details'      => null,
                'order'        => 6,
            ],
            'flag_name' => [
                'type'         => 'text',
                'display_name' => 'Název vlajky',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 0,
                'details'      => null,
                'order'        => 7,
            ],
            'flag_image_locked' => [
                'type'         => 'image',
                'display_name' => 'Obrázek zamčené vlajky',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 0,
                'details'      => null,
                'order'        => 8,
            ],
            'flag_image_unlocked' => [
                'type'         => 'image',
                'display_name' => 'Obrázek odemčené vlajky',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 0,
                'details'      => null,
                'order'        => 9,
            ],
            'radius' => [
                'type'         => 'number',
                'display_name' => 'Rádius (metry)',
                'required'     => 0,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 0,
                'details'      => '{"description":"Při nezadání hodnoty bude použita výchozí hodnota rádiusu."}',
                'order'        => 10,
            ],
            'category_hasone_category_relationship' => [
                'type'         => 'relationship',
                'display_name' => 'Rádius (metry)',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'details'      => '{"model":"App\\\\Category","table":"categories","type":"hasOne","column":"id","key":"id","label":"real_radius","pivot_table":"categories","pivot":"0"}',
                'order'        => 11,
            ],
            'created_at' => [
                'type'         => 'timestamp',
                'display_name' => 'Vytvořeno',
                'required'     => 0,
                'browse'       => 0,
                'read'         => 1,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'details'      => '{"format":"d.m.Y H:i:s"}',
                'order'        => 12,
            ],
            'updated_at' => [
                'type'         => 'timestamp',
                'display_name' => 'Aktualizováno',
                'required'     => 0,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'details'      => '{"format":"d.m.Y H:i:s"}',
                'order'        => 13,
            ],
            'category_belongsto_continent_relationship' => [
                'type'         => 'relationship',
                'display_name' => 'Kontinent',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 0,
                'add'          => 1,
                'delete'       => 0,
                'details'      => '{"model":"App\\\\Continent","table":"continents","type":"belongsTo","column":"continent_id","key":"id","label":"name","pivot_table":"categories","pivot":"0"}',
                'order'        => 14,
            ]
        ];
    }

    public function menuEntry()
    {
        return [
            'role'      => 'admin',
            'title'      => 'Kategorie',
            'url'        => '/admin/categories',
            'route'      => null,
            'target'     => '_self',
            'icon_class' => 'voyager-categories',
            'color'      => '#000000',
            'parent_id'  => null,
            'order'      => 3,
        ];
    }
}
