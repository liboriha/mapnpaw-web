<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Intervention\Image\Facades\Image;
use App\User;
use App\Place;
use App\UserMedal;
use App\UserFlag;
use App\UserCheck;
use App\UserPhoto;
use App\UserRating;
use App\UserPoint;
use App\Category;

class TestDataSeeder extends Seeder
{
    /**
     * Adds test user:
     * Email: user@test.cz
     * Heslo: 123456
     * 
     * Added medals, flags, icons of medals and flags to categories.
     * 
     * Added testing images for medals, flags, place detail and user photos.
     * 
     * Added several places.
     * 
     * Added checkins on different places. (16x)
     * 
     * User has flags of categories: Asie - Unesco, Asie - Město(City)
     * 
     * User has medals of categories: Asie - Unesco
     * 
     * User has added photo to places: Place - category 1 top 1, Place - category 3 flag
     * 
     * User has added rating to places: Place - category 1 top 1, Place - category 1 top 2
     * 
     * User has 16 points for checkins and 2 points for first photo.
     *
     * @return void
     */
    public function run()
    {
        $medal_image_locked_file = base_path('tests/images/medal_locked.png');

        $medal_image_unlocked_file = base_path('tests/images/medal_unlocked.png');

        $flag_image_locked_file = base_path('tests/images/flag_locked.png');

        $flag_image_unlocked_file = base_path('tests/images/flag_unlocked.png');

        $place_detail_image_file = base_path('tests/images/test_detail.png');

        $user_photo_image_file = base_path('tests/images/test_photo.png');

        // CREATE TEST USER
        $user = User::create([
            'email' => 'user@test.cz',
            'password' => bcrypt('123456'),
            'firstname' => 'Pavel',
            'lastname' => 'Nový',
            'nickname' => 'PN',
            'avatar' => 'users/default.png'
        ]);

        // UPDATE CATEGORY NAMES WITH IMAGES WITH TEST DATA
        foreach (Category::all() as $category) {
            $category->medal_name = "Medal " . $category->continent_id . "-" . $category->id;
            $category->medal_image_locked = "test/medal_" . $category->continent_id . "_" . $category->id . "_locked.png";
            $category->medal_image_unlocked = "test/medal_" . $category->continent_id . "_" . $category->id . "_unlocked.png";

            $this->createImageWithText($category->medal_image_locked, $medal_image_locked_file, $category->id, 20, 26);
            $this->createImageWithText($category->medal_image_unlocked, $medal_image_unlocked_file, $category->id, 20, 26);

            $category->flag_name = "Flag " . $category->continent_id . "-" . $category->id;
            $category->flag_image_locked = "test/flag_" . $category->continent_id . "_" . $category->id . "_locked.png";
            $category->flag_image_unlocked = "test/flag_" . $category->continent_id . "_" . $category->id . "_unlocked.png";

            $this->createImageWithText($category->flag_image_locked, $flag_image_locked_file, $category->id, 20, 15);
            $this->createImageWithText($category->flag_image_unlocked, $flag_image_unlocked_file, $category->id, 20, 15);

            $category->save();

            $additional_locales = array_diff(config('voyager.multilingual.locales'), [config('voyager.multilingual.default')]);
            foreach ($additional_locales as $locale) {
                $category = $category->translate($locale);
                $category->medal_name = $category->medal_name . " " . $locale;
                $category->flag_name = $category->flag_name . " " . $locale;
                $category->save();
            }
        }

        // CREATE TEST PLACES
        $testData = [];
        $places = [];

        // Create detail place image
        $detailImagePath = 'test/places/test.png';
        $this->createImageWithText($detailImagePath, $place_detail_image_file);
        // Create user photo place image
        $photoImagePath = 'test/places/photo.png';
        $this->createImageWithText($photoImagePath, $user_photo_image_file);

        // Top ten places of first category - all checked
        for ($i = 1; $i <= 10; $i++) { 
            $data = [
                'name' => 'Place - category 1 top ' . $i, 
                'gps' => [50.0835494, 14.4341414 + 0.001 * $i], 
                'category_id' => 1, 
                'is_top' => true, 
                'is_flag' => $i == 10 ? true : false,  // Last top place is flag place 
                'description' => 'Description 1 top ' . $i, 
                'address' => 'Prague', 
                'image' => $detailImagePath,
                'checks' => true
            ];

            // First place has user photo and user rating
            if ($i == 1) {
                $data['userPictures'][] = [
                    'image' => $photoImagePath
                ];
                $data['rating'][] = [
                    'stars' => 2,
                    'rating' => null
                ];
            }

            // Second place has user rating
            if ($i == 2) {
                $data['rating'][] = [
                    'stars' => 4.5,
                    'rating' => 'Awesome'
                ];
            }

            $place = $this->createTestPlaceWithRelativeData($user->id, $data);

            $places[] = $place;

            $data['place'] = $place;
            $testData[] = $data;
        }

        // Top ten places of first category - half checked
        for ($i = 1; $i <= 10; $i++) { 
            $data = [
                'name' => 'Place - category 2 top ' . $i, 
                'gps' => [49.2002211, 16.6078411 + 0.001 * $i], 
                'category_id' => 2, 
                'is_top' => true, 
                'is_flag' => $i == 10 ? true : false,  // Last top place is flag place 
                'description' => 'Description 2 top ' . $i, 
                'address' => 'Brno', 
                'image' => $detailImagePath,
                'checks' => ($i <= 5) ? true : false
            ];

            $place = $this->createTestPlaceWithRelativeData($user->id, $data);

            $places[] = $place;

            $data['place'] = $place;
            $testData[] = $data;
        }

        // Create checked flag place - has user image
        $data = [
            'name' => 'Place - category 3 flag', 
            'gps' => [49.5938686, 17.2508706], 
            'category_id' => 3, 
            'is_top' => false, 
            'is_flag' => true,
            'description' => 'Description 3 flag', 
            'address' => 'Olomouc', 
            'image' => null,
            'checks' => true,
            'userPictures' => [
                [
                    'image' => $photoImagePath
                ]
            ]
        ];

        $place = $this->createTestPlaceWithRelativeData($user->id, $data);

        $places[] = $place;

        $data['place'] = $place;
        $testData[] = $data;

        // Create flag place
        $data = [
            'name' => 'Place - category 4 flag', 
            'gps' => [49.8346453, 18.2820442], 
            'category_id' => 4, 
            'is_top' => false, 
            'is_flag' => true,
            'description' => 'Description 4 flag', 
            'address' => 'Ostrava', 
            'image' => null,
            'checks' => false
        ];

        $place = $this->createTestPlaceWithRelativeData($user->id, $data);

        $places[] = $place;

        $data['place'] = $place;
        $testData[] = $data;

        // Create place without flag and top
        $data = [
            'name' => 'Place - category 5 flag', 
            'gps' => [50.0385383, 15.7802056], 
            'category_id' => 5, 
            'is_top' => false, 
            'is_flag' => false,
            'description' => 'Description 5 flag', 
            'address' => 'Pardubice', 
            'image' => null,
            'checks' => false
        ];

        $place = $this->createTestPlaceWithRelativeData($user->id, $data);

        $places[] = $place;

        $data['place'] = $place;
        $testData[] = $data;





        // CREATE USER POINTS
        $points = [];

        // Create checkin points
        foreach ($testData as $testDataPlace) {
            if (!empty($testDataPlace['checks']) && $testDataPlace['checks'] == true) {
                $points[] = UserPoint::create([
                    'user_id' => $user->id, 
                    'place_id' => $testDataPlace['place']->id, 
                    'type' => UserPoint::TYPE_CHECKIN, 
                    'points' => config('awapp.places.points.first-checkin')
                ]);
            }
        }

        // Create first photo points
        foreach ($testData as $testDataPlace) {
            if (!empty($testDataPlace['userPictures'])) {
                $points[] = UserPoint::create([
                    'user_id' => $user->id, 
                    'place_id' => $testDataPlace['place']->id, 
                    'type' => UserPoint::TYPE_FIRST_PHOTO, 
                    'points' => config('awapp.places.points.first-photo')
                ]);
            }
        }

        // CREATE USER FLAGS
        $flags = [];

        $placesWithFlags = Place::where('is_flag', true)
            ->whereHas('userChecks', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })
            ->get();

        foreach ($placesWithFlags as $placeWithFlag) {
            $flags[] = UserFlag::create([
                'user_id' => $user->id, 
                'continent_id' => $placeWithFlag->continent_id, 
                'category_id' => $placeWithFlag->category_id, 
                'place_id' => $placeWithFlag->id
            ]);
        }

        // CREATE USER MEDAL
        $medals = [];

        foreach (Category::all() as $category) {
            $countTopPlaces = $category->places()->where('is_top', true)->count();
            if ($countTopPlaces != 0) {
                $countHasTopPlaces = $category->places()
                    ->where('is_top', true)
                    ->whereHas('userChecks', function ($query) use ($user) {
                        $query->where('user_id', $user->id);
                    })
                    ->count();

                if ($countHasTopPlaces == $countTopPlaces) {
                    $medal = UserMedal::create([
                        'user_id' => $user->id, 
                        'continent_id' => $category->continent_id, 
                        'category_id' => $category->id
                    ]);

                    $topPlacesIDs = $category->places()->where('is_top', true)->get()->pluck('id');

                    $medal->places()->sync($topPlacesIDs);

                    $medals[] = $medal;
                }
            }
        }
    }

    /**
     * Helper method for create test place with additional relative data in database with data array.
     */
    protected function createTestPlaceWithRelativeData($user_id, $data = []) 
    {
        $place = Place::create([
            'name' => $data['name'], 
            'gps' => new Point($data['gps'][0], $data['gps'][1]), 
            'category_id' => $data['category_id'], 
            'is_top' => $data['is_top'], 
            'is_flag' => $data['is_flag'], 
            'description' => $data['description'], 
            'address' => $data['address'], 
            'image' => $data['image'],
        ]); 

        if (!empty($data['userPictures'])) {
            foreach ($data['userPictures'] as $value) {
                $photo = UserPhoto::create([
                    'place_id' => $place->id,
                    'user_id' => $user_id,
                    'image' => $value['image'],
                ]);
            }
        }

        if (!empty($data['rating'])) {
            foreach ($data['rating'] as $value) {
                $rating = UserRating::create([
                    'place_id' => $place->id,
                    'user_id' => $user_id,
                    'stars' => $value['stars'],
                    'rating' => $value['rating'],
                ]);
            }
        }

        if (!empty($data['checks']) && $data['checks'] == true) {
            $check = UserCheck::create([
                'place_id' => $place->id,
                'user_id' => $user_id,
            ]);
        }

        return $place;
    }

    protected function createImageWithText($path, $filepath, $text = null, $posX = 0, $posY = 0) 
    {
        $image = Image::make($filepath);

        if ($text != null) {
            $image->text($text, $posX, $posY, function($font) {
                $font->file(3);
                $font->color('#000000');
                $font->align('center');
                $font->valign('middle');
            });
        }

        $extension = last(explode('.', $filepath));

        // Saving file to storage
        $image->encode($extension, null);

        Storage::disk(config('voyager.storage.disk'))->put($path, $image, 'public');
    }
}
