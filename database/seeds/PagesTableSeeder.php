<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Page;

class PagesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $page = Page::firstOrNew([
            'slug' => 'hello-world',
        ]);
        if (!$page->exists) {
            $page->fill([
                'author_id' => 0,
                'title'     => 'Hello World',
                'excerpt'   => 'Morbi nec nulla at justo volutpat tincidunt. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas sed vestibulum elit. ',
                'body'      => '<p>Mauris sit amet eros varius, finibus nibh a, auctor risus. Etiam pharetra sit amet metus eget sagittis. Curabitur egestas ultricies porttitor. Proin id sapien vel dui iaculis gravida. Fusce eleifend velit at orci rutrum, non aliquam sapien rutrum. Nunc iaculis justo lobortis, eleifend nulla vel, malesuada eros. Nunc ac iaculis velit, quis iaculis ex. Aliquam erat volutpat. Nulla volutpat dolor vel dolor dignissim accumsan. Sed eu rhoncus diam, nec faucibus libero. Nullam pulvinar felis et leo eleifend, at bibendum turpis pellentesque. Suspendisse a tincidunt neque. Suspendisse non suscipit nunc. Nunc porta leo sed tortor malesuada, euismod posuere libero tincidunt. Aenean consequat mi leo, vel semper enim ullamcorper sagittis.</p>',
                'image'            => 'pages/page1.jpg',
                'meta_description' => 'Meta Description',
                'meta_keywords'    => 'Keyword1, Keyword2',
                'status'           => 'ACTIVE',
            ])->save();
        }
    }
}
