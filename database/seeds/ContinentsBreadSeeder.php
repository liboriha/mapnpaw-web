<?php

use Illuminate\Database\Seeder;
use VoyagerBread\Traits\BreadSeeder;

class ContinentsBreadSeeder extends Seeder
{
    use BreadSeeder;

    public function bread()
    {
        return [
            // usually the name of the table
            'name'                  => 'continents',
            'display_name_singular' => 'Kontinent',
            'display_name_plural'   => 'Kontinenty',
            'icon'                  => 'voyager-world',
            'model_name'            => 'App\Continent',
            'controller'            => null,
            'generate_permissions'  => 1,
            'description'           => null,
        ];
    }

    public function inputFields()
    {
        return [
            'id' => [
                'type'         => 'number',
                'display_name' => 'ID',
                'required'     => 1,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'details'      => null,
                'order'        => 1,
            ],
            'name' => [
                'type'         => 'text',
                'display_name' => 'Název',
                'required'     => 1,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 1,
                'delete'       => 0,
                'details'      => null,
                'order'        => 2,
            ],
            // Removed traveler of the continent
            /*
            'medal_name' => [
                'type'         => 'text',
                'display_name' => 'Název pro cestovatele kontinentu', // Také jako Název kontinentální medaile.
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 0,
                'delete'       => 0,
                'details'      => null,
                'order'        => 3,
            ],
            'medal_image_locked' => [
                'type'         => 'image',
                'display_name' => 'Obrázek zamčeného cestovatele kontinentu',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 0,
                'delete'       => 0,
                'details'      => null,
                'order'        => 4,
            ],
            'medal_image_unlocked' => [
                'type'         => 'image',
                'display_name' => 'Obrázek odemčeného cestovatele kontinentu',
                'required'     => 0,
                'browse'       => 1,
                'read'         => 1,
                'edit'         => 1,
                'add'          => 0,
                'delete'       => 0,
                'details'      => null,
                'order'        => 5,
            ],*/
            'created_at' => [
                'type'         => 'timestamp',
                'display_name' => 'Vytvořeno',
                'required'     => 0,
                'browse'       => 0,
                'read'         => 1,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'details'      => '{"format":"d.m.Y H:i:s"}',
                'order'        => 6,
            ],
            'updated_at' => [
                'type'         => 'timestamp',
                'display_name' => 'Aktualizováno',
                'required'     => 0,
                'browse'       => 0,
                'read'         => 0,
                'edit'         => 0,
                'add'          => 0,
                'delete'       => 0,
                'details'      => '{"format":"d.m.Y H:i:s"}',
                'order'        => 7,
            ]
        ];
    }

    public function menuEntry()
    {
        return [
            'role'      => 'admin',
            'title'      => 'Kontinenty',
            'url'        => '/admin/continents',
            'route'      => null,
            'target'     => '_self',
            'icon_class' => 'voyager-world',
            'color'      => '#000000',
            'parent_id'  => null,
            'order'      => 2,
        ];
    }
}
