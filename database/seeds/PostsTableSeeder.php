<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Post;

class PostsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $post = $this->findPost('lorem-ipsum-post');
        if (!$post->exists) {
            $post->fill([
                'title'            => 'Lorem Ipsum Post',
                'author_id'        => 0,
                'seo_title'        => null,
                'excerpt'          => 'This is the excerpt for the Lorem Ipsum Post',
                'body'             => '<p>This is the body of the lorem ipsum post</p>',
                'image'            => 'posts/post1.jpg',
                'slug'             => 'lorem-ipsum-post',
                'meta_description' => 'This is the meta description',
                'meta_keywords'    => 'keyword1, keyword2, keyword3',
                'status'           => 'PUBLISHED',
                'featured'         => 0,
            ])->save();
        }

        $post = $this->findPost('my-sample-post');
        if (!$post->exists) {
            $post->fill([
                'title'     => 'My Sample Post',
                'author_id' => 0,
                'seo_title' => null,
                'excerpt'   => 'This is the excerpt for the sample Post',
                'body'      => '<p>This is the body for the sample post, which includes the body.</p>
                <h2>We can use all kinds of format!</h2>
                <p>And include a bunch of other stuff.</p>',
                'image'            => 'posts/post2.jpg',
                'slug'             => 'my-sample-post',
                'meta_description' => 'Meta Description for sample post',
                'meta_keywords'    => 'keyword1, keyword2, keyword3',
                'status'           => 'PUBLISHED',
                'featured'         => 0,
            ])->save();
        }

        $post = $this->findPost('latest-post');
        if (!$post->exists) {
            $post->fill([
                'title'            => 'Latest Post',
                'author_id'        => 0,
                'seo_title'        => null,
                'excerpt'          => 'This is the excerpt for the latest post',
                'body'             => '<p>This is the body for the latest post</p>',
                'image'            => 'posts/post3.jpg',
                'slug'             => 'latest-post',
                'meta_description' => 'This is the meta description',
                'meta_keywords'    => 'keyword1, keyword2, keyword3',
                'status'           => 'PUBLISHED',
                'featured'         => 0,
            ])->save();
        }

        $post = $this->findPost('yarr-post');
        if (!$post->exists) {
            $post->fill([
                'title'     => 'Example Post',
                'author_id' => 0,
                'seo_title' => null,
                'excerpt'   => 'Fusce ac ornare purus. Nam at nibh erat. Donec non odio molestie felis suscipit mollis sed quis eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dictum, mi eget commodo lacinia.',
                'body'      => '<p>Aenean sed interdum velit. Mauris suscipit mauris sit amet enim pharetra, nec luctus ipsum pulvinar. Curabitur quis elit at sem mollis accumsan. Aenean in metus eget urna blandit ultricies. Nulla facilisi. Integer viverra, tellus sit amet fringilla congue, metus augue commodo mi, at commodo lacus nisi at diam. Nullam sem leo, facilisis et erat id, dictum dignissim urna. Donec quis sapien rutrum, pharetra risus in, vulputate libero. Mauris a justo efficitur, gravida justo at, feugiat libero. Quisque vehicula ante quis accumsan auctor. Sed fringilla nunc eget egestas ultrices. Nam cursus vestibulum quam non feugiat. Proin congue ex dictum ligula semper porta. Donec ac libero ornare nisl dapibus tristique.</p>',
                'image'            => 'posts/post4.jpg',
                'slug'             => 'example-post',
                'meta_description' => 'this be a meta descript',
                'meta_keywords'    => 'keyword1, keyword2, keyword3',
                'status'           => 'PUBLISHED',
                'featured'         => 0,
            ])->save();
        }
    }

    /**
     * [post description].
     *
     * @param [type] $slug [description]
     *
     * @return [type] [description]
     */
    protected function findPost($slug)
    {
        return Post::firstOrNew(['slug' => $slug]);
    }
}
