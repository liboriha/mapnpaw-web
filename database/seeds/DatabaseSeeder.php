<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(VoyagerDatabaseSeeder::class);
        $this->call(VoyagerDummyDatabaseSeeder::class);
        
        $this->call(ContinentsBreadSeeder::class);
        $this->call(CategoriesBreadSeeder::class);
        $this->call(PlacesBreadSeeder::class);

        $this->call(ContinentsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
    }
}
