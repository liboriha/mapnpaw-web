<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRadiusNullableInPlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Can't do change on 'places' table because Doctrine not support 'point' type!
        Schema::table('places', function (Blueprint $table) {
            $table->dropColumn('radius');
        });
        Schema::table('places', function (Blueprint $table) {
            $table->integer('radius')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('places', function (Blueprint $table) {
            $table->dropColumn('radius');
        });
        Schema::table('places', function (Blueprint $table) {
            $table->integer('radius')->default('100');;
        });
    }
}
