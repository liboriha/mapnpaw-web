<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUsersHardDeleteContraints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('user_checks', function (Blueprint $table) {
            $table->dropForeign('user_checks_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('user_photos', function (Blueprint $table) {
            $table->dropForeign('user_photos_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('user_ratings', function (Blueprint $table) {
            $table->dropForeign('user_ratings_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('user_points', function (Blueprint $table) {
            $table->dropForeign('user_points_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('user_medals', function (Blueprint $table) {
            $table->dropForeign('user_medals_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('user_flags', function (Blueprint $table) {
            $table->dropForeign('user_flags_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
        Schema::table('user_medals_places', function (Blueprint $table) {
            $table->dropForeign('user_medals_places_user_medal_id_foreign');
            $table->foreign('user_medal_id')->references('id')->on('user_medals')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('user_checks', function (Blueprint $table) {
            $table->dropForeign('user_checks_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users');
        });
        Schema::table('user_photos', function (Blueprint $table) {
            $table->dropForeign('user_photos_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users');
        });
        Schema::table('user_ratings', function (Blueprint $table) {
            $table->dropForeign('user_ratings_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users');
        });
        Schema::table('user_points', function (Blueprint $table) {
            $table->dropForeign('user_points_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users');
        });
        Schema::table('user_medals', function (Blueprint $table) {
            $table->dropForeign('user_medals_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users');
        });
        Schema::table('user_flags', function (Blueprint $table) {
            $table->dropForeign('user_flags_user_id_foreign');
            $table->foreign('user_id')->references('id')->on('users');
        });
        Schema::table('user_medals_places', function (Blueprint $table) {
            $table->dropForeign('user_medals_places_user_medal_id_foreign');
            $table->foreign('user_medal_id')->references('id')->on('user_medals');
        });
    }
}
