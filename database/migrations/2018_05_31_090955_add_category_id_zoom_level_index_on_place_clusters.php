<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryIdZoomLevelIndexOnPlaceClusters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('place_clusters', function (Blueprint $table) {
            $table->index('zoom_level', 'category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('place_clusters', function (Blueprint $table) {
            $table->dropIndex('category_id');
        });
    }
}
