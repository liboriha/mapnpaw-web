<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAvgGpsSpatialIndexOnPlaceClustersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('place_clusters', function (Blueprint $table) {
            $table->spatialIndex('avg_gps');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('place_clusters', function (Blueprint $table) {
            $table->dropSpatialIndex('place_clusters_avg_gps_spatial');
        });
    }
}
