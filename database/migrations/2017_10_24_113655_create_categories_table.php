<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('continent_id')->unsigned()->index();
            $table->foreign('continent_id')->references('id')->on('continents');
            $table->string('name');
            $table->string('medal_image_locked')->nullable();
            $table->string('medal_image_unlocked')->nullable();
            $table->string('flag_image_locked')->nullable();
            $table->string('flag_image_unlocked')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
