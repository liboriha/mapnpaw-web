<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('continent_id')->unsigned()->index();
            $table->foreign('continent_id')->references('id')->on('continents');
            $table->integer('category_id')->unsigned()->index();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->string('name');
            $table->string('image')->nullable();
            $table->text('description');
            $table->string('address');
            $table->point('gps');
            // Maybe in future add spatial index to gps column (need MyISAM table)
            $table->integer('radius')->default('100');
            $table->boolean('is_top')->default(false);
            $table->boolean('is_flag')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places');
    }
}
