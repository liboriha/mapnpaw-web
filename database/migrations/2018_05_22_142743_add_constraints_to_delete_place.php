<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConstraintsToDeletePlace extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('places', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('user_checks', function (Blueprint $table) {
            $table->dropForeign('user_checks_place_id_foreign');
            $table->foreign('place_id')->references('id')->on('places')->onDelete('cascade');
        });
        Schema::table('user_flags', function (Blueprint $table) {
            $table->dropForeign('user_flags_place_id_foreign');
            $table->foreign('place_id')->references('id')->on('places')->onDelete('cascade');
        });
        Schema::table('user_medals_places', function (Blueprint $table) {
            $table->dropForeign('user_medals_places_place_id_foreign');
            $table->foreign('place_id')->references('id')->on('places')->onDelete('cascade');
        });
        Schema::table('user_photos', function (Blueprint $table) {
            $table->dropForeign('user_photos_place_id_foreign');
            $table->foreign('place_id')->references('id')->on('places')->onDelete('cascade');
        });
        Schema::table('user_points', function (Blueprint $table) {
            $table->dropForeign('user_points_place_id_foreign');
            $table->foreign('place_id')->references('id')->on('places')->onDelete('cascade');
        });
        Schema::table('user_ratings', function (Blueprint $table) {
            $table->dropForeign('user_ratings_place_id_foreign');
            $table->foreign('place_id')->references('id')->on('places')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('places', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('user_checks', function (Blueprint $table) {
            $table->dropForeign('user_checks_place_id_foreign');
            $table->foreign('place_id')->references('id')->on('places');
        });
        Schema::table('user_flags', function (Blueprint $table) {
            $table->dropForeign('user_flags_place_id_foreign');
            $table->foreign('place_id')->references('id')->on('places');
        });
        Schema::table('user_medals_places', function (Blueprint $table) {
            $table->dropForeign('user_medals_places_place_id_foreign');
            $table->foreign('place_id')->references('id')->on('places');
        });
        Schema::table('user_photos', function (Blueprint $table) {
            $table->dropForeign('user_photos_place_id_foreign');
            $table->foreign('place_id')->references('id')->on('places');
        });
        Schema::table('user_points', function (Blueprint $table) {
            $table->dropForeign('user_points_place_id_foreign');
            $table->foreign('place_id')->references('id')->on('places');
        });
        Schema::table('user_ratings', function (Blueprint $table) {
            $table->dropForeign('user_ratings_place_id_foreign');
            $table->foreign('place_id')->references('id')->on('places');
        });
    }
}
