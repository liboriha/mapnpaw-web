<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMedalsPlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_medals_places', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_medal_id')->unsigned();
            $table->foreign('user_medal_id')->references('id')->on('user_medals');
            $table->integer('place_id')->unsigned();
            $table->foreign('place_id')->references('id')->on('places');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_medals_places');
    }
}
