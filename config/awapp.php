<?php

return [

    /*
    |--------------------------------------------------------------------------
    | X-API-Key
    |--------------------------------------------------------------------------
    |
    | App key that must contains API requests in headers.
    |
    */

    'X-API-Key' => 'AWAPP',


    /*
    |--------------------------------------------------------------------------
    | X-API-Key
    |--------------------------------------------------------------------------
    |
    | App key that must contains API requests in headers.
    |
    */

    'places' => [
        'defaults' => [
            'radius' => 100, // Default radius for checkins (in meters)
            'wait-for-next' => 30, // Default time to wait to checkin on same place (in days)
        ],
        'medals' => [
            'TOP-get-medal-count' => 10 // Request number of top places to get medal
        ],
        'points' => [
            'first-checkin' => 1,
            'next-checkin' => 1,
            'first-photo' => 1,
            'next-photo' => 0, // Defaults without points, if need to add point, then edit database enum and saving user photos!
        ],
        'photos' => [
            // Limited count of user photo can be added to one place
            'user-limit-count' => 10, 
        ]
    ],

    'images' => [ 
        // Info to save profile image into storage
        'profile-image' => [
            'directory' => 'users',
            'prefix' => 'profile_'
        ],
        // Info to save user photo image into storage
        'user-photo' => [
            'directory' => 'userPhotos',
            'prefix' => ''
        ]
    ],

    'logs' => [
        'deduplication-path' => storage_path('logs/deduplication'),
    ],

    'map' => [
        'defaults' => [
            'radius' => 10000, // Default radius to get places without defined radius (in meters)
            'page-count' => 100,
        ],
        'clustering' => [
            'zoomTranslation' => [
                '0' => '5',
                '1' => '5',
                '2' => '5',
                '3' => '5',
                '4' => '5',
                '5' => '5',
                '6' => '4',
                '7' => '4',
                '8' => '4',
                '9' => '3',
                '10' => '3',
                '11' => '3',
                '12' => '2',
                '13' => '2',
                '14' => '2',
                '15' => '1',
                '16' => '1',
                '17' => '1',
                '18' => null,
                '19' => null,
                '20' => null,
                '21' => null,
            ],
            'build' => [
                '1' => [
                    'mapDivide' => [
                        // 180 / {lat} must be integer
                        'lat' => 30,
                        // 360 / {lng} must be integer
                        'lng' => 60,
                    ],
                    'query' => 'place',
                    'boxDivide' => [
                        'lat' => 12,
                        'lng' => 12,
                    ],
                    'clusterRadius' => 300,
                    'zoomLevel' => 1,
                ],
                '2' => [
                    'mapDivide' => [
                        // 180 / {lat} must be integer
                        'lat' => 30,
                        // 360 / {lng} must be integer
                        'lng' => 60,
                    ],
                    'query' => 'clusters',
                    'queryZoom' => 1,
                    'boxDivide' => [
                        'lat' => 12,
                        'lng' => 12,
                    ],
                    'clusterRadius' => 2500,
                    'zoomLevel' => 2,
                ],
                '3' => [
                    'mapDivide' => [
                        // 180 / {lat} must be integer
                        'lat' => 15,
                        // 360 / {lng} must be integer
                        'lng' => 30,
                    ],
                    'query' => 'clusters',
                    'queryZoom' => 2,
                    'boxDivide' => [
                        'lat' => 4,
                        'lng' => 4,
                    ],
                    'clusterRadius' => 20000,
                    'zoomLevel' => 3,
                ],
                '4' => [
                    'mapDivide' => [
                        // 180 / {lat} must be integer
                        'lat' => 3,
                        // 360 / {lng} must be integer
                        'lng' => 6,
                    ],
                    'query' => 'clusters',
                    'queryZoom' => 3,
                    'boxDivide' => [
                        'lat' => 4,
                        'lng' => 4,
                    ],
                    'clusterRadius' => 200000,
                    'zoomLevel' => 4,
                ],
                '5' => [
                    'mapDivide' => [
                        // 180 / {lat} must be integer
                        'lat' => 1,
                        // 360 / {lng} must be integer
                        'lng' => 1,
                    ],
                    'query' => 'clusters',
                    'queryZoom' => 4,
                    'boxDivide' => [
                        'lat' => 8,
                        'lng' => 16,
                    ],
                    'clusterRadius' => 2500000,
                    'zoomLevel' => 5,
                ],
            ],
        ],
    ]

];
