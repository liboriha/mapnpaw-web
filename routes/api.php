<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group(['prefix' => 'v1', 'namespace' => 'Api\V1'], function () {
    // /api/v1/...

    // Routes without JWT authentication

    // ACCOUNT
    Route::group(['prefix' => 'account'], function () {
        // /api/v1/account/...

        Route::post('/', 'AccountController@register');
        Route::post('/forgotten-password', 'AccountController@sendResetLinkEmail');
    });

    Route::group(['prefix' => 'token'], function () {
        // /api/v1/token/...

        Route::post('/', 'AccountController@login');
        Route::post('/facebook', 'AccountController@facebookLogin');
    });

    // MAP - without login
    Route::group(['prefix' => 'map'], function () {
        // /api/v1/map/...

        Route::get('/', 'MapController@getPlaces');
        Route::get('paged', 'MapController@getPlacesPaged');
    });

    Route::middleware('auth.jwt')->group(function () {
        // /api/v1/...

        // Routes with JWT authentication

        // ACCOUNT
        Route::group(['prefix' => 'profile'], function () {
            // /api/v1/profile/...

            Route::get('/', 'AccountController@getProfile');
            Route::post('/', 'AccountController@changeProfile');
            Route::post('password', 'AccountController@changePassword');
        });

        // MAP
        Route::group(['prefix' => 'map'], function () {
            // /api/v1/map/...

            Route::group(['prefix' => '{pointId}', 'where' => ['pointId' => '[0-9]+']], function () {
                // /api/v1/map/{pointId}/...

                Route::get('/', 'MapController@getPlaceDetail');
                Route::post('check-in', 'MapController@checkIn');
                Route::post('photo', 'MapController@addUserPhoto');
                Route::post('rate', 'MapController@addUserRating');
            });
        });

        // ACHIEVENEMTS
        Route::get('achievements', 'AchievementsController@getAchievements');
        Route::get('check-ins', 'AchievementsController@getCheckInPlaces');
    
        // MEDAL
        Route::get('medal/{id}', 'MedalController@getPlacesForMedal')->where(['id' => '[0-9]+']);
        Route::get('medals', 'MedalController@getMedals');

        // FLAG
        Route::get('flag/{id}', 'FlagController@getPlaceForFlag')->where(['id' => '[0-9]+']);
        Route::get('flags', 'FlagController@getFlags');

        // CATEGORY
        Route::get('categories', 'CategoryController@getCategories');
    });
});
