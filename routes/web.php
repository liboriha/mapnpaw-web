<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group([
    'namespace' => 'Browser',
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localizationRedirect']
], function () {
    Route::get('/', 'HomeController@index')->name('home');

    // Named post-login because of default redirect of unauthenticated user to login route,
    // to better find bug about it.
    Route::post(LaravelLocalization::transRoute('routes.post-login'), 'LoginController@postLogin')->name('post-login');

    Route::post(LaravelLocalization::transRoute('routes.register'), 'RegisterController@register')->name('register');

    // Reset password paths
    Route::group(['prefix' => LaravelLocalization::transRoute('routes.password-reset')], function () {
        Route::post('/', 'ResetPasswordController@sendResetLinkEmail')->name('send-reset-link');

        Route::group(['prefix' => '{token}', 'where' => ['token' => '[a-z0-9]+']], function () {
            Route::get('/', 'ResetPasswordController@showResetForm')->name('password.reset');
            Route::post('/', 'ResetPasswordController@reset')->name('password.reset-process');
        });
    });

    Route::group(['prefix' => 'facebook'], function () {
        Route::get('/redirect', 'LoginController@facebookRedirect')->name('facebook-redirect');
        Route::get('/callback', 'LoginController@facebookCallback')->name('facebook-callback');
    });

    // Public/Private (not logged/logged user) map
    Route::group(['prefix' => LaravelLocalization::transRoute('routes.map.prefix')], function () {
        Route::get('/', 'MapController@index')->name('map');
    });

    Route::group([
        'prefix' => LaravelLocalization::transRoute('routes.place.prefix')
    ], function () {
        Route::group([
            'prefix' => LaravelLocalization::transRoute('routes.place.id.prefix'),
            'where' => ['placeId' => '[0-9]+']
        ], function () {
            Route::get(LaravelLocalization::transRoute('routes.place.id.detail'), 'PlaceController@detail')
                ->name('place.detail');
            Route::get(LaravelLocalization::transRoute('routes.place.id.rating'), 'PlaceController@showRatings')
                ->name('place.rating');
        });
    });

    Route::group(['middleware' => 'auth'], function () {
        Route::get(LaravelLocalization::transRoute('routes.logout'), 'LoginController@logout')->name('logout');

        Route::group(['prefix' => LaravelLocalization::transRoute('routes.profile.prefix')], function () {
            Route::get('/', 'ProfileController@index')->name('profile');
            Route::get(LaravelLocalization::transRoute('routes.profile.edit'), 'ProfileController@editProfile')
                ->name('profile.edit');
            Route::post(LaravelLocalization::transRoute('routes.profile.edit'), 'ProfileController@updateProfile')
                ->name('profile.update');
        });

        Route::group(['prefix' => LaravelLocalization::transRoute('routes.map.prefix')], function () {
            Route::get(LaravelLocalization::transRoute('routes.map.visited-places'), 'MapController@showVisitedPlaces')
                ->name('map.visited-places');
        });

        Route::group([
            'prefix' => LaravelLocalization::transRoute('routes.place.prefix')
        ], function () {
            Route::post(LaravelLocalization::transRoute('routes.place.delete-photo'), 'PlaceController@deletePhoto')
                ->name('place.delete-photo')
                ->where(['photoId' => '[0-9]+']);
        });

        Route::get(LaravelLocalization::transRoute('routes.points-and-medals'), 'PointsAndMedalsController@index')
            ->name('points-and-medals');

        Route::group(['prefix' => LaravelLocalization::transRoute('routes.medals.prefix')], function () {
            Route::get('/', 'MedalController@index')->name('medals');

            Route::group([
                'prefix' => LaravelLocalization::transRoute('routes.medals.continent.prefix'),
                'where' => ['continentId' => '[0-9]+']
            ], function () {
                Route::get('/', 'MedalController@showCategoriesSummary')->name('medals.categories');

                Route::group([
                    'prefix' => LaravelLocalization::transRoute('routes.medals.continent.category.prefix'),
                    'where' => ['categoryId' => '[0-9]+']
                ], function () {
                    Route::get('/', 'MedalController@showPlacesSummary')->name('medals.places');

                    Route::get(
                        LaravelLocalization::transRoute('routes.medals.continent.category.place'),
                        'PlaceController@medalFlagPlaceDetail'
                    )
                        ->name('medals.place.detail')
                        ->where(['placeId'=>'[0-9]+']);

                    Route::get(
                        LaravelLocalization::transRoute('routes.medals.continent.category.rating'),
                        'PlaceController@medalFlagPlaceRating'
                    )
                        ->name('medals.place.rating')
                        ->where(['placeId'=>'[0-9]+']);
                });
            });
        });

        Route::group(['prefix' => LaravelLocalization::transRoute('routes.flags.prefix')], function () {
            Route::get('/', 'FlagController@index')->name('flags');

            Route::group([
                'prefix' => LaravelLocalization::transRoute('routes.flags.continent.prefix'),
                'where' => ['continentId' => '[0-9]+']
            ], function () {
                Route::get('/', 'FlagController@showCategoriesSummary')->name('flags.categories');

                Route::group([
                    'prefix' => LaravelLocalization::transRoute('routes.flags.continent.category.prefix'),
                    'where' => ['categoryId' => '[0-9]+']
                ], function () {
                    Route::get('/', 'FlagController@showPlacesSummary')->name('flags.places');

                    Route::get(
                        LaravelLocalization::transRoute('routes.flags.continent.category.place'),
                        'PlaceController@medalFlagPlaceDetail'
                    )
                        ->name('flags.place.detail')
                        ->where(['placeId'=>'[0-9]+']);

                    Route::get(
                        LaravelLocalization::transRoute('routes.flags.continent.category.rating'),
                        'PlaceController@medalFlagPlaceRating'
                    )
                        ->name('flags.place.rating')
                        ->where(['placeId'=>'[0-9]+']);
                });
            });
        });
    });
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
