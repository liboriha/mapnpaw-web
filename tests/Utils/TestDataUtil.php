<?php

namespace Tests\Utils;

use App\Place;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use App\User;
use App\UserPhoto;
use App\UserRating;
use App\UserCheck;
use App\UserFlag;
use App\UserMedal;
use App\UserPoint;
use App\Category;

trait TestDataUtil
{
    /**
     * Helper method for create test places in database with data array.
     *
     * @return array with IDs of created places
     */
    protected function createTestPlaces($data = [])
    {
        $IDs = [];

        foreach ($data as $placeData) {
            $place = Place::create([
                'name' => $placeData['name'] ?? 'Place',
                'gps' => new Point($placeData['gps'][0] ?? 0, $placeData['gps'][1] ?? 0),
                // Continent ID is rewrited according to continent of category ID
                'continent_id' => $placeData['continent_id'] ?? 1,
                'category_id' => $placeData['category_id'] ?? 1,
                'is_top' => $placeData['is_top'] ?? false,
                'is_flag' => $placeData['is_flag'] ?? false,
                'description' => $placeData['description'] ?? null,
                'address' => $placeData['address'] ?? null
            ]);

            if (!empty($placeData['checks'])) {
                foreach ($placeData['checks'] as $user_id) {
                    $check = new UserCheck();
                    $check->place_id = $place->id;
                    $check->user_id = $user_id;
                    $check->save();
                }
            }

            $IDs[] = $place->id;
        }

        return $IDs;
    }

    /**
     * Helper method for create test place with additional relative data in database with data array.
     */
    protected function createTestPlaceWithRelativeData($data = [])
    {
        $place = Place::create([
            'name' => $data['name'] ?? 'Place',
            'gps' => new Point($data['gps'][0] ?? 10, $data['gps'][1] ?? 10),
            // Continent ID is rewrited according to continent of category ID
            'continent_id' => $data['continent_id'] ?? 1,
            'category_id' => $data['category_id'] ?? 1,
            'is_top' => $data['is_top'] ?? false,
            'is_flag' => $data['is_flag'] ?? false,
            'description' => $data['description'] ?? null,
            'address' => $data['address'] ?? null,
            'image' => $data['image'] ?? null,
            'radius' => $data['radius'] ?? null
        ]);

        if (!empty($data['userPictures'])) {
            foreach ($data['userPictures'] as $value) {
                $photo = UserPhoto::create([
                    'place_id' => $place->id,
                    'user_id' => $value['user_id'],
                    'image' => $value['image'] ?? 'test/test.png',
                ]);

                if (isset($value['created_at'])) {
                    $photo->created_at = $value['created_at'];
                    $photo->save();
                }
            }
        }

        if (!empty($data['rating'])) {
            foreach ($data['rating'] as $value) {
                $rating = UserRating::create([
                    'place_id' => $place->id,
                    'user_id' => $value['user_id'],
                    'stars' => $value['stars'] ?? null,
                    'rating' => $value['rating'] ?? null,
                ]);

                if (isset($value['created_at'])) {
                    $rating->created_at = $value['created_at'];
                    $rating->save();
                }
            }
        }

        if (!empty($data['checks'])) {
            foreach ($data['checks'] as $value) {
                $check = new UserCheck();
                $check->place_id = $place->id;
                $check->user_id = $value['user_id'];
                $check->created_at = $value['created_at'];
                $check->save();
            }
        }

        return $place;
    }

    /**
     * Helper method for create test user with additional relative data in database with data array.
     * Create user.
     * Create medals for users - without places.
     * Create flags for users - create unique place for category (because database foreign key).
     * [
     *  'email' => 'xxx',
     *  'medals' => [
     *      2 // ID of medal that user has - medal ID is currently category ID
     *  ],
     *  'flags' => [
     *      1 // ID of flag that user has - flag ID is currently category ID
     *  ],
     *  'points' => [
     *      'type' => UserPoint::TYPE_CHECKIN, // UserPoint::TYPE_FIRST_PHOTO or UserPoint::TYPE_CHECKIN
     *      'points' => 1 // Points for user
     *  ]
     * ]
     */
    protected function createTestUserWithRelativeData($data = [])
    {
        if (!in_array(\Tests\Utils\UserUtil::class, class_uses($this))) {
            throw new \RuntimeException("To use '" . __FUNCTION__ . "' method on trait '" . __TRAIT__ .
                "', you need to use trait: '" . \Tests\Utils\UserUtil::class . "'!");
        }

        // Get email from data array
        if (empty($data['email'])) {
            // Get unique email
            while (true) {
                $email = str_random(40) . "@test.cz";
                if (!User::where('email', $email)->exists()) {
                    // Email not exists in database, break loop.
                    break;
                }
            }
        } else {
            $email = $data['email'];
        }

        $user = $this->createUser(['email' => $email]);

        $categories = Category::select('id', 'continent_id')->get();

        if (!empty($data['medals'])) {
            foreach ($data['medals'] as $value) {
                $category = $categories->where('id', $value)->first();
                if ($category == null) {
                    throw new \OutOfBoundsException("Medal with index '" . $value . "' not exists!");
                }

                // Create unique place for medal (top place)
                $place = $this->createTestPlaceWithRelativeData([
                    'category_id' => $category->id,
                    'is_top' => true
                ]);

                $medal = new UserMedal();
                $medal->user_id = $user->id;
                $medal->continent_id = $category->continent_id;
                $medal->category_id = $value;
                $medal->save();
            }
        }

        if (!empty($data['flags'])) {
            foreach ($data['flags'] as $value) {
                $category = $categories->where('id', $value)->first();
                if ($category == null) {
                    throw new \OutOfBoundsException("Flag with index '" . $value . "' not exists!");
                }

                // Get flag place of category if exists
                $place = $category->places()->isFlag()->first();

                // Create unique place for flag
                if ($place == null) {
                    $place = $this->createTestPlaceWithRelativeData([
                        'category_id' => $category->id,
                        'is_flag' => true
                    ]);
                }

                $flag = new UserFlag();
                $flag->user_id = $user->id;
                $flag->continent_id = $category->continent_id;
                $flag->category_id = $value;
                $flag->place_id = $place->id;
                $flag->save();
            }
        }

        if (!empty($data['points'])) {
            foreach ($data['points'] as $value) {
                // Create unique place for points
                $place = $this->createTestPlaceWithRelativeData();

                $point = new UserPoint();
                $point->user_id = $user->id;
                $point->place_id = $place->id;
                $point->type = $value['type'];
                $point->points = $value['points'] ?? 1;
                $point->save();
            }
        }

        return $user;
    }

    /**
     * Helper method for setting medals and flags icons and names.
     */
    protected function setMedalsAndFlagsIconAndName($withTranslations = false)
    {
        foreach (Category::all() as $category) {
            $category->medal_name = "Medal " . $category->continent_id . "-" . $category->id;
            $category->medal_image_locked = "test/medal_" . $category->continent_id . "_" . $category->id . "_locked.png";
            $category->medal_image_unlocked = "test/medal_" . $category->continent_id . "_" . $category->id . "_unlocked.png";

            $category->flag_name = "Flag " . $category->continent_id . "-" . $category->id;
            $category->flag_image_locked = "test/flag_" . $category->continent_id . "_" . $category->id . "_locked.png";
            $category->flag_image_unlocked = "test/flag_" . $category->continent_id . "_" . $category->id . "_unlocked.png";

            $category->save();

            if ($withTranslations == true) {
                $additional_locales = array_diff(config('voyager.multilingual.locales'), [config('voyager.multilingual.default')]);
                foreach ($additional_locales as $locale) {
                    $category = $category->translate($locale);
                    $category->medal_name = $category->medal_name . " " . $locale;
                    $category->flag_name = $category->flag_name . " " . $locale;
                    $category->save();
                }
            }
        }
    }
}
