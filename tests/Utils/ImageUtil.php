<?php 

namespace Tests\Utils;

use Illuminate\Support\Facades\File as FileFacade;
use Illuminate\Http\File;

trait ImageUtil
{
    /**
     * Get test image file from folder tests/images. If nameOfImage specify, than try get this file.
     * Otherwise get random image from folder tests/images.
     *
     * @return file object with image
     */
    protected function getTestImage($nameOfImage = null) 
    {
        $path = __DIR__ . "/../images/";
        $file = $path . $nameOfImage;

        if ($nameOfImage == null) {
            $files = FileFacade::glob($path . '*.*');
            $file = array_random($files);
        }

        return new File($file, true);
    }

    /**
     * Get test image in base64 encoded string. 
     * If $image is instance of File, than encode that file.
     * If $image specify image filename, than try get this file.
     * If $image is not specify, than get random image from folder tests/images.
     *
     * @return image in base64 encoded string
     */
    protected function getTestImageBase64($image = null, $withDataPrefix = true) 
    {
        // If image is not instance of file, than get test image.
        if (!($image instanceof File)) {
            $image = $this->getTestImage($image);
        }

        $dataPrefix = "";
        if ($withDataPrefix) {
            $dataPrefix = "data:";
        }

        return $dataPrefix . $image->getMimeType() . ';base64,' . base64_encode(FileFacade::get($image));
    }
}