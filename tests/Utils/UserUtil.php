<?php 

namespace Tests\Utils;

use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Hash;
use App\User;

trait UserUtil
{
    use HeadersUtil;

    /**
     * In this variable is saved last created user by function createUser.
     */
    protected $lastCreatedUser = null;

    /**
     * In this variable is saved last created user token by function loginUser.
     */
    protected $lastUserToken = null;

    /**
     * Create testing user. Without specify data will be created default test user.
     * Can set email, password, firstname, lastname, nickname, visibility, avatar. Field email and password are required.
     * Password must be unencrypted, will be encrypted.
     *
     * @return userInstance
     */
    protected function createUser($data = []) 
    {
        if (empty($data)) {
            // Set default values
            $data['email'] = 'test@test.cz';
            $data['password'] = '123456';
        }

        if (!isset($data['password'])) {
            $data['password'] = '123456';
        }

        // Check required fields.
        if (!isset($data['email'])) {
            throw new \InvalidArgumentException('Function createUser only accept data with email!');
        }

        // Use only allowed fields
        $data = array_intersect_key(
            $data, 
            array_flip([
                'email',
                'password',
                'firstname',
                'lastname',
                'nickname',
                'visibility',
                'avatar',
                'facebook_id'
            ])
        );

        // Crypt password
        $data['password'] = bcrypt($data['password']);

        // Create user for testing
        $user = new User();
        foreach ($data as $key => $value) {
            $user->$key = $value;
        }
        $user->save();

        $this->lastCreatedUser = $user;

        return $user;
    }
    
    /**
     * Function to get basic user data from user object in array.
     *
     * @return array with basic user data
     */
    protected function getBasicUserData($user) 
    {
        if (!($user instanceof User)) {
            // If not a valid user data, return null.
            return null;
        }

        $data = $user->toArray();

        // Get only basic fields
        $data = array_intersect_key(
            $data, 
            array_flip([
                'email',
                'firstname',
                'lastname',
                'nickname',
                'visibility',
                'avatar'
            ])
        );

        return $data;
    }

    /**
     * Login user and return user token to authenticate by JWT.
     * Can set user data to create new user by createUser function.
     *
     * @return userToken
     */
    protected function loginUser($data = null) 
    {
        // If user not defined, create default test user.
        if ($data == null || is_array($data)) {
            $user = $this->createUser($data);
        } else {
            $user = $data;
        }

        if ($user instanceof User) {
            // Have instance of user.

            // Get token for user - for JWT authentication. (User login)
            $this->lastUserToken = JWTAuth::fromUser($user);

            return $this->lastUserToken;

        } else {
            throw new \InvalidArgumentException('Data parametr is not instance of User model or data to create user!');
        }
    }

    /**
     * Set authorization JWT token to headers.
     * Can set user instance to login user by loginUser function.
     * Can set user data to create and login new user by loginUser function.
     *
     * @return null
     */
    protected function setAuthorizationHeaders($data = null, $additionalHeaders = [])
    {
        // If token not defined, create default test user.
        if ($data == null || is_array($data) || $data instanceof User) {
            $token = $this->loginUser($data);
        } else {
            $token = $data;
        }

        if (is_string($token)) {
            // Have instance of token.

            $headers = array_merge(['Authorization' => 'Bearer ' . $token], $additionalHeaders);

            // Set JWT authentication
            $this->setHeaders($headers);
        } else {
            throw new \InvalidArgumentException('Data parametr is not token string or instance of User model or data to create user!');
        }
    }
}
