<?php 

namespace Tests\Utils;

trait HeadersUtil
{
    protected function setHeaders($add = [], $remove = []) 
    {
        $default = [
            "X-API-Key" => config('awapp.X-API-Key')
        ];

        $merged = array_merge($default, $add);

        foreach($remove as $key) {
            unset($merged[$key]);
        }

        $this->withHeaders($merged);
    }
}