<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\Utils\HeadersUtil;
use Tests\Utils\UserUtil;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Hash;
use App\User;

class ChangePasswordTest extends TestCase
{
    use UserUtil;
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testChangePasswordWithoutAPIKey()
    {
        $response = $this->postJson('/api/v1/profile/password', ['oldPassword' => '123456', 'newPassword' => '654321']);
        
        $response->assertStatus(401);
    }

    /**
     * Test without authentication by JWT.
     *
     * @return void
     */
    public function testChangePasswordWithoutLogin()
    {
        $this->setHeaders();

        $response = $this->postJson('/api/v1/profile/password', ['oldPassword' => '123456', 'newPassword' => '654321']);
        
        $response->assertStatus(101);
    }

    /**
     * Test valid password change.
     *
     * @return void
     */
    public function testChangePassword()
    {
        $this->setAuthorizationHeaders();

        $response = $this->postJson('/api/v1/profile/password', ['oldPassword' => '123456', 'newPassword' => '654321']);
        
        $response->assertStatus(204);

        // Reload data about user from database
        $user = User::find($this->lastCreatedUser->id);
        
        $this->assertTrue(
            Hash::check('654321', $user->password)
        );
    }

    /**
     * Test with wrong oldPassword (old password is 123456).
     *
     * @return void
     */
    public function testChangePasswordBadOldPassword()
    {
        $this->setAuthorizationHeaders();

        $response = $this->postJson('/api/v1/profile/password', ['oldPassword' => '000000', 'newPassword' => '654321']);
        
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['oldPassword'],
            'message'
        ]);
    }

    /**
     * newPassword must have min lenght 6 characters.
     *
     * @return void
     */
    public function testChangePasswordBadNewPassword()
    {
        $this->setAuthorizationHeaders();

        $response = $this->postJson('/api/v1/profile/password', ['oldPassword' => '123456', 'newPassword' => '654']);
        
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['newPassword'],
            'message'
        ]);
    }
}
