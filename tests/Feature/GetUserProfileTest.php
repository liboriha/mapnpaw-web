<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\Utils\HeadersUtil;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Storage;
use App\User;
use Tymon\JWTAuth\Facades\JWTAuth;

class GetUserProfileTest extends TestCase
{
    use HeadersUtil;
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetProfileWithoutAPIKey()
    {
        $response = $this->getJson('/api/v1/profile');
        
        $response->assertStatus(401);
    }

    /**
     * Test without authentication by JWT.
     *
     * @return void
     */
    public function testGetProfileWithoutLogin()
    {
        $this->setHeaders();

        $response = $this->getJson('/api/v1/profile');
        
        $response->assertStatus(101);
    }

    /**
     * Data for testGetProfile.
     */
    public function userDataProvider() 
    {
        return [
            [
                'email' => 'test@test.cz',
                'firstname' => "Pavel",
                'lastname' => "Nový",
                'nickname' => "PN",
                'display' => 'name',
                'profilePicture' => 'users/default.png',
            ],
            [
                'email' => 'test@test.cz',
                'firstname' => 'Jitka',
                'lastname' => 'Novější',
                'nickname' => 'Krásná',
                'display' => 'nickname',
                'profilePicture' => 'users/obrazek.png',
            ],
        ];
    }

    /**
     * Test valid get profile.
     *
     * @return void
     * @dataProvider userDataProvider
     */
    public function testGetProfile($email, $firstname, $lastname, $nickname, $display, $profilePicture)
    {
        // Create user for testing
        $user = new User();

        $user->email = $email;
        $user->password = bcrypt("123546");
        $user->firstname = $firstname;
        $user->lastname = $lastname;
        $user->nickname = $nickname;
        $user->visibility = strtoupper($display);
        $user->avatar = $profilePicture;

        $user->save();

        // Get token for user - for JWT authentication. (User login)
        $token = JWTAuth::fromUser($user);

        // Set JWT authentication
        $this->setHeaders(['Authorization' => 'Bearer ' . $token]);

        $response = $this->getJson('/api/v1/profile');

        // Prepare data to validation
        $profilePicture = Storage::disk(config('voyager.storage.disk'))->url($profilePicture);

        $response->assertStatus(200);
        $response->assertJson([
            'data' => [
                'email' => $email,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'nickname' => $nickname,
                'display' => $display,
                'profilePicture' => $profilePicture
            ]
        ]);
    }
}
