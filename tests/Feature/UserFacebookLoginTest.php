<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Mockery;
use Socialite;
use Tests\TestCase;
use Tests\Utils\ImageUtil;
use Tests\Utils\UserUtil;

class UserFacebookLoginTest extends TestCase
{
    use UserUtil;
    use ImageUtil;
    use DatabaseTransactions;

    /**
     * Test without API key.
     *
     * @return void
     */
    public function testFacebookLoginWithoutAPIKey()
    {
        $response = $this->postJson('/api/v1/token/facebook', ['token' => 'token']);
        
        $response->assertStatus(401);
    }

    /**
     * Facebook token is required.
     *
     * @return void
     */
    public function testFacebookLoginWrongRequest()
    {
        $this->setHeaders();

        $response = $this->postJson('/api/v1/token/facebook');
        
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['token'],
            'message'
        ]);
    }

    /**
     * Facebook token in invalid data format.
     *
     * @return void
     */
    public function testFacebookLoginInvalidToken()
    {
        $this->setHeaders();

        // Create testing data.
        $testToken = 'tokentokentoken';

        // Create mocking of socialite package.
        $provider = Mockery::mock('Laravel\Socialite\Contracts\Provider');
        $provider->shouldReceive('fields')->andReturn($provider);
        $provider->shouldReceive('userFromToken')->with($testToken)->andThrow(
            new \GuzzleHttp\Exception\ClientException(
                "",
                Mockery::mock('Psr\Http\Message\RequestInterface'),
                new \GuzzleHttp\Psr7\Response(
                    400,
                    [],
                    json_encode([
                        'error' => [
                            'message' => 'test test'
                        ]
                    ])
                )
            )
        );

        Socialite::shouldReceive('driver')->with('facebook')->andReturn($provider);

        // Test facebook login with invalid token.
        $response = $this->postJson('/api/v1/token/facebook', ['token' => $testToken]);

        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['facebook'],
            'message'
        ]);
    }

    /**
     * Try to register facebook login with email, that already registered.
     *
     * @return void
     */
    public function testFacebookLoginEmailExists()
    {
        $this->setHeaders();

        // Create testing data.
        $testToken = 'tokentokentoken';

        $email = str_random(40) . '@' . str_random(40) . '.cz';

        // Create testing data
        $this->createUser(['email' => $email]);

        // Create mocking of socialite package.
        $abstractUser = Mockery::mock('Laravel\Socialite\Two\User');
        $abstractUser->id = 1234567890;
        $abstractUser->email = $email;

        $provider = Mockery::mock('Laravel\Socialite\Contracts\Provider');
        $provider->shouldReceive('fields')->andReturn($provider);
        $provider->shouldReceive('userFromToken')->with($testToken)->andReturn($abstractUser);

        Socialite::shouldReceive('driver')->with('facebook')->andReturn($provider);

        // Test facebook login with email that exists in database.
        $response = $this->postJson('/api/v1/token/facebook', ['token' => $testToken]);

        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['email'],
            'message'
        ]);
    }

    /**
     * Test valid facebook login.
     *
     * @return void
     */
    public function testFacebookLoginValid()
    {
        $this->setHeaders();

        Storage::fake(config('voyager.storage.disk'));

        // Create testing data.
        $testToken = 'tokentokentoken';

        $email = str_random(40) . '@' . str_random(40) . '.cz';
        $facebookid = 1234567890;
        $firstname = str_random(40);
        $lastname = str_random(40);

        // Test upload avatar image.
        $imageFile = $this->getTestImage();

        $facebookAvatarURL = "http://test.cz/" . $imageFile->getFilename();

        $image = Image::make($imageFile);

        Image::shouldReceive('make')->once()->with($facebookAvatarURL)->andReturn($image);

        // Create mocking of socialite package.
        $abstractUser = Mockery::mock('Laravel\Socialite\Two\User');
        $abstractUser->id = 1234567890;
        $abstractUser->email = $email;
        $abstractUser->user = ['first_name' => $firstname, 'last_name' => $lastname];
        $abstractUser->avatar = $facebookAvatarURL;

        $provider = Mockery::mock('Laravel\Socialite\Contracts\Provider');
        $provider->shouldReceive('fields')->andReturn($provider);
        $provider->shouldReceive('userFromToken')->with($testToken)->andReturn($abstractUser);

        Socialite::shouldReceive('driver')->with('facebook')->andReturn($provider);

        // Test facebook login with invalid token.
        $response = $this->postJson('/api/v1/token/facebook', ['token' => $testToken]);

        $response->assertStatus(200);

        // Test get user token.
        $response->assertJsonStructure([
            'data' => ['token']
        ]);

        // Test user is created.
        $this->assertDatabaseHas(
            with(new User)->getTable(),
            [
                'facebook_id' => $facebookid,
                'email' => $email,
                'firstname' => $firstname,
                'lastname' => $lastname
            ]
        );

        // Test user avatar is saved on server.
        $user = User::where('facebook_id', $facebookid)->first();

        Storage::disk(config('voyager.storage.disk'))->assertExists($user->avatar);
    }

    /**
     * Test checking email format.
     *
     * @return void
     */
    public function testFacebookLoginAlreadyRegistered()
    {
        $this->setHeaders();

        Storage::fake(config('voyager.storage.disk'));

        $testToken = 'tokentokentoken';

        $email = str_random(40) . '@' . str_random(40) . '.cz';
        $facebookid = 1234567890;
        $firstname = str_random(40);
        $lastname = str_random(40);
        $testAvatar = 'test/test.png';
        $newfirstname = str_random(40);
        $newlastname = str_random(40);

        // Create testing data
        $this->createUser([
            'email' => $email,
            'facebook_id' => $facebookid,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'avatar' => $testAvatar
        ]);

        // Create mocking of socialite package.
        $abstractUser = Mockery::mock('Laravel\Socialite\Two\User');
        $abstractUser->id = 1234567890;
        $abstractUser->email = $email;
        $abstractUser->user = ['first_name' => $newfirstname, 'last_name' => $newlastname];
        $abstractUser->avatar = '';

        $provider = Mockery::mock('Laravel\Socialite\Contracts\Provider');
        $provider->shouldReceive('fields')->andReturn($provider);
        $provider->shouldReceive('userFromToken')->with($testToken)->andReturn($abstractUser);

        Socialite::shouldReceive('driver')->with('facebook')->andReturn($provider);

        // Test facebook login with invalid token.
        $response = $this->postJson('/api/v1/token/facebook', ['token' => $testToken]);

        $response->assertStatus(200);

        // Test get user token.
        $response->assertJsonStructure([
            'data' => ['token']
        ]);

        // Not changed firstname, lastname and avatar.
        $this->assertDatabaseHas(
            with(new User)->getTable(),
            [
                'facebook_id' => $facebookid,
                'email' => $email,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'avatar' => $testAvatar
            ]
        );
    }
}
