<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\Utils\UserUtil;
use Tests\Utils\ImageUtil;
use Tests\Utils\TestDataUtil;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Place;
use App\Category;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use App\Http\Helpers\MapHelper;

class GetPlacesTest extends TestCase
{
    use UserUtil;
    use TestDataUtil;
    use DatabaseTransactions;

    /**
     * Build URL parameters
     */
    protected function buildURLParameters($lat, $lng, $radius = null, $category_name = null)
    {
        $parameters = "?lat=$lat&lng=$lng";

        if ($radius != null) {
            $parameters = $parameters . "&radius=$radius";
        }

        if ($category_name != null) {
            $parameters = $parameters . "&category=" . urlencode($category_name);
        }

        return $parameters;
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetPlacesWithoutAPIKey()
    {
        $response = $this->getJson('/api/v1/map' . $this->buildURLParameters(50.0879507, 14.4123862));
            
        $response->assertStatus(401);
    }

    /**
     * Test without authentication by JWT. Wokring even without login.
     *
     * @return void
     */
    public function testGetPlacesWithoutLogin()
    {
        $this->setHeaders();

        $response = $this->getJson('/api/v1/map' . $this->buildURLParameters(50.0879507, 14.4123862));
        
        $response->assertStatus(200);
    }

    /**
     * Test without lat and lng parameter.
     *
     * @return void
     */
    public function testGetPlacesWithoutLatitudeAndLongitudeParameters()
    {
        $this->setAuthorizationHeaders();

        $response = $this->getJson('/api/v1/map');
        
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['lat', 'lng'],
            'message'
        ]);
    }

    /**
     * Testing data for testing places by categories. Test point is 50.0879507, 14.4123862.
     */
    protected function getTestCategoryPlaces()
    {
        // Places are on point 50.0879507, 14.4123862, but diffs by categories
        return [
            ['name' => 'Cat 1, point 1', 'gps' => [50.0879507, 14.4123862], 'category_id' => 1],
            ['name' => 'Cat 1, point 2', 'gps' => [50.0879507, 14.4123862], 'category_id' => 1],
            ['name' => 'Cat 2, point 1', 'gps' => [50.0879507, 14.4123862], 'category_id' => 2],
            ['name' => 'Cat 2, point 2', 'gps' => [50.0879507, 14.4123862], 'category_id' => 2],
            ['name' => 'Cat 3, point 1', 'gps' => [50.0879507, 14.4123862], 'category_id' => 3],
            ['name' => 'Cat 3, point 2', 'gps' => [50.0879507, 14.4123862], 'category_id' => 3]
        ];
    }

    /**
     * Test valid get places by categories.
     *
     * @return void
     */
    public function testGetPlacesByCategory()
    {
        $this->setAuthorizationHeaders();

        // Create test data
        $testData = $this->getTestCategoryPlaces();
        $this->createTestPlaces($testData);

        // Test category that not exists
        $response = $this->getJson('/api/v1/map' . $this->buildURLParameters(50.0879507, 14.4123862, null, "azazazazazaz"));
        
        $response->assertStatus(404);

        $category = Category::find(1);

        // Test category with ID 1
        $response = $this->getJson('/api/v1/map' . $this->buildURLParameters(50.0879507, 14.4123862, null, $category->name));
        
        $response->assertStatus(200);
        
        $response->assertJsonFragment(['name' => $testData[0]['name']]);
        $response->assertJsonFragment(['name' => $testData[1]['name']]);

        $response->assertJsonMissing(['name' => $testData[2]['name']]);
        $response->assertJsonMissing(['name' => $testData[3]['name']]);
        $response->assertJsonMissing(['name' => $testData[4]['name']]);
        $response->assertJsonMissing(['name' => $testData[5]['name']]);

        $category = Category::find(2);

        // Test category with ID 2
        $response = $this->getJson('/api/v1/map' . $this->buildURLParameters(50.0879507, 14.4123862, null, $category->name));
        
        $response->assertStatus(200);
        
        $response->assertJsonMissing(['name' => $testData[0]['name']]);
        $response->assertJsonMissing(['name' => $testData[1]['name']]);

        $response->assertJsonFragment(['name' => $testData[2]['name']]);
        $response->assertJsonFragment(['name' => $testData[3]['name']]);

        $response->assertJsonMissing(['name' => $testData[4]['name']]);
        $response->assertJsonMissing(['name' => $testData[5]['name']]);

        $category = Category::find(3);

        // Test category with ID 3
        $response = $this->getJson('/api/v1/map' . $this->buildURLParameters(50.0879507, 14.4123862, null, $category->name));
        
        $response->assertStatus(200);
        
        $response->assertJsonMissing(['name' => $testData[0]['name']]);
        $response->assertJsonMissing(['name' => $testData[1]['name']]);
        $response->assertJsonMissing(['name' => $testData[2]['name']]);
        $response->assertJsonMissing(['name' => $testData[3]['name']]);

        $response->assertJsonFragment(['name' => $testData[4]['name']]);
        $response->assertJsonFragment(['name' => $testData[5]['name']]);
    }

    /**
     * Test valid get places by categories. (Test translation of categories.)
     *
     * @return void
     */
    public function testGetPlacesByCategoryTranslated()
    {
        $this->setAuthorizationHeaders();

        $user = $this->lastCreatedUser;

        $localesAdditional = array_diff(config('voyager.multilingual.locales'), [config('voyager.multilingual.default')]);

        // Create test data
        $testData = $this->getTestCategoryPlaces();
        $this->createTestPlaces($testData);

        $category1 = Category::find(1);
        $category1->name = "Test A";
        $category1->save();

        foreach ($localesAdditional as $locale) {
            $translateCategory1 = $category1->translate($locale);
            $translateCategory1->name = "Test " . $locale . " A";
            $translateCategory1->save();
        }

        // Same name of first category
        $category2 = Category::find(2);
        $category2->name = "Test A";
        $category2->save();

        foreach ($localesAdditional as $locale) {
            $translateCategory2 = $category2->translate($locale);
            $translateCategory2->name = "Test " . $locale . " A";
            $translateCategory2->save();
        }

        // Different name of category
        $category3 = Category::find(3);
        $category3->name = "Test C";
        $category3->save();

        foreach ($localesAdditional as $locale) {
            $translateCategory3 = $category3->translate($locale);
            $translateCategory3->name = "Test " . $locale . " C";
            $translateCategory3->save();
        }

        foreach (config('voyager.multilingual.locales') as $locale) {
            $this->setAuthorizationHeaders($user, ['X-Localization' => $locale]);

            $categoryLocalize = $category1->translate($locale);

            // Test category 1 and 2 (same name)
            $response = $this->getJson('/api/v1/map' . $this->buildURLParameters(50.0879507, 14.4123862, null, $categoryLocalize->name));

            $response->assertStatus(200);
            
            $response->assertJsonFragment(['name' => $testData[0]['name']]);
            $response->assertJsonFragment(['name' => $testData[1]['name']]);
            $response->assertJsonFragment(['name' => $testData[2]['name']]);
            $response->assertJsonFragment(['name' => $testData[3]['name']]);

            $response->assertJsonMissing(['name' => $testData[4]['name']]);
            $response->assertJsonMissing(['name' => $testData[5]['name']]);



            $categoryLocalize = $category3->translate($locale);

            // Test category 3 (different name)
            $response = $this->getJson('/api/v1/map' . $this->buildURLParameters(50.0879507, 14.4123862, null, $categoryLocalize->name));

            $response->assertStatus(200);
            
            $response->assertJsonMissing(['name' => $testData[0]['name']]);
            $response->assertJsonMissing(['name' => $testData[1]['name']]);
            $response->assertJsonMissing(['name' => $testData[2]['name']]);
            $response->assertJsonMissing(['name' => $testData[3]['name']]);

            $response->assertJsonFragment(['name' => $testData[4]['name']]);
            $response->assertJsonFragment(['name' => $testData[5]['name']]);
        }
    }

    /**
     * Testing data for testing default radius. Test point is 0, 0.
     */
    protected function getTestDefaultRadiusPlaces()
    {
        // Places have smaller and greater distance than default radius from point 0, 0
        $data = [];

        $default_radius = (float) config('awapp.map.defaults.radius');
        // Smaller distance latitude
        $smaller_latitude = 0 + (($default_radius - 0.001 * $default_radius) / MapHelper::EARTH_RADIUS) * (180 / pi());
        $data[] = ['name' => 'Smaler than default radius', 'gps' => [$smaller_latitude, 0]];
        
        // Greater distance latitude
        $greater_latitude = 0 + (($default_radius + 0.001 * $default_radius) / MapHelper::EARTH_RADIUS) * (180 / pi());
        $data[] = ['name' => 'Greater than default radius', 'gps' => [$greater_latitude, 0]];

        return $data;
    }

    /**
     * Test valid get places by default radius.
     *
     * @return void
     */
    public function testGetPlacesDefaultRadius()
    {
        $this->setAuthorizationHeaders();

        // Create test data
        $testData = $this->getTestDefaultRadiusPlaces();
        $this->createTestPlaces($testData);

        // Test with default radius
        $response = $this->getJson('/api/v1/map' . $this->buildURLParameters(0, 0));
        
        $response->assertStatus(200);
        
        $response->assertJsonFragment(['name' => $testData[0]['name']]);

        $response->assertJsonMissing(['name' => $testData[1]['name']]);



        // Test with radius greater than default radius
        $response = $this->getJson('/api/v1/map' . $this->buildURLParameters(0, 0, (float) config('awapp.map.defaults.radius') * 1.1));
        
        $response->assertStatus(200);
        
        $response->assertJsonFragment(['name' => $testData[0]['name']]);
        $response->assertJsonFragment(['name' => $testData[1]['name']]);
    }

    /**
     * Testing data for testing radius searching. Test point is 50.0879507, 14.4123862.
     */
    protected function getTestRadiusPlaces()
    {
        // Main place to test from is point 50.0879507, 14.4123862
        return [
            ['name' => '4.994 km north', 'gps' => [50.1328607, 14.4123862]],
            ['name' => '5.708 km east', 'gps' => [50.0879607, 14.4923862]],
            ['name' => '5.428 km south west', 'gps' => [50.0579507, 14.3523862]]
        ];
    }

    /**
     * Test valid get places with radius.
     *
     * @return void
     */
    public function testGetPlacesByRadius()
    {
        $this->setAuthorizationHeaders();

        // Create test data
        $testData = $this->getTestRadiusPlaces();
        $this->createTestPlaces($testData);

        // Test with radius 4.5km
        $response = $this->getJson('/api/v1/map' . $this->buildURLParameters(50.0879507, 14.4123862, 4500));
        
        $response->assertStatus(200);
        
        $response->assertJsonMissing(['name' => $testData[0]['name']]);
        $response->assertJsonMissing(['name' => $testData[1]['name']]);
        $response->assertJsonMissing(['name' => $testData[2]['name']]);



        // Test with radius 5km
        $response = $this->getJson('/api/v1/map' . $this->buildURLParameters(50.0879507, 14.4123862, 5000));
        
        $response->assertStatus(200);
        
        $response->assertJsonFragment(['name' => $testData[0]['name']]);

        $response->assertJsonMissing(['name' => $testData[1]['name']]);
        $response->assertJsonMissing(['name' => $testData[2]['name']]);



        // Test with radius 5.5km
        $response = $this->getJson('/api/v1/map' . $this->buildURLParameters(50.0879507, 14.4123862, 5500));
        
        $response->assertStatus(200);
        
        $response->assertJsonFragment(['name' => $testData[0]['name']]);

        $response->assertJsonMissing(['name' => $testData[1]['name']]);

        $response->assertJsonFragment(['name' => $testData[2]['name']]);



        // Test with radius 6km
        $response = $this->getJson('/api/v1/map' . $this->buildURLParameters(50.0879507, 14.4123862, 6500));
        
        $response->assertStatus(200);
        
        $response->assertJsonFragment(['name' => $testData[0]['name']]);
        $response->assertJsonFragment(['name' => $testData[1]['name']]);
        $response->assertJsonFragment(['name' => $testData[2]['name']]);
    }

    /**
     * Test valid get places with radius.
     *
     * @return void
     */
    public function testGetPlacesCheckedStatus()
    {
        $this->setAuthorizationHeaders();

        $user = $this->lastCreatedUser;

        // Create test data
        $testData = [
            ['name' => 'not checked', 'gps' => [10, 12]],
            ['name' => 'checked', 'gps' => [10, 12], 'checks' => [$user->id]],
        ];
        $placeIDs = $this->createTestPlaces($testData);

        // Test with radius 4.5km
        $response = $this->getJson('/api/v1/map' . $this->buildURLParameters(10, 12));
        
        $response->assertStatus(200);

        $checkTimeOfSecondPlace = Place::find($placeIDs[1])->userChecks[0]->created_at->timestamp;
        
        $response->assertJsonFragment([
            [
                'id' => $placeIDs[0],
                'lat' => 10,
                'lng' => 12,
                'name' => $testData[0]['name'],
                'checkedIn' => false,
                'lastCheckIn' => 0,
                'image' => null
            ]
        ]);
        $response->assertJsonFragment([
            [
                'id' => $placeIDs[1],
                'lat' => 10,
                'lng' => 12,
                'name' => $testData[1]['name'],
                'checkedIn' => true,
                'lastCheckIn' => $checkTimeOfSecondPlace,
                'image' => null
            ]
        ]);
    }
}
