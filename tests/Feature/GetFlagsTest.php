<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\Utils\UserUtil;
use Tests\Utils\TestDataUtil;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Continent;
use App\Category;

class GetFlagsTest extends TestCase
{
    use UserUtil;
    use TestDataUtil;
    use DatabaseTransactions;

    /**
     * Test without API key.
     *
     * @return void
     */
    public function testGetFlagsWithoutAPIKey()
    {
        $response = $this->getJson('/api/v1/flags');
            
        $response->assertStatus(401);
    }

    /**
     * Test without authentication by JWT.
     *
     * @return void
     */
    public function testGetFlagsWithoutLogin()
    {
        $this->setHeaders();

        $response = $this->getJson('/api/v1/flags');
        
        $response->assertStatus(101);
    }

    /**
     * Test valid get flags. Test without any flags record.
     *
     * @return void
     */
    public function testGetFlagsBasic()
    {
        $this->setAuthorizationHeaders();

        // Create test data
        $this->setMedalsAndFlagsIconAndName(); // Need call before get categories.

        // Test get flags - user has no flags
        $response = $this->getJson('/api/v1/flags');

        $response->assertStatus(200);

        // Prepare data to test
        $dataToTest = [
            'data' => []
        ];

        foreach (Continent::all() as $continent) {
            $tempContinent = [
                'id' => $continent->id,
                'name' => $continent->name,
                'flags' => []
            ];

            $dataToTest['data'][] = $tempContinent;
        }

        // Test response to contains all continents with flags
        $response->assertExactJson($dataToTest);
    }

    /**
     * Test valid get flags. Test with several flags record.
     *
     * @return void
     */
    public function testGetFlagsUserHasFewFlags()
    {
        // Create test data
        $this->setMedalsAndFlagsIconAndName(); // Need call before get categories.

        $flagIDs = array_rand(array_flip(Category::all()->pluck('id')->toArray()), 10); // get 10 random flags

        foreach ($flagIDs as $flagID) {
            $place = $this->createTestPlaceWithRelativeData([
                'category_id' => $flagID, // Same as category ID
                'is_flag' => true
            ]);
        }

        $testData = [
            'flags' => array_rand(array_flip($flagIDs), 5) // get 5 random flags from 10 random flags
        ];

        $user = $this->createTestUserWithRelativeData($testData);

        // Login test user
        $this->setAuthorizationHeaders($user);


        // Test get flags - user has 10 flags
        $response = $this->getJson('/api/v1/flags');

        $response->assertStatus(200);

        // Prepare data to test
        $dataToTest = [
            'data' => []
        ];

        foreach (Continent::all() as $continent) {
            $tempContinent = [
                'id' => $continent->id,
                'name' => $continent->name,
                'flags' => []
            ];

            foreach ($continent->categories as $category) {
                if (in_array($category->id, $flagIDs)) {
                    // category flag images are all filled
                    if (in_array($category->id, $testData['flags'])) {
                        $flag_image = Storage::disk(config('voyager.storage.disk'))->url($category->flag_image_unlocked);
                        $achieved = true;
                    } else {
                        $flag_image = Storage::disk(config('voyager.storage.disk'))->url($category->flag_image_locked);
                        $achieved = false;
                    }

                    $tempContinent['flags'][] = [
                        'id' => $category->id, // Flag ID is category ID
                        'image' => $flag_image,
                        'name' => $category->flag_name,
                        'achieved' => $achieved,
                        'category' => [
                            'id' => $category->id,
                            'label' => $category->name
                        ],
                        'continent' => [
                            'id' => $continent->id,
                            'label' => $continent->name
                        ]
                    ];
                }
            }

            $dataToTest['data'][] = $tempContinent;
        }

        // Test response to contains all continents with flags
        $response->assertExactJson($dataToTest);
    }

    /**
     * Test valid get flags. Test with several flags record with translations.
     *
     * @return void
     */
    public function testGetFlagsUserHasFewFlagsWithTranslations()
    {
        // Create test data
        $this->setMedalsAndFlagsIconAndName(true); // Need call before get categories.

        $flagIDs = array_rand(array_flip(Category::all()->pluck('id')->toArray()), 10); // get 10 random flags

        foreach ($flagIDs as $flagID) {
            $place = $this->createTestPlaceWithRelativeData([
                'category_id' => $flagID, // Same as category ID
                'is_flag' => true
            ]);
        }

        $testData = [
            'flags' => array_rand(array_flip($flagIDs), 5) // get 5 random flags from 10 random flags
        ];

        $user = $this->createTestUserWithRelativeData($testData);

        // Test each available locale.
        foreach (config('voyager.multilingual.locales') as $locale) {
            // Login test user
            $this->setAuthorizationHeaders($user, ['X-Localization' => $locale]);


            // Test get flags - user has 10 flags
            $response = $this->getJson('/api/v1/flags');

            $response->assertStatus(200);

            // Prepare data to test
            $dataToTest = [
                'data' => []
            ];

            foreach (Continent::all() as $continent) {
                $continent = $continent->translate($locale);

                $tempContinent = [
                    'id' => $continent->id,
                    'name' => $continent->name,
                    'flags' => []
                ];

                foreach ($continent->categories as $category) {
                    if (in_array($category->id, $flagIDs)) {
                        $category = $category->translate($locale);

                        // category flag images are all filled
                        if (in_array($category->id, $testData['flags'])) {
                            $flag_image = Storage::disk(config('voyager.storage.disk'))->url($category->flag_image_unlocked);
                            $achieved = true;
                        } else {
                            $flag_image = Storage::disk(config('voyager.storage.disk'))->url($category->flag_image_locked);
                            $achieved = false;
                        }

                        $tempContinent['flags'][] = [
                            'id' => $category->id, // Flag ID is category ID
                            'image' => $flag_image,
                            'name' => $category->flag_name,
                            'achieved' => $achieved,
                            'category' => [
                                'id' => $category->id,
                                'label' => $category->name
                            ],
                            'continent' => [
                                'id' => $continent->id,
                                'label' => $continent->name
                            ]
                        ];
                    }
                }

                $dataToTest['data'][] = $tempContinent;
            }

            // Test response to contains all continents with flags
            $response->assertExactJson($dataToTest);
        }

        // Reset application locale
        app()->setLocale(config('app.locale'));
    }
}
