<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\Utils\UserUtil;
use Tests\Utils\TestDataUtil;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Category;

class GetCategoriesTest extends TestCase
{
    use UserUtil;
    use TestDataUtil;
    use DatabaseTransactions;
    
    /**
     * Test without API key.
     *
     * @return void
     */
    public function testGetCategoriesWithoutAPIKey()
    {
        $response = $this->getJson('/api/v1/categories');
            
        $response->assertStatus(401);
    }

    /**
     * Test without authentication by JWT.
     *
     * @return void
     */
    public function testGetCategoriesWithoutLogin()
    {
        $this->setHeaders();

        $response = $this->getJson('/api/v1/categories');
        
        $response->assertStatus(101);
    }

    /**
     * Test get categories with categoryId attribute.
     *
     * @return void
     */
    public function testGetCategoriesContinentIdAttribute()
    {
        $this->setAuthorizationHeaders();

        // Test categoryId attribute as string
        $response = $this->getJson('/api/v1/categories?continentId=ahoj');
        
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['continentId'],
            'message'
        ]);

        // Test categoryId attribute - not exists continent
        $response = $this->getJson('/api/v1/categories?continentId=9999');
        
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['continentId'],
            'message'
        ]);

        // Test categoryId attribute - OK
        $response = $this->getJson('/api/v1/categories?continentId=1');
        
        $response->assertStatus(200);
    }

    /**
     * Test get categories with remove duplications.
     *
     * @return void
     */
    public function testGetCategoriesRemoveDuplication()
    {
        $this->setAuthorizationHeaders();

        $user = $this->lastCreatedUser;

        // Create test data
        $categories = Category::all();

        $localesAdditional = array_diff(config('voyager.multilingual.locales'), [config('voyager.multilingual.default')]);

        // Contains category names grouped by locale.
        $categoryNames = [];

        // Reset all category names
        foreach ($categories as $category) {
            $category->name = "Category 1";
            $category->save();

            $categoryNames[config('voyager.multilingual.default')][] = $category->name;

            foreach ($localesAdditional as $locale) {
                $translateCategory = $category->translate($locale);
                $translateCategory->name = "Category " . $locale . " 1";
                $translateCategory->save();

                $categoryNames[$locale][] = $translateCategory->name;
            }
        }

        // Set two categories different
        $category = Category::find(3);
        $category->name = "Category 2";
        $category->save();

        $categoryNames[config('voyager.multilingual.default')][] = $category->name;

        foreach ($localesAdditional as $locale) {
            $translateCategory = $category->translate($locale);
            $translateCategory->name = "Category " . $locale . " 2";
            $translateCategory->save();

            $categoryNames[$locale][] = $translateCategory->name;
        }

        // Set two categories different
        $category = Category::find(3);
        $category->name = "Category 2";
        $category->save();

        $categoryNames[config('voyager.multilingual.default')][] = $category->name;

        foreach ($localesAdditional as $locale) {
            $translateCategory = $category->translate($locale);
            $translateCategory->name = "Category " . $locale . " 2";
            $translateCategory->save();

            $categoryNames[$locale][] = $translateCategory->name;
        }

        foreach ($categoryNames as $key => $names) {
            $categoryNames[$key] = array_unique($names);
        }

        foreach (config('voyager.multilingual.locales') as $locale) {
            $this->setAuthorizationHeaders($user, ['X-Localization' => $locale]);

            // Test get categories with removed duplicities
            $response = $this->getJson('/api/v1/categories');

            $response->assertStatus(200);

            // Test translated category names are in response.
            foreach ($categoryNames[$locale] as $name) {
                $response->assertJsonFragment(['label' => $name]);
            }

            $responseData = $response->getData();

            // No more data contained in response.
            $this->assertCount(count($categoryNames[$locale]), $responseData->data);
        }
    }

    /**
     * Test get categories filtred by Continents.
     *
     * @return void
     */
    public function testGetCategoriesFilterByContinents()
    {
        $this->setAuthorizationHeaders();

        $user = $this->lastCreatedUser;

        // Create test data
        $categories = Category::all();

        $localesAdditional = array_diff(config('voyager.multilingual.locales'), [config('voyager.multilingual.default')]);

        // Contains category names grouped by locale and continent_id.
        $categoryNames = [];
        $continentsIDs = [];

        // Reset all category names by continents
        foreach ($categories as $category) {
            $category->name = "Category " . $category->continent_id;
            $category->save();

            $continentsIDs[$category->continent_id] = $category->continent_id;

            $categoryNames[config('voyager.multilingual.default')][$category->continent_id][] = $category->name;

            foreach ($localesAdditional as $locale) {
                $translateCategory = $category->translate($locale);
                $translateCategory->name = "Category " . $locale . " " . $category->continent_id;
                $translateCategory->save();

                $categoryNames[$locale][$category->continent_id][] = $translateCategory->name;
            }
        }

        foreach ($categoryNames as $locale => $categoryNamesByContientns) {
            foreach ($categoryNamesByContientns as $continent_id => $names) {
                $categoryNames[$locale][$continent_id] = array_unique($names);
            }
        }

        foreach (config('voyager.multilingual.locales') as $locale) {
            $this->setAuthorizationHeaders($user, ['X-Localization' => $locale]);

            foreach ($continentsIDs as $continentID) {
                // Test get categories filtered by continents
                $response = $this->getJson('/api/v1/categories?continentId=' . $continentID);

                $response->assertStatus(200);

                // Test translated continents category names are in response.
                foreach ($categoryNames[$locale][$continentID] as $name) {
                    $response->assertJsonFragment(['label' => $name]);
                }

                $responseData = $response->getData();

                // No more data contained in response.
                $this->assertCount(count($categoryNames[$locale][$continentID]), $responseData->data);
            }
        }
    }
}
