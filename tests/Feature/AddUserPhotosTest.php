<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\Utils\UserUtil;
use Tests\Utils\ImageUtil;
use Tests\Utils\TestDataUtil;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\UserPhoto;
use App\UserPoint;

class AddUserPhotosTest extends TestCase
{
    use UserUtil;
    use ImageUtil;
    use TestDataUtil;
    use DatabaseTransactions;

    /**
     * Test without API key.
     *
     * @return void
     */
    public function testAddUserPhotoWithoutAPIKey()
    {
        $response = $this->postJson('/api/v1/map/1/photo');
            
        $response->assertStatus(401);
    }

    /**
     * Test without authentication by JWT.
     *
     * @return void
     */
    public function testAddUserPhotoWithoutLogin()
    {
        $this->setHeaders();

        $response = $this->postJson('/api/v1/map/1/photo');
        
        $response->assertStatus(101);
    }

    /**
     * Test add user photo without image parameter.
     *
     * @return void
     */
    public function testAddUserPhotoMissingImage()
    {
        $this->setAuthorizationHeaders();

        $response = $this->postJson('/api/v1/map/1/photo');
        
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['image'],
            'message'
        ]);
    }

    /**
     * Test add user photo on place that not exists.
     *
     * @return void
     */
    public function testAddUserPhotoToPlaceThatNotExists()
    {
        $this->setAuthorizationHeaders();

        $data = [
            'image' => $this->getTestImageBase64(null, false) // Get random image from tests/images - without 'data:' prefix
        ];

        $response = $this->postJson('/api/v1/map/1111111/photo', $data);
        
        $response->assertStatus(404);
    }

    /**
     * Test valid add user photo on place. First photo.
     *
     * @return void
     */
    public function testAddUserPhotoFirstPhoto()
    {
        // Check if limit of user photos is greated than zero, otherwise test make no sense.
        if (config('awapp.places.points.user-limit-count') > 0)
            return;

        $this->setAuthorizationHeaders();

        Storage::fake(config('voyager.storage.disk'));

        $user = $this->lastCreatedUser;

        // Create test data
        $place = $this->createTestPlaceWithRelativeData();

        $data = [
            'image' => $this->getTestImageBase64(null, false) // Get random image from tests/images - without 'data:' prefix
        ];

        // Test add first photo
        $response = $this->postJson('/api/v1/map/' . $place->id . '/photo', $data);
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'points' => config('awapp.places.points.first-photo'),
            ]
        ]);

        // Get data about lastly added user photo
        $photo = UserPhoto::orderBy('id', 'desc')->first();

        // Test new user photo image exists
        Storage::disk(config('voyager.storage.disk'))->assertExists($photo['image']);

        // Test get points for first photo
        $this->assertDatabaseHas(
            with(new UserPoint)->getTable(), 
            [
                'user_id' => $user->id,
                'place_id' => $place->id,
                'type' => UserPoint::TYPE_FIRST_PHOTO,
                'points' => config('awapp.places.points.first-photo'),
            ]
        );
    }

    /**
     * Test valid add user photo on place. Next photo.
     *
     * @return void
     */
    public function testAddUserPhotoNextPhoto()
    {
        // Check if limit of user photos is greated than one, otherwise test make no sense.
        if (config('awapp.places.points.user-limit-count') > 1)
            return;

        $this->setAuthorizationHeaders();

        Storage::fake(config('voyager.storage.disk'));

        $user = $this->lastCreatedUser;

        // Create test data
        $testData = [
            'userPictures' => [
                [
                    'user_id' => $user->id,
                    'image' => 'test/first.png'
                ]
            ]
        ];
        $place = $this->createTestPlaceWithRelativeData($testData);

        $data = [
            'image' => $this->getTestImageBase64(null, false) // Get random image from tests/images - without 'data:' prefix
        ];



        // Make different first and next photo points 
        $pointsNextPhoto = config('awapp.places.points.next-photo'); 
        config(['awapp.places.points.next-photo' => config('awapp.places.points.first-photo') + 1]); 

        // Test add next photo
        $response = $this->postJson('/api/v1/map/' . $place->id . '/photo', $data);

        // Restore config
        config(['awapp.places.points.next-photo' => $pointsNextPhoto]); 
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'points' => config('awapp.places.points.first-photo') + 1,
            ]
        ]);

        // Get data about lastly added user photo
        $photo = UserPhoto::orderBy('id', 'desc')->first();

        // Test new user photo image exists
        Storage::disk(config('voyager.storage.disk'))->assertExists($photo['image']);


        // Test not get points for next photo
        $this->assertDatabaseMissing(
            with(new UserPoint)->getTable(), 
            [
                'user_id' => $user->id,
                'place_id' => $place->id,
                'type' => UserPoint::TYPE_FIRST_PHOTO,
            ]
        );
    }

    /**
     * Test add user photo on place. Test user limit count of photos can be added to one place.
     *
     * @return void
     */
    public function testAddUserPhotoReachLimit()
    {
        $this->setAuthorizationHeaders();

        Storage::fake(config('voyager.storage.disk'));

        $user = $this->lastCreatedUser;

        // Create test data - add allowed count of user photo to place
        $testData = [
            'userPictures' => []
        ];
        for ($i = 0; $i < config('awapp.places.photos.user-limit-count'); $i++) { 
            $testData['userPictures'][] = [
                'user_id' => $user->id,
                'image' => 'test/' . $i . '.png',
            ];
        }

        $place = $this->createTestPlaceWithRelativeData($testData);

        $data = [
            'image' => $this->getTestImageBase64(null, false) // Get random image from tests/images - without 'data:' prefix
        ];



        // Test add photo after limit is reached
        $response = $this->postJson('/api/v1/map/' . $place->id . '/photo', $data);
        
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['image'],
            'message'
        ]);
    }

    /**
     * Test add user photo on place. Test if user can add photo to second place 
     * after user reach limit count of photos on the first place.
     *
     * @return void
     */
    public function testAddUserPhotoReachLimitAndAddToAnotherPlace()
    {
        $this->setAuthorizationHeaders();

        Storage::fake(config('voyager.storage.disk'));

        $user = $this->lastCreatedUser;

        // Create test data - add allowed count of user photo to place
        $testData = [
            'userPictures' => []
        ];
        for ($i = 0; $i < config('awapp.places.photos.user-limit-count'); $i++) { 
            $testData['userPictures'][] = [
                'user_id' => $user->id,
                'image' => 'test/' . $i . '.png',
            ];
        }

        $place1 = $this->createTestPlaceWithRelativeData($testData);
        $place2 = $this->createTestPlaceWithRelativeData();

        $data = [
            'image' => $this->getTestImageBase64(null, false) // Get random image from tests/images - without 'data:' prefix
        ];



        // Test add photo to second place
        $response = $this->postJson('/api/v1/map/' . $place2->id . '/photo', $data);
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'points' => config('awapp.places.points.first-photo'),
            ]
        ]);

        // Get data about lastly added user photo
        $photo = UserPhoto::orderBy('id', 'desc')->first();

        // Test new user photo image exists
        Storage::disk(config('voyager.storage.disk'))->assertExists($photo['image']);

        // Test get points for first photo to second place
        $this->assertDatabaseHas(
            with(new UserPoint)->getTable(), 
            [
                'user_id' => $user->id,
                'place_id' => $place2->id,
                'type' => UserPoint::TYPE_FIRST_PHOTO,
                'points' => config('awapp.places.points.first-photo'),
            ]
        );
    }
}
