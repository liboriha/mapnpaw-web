<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\Utils\HeadersUtil;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserRegistrationTest extends TestCase
{
    use HeadersUtil;
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRegistrationWithoutAPIKey()
    {
        $response = $this->postJson('/api/v1/account', ['email' => 'test@test.cz', 'password' => '12345678']);
        
        $response->assertStatus(401);
    }

    /**
     * Test valid registration.
     *
     * @return void
     */
    public function testRegistration()
    {
        $this->setHeaders();

        $this->assertDatabaseMissing('users', [
            'email' => 'test@test.cz'
        ]);

        $response = $this->postJson('/api/v1/account', ['email' => 'test@test.cz', 'password' => '12345678']);
        
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => ['token']
        ]);
        $this->assertDatabaseHas('users', [
            'email' => 'test@test.cz'
        ]);
    }

    /**
     * Password must have min lenght 6 characters.
     *
     * @return void
     */
    public function testRegistrationShortPassword()
    {
        $this->setHeaders();

        $response = $this->postJson('/api/v1/account', ['email' => 'test@test.cz', 'password' => '123']);
        
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['password'],
            'message'
        ]);
    }

    /**
     * Test checking email format.
     *
     * @return void
     */
    public function testRegistrationInvalidEmail()
    {
        $this->setHeaders();

        $response = $this->postJson('/api/v1/account', ['email' => 'test', 'password' => '123456']);
        
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['email'],
            'message'
        ]);
    }

    /**
     * Test not allow registration with same email.
     *
     * @return void
     */
    public function testRegistrationWithEmailInDatabase()
    {
        $this->setHeaders();

        $response = $this->postJson('/api/v1/account', ['email' => 'test@test.cz', 'password' => '123456']);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => ['token']
        ]);
        $this->assertDatabaseHas('users', [
            'email' => 'test@test.cz'
        ]);

        $response = $this->postJson('/api/v1/account', ['email' => 'test@test.cz', 'password' => '123456']);

        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['email'],
            'message'
        ]);
    }
}
