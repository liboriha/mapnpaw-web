<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\Utils\UserUtil;
use Tests\Utils\TestDataUtil;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Continent;
use App\Category;

class GetMedalsTest extends TestCase
{
    use UserUtil;
    use TestDataUtil;
    use DatabaseTransactions;

    /**
     * Test without API key.
     *
     * @return void
     */
    public function testGetMedalsWithoutAPIKey()
    {
        $response = $this->getJson('/api/v1/medals');
            
        $response->assertStatus(401);
    }

    /**
     * Test without authentication by JWT.
     *
     * @return void
     */
    public function testGetMedalsWithoutLogin()
    {
        $this->setHeaders();

        $response = $this->getJson('/api/v1/medals');
        
        $response->assertStatus(101);
    }

    /**
     * Test valid get medals. Test without any medals record.
     *
     * @return void
     */
    public function testGetMedalsBasic()
    {
        $this->setAuthorizationHeaders();

        // Create test data
        $this->setMedalsAndFlagsIconAndName(); // Need call before get categories.

        // Test get medals - user has no medals
        $response = $this->getJson('/api/v1/medals');

        $response->assertStatus(200);

        // Prepare data to test
        $dataToTest = [
            'data' => []
        ];

        foreach (Continent::all() as $continent) {
            $tempContinent = [
                'id' => $continent->id,
                'name' => $continent->name,
                'has_continent_icon' => false,
                'medals' => []
            ];

            $dataToTest['data'][] = $tempContinent;
        }

        // Test response to contains all continents with medals
        $response->assertExactJson($dataToTest);
    }

    /**
     * Test valid get medals. Test without any medals record.
     *
     * @return void
     */
    public function testGetMedalsUserHasFewMedals()
    {
        // Create test data
        $this->setMedalsAndFlagsIconAndName(); // Need call before get categories.

        $medalIDs = array_rand(array_flip(Category::all()->pluck('id')->toArray()), 10); // get 10 random medals

        foreach ($medalIDs as $medalID) {
            $place = $this->createTestPlaceWithRelativeData([
                'category_id' => $medalID, // Same as category ID
                'is_top' => true
            ]);
        }

        $testData = [
            'medals' => array_rand(array_flip($medalIDs), 5) // get 5 random medals from 10 random medals
        ];

        $user = $this->createTestUserWithRelativeData($testData);

        // Login test user
        $this->setAuthorizationHeaders($user);


        // Test get medals - user has 10 medals
        $response = $this->getJson('/api/v1/medals');

        $response->assertStatus(200);

        // Prepare data to test
        $dataToTest = [
            'data' => []
        ];

        foreach (Continent::all() as $continent) {
            $tempContinent = [
                'id' => $continent->id,
                'name' => $continent->name,
                'has_continent_icon' => false,
                'medals' => []
            ];

            foreach ($continent->categories as $category) {
                if (in_array($category->id, $medalIDs)) {
                    // category medal images are all filled
                    if (in_array($category->id, $testData['medals'])) {
                        $medal_image = Storage::disk(config('voyager.storage.disk'))->url($category->medal_image_unlocked);
                        $achieved = true;
                    } else {
                        $medal_image = Storage::disk(config('voyager.storage.disk'))->url($category->medal_image_locked);
                        $achieved = false;
                    }

                    $tempContinent['medals'][] = [
                        'id' => $category->id, // Medal ID is category ID
                        'image' => $medal_image,
                        'name' => $category->medal_name,
                        'achieved' => $achieved,
                        'category' => [
                            'id' => $category->id,
                            'label' => $category->name
                        ],
                        'continent' => [
                            'id' => $continent->id,
                            'label' => $continent->name
                        ]
                    ];
                }
            }

            $dataToTest['data'][] = $tempContinent;
        }

        // Test response to contains all continents with medals
        $response->assertExactJson($dataToTest);
    }

    /**
     * Test valid get medals. Test without any medals record with translations.
     *
     * @return void
     */
    public function testGetMedalsUserHasFewMedalsWithTranslations()
    {
        // Create test data
        $this->setMedalsAndFlagsIconAndName(true); // Need call before get categories.

        $medalIDs = array_rand(array_flip(Category::all()->pluck('id')->toArray()), 10); // get 10 random medals

        foreach ($medalIDs as $medalID) {
            $place = $this->createTestPlaceWithRelativeData([
                'category_id' => $medalID, // Same as category ID
                'is_top' => true
            ]);
        }

        $testData = [
            'medals' => array_rand(array_flip($medalIDs), 5) // get 5 random medals from 10 random medals
        ];

        $user = $this->createTestUserWithRelativeData($testData);

        // Test each available locale.
        foreach (config('voyager.multilingual.locales') as $locale) {
            // Login test user
            $this->setAuthorizationHeaders($user, ['X-Localization' => $locale]);


            // Test get medals - user has 10 medals
            $response = $this->getJson('/api/v1/medals');

            $response->assertStatus(200);

            // Prepare data to test
            $dataToTest = [
                'data' => []
            ];

            foreach (Continent::all() as $continent) {
                $continent = $continent->translate($locale);

                $tempContinent = [
                    'id' => $continent->id,
                    'name' => $continent->name,
                    'has_continent_icon' => false,
                    'medals' => []
                ];

                foreach ($continent->categories as $category) {
                    if (in_array($category->id, $medalIDs)) {
                        $category = $category->translate($locale);

                        // category medal images are all filled
                        if (in_array($category->id, $testData['medals'])) {
                            $medal_image = Storage::disk(config('voyager.storage.disk'))->url($category->medal_image_unlocked);
                            $achieved = true;
                        } else {
                            $medal_image = Storage::disk(config('voyager.storage.disk'))->url($category->medal_image_locked);
                            $achieved = false;
                        }

                        $tempContinent['medals'][] = [
                            'id' => $category->id, // Medal ID is category ID
                            'image' => $medal_image,
                            'name' => $category->medal_name,
                            'achieved' => $achieved,
                            'category' => [
                                'id' => $category->id,
                                'label' => $category->name
                            ],
                            'continent' => [
                                'id' => $continent->id,
                                'label' => $continent->name
                            ]
                        ];
                    }
                }

                $dataToTest['data'][] = $tempContinent;
            }

            // Test response to contains all continents with medals
            $response->assertExactJson($dataToTest);
        }

        // Reset application locale
        app()->setLocale(config('app.locale'));
    }
}
