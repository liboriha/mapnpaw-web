<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\Utils\UserUtil;
use Tests\Utils\ImageUtil;
use Tests\Utils\TestDataUtil;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Storage;
use App\Place;
use App\UserPhoto;
use App\UserRating;
use App\UserCheck;
use App\Category;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use App\Http\Helpers\MapHelper;
use Carbon\Carbon;

class GetPlaceDetailTest extends TestCase
{
    use UserUtil;
    use TestDataUtil;
    use DatabaseTransactions;

    /**
     * Build URL parameters
     */
    protected function buildURLParameters($lat, $lng)
    {
        return "?lat=$lat&lng=$lng";
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetPlaceDetailWithoutAPIKey()
    {
        $response = $this->getJson('/api/v1/map/1' . $this->buildURLParameters(0, 0));
            
        $response->assertStatus(401);
    }

    /**
     * Test without authentication by JWT.
     *
     * @return void
     */
    public function testGetPlaceDetailWithoutLogin()
    {
        $this->setHeaders();

        $response = $this->getJson('/api/v1/map/1' . $this->buildURLParameters(0, 0));
        
        $response->assertStatus(101);
    }

    /**
     * Test without lat and lng parameter.
     *
     * @return void
     */
    public function testGetPlaceDetailWithoutLatitudeAndLongitudeParameters()
    {
        $this->setAuthorizationHeaders();

        $response = $this->getJson('/api/v1/map/1');
        
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['lat', 'lng'],
            'message'
        ]);
    }

    /**
     * Test get place that not exists.
     *
     * @return void
     */
    public function testGetPlaceDetailThatNotExists()
    {
        $this->setAuthorizationHeaders();

        $response = $this->getJson('/api/v1/map/1111111' . $this->buildURLParameters(0, 0));
        
        $response->assertStatus(404);
    }

    /**
     * Test valid get place.
     *
     * @return void
     */
    public function testGetPlaceBasic()
    {
        $this->setAuthorizationHeaders();

        // Create test data
        $testData = [
            'name' => 'Test place',
            'gps' => [12.12, 14.55],
            'image' => 'test/place.png',
            'description' => 'Description of place',
            'address' => 'Somewhere in the world'
        ];
        $place = $this->createTestPlaceWithRelativeData($testData);



        // Test get place
        $response = $this->getJson('/api/v1/map/' . $place->id . $this->buildURLParameters(0, 0));
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'name' => $testData['name'],
                'lat' => $testData['gps'][0],
                'lng' => $testData['gps'][1],
                'description' => $testData['description'],
                'address' => $testData['address'],
                'checkedIn' => false,
                'lastCheckIn' => 0,
                'canCheckIn' => false,
                'userPictures' => [],
                'rating' => []
            ]
        ]);

        // Separately test image - it contains http at the beggining (test suffix).
        $responseData = $response->getData();
        $this->assertStringEndsWith($testData['image'], $responseData->data->image);
    }

    /**
     * Test valid get place.
     *
     * @return void
     */
    public function testGetPlaceUserPictures()
    {
        $this->setAuthorizationHeaders([
            'email' => 'test@test.cz',
            'firstname' => 'Pavel',
            'lastname' => 'Nový',
            'nickname' => 'PN',
            'visibility' => 'NAME',
        ]);

        $user = $this->lastCreatedUser;

        // Create test data
        $testData1 = [
            'userPictures' => [
                [
                    'user_id' => $user->id,
                    'image' => 'test/second.png',
                    'created_at' => Carbon::now() // newer photo
                ],
                [
                    'user_id' => $user->id,
                    'image' => 'test/first.png',
                    'created_at' => Carbon::now()->subDays(1) // older photo
                ]
            ]
        ];
        $place1 = $this->createTestPlaceWithRelativeData($testData1);

        // Create second test data
        $testData2 = [
            'userPictures' => [
                [
                    'user_id' => $user->id,
                    'image' => 'test/third.png',
                    'created_at' => Carbon::now()->subDays(1) // older photo
                ],
                [
                    'user_id' => $user->id,
                    'image' => 'test/fourth.png',
                    'created_at' => Carbon::now() // newer photo
                ]
            ]
        ];
        $place2 = $this->createTestPlaceWithRelativeData($testData2);



        // Test first place
        $response = $this->getJson('/api/v1/map/' . $place1->id . $this->buildURLParameters(0, 0));

        // Prepare data to validation
        $profilePicture = Storage::disk(config('voyager.storage.disk'))->url($user->avatar);
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'userPictures' => [
                    [
                        'displayName' => $user->firstname . ' ' . $user->lastname,
                        'profilePicture' => $profilePicture
                    ],
                    [
                        'displayName' => $user->firstname . ' ' . $user->lastname,
                        'profilePicture' => $profilePicture
                    ],
                ]
            ]
        ]);

        // Separately test image - it contains http at the beggining (test suffix).
        $responseData = $response->getData();
        $this->assertCount(2, $responseData->data->userPictures);
        // Sorted by created_at desc. Newest is first.
        $this->assertStringEndsWith($testData1['userPictures'][0]['image'], $responseData->data->userPictures[0]->image);
        $this->assertStringEndsWith($testData1['userPictures'][1]['image'], $responseData->data->userPictures[1]->image);



        // Change visibility of user to nickname
        $user->visibility = "NICKNAME";
        $user->save();



        // Test second place - another data - user with nickname
        $response = $this->getJson('/api/v1/map/' . $place2->id . $this->buildURLParameters(0, 0));
        
        $response->assertStatus(200);

        // Separately test image - it contains http at the beggining (test suffix).
        $responseData = $response->getData();
        $this->assertCount(2, $responseData->data->userPictures);
        // Sorted by created_at desc. Newest is first.
        $this->assertStringEndsWith($testData2['userPictures'][1]['image'], $responseData->data->userPictures[0]->image);
        $this->assertStringEndsWith($testData2['userPictures'][0]['image'], $responseData->data->userPictures[1]->image);

        $response->assertJson([
            'data' => [
                'userPictures' => [
                    [
                        'displayName' => $user->nickname,
                        'profilePicture' => $profilePicture
                    ],
                    [
                        'displayName' => $user->nickname,
                        'profilePicture' => $profilePicture
                    ],
                ]
            ]
        ]);
    }

    /**
     * Test valid get place.
     *
     * @return void
     */
    public function testGetPlaceRatings()
    {
        $this->setAuthorizationHeaders([
            'email' => 'test@test.cz',
            'firstname' => 'Pavel',
            'lastname' => 'Nový',
            'nickname' => 'PN',
            'visibility' => 'NAME',
        ]);

        $user1 = $this->lastCreatedUser;

        $user2 = $this->createUser([
            'email' => 'test2@test.cz',
            'firstname' => 'Anna',
            'lastname' => 'Krátká',
            'nickname' => 'AK',
            'visibility' => 'NICKNAME',
        ]);

        // Create test data
        $testData1 = [
            'name' => 'First',
            'rating' => [
                [
                    'user_id' => $user1->id,
                    'stars' => '5',
                    'rating' => 'Best',
                    'created_at' => Carbon::now() // newer rating
                ],
                [
                    'user_id' => $user2->id,
                    'stars' => '4',
                    'rating' => 'Normal',
                    'created_at' => Carbon::now()->subDays(1) // older rating
                ]
            ]
        ];
        $place1 = $this->createTestPlaceWithRelativeData($testData1);

        // Create second test data
        $testData2 = [
            'name' => 'Second',
            'rating' => [
                [
                    'user_id' => $user1->id,
                    'rating' => 'Terrible',
                    'created_at' => Carbon::now()->subDays(1) // older rating
                ],
                [
                    'user_id' => $user2->id,
                    'stars' => '3.5',
                    'created_at' => Carbon::now() // newer rating
                ]
            ]
        ];
        $place2 = $this->createTestPlaceWithRelativeData($testData2);



        // Test first place
        $response = $this->getJson('/api/v1/map/' . $place1->id . $this->buildURLParameters(0, 0));

        // Prepare data to validation
        $profilePicture1 = Storage::disk(config('voyager.storage.disk'))->url($user1->avatar);
        $profilePicture2 = Storage::disk(config('voyager.storage.disk'))->url($user2->avatar);
        
        $response->assertStatus(200);

        // Sorted by created_at desc. Newest is first.
        $response->assertJson([
            'data' => [
                'rating' => [
                    [
                        'userId' => $testData1['rating'][0]['user_id'],
                        'displayName' => $user1->firstname . ' ' . $user1->lastname,
                        'profilePicture' => $profilePicture1,
                        'stars' => $testData1['rating'][0]['stars'],
                        'text' => $testData1['rating'][0]['rating'],
                        'timestamp' => $testData1['rating'][0]['created_at']->timestamp
                    ],
                    [
                        'userId' => $testData1['rating'][1]['user_id'],
                        'displayName' => $user2->nickname,
                        'profilePicture' => $profilePicture2,
                        'stars' => $testData1['rating'][1]['stars'],
                        'text' => $testData1['rating'][1]['rating'],
                        'timestamp' => $testData1['rating'][1]['created_at']->timestamp
                    ],
                ]
            ]
        ]);



        // Change visibility of user to nickname
        $user1->visibility = "NICKNAME";
        $user1->save();



        // Test second place - another data - user with nickname
        $response = $this->getJson('/api/v1/map/' . $place2->id . $this->buildURLParameters(0, 0));
        
        $response->assertStatus(200);

        // Sorted by created_at desc. Newest is first.
        $response->assertJson([
            'data' => [
                'rating' => [
                    [
                        'userId' => $testData2['rating'][1]['user_id'],
                        'displayName' => $user2->nickname,
                        'profilePicture' => $profilePicture2,
                        'stars' => $testData2['rating'][1]['stars'],
                        'text' => null,
                        'timestamp' => $testData2['rating'][1]['created_at']->timestamp
                    ],
                    [
                        'userId' => $testData2['rating'][0]['user_id'],
                        'displayName' => $user1->nickname,
                        'profilePicture' => $profilePicture1,
                        'stars' => null,
                        'text' => $testData2['rating'][0]['rating'],
                        'timestamp' => $testData2['rating'][0]['created_at']->timestamp
                    ],
                ]
            ]
        ]);
    }

    /**
     * Test valid get place.
     *
     * @return void
     */
    public function testGetPlaceCheckInStatus()
    {
        $this->setAuthorizationHeaders();

        $user = $this->lastCreatedUser;

        // Create test data
        $testData1 = [
            'name' => 'First',
            'checks' => []
        ];
        $place1 = $this->createTestPlaceWithRelativeData($testData1);

        // Create second test data
        $testData2 = [
            'name' => 'Second',
            'checks' => [
                [
                    'user_id' => $user->id,
                    'created_at' => '1500005000'
                ],
                [
                    'user_id' => $user->id,
                    'created_at' => '1500000000'
                ]
            ]
        ];
        $place2 = $this->createTestPlaceWithRelativeData($testData2);



        // Test first place - no checkin
        $response = $this->getJson('/api/v1/map/' . $place1->id . $this->buildURLParameters(0, 0));
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'checkedIn' => false,
                'lastCheckIn' => 0
            ]
        ]);



        // Test second place - has checkin
        $response = $this->getJson('/api/v1/map/' . $place2->id . $this->buildURLParameters(0, 0));
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'checkedIn' => true,
                'lastCheckIn' => 1500005000
            ]
        ]);
    }

    /**
     * Test valid get place.
     *
     * @return void
     */
    public function testGetPlaceCanCheckInRadius()
    {
        $this->setAuthorizationHeaders();

        $user = $this->lastCreatedUser;

        // Create test data (default radius)
        $testData1 = [
            'name' => 'Default radius',
            'gps' => [12, 14]
        ];
        $place1 = $this->createTestPlaceWithRelativeData($testData1);

        // Create second test data (place radius)
        $testData2 = [
            'name' => 'Place radius',
            'gps' => [34, -12],
            'radius' => '10000' // 10 km
        ];
        $place2 = $this->createTestPlaceWithRelativeData($testData2);

        // Create third test data (category radius)
        $testData3 = [
            'name' => 'Category radius',
            'category_id' => 5,
            'gps' => [-12, 58]
        ];
        $place3 = $this->createTestPlaceWithRelativeData($testData3);

        $category3 = Category::find(5);
        $category3->radius = 12000; // 12 km
        $category3->save();



        // Prepare GPS data to test first place (default radius)
        $default_radius = (float) config('awapp.places.defaults.radius');
        $lat_too_far = $testData1['gps'][0] + (($default_radius + 0.001 * $default_radius) / MapHelper::EARTH_RADIUS) * (180 / pi());
        $lat_OK = $testData1['gps'][0] + (($default_radius - 0.001 * $default_radius) / MapHelper::EARTH_RADIUS) * (180 / pi());



        // Test first place (default radius) - can't checkin
        $response = $this->getJson('/api/v1/map/' . $place1->id . $this->buildURLParameters($lat_too_far, $testData1['gps'][1]));
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'canCheckIn' => false
            ]
        ]);



        // Test first place (default radius) - can checkin
        $response = $this->getJson('/api/v1/map/' . $place1->id . $this->buildURLParameters($lat_OK, $testData1['gps'][1]));
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'canCheckIn' => true
            ]
        ]);



        // Prepare GPS data to test second place (place radius)
        $lat_too_far = $testData2['gps'][0] + (($testData2['radius'] + 0.001 * $testData2['radius']) / MapHelper::EARTH_RADIUS) * (180 / pi());
        $lat_OK = $testData2['gps'][0] + (($testData2['radius'] - 0.001 * $testData2['radius']) / MapHelper::EARTH_RADIUS) * (180 / pi());



        // Test second place (place radius) - can't checkin
        $response = $this->getJson('/api/v1/map/' . $place2->id . $this->buildURLParameters($lat_too_far, $testData2['gps'][1]));
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'canCheckIn' => false
            ]
        ]);



        // Test second place (place radius) - can checkin
        $response = $this->getJson('/api/v1/map/' . $place2->id . $this->buildURLParameters($lat_OK, $testData2['gps'][1]));
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'canCheckIn' => true
            ]
        ]);




        // Prepare GPS data to test second place (category radius)
        $lat_too_far = $testData3['gps'][0] + (($category3->radius + 0.001 * $category3->radius) / MapHelper::EARTH_RADIUS) * (180 / pi());
        $lat_OK = $testData3['gps'][0] + (($category3->radius - 0.001 * $category3->radius) / MapHelper::EARTH_RADIUS) * (180 / pi());



        // Test second place (category radius) - can't checkin
        $response = $this->getJson('/api/v1/map/' . $place3->id . $this->buildURLParameters($lat_too_far, $testData3['gps'][1]));
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'canCheckIn' => false
            ]
        ]);



        // Test second place (category radius) - can checkin
        $response = $this->getJson('/api/v1/map/' . $place3->id . $this->buildURLParameters($lat_OK, $testData3['gps'][1]));
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'canCheckIn' => true
            ]
        ]);
    }

    /**
     * Test valid get place.
     *
     * @return void
     */
    public function testGetPlaceCanCheckInWaitTime()
    {
        $this->setAuthorizationHeaders();

        $user = $this->lastCreatedUser;

        // Create test data
        $testData1 = [
            'name' => 'First',
            'gps' => [12, 14],
            'checks' => [
                [
                    'user_id' => $user->id,
                    // default wait time -1 day - can't checkin
                    'created_at' => Carbon::now()->subDays(config('awapp.places.defaults.wait-for-next') - 1)
                ]
            ]
        ];
        $place1 = $this->createTestPlaceWithRelativeData($testData1);

        // Create second test data
        $testData2 = [
            'name' => 'Second',
            'gps' => [12, 14],
            'checks' => [
                [
                    'user_id' => $user->id,
                    // default wait time +1 day - can checkin
                    'created_at' => Carbon::now()->subDays(config('awapp.places.defaults.wait-for-next') + 1)
                ]
            ]
        ];
        $place2 = $this->createTestPlaceWithRelativeData($testData2);



        // Test first place - can't checkin
        $response = $this->getJson('/api/v1/map/' . $place1->id . $this->buildURLParameters(12, 14));
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'canCheckIn' => false
            ]
        ]);



        // Test second place - can checkin
        $response = $this->getJson('/api/v1/map/' . $place2->id . $this->buildURLParameters(12, 14));
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'canCheckIn' => true
            ]
        ]);
    }

    /**
     * Test valid get place.
     *
     * @return void
     */
    public function testGetPlaceSameGPSLocation()
    {
        $this->setAuthorizationHeaders();

        $user = $this->lastCreatedUser;

        // Create test data
        $testData = [
            'name' => 'First',
            'gps' => [12, 14]
        ];
        $place = $this->createTestPlaceWithRelativeData($testData);



        // Test first place - can't checkin
        $response = $this->getJson('/api/v1/map/' . $place->id . $this->buildURLParameters(12, 14));
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'canCheckIn' => true
            ]
        ]);
    }

    /**
     * Test valid get place.
     *
     * @return void
     */
    public function testGetPlaceTestImage()
    {
        $this->setAuthorizationHeaders();

        $user = $this->lastCreatedUser;

        // Create test data
        $testData1 = [
            'name' => 'First'
        ];
        $place1 = $this->createTestPlaceWithRelativeData($testData1);

        // Create second test data
        $testData2 = [
            'name' => 'Second',
            'image' => 'test/place.png',
            'userPictures' => [
                [
                    'user_id' => $user->id,
                    'image' => 'test/userPicture.png'
                ]
            ]
        ];
        $place2 = $this->createTestPlaceWithRelativeData($testData2);

        // Create third test data
        $testData3 = [
            'name' => 'Three',
            'userPictures' => [
                [
                    'user_id' => $user->id,
                    'image' => 'test/userPicture.png'
                ],
                [
                    'user_id' => $user->id,
                    'image' => 'test/userPicture2.png'
                ]
            ]
        ];
        $place3 = $this->createTestPlaceWithRelativeData($testData3);



        // Test first place - no image
        $response = $this->getJson('/api/v1/map/' . $place1->id . $this->buildURLParameters(0, 0));
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'image' => null
            ]
        ]);



        // Test second place - place image
        $response = $this->getJson('/api/v1/map/' . $place2->id . $this->buildURLParameters(0, 0));
        
        $response->assertStatus(200);

        // Separately test image - it contains http at the beggining (test suffix).
        $responseData = $response->getData();
        $this->assertStringEndsWith($testData2['image'], $responseData->data->image);




        // Test third place - first user picture image
        $response = $this->getJson('/api/v1/map/' . $place3->id . $this->buildURLParameters(0, 0));
        
        $response->assertStatus(200);

        // Separately test image - it contains http at the beggining (test suffix).
        $responseData = $response->getData();
        $this->assertStringEndsWith($testData3['userPictures'][0]['image'], $responseData->data->image);
    }
}
