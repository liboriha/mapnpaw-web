<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\Utils\HeadersUtil;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;

class UserLoginTest extends TestCase
{
    use HeadersUtil;
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLoginWithoutAPIKey()
    {
        $response = $this->postJson('/api/v1/token', ['email' => 'test@test.cz', 'password' => '12345678']);
        
        $response->assertStatus(401);
    }

    /**
     * Test valid login.
     *
     * @return void
     */
    public function testLogin()
    {
        $this->setHeaders();

        User::create([
            'email' => 'test@test.cz',
            'password' => bcrypt('12345678')
        ]);

        $response = $this->postJson('/api/v1/token', ['email' => 'test@test.cz', 'password' => '12345678']);
        
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => ['token']
        ]);
    }

    /**
     * Password must have min lenght 6 characters.
     *
     * @return void
     */
    public function testLoginWrongRequest()
    {
        $this->setHeaders();

        $response = $this->postJson('/api/v1/token', ['email' => 'test@test.cz']);
        
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['password'],
            'message'
        ]);

        $response = $this->postJson('/api/v1/token', ['password' => '123']);
        
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['email'],
            'message'
        ]);
    }

    /**
     * Test checking email format.
     *
     * @return void
     */
    public function testLoginFailed()
    {
        $this->setHeaders();

        $response = $this->postJson('/api/v1/token', ['email' => 'test@test.cz', 'password' => '123456']);
        
        $response->assertStatus(401);
    }
}
