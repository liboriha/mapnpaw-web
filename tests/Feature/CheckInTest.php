<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\Utils\UserUtil;
use Tests\Utils\TestDataUtil;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
//use Illuminate\Support\Facades\Storage;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use App\Place;
use App\Continent;
use App\Category;
use App\UserCheck;
use App\UserFlag;
use App\UserMedal;
use App\UserPoint;

class CheckInTest extends TestCase
{
    use UserUtil;
    use TestDataUtil;
    use DatabaseTransactions;

    /**
     * Test without API key.
     *
     * @return void
     */
    public function testCheckInWithoutAPIKey()
    {
        $response = $this->postJson('/api/v1/map/1/check-in');
            
        $response->assertStatus(401);
    }

    /**
     * Test without authentication by JWT.
     *
     * @return void
     */
    public function testCheckInWithoutLogin()
    {
        $this->setHeaders();

        $response = $this->postJson('/api/v1/map/1/check-in');
        
        $response->assertStatus(101);
    }

    /**
     * Test check-in on place that not exists.
     *
     * @return void
     */
    public function testCheckInThatNotExists()
    {
        $this->setAuthorizationHeaders();

        $response = $this->postJson('/api/v1/map/1111111/check-in');
        
        $response->assertStatus(404);
    }

    /**
     * Test valid check-in on place.
     *
     * @return void
     */
    public function testCheckInBasic()
    {
        $this->setAuthorizationHeaders();

        $user = $this->lastCreatedUser;

        // Create test data
        $testData = [
            [
                'name' => 'Test place',
            ]
        ];
        list($placeID) = $this->createTestPlaces($testData);

        // Test check-in place
        $response = $this->postJson('/api/v1/map/' . $placeID . '/check-in');
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'points' => config('awapp.places.points.first-checkin'),
                'top' => null,
                'flag' => null,
                'message' => [
                    'flag' => 'check',
                    'title' => 'Check-in proběhl úspěšně, přičetli jsme vám 1 bod.',
                    'subtitle' => ''
                ]
            ]
        ]);

        // Test checkin is saved in database
        $this->assertDatabaseHas(
            with(new UserCheck)->getTable(), 
            [
                'user_id' => $user->id,
                'place_id' => $placeID,
            ]
        );

        // Test get points for first checkin
        $this->assertDatabaseHas(
            with(new UserPoint)->getTable(), 
            [
                'user_id' => $user->id,
                'place_id' => $placeID,
                'type' => UserPoint::TYPE_CHECKIN,
                'points' => config('awapp.places.points.first-checkin'),
            ]
        );
    }

    /**
     * Test valid check-in on place.
     *
     * @return void
     */
    public function testCheckInAndNextCheckIn()
    {
        $this->setAuthorizationHeaders();
        
        $user = $this->lastCreatedUser;

        // Create test data
        $testData = [
            [
                'name' => 'Test place',
            ]
        ];

        list($placeID) = $this->createTestPlaces($testData);



        // Test first check-in place
        $response = $this->postJson('/api/v1/map/' . $placeID . '/check-in');
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'points' => config('awapp.places.points.first-checkin'),
            ]
        ]);

        // Test get points for first checkin
        $this->assertDatabaseHas(
            with(new UserPoint)->getTable(), 
            [
                'user_id' => $user->id,
                'place_id' => $placeID,
                'type' => UserPoint::TYPE_CHECKIN,
                'points' => config('awapp.places.points.first-checkin'),
            ]
        );



        $customValueForNextCheckin = config('awapp.places.points.first-checkin') + 1;

        // Set config - allow next check-in immidiatelly. - Now not tested, but for sure if future.
        $waitForNextConfig = config('awapp.places.defaults.wait-for-next'); 
        $pointsNextCheckIn = config('awapp.places.points.next-checkin'); 
        config([
            'awapp.places.defaults.wait-for-next' => 0,
            'awapp.places.points.next-checkin' => $customValueForNextCheckin
        ]); 

        // Test second check-in place
        $response = $this->postJson('/api/v1/map/' . $placeID . '/check-in');

        // Restore config
        config([
            'awapp.places.defaults.wait-for-next' => $waitForNextConfig,
            'awapp.places.points.next-checkin' => $pointsNextCheckIn
        ]); 
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'points' => $customValueForNextCheckin,
            ]
        ]);

        // Test get points for second checkin
        $this->assertDatabaseHas(
            with(new UserPoint)->getTable(), 
            [
                'user_id' => $user->id,
                'place_id' => $placeID,
                'type' => UserPoint::TYPE_CHECKIN,
                'points' => $customValueForNextCheckin,
            ]
        );



        $checkinCount = UserCheck::where('user_id', $user->id)->where('place_id', $placeID)->count();
        // Test if added two checkin to database
        $this->assertTrue($checkinCount == 2);
    }

    /**
     * Test valid check-in on places with flag.
     *
     * @return void
     */
    public function testCheckInWithFlag()
    {
        $this->setAuthorizationHeaders();
        
        $user = $this->lastCreatedUser;

        // Create test data
        $this->setMedalsAndFlagsIconAndName();

        $testData = [
            [
                'name' => 'Test place',
                'category_id' => 1,
                'is_flag' => true
            ],
            [
                'name' => 'Test place',
                'category_id' => 15,
                'is_flag' => true
            ]
        ];

        list($placeID_1, $placeID_2) = $this->createTestPlaces($testData);

        $category_1 = Category::find($testData[0]['category_id']);
        $category_2 = Category::find($testData[1]['category_id']);
        $continent_1 = Continent::find($category_1->continent_id);
        $continent_2 = Continent::find($category_2->continent_id);



        // Test check-in on first place
        $response = $this->postJson('/api/v1/map/' . $placeID_1 . '/check-in');
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'points' => config('awapp.places.points.first-checkin'),
                'flag' => [
                    'id' => $category_1->id, // Same as category ID - flag is with categories, not an entity
                    'name' => 'Flag ' . $continent_1->id . '-' . $category_1->id,
                    'achieved' => true,
                    'continent' => [
                        'id' => $continent_1->id,
                        'label' => $continent_1->name
                    ],
                    'category' => [
                        //'id' => $category_1->id,
                        'label' => $category_1->name
                    ]
                ],
                'message' => [
                    'flag' => 'flag',
                    'title' => 'Check-in proběhl úspěšně, přičetli jsme vám 1 bod.',
                    'subtitle' => 'Získáváte vlajku v kategorii '
                        . $category_1->name . ' na kontinentu '
                        . $continent_1->name
                ]
            ]
        ]);

        // Separately test image - it contains http at the beggining (test suffix).
        $responseData = $response->getData();
        $this->assertStringEndsWith(
            "test/flag_" . $continent_1->id . "_" . $category_1->id . "_unlocked.png", 
            $responseData->data->flag->image
        );

        // Test user flag is saved in database
        $this->assertDatabaseHas(
            with(new UserFlag)->getTable(), 
            [
                'user_id' => $user->id,
                'place_id' => $placeID_1,
            ]
        );



        // Test check-in on second place
        $response = $this->postJson('/api/v1/map/' . $placeID_2 . '/check-in');
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'points' => config('awapp.places.points.first-checkin'),
                'flag' => [
                    'id' => $category_2->id, // Same as category ID - flag is with categories, not an entity
                    'name' => 'Flag ' . $continent_2->id . '-' . $category_2->id,
                    'achieved' => true,
                    'continent' => [
                        'id' => $continent_2->id,
                        'label' => $continent_2->name
                    ],
                    'category' => [
                        //'id' => $category_2->id,
                        'label' => $category_2->name
                    ]
                ],
                'message' => [
                    'flag' => 'flag',
                    'title' => 'Check-in proběhl úspěšně, přičetli jsme vám 1 bod.',
                    'subtitle' => 'Získáváte vlajku v kategorii '
                        . $category_2->name . ' na kontinentu '
                        . $continent_2->name
                ]
            ]
        ]);

        // Separately test image - it contains http at the beggining (test suffix).
        $responseData = $response->getData();
        $this->assertStringEndsWith(
            "test/flag_" . $continent_2->id . "_" . $category_2->id . "_unlocked.png", 
            $responseData->data->flag->image
        );

        // Test user flag is saved in database
        $this->assertDatabaseHas(
            with(new UserFlag)->getTable(), 
            [
                'user_id' => $user->id,
                'place_id' => $placeID_2,
            ]
        );
    }



    /**
     * Test valid check-in on places that is TOP.
     *
     * @return void
     */
    public function testCheckInWithTOP()
    {
        $this->setAuthorizationHeaders();
        
        $user = $this->lastCreatedUser;

        // Create test data
        $this->setMedalsAndFlagsIconAndName();

        $testData = [];
        for ($i=0; $i < config('awapp.places.medals.TOP-get-medal-count'); $i++) { 
            $testData[] = [
                'name' => 'Test place ' . $i,
                'is_top' => true
            ];
        }

        $placesIDs = $this->createTestPlaces($testData);

        $category = Category::find(1);
        $continent = Continent::find($category->continent_id);

        // Create additional test data (test if API use only right test data)
        $additionalTestData = [
            [
                'name' => 'Same category but not top',
                'category_id' => 1,
                'is_top' => false
            ],
            [
                'name' => 'Different category but top',
                'category_id' => 2,
                'is_top' => true
            ]
        ];

        $this->createTestPlaces($additionalTestData);


        if (config('awapp.places.medals.TOP-get-medal-count') > 1) {
            // Test not get medal, but has medal record

            for ($i=0; $i < config('awapp.places.medals.TOP-get-medal-count') - 1; $i++) {

                // Test check-in on top place
                $response = $this->postJson('/api/v1/map/' . $placesIDs[$i] . '/check-in');
                
                $response->assertStatus(200);

                $rem = (config('awapp.places.medals.TOP-get-medal-count') - $i - 1);

                $response->assertJson([
                    'data' => [
                        'points' => config('awapp.places.points.first-checkin'),
                        'top' => [
                            'topPointsRemaining' => config('awapp.places.medals.TOP-get-medal-count') - $i - 1,
                            'medal' => [
                                'id' => $category->id, // Same as category ID - medals is with categories, not an entity
                                'name' => 'Medal ' . $continent->id . '-' . $category->id,
                                'achieved' => false, // Still not achieved - we collect TOP places 
                                'continent' => [
                                    'id' => $continent->id,
                                    'label' => $continent->name
                                ],
                                'category' => [
                                    //'id' => $category->id,
                                    'label' => $category->name
                                ]
                            ]
                        ],
                        'message' => [
                            'flag' => 'check',
                            'title' => 'Check-in proběhl úspěšně, přičetli jsme vám 1 bod.',
                            'subtitle' => 'Ten je zároveň TOP bodem v kategorii '
                                . $category->name . ' na kontinentu ' . $continent->name
                                . '. K získání medaile v této kategorii vám chybí navštívit ještě '
                                . trans_choice('checkin-messages.top-points', $rem, ['num' => $rem]) . '.'
                        ]
                    ]
                ]);

                // Separately test image - it contains http at the beggining (test suffix).
                $responseData = $response->getData();
                $this->assertStringEndsWith(
                    // locked image, medal still not achieved
                    "test/medal_" . $continent->id . "_" . $category->id . "_locked.png", 
                    $responseData->data->top->medal->image
                );

                // Test user flag is saved in database
                $this->assertDatabaseMissing(
                    with(new UserMedal)->getTable(), 
                    [
                        'user_id' => $user->id,
                        'category_id' => $category->id,
                    ]
                );
            }
        }

        $lastIndex = config('awapp.places.medals.TOP-get-medal-count') - 1;

        // Test check-in on last top place
        $response = $this->postJson('/api/v1/map/' . $placesIDs[$lastIndex] . '/check-in');
        
        $response->assertStatus(200);

        $response->assertJson([
            'data' => [
                'points' => config('awapp.places.points.first-checkin'),
                'top' => [
                    'topPointsRemaining' => config('awapp.places.medals.TOP-get-medal-count') - $lastIndex - 1,
                    'medal' => [
                        'id' => $category->id, // Same as category ID - medals is with categories, not an entity
                        'name' => 'Medal ' . $continent->id . '-' . $category->id,
                        'achieved' => true, // Achieved - we collect all TOP places 
                        'continent' => [
                            'id' => $continent->id,
                            'label' => $continent->name
                        ],
                        'category' => [
                            //'id' => $category->id,
                            'label' => $category->name
                        ]
                    ]
                ],
                'message' => [
                    'flag' => 'medal',
                    'title' => 'Check-in proběhl úspěšně, přičetli jsme vám 1 bod.',
                    'subtitle' => 'Získáváte medaili za TOP 10 bodů v kategorii '
                        . $category->name . ' na kontinentu '
                        . $continent->name
                ]
            ]
        ]);

        // Separately test image - it contains http at the beggining (test suffix).
        $responseData = $response->getData();
        $this->assertStringEndsWith(
            // unlocked image, medal is achieved
            "test/medal_" . $continent->id . "_" . $category->id . "_unlocked.png", 
            $responseData->data->top->medal->image
        );

        // Test user medal is saved in database
        $this->assertDatabaseHas(
            with(new UserMedal)->getTable(), 
            [
                'user_id' => $user->id,
                'category_id' => $category->id,
            ]
        );

        // Get data about lastly added user medal
        $userMedal = UserMedal::orderBy('id', 'desc')->first();

        // Test user medal has related all achieved top places
        foreach ($placesIDs as $placesID) {
            $this->assertDatabaseHas(
                'user_medals_places', 
                [
                    'user_medal_id' => $userMedal->id,
                    'place_id' => $placesID,
                ]
            );
        }
    }
}
