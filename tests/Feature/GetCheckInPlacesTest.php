<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\Utils\UserUtil;
use Tests\Utils\TestDataUtil;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use App\UserCheck;
use Carbon\Carbon;

class GetCheckInPlacesTest extends TestCase
{
    use UserUtil;
    use TestDataUtil;
    use DatabaseTransactions;

    /**
     * Test without API key.
     *
     * @return void
     */
    public function testGetCheckInPlacesWithoutAPIKey()
    {
        $response = $this->getJson('/api/v1/check-ins');
            
        $response->assertStatus(401);
    }

    /**
     * Test without authentication by JWT.
     *
     * @return void
     */
    public function testGetCheckInPlacesWithoutLogin()
    {
        $this->setHeaders();

        $response = $this->getJson('/api/v1/check-ins');
        
        $response->assertStatus(101);
    }

    /**
     * Test valid get check-in places.
     *
     * @return void
     */
    public function testGetCheckInPlacesBasic()
    {
        $this->setAuthorizationHeaders();

        $user = $this->lastCreatedUser;

        // Create test data
        $testData = [
            'name' => 'Test place',
            'gps' => [10, 20],
            'image' => 'test/test.png',
            'checks' => [
                [
                    'user_id' => $user->id,
                    'created_at' => Carbon::now()
                ]
            ]
        ];

        $place = $this->createTestPlaceWithRelativeData($testData);

        // Test get checkin
        $response = $this->getJson('/api/v1/check-ins');

        $response->assertStatus(200);

        // Prepare place image to test
        $place_image = Storage::disk(config('voyager.storage.disk'))->url($place->image);

        // Test response to contains all flags and medals and points
        $response->assertJson([
            'data' => [
                [
                    'timestamp' => $testData['checks'][0]['created_at']->timestamp,
                    'point' => [
                        'id' => $place->id,
                        'lat' => $place->gps->getLat(),
                        'lng' => $place->gps->getLng(),
                        'name' => $place->name,
                        'image' => $place_image,
                        'checkedIn' => true
                    ]
                ]
            ]
        ]);
    }

    /**
     * Test valid get check-in places. More places.
     *
     * @return void
     */
    public function testGetCheckInPlacesMorePlaces()
    {
        $this->setAuthorizationHeaders();

        $user = $this->lastCreatedUser;

        // Create test data
        $places = [];

        // Generates random days, if date is same we cannot guarantee order
        $randomDays = array_rand(range(0, 100), 10); 

        for ($i = 0; $i < 10; $i++) { 
            // Generate random test data
            $testData = [
                'name' => str_random(20),
                'gps' => [rand(0, 50), rand(0, 50)],
                'image' => str_random(5) . '/' . str_random(5) . '.png',
                'checks' => [
                    [
                        'user_id' => $user->id,
                        'created_at' => Carbon::now()->subDays($randomDays[$i])
                    ]
                ]
            ];

            $place = $this->createTestPlaceWithRelativeData($testData);

            $place->testData = $testData;

            $places[] = $place;
        }

        // Sort places by checkins from newest.
        $places = with(new Collection($places))->sortByDesc('testData.checks.*.created_at');


        // Test get checkins
        $response = $this->getJson('/api/v1/check-ins');

        $response->assertStatus(200);

        // Prepare data to test
        $dataToTest = [
            'data' => []
        ];

        foreach ($places as $key => $place) {
            $place_image = Storage::disk(config('voyager.storage.disk'))->url($place->image);

            $dataToTest['data'][] = [
                'timestamp' => $place->testData['checks'][0]['created_at']->timestamp,
                'point' => [
                    'id' => $place->id,
                    'lat' => $place->gps->getLat(),
                    'lng' => $place->gps->getLng(),
                    'name' => $place->name,
                    'image' => $place_image,
                    'checkedIn' => true
                ]
            ];
        }

        // Test response to contains all checkins of user sorted from newest.
        $response->assertJson($dataToTest);
    }

    /**
     * Test valid get check-in places. More check-in on one places
     *
     * @return void
     */
    public function testGetCheckInPlacesMoreCheckInOnOnePlace()
    {
        $this->setAuthorizationHeaders();

        $user = $this->lastCreatedUser;

        // Create test data
        $testData1 = [
            'name' => 'Test place',
            'gps' => [10, 20],
            'image' => 'test/test.png',
            'checks' => [
                [
                    'user_id' => $user->id,
                    'created_at' => Carbon::now()->subDays(4)
                ],
                [
                    'user_id' => $user->id,
                    'created_at' => Carbon::now()->subDays(5)
                ],
                [
                    'user_id' => $user->id,
                    'created_at' => Carbon::now()->subDays(3) // Newest checkin
                ]
            ]
        ];

        $place1 = $this->createTestPlaceWithRelativeData($testData1);

        // Newest checkin on place
        $testData2 = [
            'name' => 'Test place 2',
            'gps' => [15, 22],
            'image' => 'test/test2.png',
            'checks' => [
                [
                    'user_id' => $user->id,
                    'created_at' => Carbon::now() // Newest checkin
                ],
                [
                    'user_id' => $user->id,
                    'created_at' => Carbon::now()->subDays(2)
                ],
                [
                    'user_id' => $user->id,
                    'created_at' => Carbon::now()->subDays(1)
                ]
            ]
        ];

        $place2 = $this->createTestPlaceWithRelativeData($testData2);



        // Test get checkins
        $response = $this->getJson('/api/v1/check-ins');

        $response->assertStatus(200);

        // Prepare place image to test
        $place_image1 = Storage::disk(config('voyager.storage.disk'))->url($place1->image);

        // Prepare place image to test
        $place_image2 = Storage::disk(config('voyager.storage.disk'))->url($place2->image);

        // Test response to contains all flags and medals and points
        $response->assertJson([
            'data' => [
                [
                    'timestamp' => $testData2['checks'][0]['created_at']->timestamp,
                    'point' => [
                        'id' => $place2->id,
                        'lat' => $place2->gps->getLat(),
                        'lng' => $place2->gps->getLng(),
                        'name' => $place2->name,
                        'image' => $place_image2,
                        'checkedIn' => true
                    ]
                ],
                [
                    'timestamp' => $testData1['checks'][2]['created_at']->timestamp,
                    'point' => [
                        'id' => $place1->id,
                        'lat' => $place1->gps->getLat(),
                        'lng' => $place1->gps->getLng(),
                        'name' => $place1->name,
                        'image' => $place_image1,
                        'checkedIn' => true
                    ]
                ]
            ]
        ]);
    }
}
