<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\Utils\UserUtil;
use Tests\Utils\TestDataUtil;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\UserRating;

class AddUserRatingsTest extends TestCase
{
    use UserUtil;
    use TestDataUtil;
    use DatabaseTransactions;
    
    /**
     * Test without API key.
     *
     * @return void
     */
    public function testAddUserRatingWithoutAPIKey()
    {
        $response = $this->postJson('/api/v1/map/1/rate');
            
        $response->assertStatus(401);
    }

    /**
     * Test without authentication by JWT.
     *
     * @return void
     */
    public function testAddUserRatingWithoutLogin()
    {
        $this->setHeaders();

        $response = $this->postJson('/api/v1/map/1/rate');
        
        $response->assertStatus(101);
    }

    /**
     * Test add user rating with bad stars attribute.
     *
     * @return void
     */
    public function testAddUserRatingBadStarsAttribute()
    {
        $this->setAuthorizationHeaders();

        // Test missing stars attribute
        $response = $this->postJson('/api/v1/map/1/rate');
        
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['stars'],
            'message'
        ]);


        // Create test data
        $data = [
            'stars' => 0.9, // range 1 - 5
        ];

        // Test stars attribute - lower value than allowed
        $response = $this->postJson('/api/v1/map/1/rate');
        
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['stars'],
            'message'
        ]);


        // Create test data
        $data = [
            'stars' => 5.1, // range 1 - 5
        ];

        // Test stars attribute - higher value than allowed
        $response = $this->postJson('/api/v1/map/1/rate');
        
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['stars'],
            'message'
        ]);
    }

    /**
     * Test add user rating with bad text attribute.
     *
     * @return void
     */
    public function testAddUserRatingBadTextAttribute()
    {
        $this->setAuthorizationHeaders();

        // Create test data
        $data = [
            'stars' => 1, // range 1 - 5
            'text' => str_repeat("a", 141) // max length 140 characters
        ];

        // Test text attribute - longer value than allowed
        $response = $this->postJson('/api/v1/map/1/rate', $data);
        
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['text'],
            'message'
        ]);
    }

    /**
     * Test add user rating on place that not exists.
     *
     * @return void
     */
    public function testAddUserRatingToPlaceThatNotExists()
    {
        $this->setAuthorizationHeaders();

        $data = [
            'stars' => 1, // range 1 - 5
            'text' => 'test' // max length 140 characters
        ];

        $response = $this->postJson('/api/v1/map/1111111/rate', $data);
        
        $response->assertStatus(404);
    }

    /**
     * Test valid add user rating to place. Both attributes.
     *
     * @return void
     */
    public function testAddUserRatingBasic()
    {
        $this->setAuthorizationHeaders();

        $user = $this->lastCreatedUser;

        // Create test data
        $place = $this->createTestPlaceWithRelativeData();

        $data = [
            'stars' => 1, // range 1 - 5
            'text' => '' // max length 140 characters
        ];

        // Test add rating - lower bounds of attribute values
        $response = $this->postJson('/api/v1/map/' . $place->id . '/rate', $data);
        
        $response->assertStatus(204);

        $testText = $data['text'] != null ? $data['text'] : null; // if $data['test'] is '' or null, test null value

        $this->assertDatabaseHas(
            with(new UserRating)->getTable(), 
            [
                'user_id' => $user->id,
                'place_id' => $place->id,
                'stars' => $data['stars'],
                'rating' => $testText,
            ]
        );



        // Create test data
        $place = $this->createTestPlaceWithRelativeData();

        $data = [
            'stars' => 5, // range 1 - 5
            'text' => str_repeat("a", 140) // max length 140 characters
        ];

        // Test add rating - upper bounds of attribute values
        $response = $this->postJson('/api/v1/map/' . $place->id . '/rate', $data);
        
        $response->assertStatus(204);

        $this->assertDatabaseHas(
            with(new UserRating)->getTable(), 
            [
                'user_id' => $user->id,
                'place_id' => $place->id,
                'stars' => $data['stars'],
                'rating' => $data['text'],
            ]
        );
    }

    /**
     * Test valid add user rating to place. Text attribute null.
     *
     * @return void
     */
    public function testAddUserRatingTextAttributeNull()
    {
        $this->setAuthorizationHeaders();

        $user = $this->lastCreatedUser;

        // Create test data
        $place = $this->createTestPlaceWithRelativeData();

        $data = [
            'stars' => 1, // range 1 - 5
            'text' => null // max length 140 characters
        ];

        // Test add rating - null text attribute
        $response = $this->postJson('/api/v1/map/' . $place->id . '/rate', $data);
        
        $response->assertStatus(204);

        $this->assertDatabaseHas(
            with(new UserRating)->getTable(), 
            [
                'user_id' => $user->id,
                'place_id' => $place->id,
                'stars' => $data['stars'],
                'rating' => $data['text'],
            ]
        );

        // Create test data
        $place = $this->createTestPlaceWithRelativeData();

        $data = [
            'stars' => 1, // range 1 - 5
        ];

        // Test text attribute - longer value than allowed
        $response = $this->postJson('/api/v1/map/' . $place->id . '/rate', $data);
        
        $response->assertStatus(204);

        $this->assertDatabaseHas(
            with(new UserRating)->getTable(), 
            [
                'user_id' => $user->id,
                'place_id' => $place->id,
                'stars' => $data['stars'],
                'rating' => null,
            ]
        );
    }

    /**
     * Test valid add user rating to place. Rewrite rating.
     *
     * @return void
     */
    public function testAddUserRatingRewrite()
    {
        $this->setAuthorizationHeaders();

        $user = $this->lastCreatedUser;

        // Create test data
        $place = $this->createTestPlaceWithRelativeData();

        $data = [
            'stars' => 1, // range 1 - 5
            'text' => null // max length 140 characters
        ];

        // Test first rating - null text attribute
        $response = $this->postJson('/api/v1/map/' . $place->id . '/rate', $data);
        
        $response->assertStatus(204);

        $this->assertDatabaseHas(
            with(new UserRating)->getTable(), 
            [
                'user_id' => $user->id,
                'place_id' => $place->id,
                'stars' => $data['stars'],
                'rating' => $data['text'],
            ]
        );



        // Create test data        
        $data = [
            'stars' => 3.8, // range 1 - 5
            'text' => 'test' // max length 140 characters
        ];

        // Test second rating - both attributes
        $response = $this->postJson('/api/v1/map/' . $place->id . '/rate', $data);
        
        $response->assertStatus(204);

        $this->assertDatabaseHas(
            with(new UserRating)->getTable(), 
            [
                'user_id' => $user->id,
                'place_id' => $place->id,
                'stars' => $data['stars'],
                'rating' => $data['text'],
            ]
        );



        // Create test data        
        $data = [
            'stars' => 2.1, // range 1 - 5
            'text' => '' // max length 140 characters
        ];

        // Test third rating - again null text attribute (delete text)
        $response = $this->postJson('/api/v1/map/' . $place->id . '/rate', $data);
        
        $response->assertStatus(204);

        $testText = $data['text'] != null ? $data['text'] : null; // if $data['test'] is '' or null, test null value

        $this->assertDatabaseHas(
            with(new UserRating)->getTable(), 
            [
                'user_id' => $user->id,
                'place_id' => $place->id,
                'stars' => $data['stars'],
                'rating' => $testText,
            ]
        );


        $ratingsCount = UserRating::where('user_id', $user->id)->where('place_id', $place->id)->count();
        // Test if only one record exists
        $this->assertTrue($ratingsCount == 1);
    }
}
