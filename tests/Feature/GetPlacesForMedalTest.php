<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\Utils\UserUtil;
use Tests\Utils\TestDataUtil;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Category;
use App\User;
use Carbon\Carbon;

class GetPlacesForMedalTest extends TestCase
{
    use UserUtil;
    use TestDataUtil;
    use DatabaseTransactions;

    /**
     * Helper method for creating test category (test medal).
     *
     * @return int category ID (same as medal ID)
     */
    protected function createTestCategory()
    {
        $category = Category::create([
            'continent_id' => 1,
            'name' => 'test'
        ]);

        return $category->id;
    }

    /**
     * Test without API key.
     *
     * @return void
     */
    public function testGetPlacesForMedalWithoutAPIKey()
    {
        $response = $this->getJson('/api/v1/medal/1');
            
        $response->assertStatus(401);
    }

    /**
     * Test without authentication by JWT.
     *
     * @return void
     */
    public function testGetPlacesForMedalWithoutLogin()
    {
        $this->setHeaders();

        $response = $this->getJson('/api/v1/medal/1');
        
        $response->assertStatus(101);
    }

    /**
     * Test get places for medal that not exists.
     *
     * @return void
     */
    public function testGetPlacesForMedalThatNotExists()
    {
        $this->setAuthorizationHeaders();

        $response = $this->getJson('/api/v1/medal/1111111');
        
        $response->assertStatus(404);
    }

    /**
     * Test get places for medal that not exists.
     *
     * @return void
     */
    public function testGetPlacesForMedalThatNotExistsButCategoryExists()
    {
        $this->setAuthorizationHeaders();

        $testMedalID = $this->createTestCategory();

        // Create test data
        $testData = [
            'category_id' => 1,
            'is_top' => true,
        ];

        // Create top place for different medal
        $place = $this->createTestPlaceWithRelativeData($testData);

        // Test get medals - user has no medals
        $response = $this->getJson('/api/v1/medal/' . $testMedalID);

        $response->assertStatus(200);

        // Test response to contains place info
        $response->assertExactJson([
            'data' => []
        ]);
    }

    /**
     * Test valid get places for medal. Test with one top place.
     *
     * @return void
     */
    public function testGetPlacesForMedalOnePlace()
    {
        $this->setAuthorizationHeaders();

        $user = $this->lastCreatedUser;

        $testMedalID = $this->createTestCategory();

        // Create test data
        $testData = [
            'name' => 'Test place',
            'gps' => [14, 12],
            'category_id' => $testMedalID,
            'is_top' => true,
            'description' => 'Description of place',
            'address' => 'Prague',
            'image' => 'test/image.png'
        ];

        $place = $this->createTestPlaceWithRelativeData($testData);

        // Test get medals - user has no medals
        $response = $this->getJson('/api/v1/medal/' . $testMedalID);

        $response->assertStatus(200);

        $place_image = Storage::disk(config('voyager.storage.disk'))->url($place->image);

        // Test response to contains place info
        $response->assertExactJson([
            'data' => [
                [
                    'id' => $place->id,
                    'lat' => $place->gps->getLat(),
                    'lng' => $place->gps->getLng(),
                    'name' => $place->name,
                    'image' => $place_image,
                    'description' => $place->description,
                    'address' => $place->address,
                    'checkedIn' => false,
                    'lastCheckIn' => 0,
                    'canCheckIn' => null, // not setted, not send GPS position to determine value
                    'inRange' => null, // not setted, not send GPS position to determine value
                    'range' => config('awapp.places.defaults.radius'),
                    'userPictures' => [],
                    'rating' => [],
                ]
            ]
        ]);
    }

    /**
     * Test valid get places for medal. Test with one top place, with user photos, ratings and checked.
     *
     * @return void
     */
    public function testGetPlacesForMedalOnePlaceWithPicturesAndRatings()
    {
        $this->setAuthorizationHeaders();

        $user = $this->lastCreatedUser;

        $user2 = $this->createUser([
            'email' => str_random(40) . '@test.cz',
            'nickname' => 'test',
            'visibility' => User::VISIBILITY_NICKNAME
        ]);

        $testMedalID = $this->createTestCategory();

        // Create test data
        $testData = [
            'name' => 'Test place',
            'gps' => [14, 12],
            'category_id' => $testMedalID,
            'is_top' => true,
            'description' => 'Description of place',
            'address' => 'Prague',
            'image' => 'test/image.png',
            'userPictures' => [
                [
                    'user_id' => $user->id,
                    'image' => 'test/user_photo.png'
                ],
                [
                    'user_id' => $user2->id,
                    'image' => 'test/user_photo2.png'
                ]
            ],
            'rating' => [
                [
                    'user_id' => $user->id,
                    'stars' => 4.3,
                    'rating' => 'Amazing',
                    'created_at' => Carbon::now()->subDays(2)
                ],
                [
                    'user_id' => $user2->id,
                    'stars' => 2.3,
                    'rating' => 'Boring',
                    'created_at' => Carbon::now()->subDays(1)
                ]
            ],
            'checks' => [
                [
                    'user_id' => $user->id,
                    'created_at' => Carbon::now()
                ]
            ]
        ];

        $place = $this->createTestPlaceWithRelativeData($testData);

        // Test get medals - user has no medals
        $response = $this->getJson('/api/v1/medal/' . $testMedalID);

        $response->assertStatus(200);

        $place_image = Storage::disk(config('voyager.storage.disk'))->url($place->image);

        $profile_image = Storage::disk(config('voyager.storage.disk'))->url($user->avatar);
        $profile_image2 = Storage::disk(config('voyager.storage.disk'))->url($user2->avatar);

        $photo_image = Storage::disk(config('voyager.storage.disk'))->url($testData['userPictures'][0]['image']);
        $photo_image2 = Storage::disk(config('voyager.storage.disk'))->url($testData['userPictures'][1]['image']);

        if ($user->visibility == User::VISIBILITY_NICKNAME) {
            $profile_name = $user->nickname;
        } else {
            $profile_name = trim($user->firstname . " " . $user->lastname);
        }
        if ($user2->visibility == User::VISIBILITY_NICKNAME) {
            $profile_name2 = $user2->nickname;
        } else {
            $profile_name2 = trim($user2->firstname . " " . $user2->lastname);
        }

        // Test response to contains place info
        $response->assertExactJson([
            'data' => [
                [
                    'id' => $place->id,
                    'lat' => $place->gps->getLat(),
                    'lng' => $place->gps->getLng(),
                    'name' => $place->name,
                    'image' => $place_image,
                    'description' => $place->description,
                    'address' => $place->address,
                    'checkedIn' => true,
                    'lastCheckIn' => $testData['checks'][0]['created_at']->timestamp,
                    'canCheckIn' => null, // not setted, not send GPS position to determine value
                    'inRange' => null, // not setted, not send GPS position to determine value
                    'range' => config('awapp.places.defaults.radius'),
                    'userPictures' => [
                        [
                            'userId' => $user->id,
                            'displayName' => $profile_name,
                            'profilePicture' => $profile_image,
                            'image' => $photo_image,
                        ],
                        [
                            'userId' => $user2->id,
                            'displayName' => $profile_name2,
                            'profilePicture' => $profile_image2,
                            'image' => $photo_image2,
                        ]
                    ],
                    'rating' => [
                        [
                            'userId' => $user->id,
                            'displayName' => $profile_name,
                            'profilePicture' => $profile_image,
                            'stars' => $testData['rating'][0]['stars'],
                            'text' => $testData['rating'][0]['rating'],
                            'timestamp' => $testData['rating'][0]['created_at']->timestamp,
                        ],
                        [
                            'userId' => $user2->id,
                            'displayName' => $profile_name2,
                            'profilePicture' => $profile_image2,
                            'stars' => $testData['rating'][1]['stars'],
                            'text' => $testData['rating'][1]['rating'],
                            'timestamp' => $testData['rating'][1]['created_at']->timestamp,
                        ]
                    ],
                ]
            ]
        ]);
    }

    /**
     * Test valid get places for medal. Test with more places.
     *
     * @return void
     */
    public function testGetPlacesForMedalMorePlaces()
    {
        $this->setAuthorizationHeaders();

        $user = $this->lastCreatedUser;

        $testMedalID = $this->createTestCategory();

        // Create test data
        $testData = [
            'name' => 'Test place',
            'gps' => [14, 12],
            'category_id' => $testMedalID,
            'is_top' => true,
            'description' => 'Description of place',
            'address' => 'Prague',
            'image' => 'test/image.png',
            'checks' => [
                [
                    'user_id' => $user->id,
                    'created_at' => Carbon::now()
                ]
            ]
        ];

        $place = $this->createTestPlaceWithRelativeData($testData);

        $testData['id'] = $place->id;

        $testDataAll[] = $testData;



        $testData = [
            'name' => 'Test place 2',
            'gps' => [11, 33],
            'category_id' => $testMedalID,
            'is_top' => true,
            'description' => 'Description of place 2',
            'address' => 'Brno',
            'image' => 'test/image2.png',
        ];

        $place = $this->createTestPlaceWithRelativeData($testData);

        $testData['id'] = $place->id;

        $testDataAll[] = $testData;



        $testData = [
            'name' => 'Test place 3',
            'gps' => [5, 6],
            'category_id' => $testMedalID,
            'is_top' => true,
            'description' => 'Description of place 3',
            'address' => 'Olomouc',
            'image' => 'test/image3.png',
            'checks' => [
                [
                    'user_id' => $user->id,
                    'created_at' => Carbon::now()
                ]
            ]
        ];

        $place = $this->createTestPlaceWithRelativeData($testData);

        $testData['id'] = $place->id;

        $testDataAll[] = $testData;



        // Test get medals - user has no medals
        $response = $this->getJson('/api/v1/medal/' . $testMedalID);

        $response->assertStatus(200);

        // Prepare data to test
        $dataToTest = [
            'data' => []
        ];

        foreach ($testDataAll as $testData) {
            $place_image = Storage::disk(config('voyager.storage.disk'))->url($testData['image']);

            $checked = !empty($testData['checks']);
            $checkedTime = $testData['checks'][0]['created_at']->timestamp ?? 0;

            $dataToTest['data'][] = [
                'id' => $testData['id'],
                'lat' => $testData['gps'][0],
                'lng' => $testData['gps'][1],
                'name' => $testData['name'],
                'image' => $place_image,
                'description' => $testData['description'],
                'address' => $testData['address'],
                'checkedIn' => $checked,
                'lastCheckIn' => $checkedTime,
                'canCheckIn' => null, // not setted, not send GPS position to determine value
                'inRange' => null, // not setted, not send GPS position to determine value
                'range' => config('awapp.places.defaults.radius'),
                'userPictures' => [],
                'rating' => [],
            ];
        }

        // Test response to contains places info
        $response->assertExactJson($dataToTest);
    }
}
