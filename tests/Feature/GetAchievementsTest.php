<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\Utils\UserUtil;
use Tests\Utils\TestDataUtil;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Category;
use App\UserPoint;

class GetAchievementsTest extends TestCase
{
    use UserUtil;
    use TestDataUtil;
    use DatabaseTransactions;

    /**
     * Test without API key.
     *
     * @return void
     */
    public function testGetAchievementsWithoutAPIKey()
    {
        $response = $this->getJson('/api/v1/achievements');
            
        $response->assertStatus(401);
    }

    /**
     * Test without authentication by JWT.
     *
     * @return void
     */
    public function testGetAchievementsWithoutLogin()
    {
        $this->setHeaders();

        $response = $this->getJson('/api/v1/achievements');
        
        $response->assertStatus(101);
    }

    /**
     * Test valid get achievements. Test without flags and medals.
     *
     * @return void
     */
    public function testGetAchievementsBasic()
    {
        // Create test data
        $this->setMedalsAndFlagsIconAndName(); // Need call before get categories.

        $categories = Category::with('continent')->get();

        $user = $this->createTestUserWithRelativeData();

        // Login tested user
        $this->setAuthorizationHeaders($user);

        // Test user without flags
        $response = $this->getJson('/api/v1/achievements');

        $response->assertStatus(200);

        // Prepare data to test
        $dataToTest = [
            'data' => [
                'checkinPoints' => 0,
                'photoPoints' => 0,
                'medals' => [],
                'flags' => []
            ]
        ];

        // Test response to contains all flags and medals and points
        $response->assertExactJson($dataToTest);
    }

    /**
     * Test valid get achievements. Test with mixed flags and medals.
     *
     * @return void
     */
    public function testGetAchievementsMixedFlagsAndMedals()
    {
        // Create test data
        $this->setMedalsAndFlagsIconAndName(); // Need call before get categories.

        $categories = Category::with('continent')->get();

        $medalIDs = array_rand(array_flip($categories->pluck('id')->toArray()), 10); // get 10 random medals
        $flagIDs = array_rand(array_flip($categories->pluck('id')->toArray()), 10); // get 10 random flags

        // Create all places with mixed flags places and top places - mixed flags and medals
        foreach ($categories as $category) {
            $place = $this->createTestPlaceWithRelativeData([
                'category_id' => $category->id,
                'is_top' => in_array($category->id, $medalIDs),
                'is_flag' => in_array($category->id, $flagIDs),
            ]);
        }

        // Login tested user
        $this->setAuthorizationHeaders();

        // Test user without flags
        $response = $this->getJson('/api/v1/achievements');

        $response->assertStatus(200);

        // Prepare data to test
        $dataToTest = [
            'data' => [
                'checkinPoints' => 0,
                'photoPoints' => 0,
                'medals' => [],
                'flags' => []
            ]
        ];

        // Prepare data to test - add medals
        foreach ($categories as $category) {
            if (in_array($category->id, $medalIDs)) {
                $medal_image = Storage::disk(config('voyager.storage.disk'))->url($category->medal_image_locked);

                $dataToTest['data']['medals'][] = [
                    'id' => $category->id, // category ID is same as medal ID
                    'image' => $medal_image,
                    'name' => $category->medal_name,
                    'achieved' => false,
                    'category' => [
                        'id' => $category->id,
                        'label' => $category->name,
                    ],
                    'continent' => [
                        'id' => $category->continent_id,
                        'label' => $category->continent->name,
                    ],
                ];
            }
        }

        // Prepare data to test - add flags
        foreach ($categories as $category) {
            if (in_array($category->id, $flagIDs)) {
                $flag_image = Storage::disk(config('voyager.storage.disk'))->url($category->flag_image_locked);

                $dataToTest['data']['flags'][] = [
                    'id' => $category->id, // category ID is same as flag ID
                    'image' => $flag_image,
                    'name' => $category->flag_name,
                    'achieved' => false,
                    'category' => [
                        'id' => $category->id,
                        'label' => $category->name,
                    ],
                    'continent' => [
                        'id' => $category->continent_id,
                        'label' => $category->continent->name,
                    ],
                ];
            }
        }

        // Test response to contains all flags and medals and points
        $response->assertExactJson($dataToTest);
    }

    /**
     * Test valid get achievements. Test with all flags and medals. User not have medals and flags.
     *
     * @return void
     */
    public function testGetAchievementsAllFlagsAndMedals()
    {
        // Create test data
        $this->setMedalsAndFlagsIconAndName(); // Need call before get categories.

        $categories = Category::with('continent')->get();

        // Create all top and flag places - all flags and medals exists
        foreach ($categories as $category) {
            $place = $this->createTestPlaceWithRelativeData([
                'category_id' => $category->id,
                'is_top' => true,
                'is_flag' => true,
            ]);
        }

        // Login tested user
        $this->setAuthorizationHeaders();

        // Test user without flags
        $response = $this->getJson('/api/v1/achievements');

        $response->assertStatus(200);

        // Prepare data to test
        $dataToTest = [
            'data' => [
                'checkinPoints' => 0,
                'photoPoints' => 0,
                'medals' => [],
                'flags' => []
            ]
        ];

        // Prepare data to test - add medals
        foreach ($categories as $category) {
            $medal_image = Storage::disk(config('voyager.storage.disk'))->url($category->medal_image_locked);

            $dataToTest['data']['medals'][] = [
                'id' => $category->id, // category ID is same as medal ID
                'image' => $medal_image,
                'name' => $category->medal_name,
                'achieved' => false,
                'category' => [
                    'id' => $category->id,
                    'label' => $category->name,
                ],
                'continent' => [
                    'id' => $category->continent_id,
                    'label' => $category->continent->name,
                ],
            ];
        }

        // Prepare data to test - add flags
        foreach ($categories as $category) {
            $flag_image = Storage::disk(config('voyager.storage.disk'))->url($category->flag_image_locked);

            $dataToTest['data']['flags'][] = [
                'id' => $category->id, // category ID is same as flag ID
                'image' => $flag_image,
                'name' => $category->flag_name,
                'achieved' => false,
                'category' => [
                    'id' => $category->id,
                    'label' => $category->name,
                ],
                'continent' => [
                    'id' => $category->continent_id,
                    'label' => $category->continent->name,
                ],
            ];
        }

        // Test response to contains all flags and medals and points
        $response->assertJson($dataToTest);
    }

    /**
     * Test valid get achievements. Test with all flags and medals. User have all medals and flags.
     *
     * @return void
     */
    public function testGetAchievementsAllFlagsAndMedalsUserHaveAll()
    {
        // Create test data
        $this->setMedalsAndFlagsIconAndName(); // Need call before get categories.

        $categories = Category::with('continent')->get();

        // Create all top and flag places - all flags and medals exists
        foreach ($categories as $category) {
            $place = $this->createTestPlaceWithRelativeData([
                'category_id' => $category->id,
                'is_top' => true,
                'is_flag' => true,
            ]);
        }

        // Set user have all flags and medals
        $user = $this->createTestUserWithRelativeData([
            'medals' => $categories->pluck('id')->toArray(),
            'flags' => $categories->pluck('id')->toArray(),
        ]);

        // Login tested user
        $this->setAuthorizationHeaders($user);

        // Test user without flags
        $response = $this->getJson('/api/v1/achievements');

        $response->assertStatus(200);

        // Prepare data to test
        $dataToTest = [
            'data' => [
                'checkinPoints' => 0,
                'photoPoints' => 0,
                'medals' => [],
                'flags' => []
            ]
        ];

        // Prepare data to test - add medals
        foreach ($categories as $category) {
            $medal_image = Storage::disk(config('voyager.storage.disk'))->url($category->medal_image_unlocked);

            $dataToTest['data']['medals'][] = [
                'id' => $category->id, // category ID is same as medal ID
                'image' => $medal_image,
                'name' => $category->medal_name,
                'achieved' => true,
                'category' => [
                    'id' => $category->id,
                    'label' => $category->name,
                ],
                'continent' => [
                    'id' => $category->continent_id,
                    'label' => $category->continent->name,
                ],
            ];
        }

        // Prepare data to test - add flags
        foreach ($categories as $category) {
            $flag_image = Storage::disk(config('voyager.storage.disk'))->url($category->flag_image_unlocked);

            $dataToTest['data']['flags'][] = [
                'id' => $category->id, // category ID is same as flag ID
                'image' => $flag_image,
                'name' => $category->flag_name,
                'achieved' => true,
                'category' => [
                    'id' => $category->id,
                    'label' => $category->name,
                ],
                'continent' => [
                    'id' => $category->continent_id,
                    'label' => $category->continent->name,
                ],
            ];
        }

        // Test response to contains all flags and medals and points
        $response->assertJson($dataToTest);
    }

    /**
     * Test valid get achievements. Test with all flags and medals.
     * Some of medals user have and some of flags user have, other medals and flags user don't have.
     *
     * @return void
     */
    public function testGetAchievementsAllFlagsAndMedalsUserHaveSome()
    {
        // Create test data
        $this->setMedalsAndFlagsIconAndName(); // Need call before get categories.

        $categories = Category::with('continent')->get();

        // Create all top and flag places - all flags and medals exists
        foreach ($categories as $category) {
            $place = $this->createTestPlaceWithRelativeData([
                'category_id' => $category->id,
                'is_top' => true,
                'is_flag' => true,
            ]);
        }

        // Set user have 10 random medals and 10 random flags.
        $testData = [
            'medals' => array_rand(array_flip($categories->pluck('id')->toArray()), 10), // get 10 random medals
            'flags' => array_rand(array_flip($categories->pluck('id')->toArray()), 10), // get 10 random flags
        ];

        $user = $this->createTestUserWithRelativeData($testData);

        // Login tested user
        $this->setAuthorizationHeaders($user);

        // Test user without flags
        $response = $this->getJson('/api/v1/achievements');

        $response->assertStatus(200);

        // Prepare data to test
        $dataToTest = [
            'data' => [
                'checkinPoints' => 0,
                'photoPoints' => 0,
                'medals' => [],
                'flags' => []
            ]
        ];

        // Prepare data to test - add medals
        foreach ($categories as $category) {
            if (in_array($category->id, $testData['medals'])) {
                // user have medal
                $medal_image = Storage::disk(config('voyager.storage.disk'))->url($category->medal_image_unlocked);
                $achieved = true;
            } else {
                // user not have medal
                $medal_image = Storage::disk(config('voyager.storage.disk'))->url($category->medal_image_locked);
                $achieved = false;
            }

            $dataToTest['data']['medals'][] = [
                'id' => $category->id, // category ID is same as medal ID
                'image' => $medal_image,
                'name' => $category->medal_name,
                'achieved' => $achieved,
                'category' => [
                    'id' => $category->id,
                    'label' => $category->name,
                ],
                'continent' => [
                    'id' => $category->continent_id,
                    'label' => $category->continent->name,
                ],
            ];
        }

        // Prepare data to test - add flags
        foreach ($categories as $category) {
            if (in_array($category->id, $testData['flags'])) {
                // user have flag
                $flag_image = Storage::disk(config('voyager.storage.disk'))->url($category->flag_image_unlocked);
                $achieved = true;
            } else {
                // user not have flag
                $flag_image = Storage::disk(config('voyager.storage.disk'))->url($category->flag_image_locked);
                $achieved = false;
            }

            $dataToTest['data']['flags'][] = [
                'id' => $category->id, // category ID is same as flag ID
                'image' => $flag_image,
                'name' => $category->flag_name,
                'achieved' => $achieved,
                'category' => [
                    'id' => $category->id,
                    'label' => $category->name,
                ],
                'continent' => [
                    'id' => $category->continent_id,
                    'label' => $category->continent->name,
                ],
            ];
        }

        // Test response to contains all flags and medals and points
        $response->assertJson($dataToTest);
    }

    /**
     * Test valid get achievements. Test counting points.
     *
     * @return void
     */
    public function testGetAchievementsPoints()
    {
        // Create test data
        $testData = [
            'points' => []
        ];

        // Variables to sum points.
        $firstPhotoPointsSum = 0;
        $checkinPointsSum = 0;

        // Adds 10 test user first photo points
        // test with different points value (to be sure counting sum value of points)
        for ($i = 0; $i < 10; $i++) {
            $points = rand(1, 5);
            $firstPhotoPointsSum += $points;

            $testData['points'][] = [
                'type' => UserPoint::TYPE_FIRST_PHOTO,
                'points' => $points
            ];
        }

        // Adds 10 test user checkin points
        // test with different points value (to be sure counting sum value of points)
        for ($i = 0; $i < 10; $i++) {
            $points = rand(1, 5);
            $checkinPointsSum += $points;

            $testData['points'][] = [
                'type' => UserPoint::TYPE_CHECKIN,
                'points' => $points
            ];
        }

        $user = $this->createTestUserWithRelativeData($testData);

        // Login tested user
        $this->setAuthorizationHeaders($user);

        // Test user without flags
        $response = $this->getJson('/api/v1/achievements');
        
        $response->assertStatus(200);

        // Test response to contains all flags and medals and points
        $response->assertJson([
            'data' => [
                'checkinPoints' => $checkinPointsSum,
                'photoPoints' => $firstPhotoPointsSum
            ]
        ]);
    }
}
