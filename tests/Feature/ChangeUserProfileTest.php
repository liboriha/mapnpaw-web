<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\Utils\UserUtil;
use Tests\Utils\ImageUtil;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Testing\MimeType;
use Illuminate\Http\UploadedFile;
use App\User;

class ChangeUserProfileTest extends TestCase
{
    use UserUtil;
    use ImageUtil;
    use DatabaseTransactions;

    /**
     * Returns basic test user data.
     *
     * @return data
     */
    protected function getBasicTestData() 
    {
        return [
            'email' => 'pavel@novy.cz', 
            'firstname' => 'Pavel', 
            'lastname' => 'Nový', 
            'nickname' => 'PN', 
            'display' => 'nickname', 
            'profilePicture' => ''
        ];
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testChangeUserProfileWithoutAPIKey()
    {
        $response = $this->postJson('/api/v1/profile', $this->getBasicTestData());
            
        $response->assertStatus(401);
    }

    /**
     * Test without authentication by JWT.
     *
     * @return void
     */
    public function testChangeUserProfileWithoutLogin()
    {
        $this->setHeaders();

        $response = $this->postJson('/api/v1/profile', $this->getBasicTestData());
        
        $response->assertStatus(101);
    }

    /**
     * Test valid user profile change without image.
     *
     * @return void
     */
    public function testChangeUserProfileWithoutImage()
    {
        $this->setAuthorizationHeaders();

        // Reload data about user from database
        $user = User::find($this->lastCreatedUser->id);

        $userData = $this->getBasicUserData($user);

        // Test default user data
        $this->assertArraySubset(
            [
                'email' => 'test@test.cz',
                'firstname' => '',
                'lastname' => '',
                'nickname' => '',
                'visibility' => 'NAME',
                'avatar' => config('voyager.user.default_avatar', 'users/default.png')
            ], 
            $userData
        );

        $response = $this->postJson('/api/v1/profile', $this->getBasicTestData());
        
        $response->assertStatus(200);
        
        // Reload data about user from database
        $user = User::find($user->id);

        $userData = $this->getBasicUserData($user);

        // Test changed user data
        $this->assertArraySubset(
            [
                'email' => 'pavel@novy.cz',
                'firstname' => 'Pavel',
                'lastname' => 'Nový',
                'nickname' => 'PN',
                'visibility' => 'NICKNAME',
                'avatar' => config('voyager.user.default_avatar', 'users/default.png')
            ], 
            $userData
        );
    }

    /**
     * Test valid user profile change with image (facebook profile - no email change).
     *
     * @return void
     */
    public function testChangeUserProfileWithImageFacebookProfile()
    {
        $this->setAuthorizationHeaders();

        Storage::fake(config('voyager.storage.disk'));

        // Reload data about user from database
        $user = User::find($this->lastCreatedUser->id);

        $defaultUserData = $this->getBasicUserData($user);

        // Test default user data
        $this->assertArraySubset(
            [
                'email' => 'test@test.cz',
                'firstname' => '',
                'lastname' => '',
                'nickname' => '',
                'visibility' => 'NAME',
                'avatar' => config('voyager.user.default_avatar', 'users/default.png')
            ], 
            $defaultUserData
        );

        // Prepare test image
        $testImage = $this->getTestImage();
        $testImageBase64 = $this->getTestImageBase64($testImage, false);

        // Prepare data
        $data = $this->getBasicTestData();
        $data['email'] = 'test@test.cz'; // Facebook user can't change email!
        $data['profilePicture'] = $testImageBase64;

        $response = $this->postJson('/api/v1/profile', $data);
        
        $response->assertStatus(200);
        
        // Reload data about user from database
        $user = User::find($user->id);

        $userData = $this->getBasicUserData($user);

        // Test changed user data
        $this->assertArraySubset(
            [
                'email' => 'test@test.cz',
                'firstname' => 'Pavel',
                'lastname' => 'Nový',
                'nickname' => 'PN',
                'visibility' => 'NICKNAME'
            ], 
            $userData
        );

        // Test avatar changed
        $this->assertNotSame($defaultUserData['avatar'], $userData['avatar']);

        // Test new profile image exists
        Storage::disk(config('voyager.storage.disk'))->assertExists($userData['avatar']);
    }

    /**
     * Test valid user profile change with image.
     *
     * @return void
     */
    public function testChangeUserProfileWithImage()
    {
        $this->setAuthorizationHeaders();

        Storage::fake(config('voyager.storage.disk'));

        // Reload data about user from database
        $user = User::find($this->lastCreatedUser->id);

        $defaultUserData = $this->getBasicUserData($user);

        // Test default user data
        $this->assertArraySubset(
            [
                'email' => 'test@test.cz',
                'firstname' => '',
                'lastname' => '',
                'nickname' => '',
                'visibility' => 'NAME',
                'avatar' => config('voyager.user.default_avatar', 'users/default.png')
            ], 
            $defaultUserData
        );

        // Prepare test image
        $testImage = $this->getTestImage();
        $testImageBase64 = $this->getTestImageBase64($testImage, false);

        // Prepare data
        $data = $this->getBasicTestData();
        $data['profilePicture'] = $testImageBase64;

        $response = $this->postJson('/api/v1/profile', $data);
        
        $response->assertStatus(200);
        
        // Reload data about user from database
        $user = User::find($user->id);

        $userData = $this->getBasicUserData($user);

        // Test changed user data
        $this->assertArraySubset(
            [
                'email' => 'pavel@novy.cz',
                'firstname' => 'Pavel',
                'lastname' => 'Nový',
                'nickname' => 'PN',
                'visibility' => 'NICKNAME'
            ], 
            $userData
        );

        // Test avatar changed
        $this->assertNotSame($defaultUserData['avatar'], $userData['avatar']);

        // Test new profile image exists
        Storage::disk(config('voyager.storage.disk'))->assertExists($userData['avatar']);



        // -------------------------------------------------
        // Test with second user - same uploaded image name.

        $this->setAuthorizationHeaders([
            'email' => 'test2@test.cz',
            'visibility' => 'NAME',
            'avatar' => config('voyager.user.default_avatar', 'users/default.png')
        ]);

        // Reload data about user from database
        $user = User::find($this->lastCreatedUser->id);

        $defaultUserData = $this->getBasicUserData($user);

        // Test default user data
        $this->assertArraySubset(
            [
                'email' => 'test2@test.cz',
                'firstname' => '',
                'lastname' => '',
                'nickname' => '',
                'visibility' => 'NAME',
                'avatar' => config('voyager.user.default_avatar', 'users/default.png')
            ], 
            $defaultUserData
        );


        // Prepare data
        $data = [
            'email' => 'test2@test.cz', 
            'firstname' => 'Anna', 
            'lastname' => 'Krátká', 
            'nickname' => 'AK', 
            'display' => 'name', 
            'profilePicture' => $testImageBase64
        ];

        $response = $this->postJson('/api/v1/profile', $data);
        
        $response->assertStatus(200);
        
        // Reload data about user from database
        $user = User::find($user->id);

        $userData = $this->getBasicUserData($user);

        // Test changed user data
        $this->assertArraySubset(
            [
                'email' => 'test2@test.cz',
                'firstname' => 'Anna',
                'lastname' => 'Krátká',
                'nickname' => 'AK',
                'visibility' => 'NAME'
            ], 
            $userData
        );

        // Test avatar changed
        $this->assertNotSame($defaultUserData['avatar'], $userData['avatar']);

        // Test new profile image exists
        Storage::disk(config('voyager.storage.disk'))->assertExists($userData['avatar']);
    }

    /**
     * Test change user profile email to existing email.
     *
     * @return void
     */
    public function testChangeUserProfileEmailToExistUser()
    {
        $this->setAuthorizationHeaders();

        // Reload data about user from database
        $user = User::find($this->lastCreatedUser->id);

        $userData = $this->getBasicUserData($user);

        // Test default user data
        $this->assertArraySubset(
            [
                'email' => 'test@test.cz',
                'firstname' => '',
                'lastname' => '',
                'nickname' => '',
                'visibility' => 'NAME',
                'avatar' => config('voyager.user.default_avatar', 'users/default.png')
            ], 
            $userData
        );

        // Create blocking user
        $this->createUser([
            'email' => 'pavel@novy.cz'
        ]);

        $response = $this->postJson('/api/v1/profile', $this->getBasicTestData());
        
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['email'],
            'message'
        ]);
    }

    /**
     * Test change facebook user profile email.
     *
     * @return void
     */
    public function testChangeUserProfileEmailFacebookProfile()
    {
        $this->setAuthorizationHeaders([
            'email' => 'test@test.cz',
            'visibility' => 'NAME',
            'avatar' => config('voyager.user.default_avatar', 'users/default.png'),
            'facebook_id' => '123456' // Is facebook user profile
        ]);

        // Reload data about user from database
        $user = User::find($this->lastCreatedUser->id);

        $userData = $this->getBasicUserData($user);

        // Test default user data
        $this->assertArraySubset(
            [
                'email' => 'test@test.cz',
                'firstname' => '',
                'lastname' => '',
                'nickname' => '',
                'visibility' => 'NAME',
                'avatar' => config('voyager.user.default_avatar', 'users/default.png')
            ], 
            $userData
        );

        // Prepare data
        $data = $this->getBasicTestData();

        $response = $this->postJson('/api/v1/profile', $data);
        
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['email'],
            'message'
        ]);
    }

    /**
     * Test change user profile with image - remove old profile image (except default image)
     *
     * @return void
     */
    public function testChangeUserProfileWithImageRemoveOld()
    {
        $this->setAuthorizationHeaders();

        Storage::fake(config('voyager.storage.disk'));

        // Reload data about user from database
        $user = User::find($this->lastCreatedUser->id);
        
        // Remove avatar - use default avatar.
        $user->avatar = null;
        $user->save();

        $defaultUserData = $this->getBasicUserData($user);

        // Test default user data
        $this->assertArraySubset(
            [
                'email' => 'test@test.cz',
                'firstname' => '',
                'lastname' => '',
                'nickname' => '',
                'visibility' => 'NAME',
                'avatar' => config('voyager.user.default_avatar', 'users/default.png')
            ], 
            $defaultUserData
        );


        // Prepare test image
        $testImage = $this->getTestImage();
        $testImageBase64 = $this->getTestImageBase64($testImage, false);

        // Prepare data
        $data = $this->getBasicTestData();
        $data['profilePicture'] = $testImageBase64;

        $response = $this->postJson('/api/v1/profile', $data);
        
        $response->assertStatus(200);
        
        
        // Reload data about user from database
        $user = User::find($user->id);

        $userData = $this->getBasicUserData($user);

        // Test avatar changed
        $this->assertNotSame($defaultUserData['avatar'], $userData['avatar']);

        // Test new profile image exists
        Storage::disk(config('voyager.storage.disk'))->assertExists($userData['avatar']);


        // ---------------------------------
        // Test upload second profile image

        $defaultUserData = $userData;

        $response = $this->postJson('/api/v1/profile', $data);
        
        $response->assertStatus(200);
        
        
        // Reload data about user from database
        $user = User::find($user->id);

        $userData = $this->getBasicUserData($user);

        // Test avatar changed
        $this->assertNotSame($defaultUserData['avatar'], $userData['avatar']);

        // Test new profile image exists
        Storage::disk(config('voyager.storage.disk'))->assertExists($userData['avatar']);

        // Test old profile image not exists
        Storage::disk(config('voyager.storage.disk'))->assertMissing($defaultUserData['avatar']);
    }
}
