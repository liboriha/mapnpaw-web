<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\Utils\UserUtil;
use Tests\Utils\TestDataUtil;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Http\Notifications\ResetPassword;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Password;
use App\User;

class ResetPasswordTest extends TestCase
{
    use UserUtil;
    use TestDataUtil;
    use DatabaseTransactions;

    /**
     * Test without API key.
     *
     * @return void
     */
    public function testResetPasswordWithoutAPIKey()
    {
        $response = $this->postJson('/api/v1/account/forgotten-password');
            
        $response->assertStatus(401);
    }

    /**
     * Test reset password for user that not exists.
     *
     * @return void
     */
    public function testResetPasswordUserThatNotExists()
    {
        $this->setHeaders();

        $testData = [
            'email' => str_random(40) . '@' . str_random(40) . '.cz'
        ];

        $response = $this->postJson('/api/v1/account/forgotten-password', $testData);
        
        $response->assertStatus(404);
    }

    /**
     * Test reset password with invalid email.
     *
     * @return void
     */
    public function testResetPasswordInvalidEmail()
    {
        $this->setHeaders();

        $testData = [
            'email' => str_random(40)
        ];

        $response = $this->postJson('/api/v1/account/forgotten-password', $testData);
        
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['email'],
            'message'
        ]);
    }

    /**
     * Test reset password of facebook user - can't reset.
     *
     * @return void
     */
    public function testResetPasswordFacebookUser()
    {
        $this->setHeaders();

        // Create testing data.
        $testData = [
            'email' => str_random(40) . '@' . str_random(40) . '.cz'
        ];

        // Create facebook user.
        $this->createUser([
            'email' => $testData['email'],
            'facebook_id' => 123456789
        ]);

        // Test reset password of facebook user.
        $response = $this->postJson('/api/v1/account/forgotten-password', $testData);
        
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'errors' => ['email'],
            'message'
        ]);
    }

    /**
     * Test valid reset password for user. Test add record to database.
     *
     * @return void
     */
    public function testResetPasswordCreateResetRecord()
    {
        $this->setHeaders();

        // Create testing data.
        $testData = [
            'email' => str_random(40) . '@' . str_random(40) . '.cz'
        ];

        $user = $this->createUser($testData);

        Notification::fake();

        // Test begin reset password - send email.
        $response = $this->postJson('/api/v1/account/forgotten-password', $testData);

        $response->assertStatus(204);

        $token = '';

        // Get reset token.
        Notification::assertSentTo(
            $user,
            ResetPassword::class,
            function ($notification, $channels) use (&$token) {
                $token = $notification->token;

                return true;
            }
        );

        // Test reset record exists.
        $this->assertDatabaseHas(
            config('auth.passwords.users.table'),
            [
                'email' => $testData['email']
            ]
        );

        // Check token in database match token in email.
        $resetRecord = DB::table(config('auth.passwords.users.table'))->where('email', $testData['email'])->first();

        $this->assertTrue(
            Hash::check($token, $resetRecord->token)
        );
    }

    /**
     * Test reset password for user. Token not valid.
     *
     * @return void
     */
    public function testResetPasswordProcessTokenNotExists()
    {
        $this->setHeaders();

        // Create testing data.
        $userData = [
            'email' => str_random(40) . '@' . str_random(40) . '.cz'
        ];

        $user = $this->createUser($userData);

        // Get token that not exists.
        while (true) {
            $token = strtolower(str_random(40));

            $resetRecord = DB::table(config('auth.passwords.users.table'))->where('token', $token)->first();
            if ($resetRecord == null) {
                break;
            }
        }

        // Not have reset password record
        $this->assertDatabaseMissing(
            config('auth.passwords.users.table'),
            [
                'email' => $user->email
            ]
        );

        // Create testing data.
        $newPassword = str_random(40);

        $testData = [
            'token' => $token,
            'email' => $user->email,
            'password' => $newPassword,
            'password_confirmation' => $newPassword
        ];

        Notification::fake();

        // Disable csrf verification - on testing purpose.
        $this->withoutMiddleware(\App\Http\Middleware\VerifyCsrfToken::class);

        // Test reset password with invalid token.
        $resetUrl = route('password.reset-process', ['token' => $token]);
        $response = $this->post($resetUrl, $testData);

        $response->assertStatus(302);

        $this->assertTrue(!empty(session('errors')));

        // Not have reset password record
        $this->assertDatabaseMissing(
            config('auth.passwords.users.table'),
            [
                'email' => $user->email
            ]
        );

        // Not changed password
        $this->assertFalse(
            Hash::check($newPassword, $user->password)
        );
    }

    /**
     * Test reset password for user. Token is valid.
     *
     * @return void
     */
    public function testResetPasswordProcessTokenChangePassword()
    {
        $this->setHeaders();

        // Create testing data.
        $userData = [
            'email' => str_random(40) . '@' . str_random(40) . '.cz'
        ];

        $user = $this->createUser($userData);

        // Get token that not exists.
        while (true) {
            $token = strtolower(str_random(40));

            $resetRecord = DB::table(config('auth.passwords.users.table'))->where('token', $token)->first();
            if ($resetRecord == null) {
                break;
            }
        }

        // Add reset record to database
        DB::table(config('auth.passwords.users.table'))->insert(
            [
                'email' => $user->email,
                'token' => Hash::make($token)
            ]
        );

        // Not have reset password record
        $this->assertDatabaseHas(
            config('auth.passwords.users.table'),
            [
                'email' => $user->email
            ]
        );

        $resetRecord = DB::table(config('auth.passwords.users.table'))->where('email', $user->email)->first();

        $this->assertTrue(
            Hash::check($token, $resetRecord->token)
        );

        // Create testing data.
        $newPassword = str_random(40);

        $testData = [
            'token' => $token,
            'email' => $user->email,
            'password' => $newPassword,
            'password_confirmation' => $newPassword
        ];

        Notification::fake();

        // Disable csrf verification - on testing purpose.
        $this->withoutMiddleware(\App\Http\Middleware\VerifyCsrfToken::class);

        // Test reset password.
        $resetUrl = route('password.reset-process', ['token' => $token]);
        $response = $this->post($resetUrl, $testData);

        $response->assertStatus(302);

        $this->assertEquals(session('message'), trans(Password::PASSWORD_RESET));
        
        // Not have reset password record
        $this->assertDatabaseMissing(
            config('auth.passwords.users.table'),
            [
                'email' => $user->email
            ]
        );

        // Reload user with actual user password
        $user = User::find($user->id);

        // Not changed password
        $this->assertTrue(
            Hash::check($newPassword, $user->password)
        );
    }

    /**
     * Test reset password for user. Process complete reset password process.
     *
     * @return void
     */
    public function testResetPasswordCompleteProcess()
    {
        $this->setHeaders();

        // Create testing data.
        $testData = [
            'email' => str_random(40) . '@' . str_random(40) . '.cz'
        ];

        $user = $this->createUser($testData);

        Notification::fake();

        // Test begin reset password - send email.
        $response = $this->postJson('/api/v1/account/forgotten-password', $testData);

        $response->assertStatus(204);

        $token = '';

        // Get reset token.
        Notification::assertSentTo(
            $user,
            ResetPassword::class,
            function ($notification, $channels) use (&$token) {
                $token = $notification->token;

                return true;
            }
        );

        // Test reset record exists.
        $this->assertDatabaseHas(
            config('auth.passwords.users.table'),
            [
                'email' => $testData['email']
            ]
        );

        // Check token in database match token in email.
        $resetRecord = DB::table(config('auth.passwords.users.table'))->where('email', $testData['email'])->first();

        $this->assertTrue(
            Hash::check($token, $resetRecord->token)
        );

        // Create testing data.
        $newPassword = str_random(40);

        $testData = [
            'token' => $token,
            'email' => $user->email,
            'password' => $newPassword,
            'password_confirmation' => $newPassword
        ];

        // Disable csrf verification - on testing purpose.
        $this->withoutMiddleware(\App\Http\Middleware\VerifyCsrfToken::class);

        // Test reset password.
        $resetUrl = route('password.reset-process', ['token' => $token]);
        $response = $this->post($resetUrl, $testData);

        $response->assertStatus(302);

        $this->assertEquals(session('message'), trans(Password::PASSWORD_RESET));
        
        // Not have reset password record
        $this->assertDatabaseMissing(
            config('auth.passwords.users.table'),
            [
                'email' => $user->email
            ]
        );

        // Reload user with actual user password
        $user = User::find($user->id);

        // Not changed password
        $this->assertTrue(
            Hash::check($newPassword, $user->password)
        );
    }
}
