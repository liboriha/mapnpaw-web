<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Support\Facades\Artisan;

trait CreatesApplication
{
    /**
     * Indicates if the test database has been migrated and seeded.
     *
     * @var bool
     */
    public static $migratedAndSeeded = false;

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        $dbInstall = env('DB_INSTALL', true);

        if ($dbInstall == true && self::$migratedAndSeeded == false) {
            Artisan::call('migrate:fresh', ['--seed' => null]);

            self::$migratedAndSeeded = true;
        }

        return $app;
    }
}
