#!/bin/bash

mkdir -p public/vendor/tcg/voyager
cp -r vendor/tcg/voyager/publishable/assets/ public/vendor/tcg/voyager/assets/

cp storage/app/public/settings/loader.png public/vendor/tcg/voyager/assets/images/logo-icon.png
