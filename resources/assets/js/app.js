
require('./bootstrap');



const menu = document.querySelector('.js-menu');
const menuIcon = document.querySelector('.js-menu-icon');
const menuCross = document.querySelector('.js-menu-cross');
const menuLiA = menu.querySelectorAll('li a');
const mutation = document.querySelector('.js-mutation');
const html = document.querySelector("html");
const body = document.querySelector("body");

function toggleMenu() {
    
    menuIcon.addEventListener('click', function () {
        menu.classList.add('is-active');
        body.classList.add('scroll-disable');
        mutation.classList.add('is-active');
    });

    menuCross.addEventListener('click', function () {
        menu.classList.remove('is-active');
        body.classList.remove('scroll-disable');
        mutation.classList.remove('is-active');
    });

    for (let i = 0; i < menuLiA.length; i++) {
        menuLiA[i].addEventListener('click', function () {
            menu.classList.remove('is-active');
            body.classList.remove('scroll-disable');
            mutation.classList.remove('is-active');
        });
        
    }

}

function getDeviceWidth() {

    const deviceWidth = html.offsetWidth;

    if (deviceWidth < 640) {
        return "small";
    }

    else if (deviceWidth  <= 1200 && deviceWidth >= 640) {
        return "medium";
    }

    else {
        return "large";
    }

}

if (menu != null) {
    toggleMenu();    
}

const filter = document.querySelector('.js-section-filter');

function filterToggle(){

    filter.addEventListener('click', function () {
        filter.classList.toggle('is-active');
    });
}

if (filter != null) {
    filterToggle();
}


const topBar = document.querySelector('.js-top-bar');

function navigationMenu(scroll) {

    if (scroll > 5) {
        topBar.classList.add('is-active');
    }

    else {
        topBar.classList.remove('is-active');
    }
}

function getScrollValue() {
    return html.scrollTop || body.scrollTop;
}

function stickyTopBar() {

    window.addEventListener("scroll", function () {
        navigationMenu(getScrollValue());
    });

    navigationMenu(getScrollValue());

}

if (topBar != null) {
    stickyTopBar();
}

const smoothButton =document.querySelectorAll('.js-smooth');

function scrollToAnchor(y, duration) {

    var initialY = getScrollValue();
    var baseY = (initialY + y) * 0.5;
    var difference = initialY - baseY;
    var startTime = performance.now();

    function step() {

        var normalizedTime = (performance.now() - startTime) / duration;
        if (normalizedTime > 1) normalizedTime = 1;

        window.scrollTo(0, baseY + difference * Math.cos(normalizedTime * Math.PI));
        if (normalizedTime < 1) window.requestAnimationFrame(step);

    }

    window.requestAnimationFrame(step);

}

function scrollToPosition() {

    function addEvent(button, i) {

        const id = button.getAttribute('data-href');

            const elementOffset = document.querySelector(id).offsetTop; //- getTopbarHeight(); 

        button.addEventListener('click', function () {

            const scroll = getScrollValue();
            const duration = elementOffset >= scroll ? Math.sqrt(elementOffset - scroll) * 12 : Math.sqrt(scroll - elementOffset) * 12;

            scrollToAnchor(elementOffset, duration);

        });

    }

    for (let i = 0; i < smoothButton.length; i++) {

        if (smoothButton[i].getAttribute('data-href') != "") {
            addEvent(smoothButton[i], i);   
        }
    }

}

scrollToPosition();

const back = document.querySelectorAll(".js-back");
const buttonPopUp = document.querySelectorAll(".js-button-popup");
const popUp = document.querySelectorAll(".js-pop-up");

function popUpToggleShow(dataPopup) {

    body.classList.add('scroll-disable');

    for (let i = 0; i < popUp.length; i++) {

        if (popUp[i].getAttribute('data-popup') == dataPopup) {
            popUp[i].classList.add("is-active");

        }
        else {
            popUp[i].classList.remove("is-active");

        }

    }

}

function popUpToggleHide() {

    body.classList.remove('scroll-disable');

    for (let i = 0; i < popUp.length; i++) {
        popUp[i].classList.remove('is-active');

    }

}

function addShowPopupListener(button) {

    button.addEventListener('click', function (e) {
        popUpToggleShow(this.getAttribute('data-popup'));
        e.preventDefault();
        e.stopPropagation();
    });

}

function popUpToggle() {
    const back = document.querySelectorAll(".js-back");
    const buttonPopUp = document.querySelectorAll(".js-button-popup");

    for (let i = 0; i < buttonPopUp.length; i++) {
        addShowPopupListener(buttonPopUp[i]);
    }

    for (let i = 0; i < back.length; i++) {

        back[i].addEventListener('click', function () {
            popUpToggleHide();
        });
    }
}

if (document.showPopup != undefined) {
    popUpToggleShow(document.showPopup);
}

if (popUp.length > 0) {
    popUpToggle();
}

const sideBlock = document.querySelector('.js-side-block');
const sideBlockButton = document.querySelector('.js-side-block-button');
const mapElement = document.querySelector('.js-map');

function hideSideBlock() {

    if (getDeviceWidth() == "small") {
        sideBlock.classList.add('is-transform');
        mapElement.classList.add('is-full-screen');
    }

}

if (sideBlockButton != null) {
    hideSideBlock();    
}

function sideBlockToggle(){

    const sideBlockIcon = sideBlockButton.querySelector('.js-side-block-button-icon');

    sideBlockButton.addEventListener('click', function () {
        sideBlock.classList.toggle('is-transform');
        mapElement.classList.toggle('is-full-screen');
        sideBlockIcon.classList.toggle('is-rotate');
    });
}

if (sideBlockButton != null) {
    sideBlockToggle();    
}

const contentChange = document.querySelector('.js-hide-content');
const buttonChanger = document.querySelector('.js-content-changer');

function changeContent(changer, content) {
    
    changer.addEventListener('click', function () {
        content.classList.add('is-active');
    });

    content.addEventListener('click', function () {
       content.classList.remove('is-active'); 
    });
}

if (contentChange != null) {
    changeContent(buttonChanger, contentChange);    
}

function inputPhoto(){
    const input = document.querySelector('.js-input-photo');
    const photo = document.querySelector('.js-photo-from-input');
    let file = input.files;

    function readUrl(){
        const reader = new FileReader();

        reader.onload = (function (aImg) { return function (e) { aImg.src = e.target.result; }; })(img);
        reader.readAsDataURL(file);

    }



    function showPhoto(value){
        
    }
}

const profileImage = document.querySelector('.js-profile-image');
const profileInput = document.querySelector('.js-profile-input');

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            // profileImage.setAttribute('style', 'background-image:url(' + e.target.result + ')');
            profileImage.style.backgroundImage = 'url(' + e.target.result + ')';
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function readUrlOnInput() {
    profileInput.addEventListener('change', function () {
        readURL(this);
    })
}

if (profileInput != undefined) {
    readUrlOnInput();
}

let coord = {
    lat: 22,
    lng: 7.5,
    zoom: 3
};

function getMapRadiusInMeters() {
    if (googleMapInstance === false) {
        return 20000000;
    }
    var bounds = googleMapInstance.getBounds();


    var center = bounds.getCenter();
    var ne = bounds.getNorthEast();

    // r = radius of the earth in statute miles
    var r = 6371.0;

    // Convert lat or lng from decimal degrees into radians (divide by 57.2958)
    var lat1 = center.lat() / 57.2958;
    var lon1 = center.lng() / 57.2958;
    var lat2 = ne.lat() / 57.2958;
    var lon2 = ne.lng() / 57.2958;

    // distance = circle radius from center to Northeast corner of bounds
    var dis = r * Math.acos(Math.sin(lat1) * Math.sin(lat2) +
      Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1));

    return dis * 1000;
}

//inputPhoto();
function fillMapPoints(data) {

    const url = '/api/v1/map/';

    axios.get(
        url, 
        {
            params: {
                lat: coord.lat,
                lng: coord.lng,
                zoom: coord.zoom,
                radius: getMapRadiusInMeters(),
                category: data.category != undefined ? data.category : ''
            },
            headers: {
                'Content-Type': 'application/json',
                'X-API-Key': document.querySelector('meta[name="X-API-Key"]').getAttribute('content'),
                'Authorization': 'Bearer ' + document.querySelector('meta[name="jwt-token"]').getAttribute('content'),
                'Accept': 'application/json',
                'X-Localization': document.querySelector('meta[name="API-locale"]').getAttribute('content')
            }
        }
    ).then(function(response){
        placesMap(response.data.data);
    });

}

function fillMapPointsHistory() {

    const url = '/api/v1/check-ins';

    axios.get(
        url, 
        {
            headers: {
                'Content-Type': 'application/json',
                'X-API-Key': document.querySelector('meta[name="X-API-Key"]').getAttribute('content'),
                'Authorization': 'Bearer ' + document.querySelector('meta[name="jwt-token"]').getAttribute('content'),
                'Accept': 'application/json',
                'X-Localization': document.querySelector('meta[name="API-locale"]').getAttribute('content')
            }
        }
    ).then(function(response) {

        const data = response.data.data;

        let mapData = [];

        for (let i = 0; i < data.length; i ++) {
            mapData.push(data[i].point);
        }

        placesMap(mapData);
        if (mapData.length !== 0) {
          placesList(mapData) 
        }

    });

}

let mapPosition = {
    zoom: 4, //default
    center: {
        lat: 19.013886, //default
        lng: -21.254933 //default
    }
}

var googleMapInstance = false;
var oldCluster = false;

var selectLevel = 1;
function getZoomFromLevel (level) {
  if (level === 1) {
    return 5
  } 
  if (level === 2) {
    return 8
  }
  if (level === 3) {
    return 11
  }
  if (level === 4) {
    return 14
  }
  if (level === 5) {
    return 17
  }
  if (level === 6) {
    return 18
  }
}
function getLevel (zoom) {
  if (zoom > 0 && zoom <= 5) {
    return 1
  }
  if (zoom > 5 && zoom <= 8) {
    return 2
  }
  if (zoom > 8 && zoom <= 11) {
    return 3
  }
  if (zoom > 11 && zoom <= 14) {
    return 4
  }
  if (zoom > 14 && zoom <= 17) {
    return 5
  }
  if (zoom > 17 && zoom <= 21) {
    return 6
  }
}
var markers = []
var newMarker

function placesMap(data) {

    if (googleMapInstance === false) {
  
    	googleMapInstance = new google.maps.Map(mapElement, {
    		zoom: coord.zoom,
    		center: {lat: coord.lat, lng: coord.lng},
            mapTypeId: 'roadmap',
            //scrollwheel: false,
            disableDefaultUI: true,
        });
      googleMapInstance.addListener('idle', function name() {
        coord.lat = googleMapInstance.getCenter().lat();
        coord.lng = googleMapInstance.getCenter().lng();
        coord.zoom = googleMapInstance.getZoom();
        // if (selectLevel !== getLevel(coord.zoom)) {
        //     selectLevel = getLevel(coord.zoom)

        //     console.log("load");
        //     fillPublicMapPoints({
        //         category: document.querySelector('.js-place-categories').value
        //     });
        // }
        if (mapElement.getAttribute('data-public')) {
          fillPublicMapPoints({
            category: document.querySelector('.js-place-categories').value
          }) 
        } else if (mapElement.getAttribute('data-map') === 'places-history') {
          fillMapPointsHistory() 
        } else {
          fillMapPoints({
            category: document.querySelector('.js-place-categories').value
          })
        }
      })

        if (mapElement.getAttribute('data-map') === 'places-history' && data.length !== 0) {
          googleMapInstance.setZoom(3)
          googleMapInstance.setCenter({
            lat: data[0].lat,
            lng: data[0].lng
          })
        }

        coord.lat = googleMapInstance.getCenter().lat();
        coord.lng = googleMapInstance.getCenter().lng();
        coord.zoom = googleMapInstance.getZoom();


    } else {
      clearMarkers(markers)
    }
    
    function HTMLMarker(lat, lng, name, id, data){
        this.lat = lat
        this.lng = lng
        this.data = data
        this.name = name
        this.id = id
        this.pos = new google.maps.LatLng(lat,lng);
        // this.id = id;
        // this.checkedIn = checkedIn;
    }
    
    HTMLMarker.prototype = new google.maps.OverlayView();
    
    HTMLMarker.prototype.onAdd = function(){
        div = document.createElement('div');
        div.className = "custom-map-makrer";
        div.setAttribute('data-place-id', this.id);
        div.setAttribute('data-checkedin', this.checkedIn);
        div.setAttribute('data-type', this.data.type)
        var span = document.createElement('span')
        span.innerHTML = this.name
        div.appendChild(span)
        var panes = this.getPanes();
        panes.overlayImage.appendChild(div);
        this.div = div;
    }

    HTMLMarker.prototype.onRemove = function () {
      this.div.parentNode.removeChild(this.div);
    }
    
    HTMLMarker.prototype.draw = function(){
        var overlayProjection = this.getProjection();
        var position = overlayProjection.fromLatLngToDivPixel(this.pos);
        var panes = this.getPanes();
        this.div.style.left = position.x - 5 + 'px'
        this.div.style.top = position.y - 5 + 'px'
        let el = this
        let newCenter = this.pos
        let createItemData = {
          noRemove: true,
          name: this.name,
          checkedIn: this.checkedIn,
          lastCheckIn: this.data.lastCheckIn,
          id: this.id,
          image: this.data.image
        }

        google.maps.event.addDomListener(this.div, 'click', function() {
          let type = this.getAttribute('data-type')
          if (isNaN(Number(createItemData.name)) && type === 'basic' && getLevel(googleMapInstance.getZoom()) === 6) {
            this.classList.add('is-active')
            placesList([createItemData])
            targetPlaceElement(createItemData.id)
          }
          let newLevel = getLevel(googleMapInstance.getZoom()) + 1
          if (newLevel !== 7) {
            let newZoom = getZoomFromLevel(newLevel)
            let newLat = newCenter.lat()
            let newLng = newCenter.lng()
            googleMapInstance.setZoom(newZoom)
            googleMapInstance.setCenter({ lat: newLat, lng: newLng })
          }
        });
    }
  
    var locations = [];

    for (let i = 0; i < data.length; i ++) {
        let type
        if (data[i].count > 1) {
          type = 'cluster'
        } else {
          type = 'basic'
        }
        locations.push({
            location: {
                lat: data[i].lat,
                lng: data[i].lng
            },
            name: data[i].name,
            id: data[i].id,
            data: {
              checkedIn: data[i].checkedIn,
              type: type,
              lastCheckIn: data[i].lastCheckIn,
              image: data[i].image
            }
        });
    }
    
    locations.forEach(location => {

      const marker = new HTMLMarker(location.location.lat, location.location.lng, location.name, location.id, location.data)

      markers.push(marker);
      marker.setMap(googleMapInstance);
    });

    // if (oldCluster !== false) {
    //     oldCluster.clearMarkers();
    // }

    // var markerCluster = new MarkerClusterer(
    //     googleMapInstance,
    //     markers,
    //     {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'}
    // );
    // oldCluster = markerCluster;

}

function clearMarkers(points) {
  
  for (var i = 0; i < points.length; i ++) {
    points[i].setMap(null);
    //document.removeChild(document.querySelector(points[i]));
  }

  markers = [];

}

function placesList(data, noRemove) {
    const placeItemList = document.querySelector('.js-place-item-list');
    const placeItems = placeItemList.querySelectorAll('.js-place-item');
    let noExistElement
    if (data.length !== 0) {
      noExistElement = Array.prototype.slice.call(placeItemList.querySelectorAll('.js-place-item')).every(item => {
        return parseInt(item.getAttribute('data-place-id')) !== data[0].id
      })  
    }
    if (noRemove === undefined || noExistElement) {
      if (noRemove === undefined) {
        for (let i = 1; i < placeItems.length; i++) {
          placeItemList.removeChild(placeItems[i]);
        }
      }

      for (let i = 0; i < data.length; i++) {

        const newPlaceItem = document.querySelector('.js-place-item').cloneNode(true);
        const newPlaceItemImage = newPlaceItem.querySelector('.js-place-item-image');
        const newPlaceItemName = newPlaceItem.querySelector('.js-place-item-name');
        const newPlaceItemCheckIn = newPlaceItem.querySelector('.js-place-item-checkin');
        newPlaceItem.setAttribute('href', newPlaceItem.getAttribute('href').replace('empty', data[i].id));
        newPlaceItem.setAttribute('data-place-id', data[i].id);
        newPlaceItemImage.style.backgroundImage = 'url("' + data[i].image + '")';
        newPlaceItemName.innerHTML = data[i].name;

        if (data[i].checkedIn === true) {
          newPlaceItemCheckIn.innerHTML = newPlaceItemCheckIn.innerHTML + timeConverter(data[i].lastCheckIn);
        }
        else {
          newPlaceItemCheckIn.classList.add('is-hidden');
        }

        newPlaceItem.classList.remove('is-hidden');

        placeItemList.appendChild(newPlaceItem);

      }
      popUpToggle(); 
    }

}

function targetPlaceElement(id) {

    const placeItemList = document.querySelector('.js-place-item-list');
    const placeItemWrap = document.querySelector('.js-place-item-wrap')
    const placeItem = placeItemList.querySelectorAll('.js-place-item');
    const thisPlaceItem = placeItemList.querySelector('.js-place-item[data-place-id="' + id + '"]');
    
    for (let i = 0; i < placeItem.length; i ++) {
        placeItem[i].classList.remove('is-active');
    }

   placeItemWrap.scrollTop = thisPlaceItem.offsetTop

    thisPlaceItem.classList.add('is-active');

}

function timeConverter(UNIX_timestamp) {

    const a = new Date(UNIX_timestamp * 1000);
    //const months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    const months = [1,2,3,4,5,6,7,8,9,10,11,11];
    const year = a.getFullYear();
    const month = months[a.getMonth()];
    const date = a.getDate();
    const hour = a.getHours();
    const min = a.getMinutes();
    const sec = a.getSeconds();
    
    //const time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;

    const time = date + '.' + month + '.' + year;

    return time;

}

if (mapElement != undefined && mapElement.getAttribute('data-public') != 'true') {

    const url = '/api/v1/check-ins';

    axios.get(
        url, 
        {
            headers: {
                'Content-Type': 'application/json',
                'X-API-Key': document.querySelector('meta[name="X-API-Key"]').getAttribute('content'),
                'Authorization': 'Bearer ' + document.querySelector('meta[name="jwt-token"]').getAttribute('content'),
                'Accept': 'application/json',
                'X-Localization': document.querySelector('meta[name="API-locale"]').getAttribute('content')
            }
        }
    ).then(function(response){

        if (mapElement.getAttribute('data-map') == 'places') {

            const placeCategorySelect = document.querySelector('.js-place-categories');
            const placeCategoryButton = document.querySelector('.js-place-categories-button');

            if (response.data.data.length > 0) {

                const data = response.data.data[0];

                mapPosition = {
                    zoom: 4,
                    center: {
                        lat: data.point.lat, 
                        lng: data.point.lng
                    },
                }

            }

            fillMapPoints({
                category: placeCategorySelect.value
            });

            placeCategoryButton.addEventListener('click', function(){

                fillMapPoints({
                    category: placeCategorySelect.value
                });
        
            }); 

        }
        else if (mapElement.getAttribute('data-map') == 'places-history') {

            if (response.data.data.length > 0) {

                const data = response.data.data[0];

                mapPosition = {
                    zoom: 4,
                    center: {
                        lat: data.point.lat, 
                        lng: data.point.lng
                    },
                }

            }

            fillMapPointsHistory();

        }  

    });   

}

function fillPublicMapPoints(data) {

    const url = '/api/v1/map/';

    //console.log(coord.lat, coord.lng, coord.zoom, getMapRadiusInMeters())

    axios.get(
        url,
        {
            params: {
                lat: coord.lat,
                lng: coord.lng,
                zoom: coord.zoom,
                radius: getMapRadiusInMeters(),
                category: data.category != undefined ? data.category : '',
            },
            headers: {
                'Content-Type': 'application/json',
                'X-API-Key': document.querySelector('meta[name="X-API-Key"]').getAttribute('content'),
                //'Authorization': 'Bearer ' + document.querySelector('meta[name="jwt-token"]').getAttribute('content'),
                'Accept': 'application/json',
                'X-Localization': document.querySelector('meta[name="API-locale"]').getAttribute('content')
            }
        }
    ).then(function (response) {
        placesMap(response.data.data);
    });

}
if (publicMapIf()) {

    const placeCategorySelect = document.querySelector('.js-place-categories');
    const placeCategoryButton = document.querySelector('.js-place-categories-button');

    fillPublicMapPoints({
        category: placeCategorySelect.value
    });
    
    placeCategoryButton.addEventListener('click', function () {

        fillPublicMapPoints({
            category: placeCategorySelect.value
        });

    }); 

}

function publicMapIf(){
    return mapElement !== undefined && mapElement.getAttribute('data-public');
}

var pageList = 1
var isDownloaded = false

function getPlacesList (data) {
 if (!isDownloaded) {
    const url = 'api/v1/map/paged'
    let headers
    isDownloaded = true
    if (publicMapIf()) {
      headers = {
        'Content-Type': 'application/json',
        'X-API-Key': document.querySelector('meta[name="X-API-Key"]').getAttribute('content'),
        //'Authorization': 'Bearer ' + document.querySelector('meta[name="jwt-token"]').getAttribute('content'),
        'Accept': 'application/json',
        'X-Localization': document.querySelector('meta[name="API-locale"]').getAttribute('content')
      }
    } else {
      headers = {
        'Content-Type': 'application/json',
        'X-API-Key': document.querySelector('meta[name="X-API-Key"]').getAttribute('content'),
        'Authorization': 'Bearer ' + document.querySelector('meta[name="jwt-token"]').getAttribute('content'),
        'Accept': 'application/json',
        'X-Localization': document.querySelector('meta[name="API-locale"]').getAttribute('content')
      }
    }
    axios.get(
      url,
      {
        params: {
          lat: coord.lat,
          lng: coord.lng,
          page: pageList,
          radius: getMapRadiusInMeters(),
          category: data.category != undefined ? data.category : '',
        },
        headers: headers
      }
    )
    .then(function (response) {
      if (data.noRemove !== undefined) {
        placesList(response.data.data, true)
      } else {
        placesList(response.data.data);
      }
      isDownloaded = false
      const placeCategorySelect = document.querySelector('.js-place-categories');
      const placeCategoryButton = document.querySelector('.js-place-categories-button')
      getPlacesListOnScroll(document.querySelector('.js-place-item-list'), document.querySelector('.js-place-item-wrap'))
      placeCategoryButton.addEventListener('click', function () {
        pageList = 1
        getPlacesList({
          category: placeCategorySelect.value
        });

      });
    })
    .catch(error => console.log(error))
  }
 }

if (mapElement !== undefined && mapElement.getAttribute('data-map') !== 'places-history') {
  getPlacesList({
    category: document.querySelector('.js-place-categories').value
  })
}

function getPlacesListOnScroll (element, parent) {
  parent.addEventListener('scroll', function () {
    let height = element.offsetHeight
    if (parent.scrollTop + parent.offsetHeight === height) {
      pageList++
      getPlacesList({
        category: document.querySelector('.js-place-categories').value,
        noRemove: true
      })
    }
  })
}

