import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class Example extends Component {
    render() {
        return (
            <div className="component">  
                <div className="panel-body">
                    I'm an example component!
                </div>
            </div>
        );
    }
}

if (document.getElementById('js-react-example')) {
    ReactDOM.render(<Example />, document.getElementById('js-react-example'));
}
