<?php

return [
    'detail' => [
        'for-all' => [
            'asia' => 'ASIA', 'africa' => 'AFRICA', 'america' => 'AMERICA', 'europe' => 'EUROPE', 'australia' => 'AUSTRALIA',
        ],

        'for-medals' => [
            '1' => 'Medals'
        ],

        'for-flag' => [
            '1' => 'Flags'
        ],

        'buttons' => [
            '1' => 'Detail'
        ]
    ]
];
