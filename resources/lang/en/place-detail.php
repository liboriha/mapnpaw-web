<?php

return [
    'place-detail' => [

        'texts' => [
            '1' => 'Address', '2' => 'Description', '3' => 'Other users photos', '4' => 'Category'
        ],

        'raiting' => [
            '1' => 'Users rating', '2' => 'Display rating', '3' => 'Unrated',
        ]
    ]
];
