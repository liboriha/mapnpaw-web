<?php

return [
    'form' => [
        'inputs' => [
            'placeholders' =>[
                'mail' => 'E-mail', 'password' => 'Password', 'password_confirmation' => 'Password confirmation',
            ],
        ],

        'labels' => [
            'name' => 'Name', 'last-name' => 'Surname', 'nickname' => 'Nickname', 'mail' => 'E-mail', 'change-password' => 'Change password', 'name-visible' =>'Name visible', 'nickname-visible' => 'Nickname visible', 'profile-photo' => 'Upload photo<br>from computer', 'visible' => 'Make visible', 'photo' => 'Photo',
        ],

        'buttons' => [
            'facebook-login' => 'Login through FACEBOOK', 'facebook-register' => 'REGISTER through FACEBOOK', 'register' => 'Register', 'login' => 'Log in', 'logout' => 'Log out', 'profile-edit' => 'Edit profile', 'save' => 'Save', 'send' => 'Send'
        ],

        'texts' => [
            '1' => 'or', '2' => 'I´m new here. ', '3' => 'Forgotten password? Enter email address below and we´ll email instuctions for setting a new one.', '4'  => 'Already have account?', '5' => 'To change password enter your email and new password.',
        ],

        'anchors' => [
            '1' => 'Forgotten password?', '2' => 'Register', '3' => 'Log in'
        ],
        
        'help' => [
          'facebook-user' => 'When you sign up with Facebook, you can not edit your photo, name, surname, or email.'
        ]
    ],
];
