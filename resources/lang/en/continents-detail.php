<?php

return [
    'continents-detail' => [
        'for-all' => [
            'unesco' => 'UNESCO', 'nature' => 'NATURE', 'town' => 'CITIES', 'historic' => 'HISTORY', 'museum' => 'MUSEUMS', 'factory' => ' TECHNICAL HERITAGE', 'view' => 'VIEWS', 'sport' => 'SPORT', 'culture' => 'CULTURE', 'park' => 'FUNPARK'
        ],

        'buttons' => [
            '1' => 'Detail'
        ]
    ]
];
