<?php

return [
    'points-and-medals' => [
        
        'texts' => [
            '1' => 'TOTAL POINTS', '2' => 'MEDALS', '3' => 'FLAGS',
        ],

        'buttons' => [
            '1' => 'Detail'
        ],

        'hide-box' => [
             '1' => 'TOTAL POINTS', '2' => 'Points earned for photos', '3' => 'Points earned for check in', '4' => '{0} :points POINTS|{1} :points POINT|[2,4] :points POINTS|[5,*] :points POINTS',
        ],
    ]
];
