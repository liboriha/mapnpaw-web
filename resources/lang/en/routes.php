<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Routes Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for translating routes URL.
    |
    */

    'post-login' => 'login',
    'register' => 'register',
    'password-reset' => 'password-reset',
    'logout' => 'logout',
    'profile' => [
        'prefix' => 'profile',
        'edit' => 'edit',
    ],
    'map' => [
        'prefix' => 'map',
        'visited-places' => 'visited-places',
    ],
    'place' => [
        'prefix' => 'place',
        'id' => [
            'prefix' => '{placeId}',
            'detail' => 'detail',
            'rating' => 'rating',
        ],
        'delete-photo' => 'delete-photo/{photoId}',
    ],
    'points-and-medals' => 'points-and-medals',
    'medals' => [
        'prefix' => 'medals',
        'continent' => [
            'prefix' => 'continent/{continentId}',
            'category' => [
                'prefix' => 'category/{categoryId}',
                'place' => 'place/{placeId}/detail',
                'rating' => 'place/{placeId}/rating',
            ],
        ],
    ],
    'flags' => [
        'prefix' => 'flags',
        'continent' => [
            'prefix' => 'continent/{continentId}',
            'category' => [
                'prefix' => 'category/{categoryId}',
                'place' => 'place/{placeId}/detail',
                'rating' => 'place/{placeId}/rating',
            ],
        ],
    ],

];
