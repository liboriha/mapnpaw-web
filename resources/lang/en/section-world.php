<?php

return [
    'section-world' => [
        '1' => 'Life is short and the world is huge', '2' => 'Travel around the world with Map´n paw. Choose a paw on the most interesting places on the planet. Experience the world through this unique game. It is real travelling around the real world. Don´t sit at home, download the application and have an adventure.'
    ]
];
