<?php

return [
    'how-it-function' => [
        '1' => 'How does it work', '2' => 'Points, Photos', '3' => 'All playable places are marked with a paw. When your phone is within the marked location(paw), then it is possible to check in and receive point. After the first photo you will receive another point. Your achievement is shared on Facebook and your friends will see what you saw and what they can experience.', '4' => 'Medals', '5' => 'When you visit all TOP 10 places in one of 10 categories (UNESCO-NATURE-CITIES-HISTORY-MUSEUMS-TECHNICAL HERITAGE-VIEWS-SPORT-CULTURE-FUNPARKS), receive a medal from this category. All 5 continents have 10 medals, you are playing for  50 medals.','6' => 'Flags', '7' => 'The most interesting places in the world we joined together in flags. Check in to one of these unique places to receive a flag. Flags are sorted into 10 categories and 5 continents like medals. It is easy, off you go.',
    ]
];
