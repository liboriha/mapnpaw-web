<?php

return [
    'normal' => 'Check-in was successful, you receive :points.',
    'points' => '{0} :num points|{1} :num point|[2,4] :num points|[5,*] :num points',
    'top-points' => '{0} :num TOP points|{1} :num TOP point|[2,4] :num TOP points|[5,*] :num TOP points',
    'top-points-remaining' => 'This is also a TOP place in category :cat in the continent :cont. '
        . 'To receive a medal in this category, you still have to visit '
        . ':remaining.',
    'top' => 'You receive medal for TOP 10 places in category :cat in the continent :cont',
    'flag' => 'You receive flag in category :cat in the continent :cont'
];
