<?php

return [
    'map' => [
        'texts' => [
            '1' => 'Choose category', '2' => 'ALL',
        ],

        'select' => [
            'unesco' => 'UNESCO', 'nature' => 'NATURE', 'town' => 'CITIES', 'historic' => 'HISTORY', 'museum' => 'MUSEUMS', 'factory' => 'TECHNICAL HERITAGE', 'view' => 'VIEWS', 'sport' => 'SPORT', 'culture' => 'CULTURE', 'park' => 'FUNPARK'

        ],

        'buttons' => [
            '1' => 'Search'
        ],
    ]
];
