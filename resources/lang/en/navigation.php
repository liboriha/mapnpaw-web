<?php

return [
    'navigation-landing' => [
        '1' => 'Home', '2' => 'How does it work', '3' => 'Download', '4' => 'Log in', '5' => 'Registration', '6' => 'Dashboard', '7' => 'Log out', '8' => 'Map'
    ],

    'navigation-dashboard' => [
        '1' => 'Back', '2' => 'Map', '3' => 'Places visited', '4' => 'Points & medals', '5' => 'Profile'
    ]
];
