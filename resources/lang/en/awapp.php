<?php

return [

    /*
    |--------------------------------------------------------------------------
    | AWAPP Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during processing AWAPP application
    | for various messages that we need to display to the user. You are free 
    | to modify these language lines according to your application's 
    | requirements.
    |
    */

    'admin' => [
        'places' => [
            'filter' => [
                'continents' => 'Continent:',
                'categories' => 'Category:',
                'not-selected' => '-- Not selected --',
            ],
            'state' => [
                'title' => 'State:',
                'top-places' => 'In category we have :count/:max_count TOP 10 places.',
                'flag-place' => '[0,1] In category we have :count flag place.|[2,*] In category we have :count flag places.',
                'warning' => 'Warning',
            ],
            'address-gps-conversion' => [
                'button-title' => 'Convert address to GPS position',
                'error-message' => 'Address conversion was not successful for the following reason:',
            ],
        ],
    ],

    'login' => [
        'error-messages' => [
            'facebook_not_have_email' => "Can't register facebook user because of missing email.",
            'normal_login_as_facebook' => "Account which you want to login is regular account, try login by regular way.",
            'facebook_cant_register' => "Can't register facebook user because of email conflict.",
            'facebook_login_as_normal' => "Account which you want to login is facebook account, try login through facebook.",
        ],
    ],

    'register' => [
        'success-messages' => [
            'registered' => 'You was successfully registered.',
        ],
    ],

    'reset-password' => [
        'error-messages' => [
            'facebook-cant-reset-password' => "Can't reset password of user registrated by facebook service.",
            'failed' => "Sending of e-mail for resetting user password failed.",
        ],
        'success-messages' => [
            'send-email' => 'On user e-mail was send message with link for resetting user password.',
        ],
    ],

    'profile' => [
        'success-messages' => [
            'updated' => 'Profile was successfully updated.',
        ],
    ],

    'place' => [
        'success-messages' => [
            'photo-deleted' => 'User photo was successfully deleted.',
        ],
    ],

];
