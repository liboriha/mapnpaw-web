<?php

return [
    'place-detail' => [

        'texts' => [
            '1' => 'Adresa', '2' => 'Popis', '3' => 'Fotky ostatních uživatelů', '4' => 'Kategorie'
        ],

        'raiting' => [
            '1' => 'Hodnocení uživatelů', '2' => 'Zobrazit hodnocení', '3' => 'Nehodnoceno',
        ]
    ]
];
