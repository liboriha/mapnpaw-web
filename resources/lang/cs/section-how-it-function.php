<?php

return [
    'how-it-function' => [
        '1' => 'Jak to funguje', '2' => 'Body, fotky', '3' => 'Všechna hrací místa vidíš v mapě jako tlapky. Když se tvůj telefon dostane do okruhu místa=tlapky, tak si dáš check-in a získáš bod. Za první fotku místa získáš další bod. Svůj úspěch sdílej na facebooku a ukaž kamarádům, cos zažil a co mohou zažít také.', '4' => 'Medaile', '5' => 'Až navštívíš všech 10 TOP míst v jedné z 10 kategorií (UNESCO-PŘÍRODA-MĚSTA-HISTORIE-MUZEA-TECHNIKA-VYHLÍDKY-SPORT-KULTURA-ZÁBAVNÍ PARKY), získáš medaili za tuto kategorii. Každý z 5 kontinentů má svých 10 medailí, hraješ tedy o 50 medailí.','6' => 'Vlajky', '7' => 'Ta nejzajímavější místa světa jsme shromáždili do vlajek. Checknutím se na takovémto jedinečném místě pro tebe znamená zisk vlajky. Vlajky řadíme do 10 kategorií a 5 kontinentů, podobně jako medaile. Je to tak snadné, stačí vyrazit.',
    ]
];
