<?php

return [

    /*
    |--------------------------------------------------------------------------
    | AWAPP Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during processing AWAPP application
    | for various messages that we need to display to the user. You are free 
    | to modify these language lines according to your application's 
    | requirements.
    |
    */

    'admin' => [
        'places' => [
            'filter' => [
                'continents' => 'Kontinent:',
                'categories' => 'Kategorie:',
                'not-selected' => '-- Nezvoleno --',
            ],
            'state' => [
                'title' => 'Stav:',
                'top-places' => 'V kategorii je momenálně :count/:max_count TOP 10 míst.',
                'flag-place' => '{0} V kategorii je momenálně :count vlajkových míst.|{1} V kategorii je momenálně :count vlajkové místo.|[2,4] V kategorii jsou momenálně :count vlajková místa.|[5,*] V kategorii je momenálně :count vlajkových míst.',
                'warning' => 'Pozor',
            ],
            'address-gps-conversion' => [
                'button-title' => 'Převést adresu na GPS pozici',
                'error-message' => 'Převod adresy na GPS nebyl úspěšný z důvodu:',
            ],
        ],
    ],

    'login' => [
        'error-messages' => [
            'facebook-not-have-email' => 'Nelze zaregistrovat facebookového uživatele kvůli chybějícímu e-mailu.',
            'normal-login-as-facebook' => 'Účet na který se chcete přihlásit je normální účet, zkuste se přihlásit běžným způsobem.',
            'facebook-cant-register' => 'Nelze zaregistrovat facebookového uživatele kvůli konfliktu e-mailu.',
            'facebook-login-as-normal' => 'Účet na který se chcete přihlásit je facebookový účet, zkuste se přihlásit pomocí facebooku.',
        ],
    ],

    'register' => [
        'success-messages' => [
            'registered' => 'Byl/a jste úspěšně zaregistrován/a.',
        ],
    ],

    'reset-password' => [
        'error-messages' => [
            'facebook-cant-reset-password' =>  'Nelze resetovat heslo uživatele registrovaného pomocí facebooku.',
            'failed' => 'Nepodařilo se odeslat e-mail k resetování hesla.',
        ],
        'success-messages' => [
            'send-email' => 'Na e-mail uživatele byl poslán odkaz k resetování hesla.',
        ],
    ],

    'profile' => [
        'success-messages' => [
            'updated' => 'Profil byl úspěšně aktualizován.',
        ],
    ],

    'place' => [
        'success-messages' => [
            'photo-deleted' => 'Uživatelská fotka byla úspěšně smazána.',
        ],
    ],

];
