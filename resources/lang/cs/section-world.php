<?php

return [
    'section-world' => [
        '1' => 'Život je krátký a svět obrovský', '2' => 'Cestuj s Map´n paw okolo celého světa. Sbírej tlapky na nejzajímavějších místech planety. Poznávej svět formou jedinečné hry. Jedná se o skutečné cestování po reálném světě. Neseď doma, stáhni si apku a vydej se vstříc dobrodružství.'
    ]
];
