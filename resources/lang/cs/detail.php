<?php

return [
    'detail' => [
        'for-all' => [
            'asia' => 'ASIE', 'africa' => 'AFRIKA', 'america' => 'AMERIKA', 'europe' => 'EVROPA', 'australia' => 'AUSTRÁLIE',
        ],

        'for-medals' => [
            '1' => 'Medaile'
        ],

        'for-flag' => [
            '1' => 'Vlajky'
        ],

        'buttons' => [
            '1' => 'Detail'
        ]
    ]
];
