<?php

return [
    'form' => [
        'inputs' => [
            'placeholders' =>[
                'mail' => 'E-mail', 'password' => 'Heslo', 'password_confirmation' => 'Heslo znovu',
            ],
        ],

        'labels' => [
            'name' => 'Jméno', 'last-name' => 'Příjmení', 'nickname' => 'Přezdívka', 'mail' => 'E-mail', 'change-password' => 'Změnit heslo', 'name-visible' =>'Chci být vidět podle jména', 'nickname-visible' => 'Chci být vidět podle přezdívky', 'profile-photo' => 'Nahrát fotografii<br>z počítače', 'visible' => 'Chci být vidět podle', 'photo' => 'Fotografie',
        ],

        'buttons' => [
            'facebook-login' => 'PŘIHLÁSIT SE PŘES FACEBOOK', 'facebook-register' => 'REGISTROVAT SE PŘES FACEBOOK', 'register' => 'Registrovat se', 'login' => 'Přihlásit se', 'logout' => 'Odhlásit se', 'profile-edit' => 'Upravit profil', 'save' => 'Uložit', 'send' => 'Odeslat'
        ],

        'texts' => [
            '1' => 'nebo', '2' => 'Jsem tu nový. ', '3' => 'Zapomněli jste heslo? Zadejte e-mail, na který Vám přijde odkaz pro změnu hesla.', '4' => 'Již máte účet?', '5' => 'Pro změnu hesla zadejte svůj email a nové heslo.',
        ],

        'anchors' => [
            '1' => 'Zapomněli jste heslo?', '2' => 'Registrovat', '3' => 'Přihlášení'
        ],

        'help' => [
          'facebook-user' => 'Při registraci přes Facebook není možné editovat fotku, jméno, příjmení ani e-mail.'
        ]
    ],
];
