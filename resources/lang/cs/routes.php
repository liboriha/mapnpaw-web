<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Routes Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for translating routes URL.
    |
    */

    'post-login' => 'prihlasit',
    'register' => 'registrovat',
    'password-reset' => 'reset-hesla',
    'logout' => 'odhlasit',
    'profile' => [
        'prefix' => 'profil',
        'edit' => 'editace',
    ],
    'map' => [
        'prefix' => 'mapa',
        'visited-places' => 'navstivena-mista',
    ],
    'place' => [
        'prefix' => 'misto',
        'id' => [
            'prefix' => '{placeId}',
            'detail' => 'detail',
            'rating' => 'hodnoceni',
        ],
        'delete-photo' => 'vymazat-fotku/{photoId}',
    ],
    'points-and-medals' => 'body-a-medaile',
    'medals' => [
        'prefix' => 'medaile',
        'continent' => [
            'prefix' => 'kontinent/{continentId}',
            'category' => [
                'prefix' => 'kategorie/{categoryId}',
                'place' => 'misto/{placeId}/detail',
                'rating' => 'misto/{placeId}/hodnoceni',
            ],
        ],
    ],
    'flags' => [
        'prefix' => 'vlajky',
        'continent' => [
            'prefix' => 'kontinent/{continentId}',
            'category' => [
                'prefix' => 'kategorie/{categoryId}',
                'place' => 'misto/{placeId}/detail',
                'rating' => 'misto/{placeId}/hodnoceni',
            ],
        ],
    ],

];
