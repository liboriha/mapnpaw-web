<?php

return [
    'normal' => 'Check-in proběhl úspěšně, přičetli jsme vám :points.',
    'points' => '{0} :num bodů|{1} :num bod|[2,4] :num body|[5,*] :num bodů',
    'top-points' => '{0} :num TOP bodů|{1} :num TOP bod|[2,4] :num TOP body|[5,*] :num TOP bodů',
    'top-points-remaining' => 'Ten je zároveň TOP bodem v kategorii :cat na kontinentu :cont. '
        . 'K získání medaile v této kategorii vám chybí navštívit ještě '
        . ':remaining.',
    'top' => 'Získáváte medaili za TOP 10 bodů v kategorii :cat na kontinentu :cont',
    'flag' => 'Získáváte vlajku v kategorii :cat na kontinentu :cont'
];