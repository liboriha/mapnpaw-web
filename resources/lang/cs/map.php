<?php

return [
    'map' => [
        'texts' => [
            '1' => 'Vyberte kategorii', '2' => 'VŠECHNY',
        ],

        'select' => [
            'unesco' => 'UNESCO', 'nature' => 'PŘÍRODA', 'town' => 'MĚSTA', 'historic' => 'HISTORIE', 'museum' => 'MUZEA', 'factory' => 'TECHNIKA', 'view' => 'VYHLÍDKY', 'sport' => 'SPORT', 'culture' => 'KULTURA', 'park' => 'ZÁBAVNÍ PARKY'

        ],

        'buttons' => [
            '1' => 'Vyhledat'
        ],
    ]
];
