<?php

return [
    'continents-detail' => [
        'for-all' => [
            'unesco' => 'UNESCO', 'nature' => 'PŘÍRODA', 'town' => 'MĚSTA', 'historic' => 'HISTORIE', 'museum' => 'MUZEA', 'factory' => 'TECHNIKA', 'view' => 'VYHLÍDKY', 'sport' => 'SPORT', 'culture' => 'KULTURA', 'park' => 'ZÁBAVNÍ PARKY'
        ],

        'buttons' => [
            '1' => 'Detail'
        ]
    ]
];
