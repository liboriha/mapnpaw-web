<?php

return [
    'points-and-medals' => [
        
        'texts' => [
            '1' => 'CELKOVÝ POČET BODŮ', '2' => 'MEDAILE', '3' => 'VLAJKY',
        ],

        'buttons' => [
            '1' => 'Detail'
        ],

        'hide-box' => [
             '1' => 'CELKOVÝ POČET BODŮ', '2' => 'Získáno za přidané fotografie', '3' => 'Získáno za check-in', '4' => '{0} :points BODŮ|{1} :points BOD|[2,4] :points BODY|[5,*] :points BODŮ',
        ],
    ]
];
