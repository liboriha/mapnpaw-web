<?php

return [
    'navigation-landing' => [
        '1' => 'Domů', '2' => 'Jak to funguje', '3' => 'Stáhnout', '4' => 'Přihlásit', '5' => 'Registrace', '6' => 'Menu', '7' => 'Odhlásit', '8' => 'Mapa'
    ],

    'navigation-dashboard' => [
        '1' => 'Zpět', '2' => 'Mapa', '3' => 'Navštívená místa', '4' => 'Body & medaile', '5' => 'Profil'
    ]
];
