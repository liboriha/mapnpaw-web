
@extends("app")

@section('content')

    @include('include/navigation')

    @include('include/second-navigation', [$historyBarType = 'medals', $historyBarPage = 'placeList'])


  <div class="page-row section section-with-secondary-bar place-list">

        <div class="section-content section-margin-top">

            <div class="row">

                @foreach ($places as $place)

                    <div class="small-12 medium-4 large-3 column {{ $place->checkedIn ? 'checkedin' : '' }} {{ $loop->last ? 'end' : '' }}">

                        <a href="{{ route('medals.place.detail', ['continentId' => $continentId, 'categoryId' => $categoryId, 'placeId' => $place->id]) }}" class="card-item">

                            <div class="card-content card-content-no-flex card-content-hover" style="background-image: url('{{ \App\Http\Helpers\FileHelper::getURL($place->image) }}')">

                                <div class="card-label card-label-min-bottom">

                                    <div class="card-text card-text-shadow" style="font-weight: 500">{{ $place->name }}</div>
                                    @if ($place->checkedIn)
                                        <p>{{ \Carbon\Carbon::createFromTimestamp($place->lastCheckIn)->format('j.n.Y') }}</p>
                                    @endif

                                </div>

                            </div>

                        </a>

                    </div>

                @endforeach

            </div>

        </div>

    </div>

@endsection