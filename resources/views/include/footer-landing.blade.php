
<div class="page-row footer-landing">

    <div class="footer-landing-content">

        <div class="navigation-landing-logo footer-landing-logo"></div>

        <div class="footer-landing-icon-group">

            <a href="https://www.google.com/" target="_blank" class="footer-landing-icon icon-instagram"></a>

            <a href="https://www.google.com/" target="_blank" class="footer-landing-icon icon-facebook"></a>

        </div>

        <div class="footer-text">@lang('footer.footer-landing.1')</div>

    </div>

</div>


