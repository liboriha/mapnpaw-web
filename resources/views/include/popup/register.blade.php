
    <div class="page-row form js-pop-up" data-popup="register">

        <div class="row">

            <div class="small-12 column">
            
                <div class="form-content">

                    <a class="form-back js-back"></a>

                    <form action="{{ route('register') }}" method="POST">

                        {{ csrf_field() }}

                        <div class="form-logo"></div>

                        <a href="{{ route('facebook-redirect') }}" class="main-button form-button fb-button"><i class="button-fb-img"></i>@lang('form.form.buttons.facebook-register')</a>

                        <p class="">@lang('form.form.texts.1')</p>

                        <div class="form-wrap">

                            <div class="input-row {{ \App\BladeHelper::failClass('email', 'register') }}">             
                                        
                                <input placeholder="@lang('form.form.inputs.placeholders.mail')" type="text" name="email" class="input" value="{{ \App\BladeHelper::old('email', null, 'register') }}">
                                    
                                {!! \App\BladeHelper::addFailParagraph('email', 'register') !!}
                                    
                            </div>

                            <div class="input-row {{ \App\BladeHelper::failClass('password', 'register') }}">
                                        
                                <input placeholder="@lang('form.form.inputs.placeholders.password')" type="password" name="password" class="input">
                                    
                                {!! \App\BladeHelper::addFailParagraph('password', 'register') !!}
                                    
                            </div>

                        </div>

                        <button type="submit" class="main-button form-button">@lang('form.form.buttons.register')</button>

                        <p class="p-center-bold">@lang('form.form.texts.4') <a class="js-button-popup" data-popup="log-in">@lang('form.form.anchors.3')</a></p>

                    </form>

                </div>

            </div>

        </div>

    </div>
