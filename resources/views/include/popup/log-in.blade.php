

    <div class="page-row form js-pop-up" data-popup="log-in">

        <div class="row">

            <div class="small-12 column">
            
                <div class="form-content">

                    <a class="form-back js-back"></a>

                    <form action="{{ route('post-login') }}" method="POST">

                        {{ csrf_field() }}

                        <div class="form-logo"></div>

                        @if ($errors->has('login'))
                            <p class="form-p-small fail">
                                @foreach($errors->get('login') as $error)
                                    {{ $error }}<br/>
                                @endforeach
                            </p>
                        @endif

                        <a href="{{ route('facebook-redirect') }}" class="main-button form-button fb-button"><i class="button-fb-img"></i>@lang('form.form.buttons.facebook-login')</a>

                        <p class="">@lang('form.form.texts.1')</p>

                        <div class="form-wrap">

                            <div class="input-row {{ \App\BladeHelper::failClass('email', 'log-in') }}">
                                        
                                <input placeholder="@lang('form.form.inputs.placeholders.mail')" type="text" name="email" class="input" value="{{ \App\BladeHelper::old('email', null, 'log-in') }}">
                                    
                                {!! \App\BladeHelper::addFailParagraph('email', 'log-in') !!}

                            </div>

                            <div class="input-row {{ \App\BladeHelper::failClass('password', 'log-in') }}">
                                        
                                <input placeholder="@lang('form.form.inputs.placeholders.password')" type="password" name="password" class="input">
                                    
                                {!! \App\BladeHelper::addFailParagraph('password', 'log-in') !!}
                                    
                            </div>

                        </div>

                        <a class="a-on-right js-button-popup" data-popup="forgot-password">@lang('form.form.anchors.1')</a>

                        <button type="submit" class="main-button form-button">@lang('form.form.buttons.login')</button>

                        <p class="p-center-bold">@lang('form.form.texts.2')<a class="js-button-popup" data-popup="register">@lang('form.form.anchors.2')</a></p>

                    </form>

                </div>

            </div>

        </div>

    </div>