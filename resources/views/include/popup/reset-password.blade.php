

    <div class="page-row form js-pop-up" data-popup="reset-password">

        <div class="row">

            <div class="small-12 column">
            
                <div class="form-content">

                    <a class="form-back js-back"></a>

                    <div class="form-p">@lang('form.form.texts.4')</div>

                    <form action="{{ route('password.reset-process', ['token' => $restoreToken]) }}" method="POST">

                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $restoreToken }}">

                        <div class="form-wrap">

                            <div class="input-row {{ \App\BladeHelper::failClass('email', 'reset-password') }}">             
                                        
                                <input placeholder="@lang('form.form.inputs.placeholders.mail')" type="text" name="email" class="input" value="{{ \App\BladeHelper::old('email', null, 'reset-password') }}">
                                    
                                {!! \App\BladeHelper::addFailParagraph('email', 'reset-password') !!}
                                    
                            </div>

                            <div class="input-row {{ \App\BladeHelper::failClass('password', 'reset-password') }}">             
                                        
                                <input placeholder="@lang('form.form.inputs.placeholders.password')" type="password" name="password" class="input">
                                    
                                {!! \App\BladeHelper::addFailParagraph('password', 'reset-password') !!}
                                    
                            </div>

                            <div class="input-row {{ \App\BladeHelper::failClass('password_confirmation', 'reset-password') }}">             
                                        
                                <input placeholder="@lang('form.form.inputs.placeholders.password_confirmation')" type="password" name="password_confirmation" class="input">
                                    
                                {!! \App\BladeHelper::addFailParagraph('password_confirmation', 'reset-password') !!}
                                    
                            </div>

                        </div>

                        <button type="submit" class="main-button form-button">@lang('form.form.buttons.send')</button>

                    </form>

                </div>

            </div>

        </div>

    </div>