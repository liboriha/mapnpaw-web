

    <div class="page-row form js-pop-up" data-popup="forgot-password">

        <div class="row">

            <div class="small-12 column">
            
                <div class="form-content">

                    <a class="form-back js-back"></a>

                    <div class="form-p">@lang('form.form.texts.3')</div>

                    <form action="{{ route('send-reset-link') }}" method="POST">

                        {{ csrf_field() }}

                        <div class="input-group">

                            <div class="input-row {{ \App\BladeHelper::failClass('email', 'forgot-password') }}">             
                                        
                                <input placeholder="@lang('form.form.inputs.placeholders.mail')" type="text" name="email" class="input" value="{{ \App\BladeHelper::old('email', null, 'forgot-password') }}">
                                    
                                {!! \App\BladeHelper::addFailParagraph('email', 'forgot-password') !!}
                                    
                            </div>

                        </div>

                        <button type="submit" class="main-button form-button">@lang('form.form.buttons.send')</button>

                    </form>

                </div>

            </div>

        </div>

    </div>