
<div class="history-bar">

    <div class="history-bar-content">


    @if(isset($historyBarType) && $historyBarType == 'flag')
 

        <a href="{{ route('points-and-medals') }}" class="history-bar-item">@lang('navigation-second.navigation-second.6')</a>

        <span class="history-bar-item">></span>

        <a href="{{ route('flags') }}" class="history-bar-item @if($historyBarPage == 'detail')selected @endif">{{ $continentName ?? __('navigation-second.navigation-second.2') }}</a>

        <span class="history-bar-item @if($historyBarPage == 'detail')is-hidden @endif">></span>

        <a href="{{ route('flags.categories', ['continentId' => $continentId ?? 0]) }}" class="history-bar-item @if($historyBarPage == 'continentsDetail')selected @endif @if($historyBarPage == 'detail')is-hidden @endif">{{ $categoryName ?? __('navigation-second.navigation-second.3') }}</a>

        <span class="history-bar-item @if($historyBarPage == 'detail' || $historyBarPage == 'continentsDetail')is-hidden @endif">></span>

        <a href="{{ route('flags.places', ['continentId' => $continentId ?? 0, 'categoryId' => $categoryId ?? 0]) }}" class="history-bar-item @if($historyBarPage == 'placeList')selected @endif @if($historyBarPage == 'detail' || $historyBarPage == 'continentsDetail')is-hidden @endif">@lang('navigation-second.navigation-second.4')</a>

        <span class="history-bar-item @if($historyBarPage == 'detail' || $historyBarPage == 'continentsDetail' ||  $historyBarPage == 'placeList')is-hidden @endif">></span>

        <a href="{{ route('flags.place.detail', ['continentId' => $continentId ?? 0, 'categoryId' => $categoryId ?? 0, 'placeId' => $placeId ?? 0]) }}" class="history-bar-item @if($historyBarPage == 'placeList')selected @endif @if($historyBarPage == 'detail' || $historyBarPage == 'continentsDetail' ||  $historyBarPage == 'placeList')is-hidden @endif">{{ $placeName ?? '' }}</a>

    @else

        <a href="{{ route('points-and-medals') }}" class="history-bar-item">@lang('navigation-second.navigation-second.5')</a>

        <span class="history-bar-item">></span>

        <a href="{{ route('medals') }}" class="history-bar-item @if($historyBarPage == 'detail')selected @endif">{{ $continentName ?? __('navigation-second.navigation-second.2') }}</a>

        <span class="history-bar-item @if($historyBarPage == 'detail')is-hidden @endif">></span>

        <a href="{{ route('medals.categories', ['continentId' => $continentId ?? 0]) }}" class="history-bar-item @if($historyBarPage == 'continentsDetail')selected @endif @if($historyBarPage == 'detail')is-hidden @endif">{{ $categoryName ?? __('navigation-second.navigation-second.3') }}</a>

        <span class="history-bar-item @if($historyBarPage == 'detail' || $historyBarPage == 'continentsDetail')is-hidden @endif">></span>

        <a href="{{ route('medals.places', ['continentId' => $continentId ?? 0, 'categoryId' => $categoryId ?? 0]) }}" class="history-bar-item @if($historyBarPage == 'placeList')selected @endif @if($historyBarPage == 'detail' || $historyBarPage == 'continentsDetail')is-hidden @endif">@lang('navigation-second.navigation-second.4')</a>

        <span class="history-bar-item @if($historyBarPage == 'detail' || $historyBarPage == 'continentsDetail' ||  $historyBarPage == 'placeList')is-hidden @endif">></span>

        <a href="{{ route('medals.place.detail', ['continentId' => $continentId ?? 0, 'categoryId' => $categoryId ?? 0, 'placeId' => $placeId ?? 0]) }}" class="history-bar-item @if($historyBarPage == 'placeList')selected @endif @if($historyBarPage == 'detail' || $historyBarPage == 'continentsDetail' ||  $historyBarPage == 'placeList')is-hidden @endif">{{ $placeName ?? '' }}</a>


     @endif

    </div>

</div>
