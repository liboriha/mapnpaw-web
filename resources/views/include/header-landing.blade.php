
<div class="page-row header-landing" id="home">

    <div class="header-landing-content">

        <div class="row">

            <div class="small-12 column">

                <div class="header-landing-wrap">

                <div class="header-landing-box">
                
                    <div class="header-app-logo"></div>
                
                    <h1 class="h1-landing">@lang('header-landing.header-landing.1')</h1>
                
                    <div class="landing-button-group">
                
                        <a class="button-landing">
                            <i class="button-landing-img app-store"></i>@lang('buttons-landing.download-button.app-store')</a>
                
                        <a class="button-landing">
                            <i class="button-landing-img play-store"></i>@lang('buttons-landing.download-button.play-store')</a>
                
                    </div>
                
                </div>
                
                <div class="header-landing-iphone"></div>

                </div>



            </div>

        </div>

        <a class="header-arrow js-smooth" data-href="#how-it-function"></a>

    </div>

</div>


