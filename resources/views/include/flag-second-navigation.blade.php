
<div class="history-bar">

    <div class="history-bar-content">

        <a href="{{ route('points-and-medals') }}" class="history-bar-item">Vlajky</a>

        <span class="history-bar-item">></span>

        <a href="{{ route('flags') }}" class="history-bar-item">Kontinenty</a>

        <span class="history-bar-item">></span>

        <a href="{{ route('flags.categories', ['continentId' => $continentId ?? 0]) }}" class="history-bar-item selected">Kategorie</a>

    </div>

</div>


