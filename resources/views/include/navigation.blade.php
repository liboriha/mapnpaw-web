
<div class="page-row navigation">

    <div class="navigation-content">

        <a href="{{ route('home') }}" class="navigation-logo"></a>

        <div class="navigation-icon js-menu-icon">

            <div class="line"></div>

            <div class="line"></div>

            <div class="line"></div>

        </div>


        <a class="navigation-back @if(Route::currentRouteName() == 'map' || Route::currentRouteName() == 'map.visited-places' || Route::currentRouteName() == 'points-and-medals' || Route::currentRouteName() == 'profile') is-hidden @endif" onclick="window.history.back()"><i class="navigation-back-icon"></i>@lang("navigation.navigation-dashboard.1")</a>

        <ul class="navigation-wrap js-menu">

            <li class="navigation-cross">
                
                <div class="cross-content js-menu-cross">

                    <div class="line"></div>

                    <div class="line"></div>

                </div>
            
            </li>

            <li class="@if (Route::currentRouteName() == 'map' || Route::currentRouteName() == 'place.detail' || Route::currentRouteName() == 'place.rating')is-active @endif"><a href="{{ route('map') }}">@lang("navigation.navigation-dashboard.2")</a></li>

            <li class="@if (Route::currentRouteName() == 'map.visited-places' || Route::currentRouteName() == 'map.visited-places' || Route::currentRouteName() == 'map.visited-places')is-active @endif"><a href="{{ route('map.visited-places') }}">@lang("navigation.navigation-dashboard.3")</a></li>

            <li class="@if (Route::currentRouteName() == 'points-and-medals' || Route::currentRouteName() == 'medals' || Route::currentRouteName() == 'medals.categories' || Route::currentRouteName() == 'medals.places' || Route::currentRouteName() == 'medals.place.detail' || Route::currentRouteName() == 'medals.place.rating' || Route::currentRouteName() == 'flags' || Route::currentRouteName() == 'flags.categories' || Route::currentRouteName() == 'flags.places' || Route::currentRouteName() == 'flags.place.detail' || Route::currentRouteName() == 'flags.place.rating')is-active @endif"><a href="{{ route('points-and-medals') }}">@lang("navigation.navigation-dashboard.4")</a></li>

            <li class="profile-li">

                <div class="navigation-profile">

                    <a href="{{ route('profile') }}">{{ Auth::user()->name != '' ? Auth::user()->name : __("navigation.navigation-dashboard.5") }}<i class="navigation-profile-photo" style="background-image: url('{{ \App\Http\Helpers\FileHelper::getURL(Auth::user()->avatar) }}')"></i></a>

                </div>

            </li>

        </ul>

        <ul class="header-mutation js-mutation">

            <div class="header-mutation-arrow"></div>
            @foreach (LaravelLocalization::getSupportedLocales() as $localeKey => $locale)

                <li class="{{ LaravelLocalization::getCurrentLocale() == $localeKey ? 'is-active' : '' }}">
                    <a href="{{ LaravelLocalization::getLocalizedURL($localeKey, route('map')) }}">{{ $localeKey }}</a>
                </li>

            @endforeach
        </ul>

    </div>

    {{-- Display success, error, info messages. --}}
    @if (session('alert-type', null) != null)
        <div class="notification-bar {{ session('alert-type', '') }}">
            {{ session('message') }}
        </div>
    @endif

</div>


