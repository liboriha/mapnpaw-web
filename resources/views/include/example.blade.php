
<div class="page-row pixelmate-demo">

    <div class="text-center">

        <a class="pixelmate-logo">
            <img src="{{ asset('images/brands/logo.svg') }}" alt="www.pixelmate.cz">
        </a>

    </div>

</div>

<div class="page-row pixelmate-example-grid">

    <div class="row">
    
        <div class="small-12 medium-6 large-4 columns">
            <div class="pixelmate-example-block"></div>
        </div>

        <div class="small-12 medium-6 large-4 columns">
            <div class="pixelmate-example-block"></div>
        </div>

        <div class="small-12 medium-6 large-4 columns">
            <div class="pixelmate-example-block"></div>
        </div>

        <div class="small-12 medium-6 large-4 columns">
            <div class="pixelmate-example-block"></div>
        </div>

        <div class="small-12 medium-6 large-4 columns">
            <div class="pixelmate-example-block"></div>
        </div>

        <div class="small-12 medium-6 large-4 columns">
            <div class="pixelmate-example-block" id="js-react-example"></div>
        </div>

    </div>

</div>


