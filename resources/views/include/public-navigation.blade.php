
<div class="page-row navigation">

    <div class="navigation-content">

        <a href="{{ route('home') }}" class="navigation-logo"></a>

        <div class="navigation-icon js-menu-icon">

            <div class="line"></div>

            <div class="line"></div>

            <div class="line"></div>

        </div>

        <ul class="navigation-wrap js-menu">

            <li class="navigation-cross">
                
                <div class="cross-content js-menu-cross">

                    <div class="line"></div>

                    <div class="line"></div>

                </div>
            
            </li>

            <li class="@if (Route::currentRouteName() == 'map') is-active @endif"><a href="{{ route('map') }}">@lang('navigation.navigation-landing.8')</a></li>

            <li><a href="{{ route('home') }}#home">@lang("navigation.navigation-landing.1")</a></li>

            <li><a href="{{ route('home') }}#how-it-function">@lang('navigation.navigation-landing.2')</a></li>

            <li><a href="{{ route('home') }}#download">@lang('navigation.navigation-landing.3')</a></li>

            <li><a  class="js-button-popup" data-popup="log-in">@lang('navigation.navigation-landing.4')</a></li>

            <li class="navigation-item-converse"><a class="js-button-popup" data-popup="register">@lang('navigation.navigation-landing.5')</a></li>

        </ul>

        <ul class="header-mutation js-mutation">

            <div class="header-mutation-arrow"></div>
            @foreach (LaravelLocalization::getSupportedLocales() as $localeKey => $locale)

                <li class="{{ LaravelLocalization::getCurrentLocale() == $localeKey ? 'is-active' : '' }}">
                    <a href="{{ LaravelLocalization::getLocalizedURL($localeKey, route('map')) }}">{{ $localeKey }}</a>
                </li>

            @endforeach
        </ul>

    </div>

    {{-- Display success, error, info messages. --}}
    @if (session('alert-type', null) != null)
        <div class="notification-bar {{ session('alert-type', '') }}">
            {{ session('message') }}
        </div>
    @endif

</div>

@section('popups')

    @include('include/popup/log-in')

    @include('include/popup/forgot-password')

    @if (isset($restoreToken))
        @include('include/popup/reset-password')
    @endif

    @include('include/popup/register')

@endsection
