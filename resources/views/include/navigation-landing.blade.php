
    {{-- Display success, error, info messages. --}}
    @if (session('alert-type', null) != null)
        <div class="notification-bar notification-bar-landing {{ session('alert-type', '') }}">
            {{ session('message') }}
        </div>
    @endif

<div class="page-row navigation-landing js-top-bar">

    <div class="navigation-landing-content">

        <a class="navigation-landing-logo"></a>

        <div class="navigation-landing-icon js-menu-icon">

            <div class="line"></div>

            <div class="line"></div>

            <div class="line"></div>

        </div>

        <ul class="navigation-landing-wrap js-menu">

            <li class="navigation-landing-cross">
                
                <div class="cross-content js-menu-cross">

                    <div class="line"></div>

                    <div class="line"></div>

                </div>
            
            </li>

            @if (!Auth::check())
            
                <li><a href="{{ route('map') }}">@lang('navigation.navigation-landing.8')</a></li>

            @endif

            <li><a class="js-smooth" data-href="#home">@lang("navigation.navigation-landing.1")</a></li>

            <li><a class="js-smooth" data-href="#how-it-function">@lang('navigation.navigation-landing.2')</a></li>

            <li><a class="js-smooth" data-href="#download">@lang('navigation.navigation-landing.3')</a></li>

            @if (Auth::check())
                @if (Auth::user()->hasRole('user'))

                    <li><a href="{{ route('map') }}">@lang('navigation.navigation-landing.6')</a></li>

                @elseif (Auth::user()->hasRole('admin') || Auth::user()->hasRole('superadmin'))

                    <li><a href="{{ route('voyager.dashboard') }}">@lang('navigation.navigation-landing.6')</a></li>
                    
                @endif

                <li><a href="{{ route('logout') }}">@lang('navigation.navigation-landing.7')</a></li>
            @else

                <li><a  class="js-button-popup" data-popup="log-in">@lang('navigation.navigation-landing.4')</a></li>

                <li><a class="menu-item-converse js-button-popup" data-popup="register">@lang('navigation.navigation-landing.5')</a></li>
            @endif

        </ul>

        <ul class="header-mutation header-mutation-landing js-mutation">
            <div class="header-mutation-arrow"></div>
            @foreach (LaravelLocalization::getSupportedLocales() as $localeKey => $locale)

                <li class="{{ LaravelLocalization::getCurrentLocale() == $localeKey ? 'is-active' : '' }}">
                    <a href="{{ LaravelLocalization::getLocalizedURL($localeKey, route('home')) != null ? LaravelLocalization::getLocalizedURL($localeKey, route('home')) : '/' }}">{{ $localeKey }}</a>
                </li>

            @endforeach
        </ul>

    </div>

</div>


