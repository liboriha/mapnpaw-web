
@extends("app")

@section('content')

    @if (Auth::check())
        @include('include/navigation')
    @else
        @include('include/public-navigation')
    @endif

    @if (Route::currentRouteName() == 'medals.place.detail')
        @include('include/second-navigation', [$historyBarType = 'medals', $historyBarPage = 'placeDetail'])
    @elseif (Route::currentRouteName() == 'flags.place.detail')
        @include('include/second-navigation', [$historyBarType = 'flag', $historyBarPage = 'placeDetail'])
    @endif


    <div class="page-row section place-detail {{ $place->checkedIn ? 'checkedin' : '' }}">

        <div class="header" style="background-image: url('{{ \App\Http\Helpers\FileHelper::getURL($place->image) }}')"></div>

            <div class="row">

                <div class="small-12 column">

                    <div class="section-content"  style="padding-bottom: 0">

                        <div class="section-relative">

                            <h1 class="h1 width-48 {{ $place->checkedIn ? 'checkedin' : '' }}">{{ $place->name }}</h1>

                            @if ($place->checkedIn)
                                <p class="section-p"><span style="font-weight: normal">{{ \Carbon\Carbon::createFromTimestamp($place->lastCheckIn)->format('j.n.Y') }}</span></p>
                            @endif

                            <p class="section-p width-48"><span style="font-weight: normal">@lang('place-detail.place-detail.texts.4')</span><br>{{ $place->category->name ?? null }}</p>

                            <p class="section-p width-48"><span style="font-weight: normal">@lang('place-detail.place-detail.texts.1')</span><br>{{ $place->address }}</p>

                            <p class="section-p width-48" style="margin-bottom: 2rem"><span style="font-weight: normal">@lang('place-detail.place-detail.texts.2')</span><br>{{ $place->description }}</p>

                            <div class="section-relative-horizontal-line"></div>

                            <div class="section-raiting">

                                <p class="section-p">@lang('place-detail.place-detail.raiting.1')</p>

                                @if ($place->getRelation('averageStarsRatings') != null)
                                    {{-- Has ratings --}}

                                    <div class="section-raiting-content">
                                    
                                        <div class="section-raiting-item">

                                            <div class="section-raiting-item-content {{ round($place->averageStarsRatings) >= 1 ? 'is-active' : '' }}">

                                            </div>
                                            
                                        </div>
                                        <div class="section-raiting-item">

                                            <div class="section-raiting-item-content {{ round($place->averageStarsRatings) >= 2 ? 'is-active' : '' }}">

                                            </div>
                                        </div>
                                        <div class="section-raiting-item">

                                            <div class="section-raiting-item-content {{ round($place->averageStarsRatings) >= 3 ? 'is-active' : '' }}">

                                            </div>
                                        </div>
                                        <div class="section-raiting-item">

                                            <div class="section-raiting-item-content {{ round($place->averageStarsRatings) >= 4 ? 'is-active' : '' }}">

                                            </div>
                                        </div>
                                        <div class="section-raiting-item">

                                            <div class="section-raiting-item-content {{ round($place->averageStarsRatings) >= 5 ? 'is-active' : '' }}">

                                            </div>
                                        </div>
                                    
                                    </div>

                                    @if (Route::currentRouteName() == 'medals.place.detail')
                                        <a href="{{ route('medals.place.rating', ['continentId' => $continentId, 'categoryId' => $categoryId, 'placeId' => $place->id]) }}" style="text-decoration: underline">@lang('place-detail.place-detail.raiting.1')</a>
                                    @elseif (Route::currentRouteName() == 'flags.place.detail')
                                        <a href="{{ route('flags.place.rating', ['continentId' => $continentId, 'categoryId' => $categoryId, 'placeId' => $place->id]) }}" style="text-decoration: underline">@lang('place-detail.place-detail.raiting.1')</a>
                                    @else
                                        <a href="{{ route('place.rating', ['placeId' => $place->id]) }}" style="text-decoration: underline">@lang('place-detail.place-detail.raiting.2')</a>
                                    @endif

                                @else

                                    <p class="section-p">@lang('place-detail.place-detail.raiting.3')</p>

                                @endif

                            </div>

                            <div class="section-hr on-bottom"></div>

                        </div>


                    </div>

                    @if (!$place->userPhotos->isEmpty())

                        <div class="section-content">

                            <h3 style="margin-bottom: 1.5rem">@lang('place-detail.place-detail.texts.3')</h3>

                            {{-- Show user photos - only owner photos - can delete photos --}}
                            @if ($ownerUserPhotos != null)
                                @php
                                    $userOfUserPhotos = $ownerUserPhotos->first()->user;
                                @endphp

                                <div class="section-visit">

                                    <div class="section-visit-content">

                                        <div class="section-visit-photo" style="background-image: url('{{ \App\Http\Helpers\FileHelper::getURL($userOfUserPhotos->avatar) }}')"></div>

                                        <h3 class="section-visit-text">{{ $userOfUserPhotos->name }}</h3>

                                    </div>

                                </div>

                                <div class="img-box-group">

                                    {{-- Photos of one user --}}
                                    @foreach ($ownerUserPhotos as $userPhoto)

                                        <a href="{{ \App\Http\Helpers\FileHelper::getURL($userPhoto->image) }}" data-lightbox="roadtrip" class="img-box-item">
                                
                                            <div class="img-box-item-content" style="background-image: url('{{ \App\Http\Helpers\FileHelper::getURL($userPhoto->image) }}')">
                                                
                                                <div class="img-box-item-delete">

                                                    <form action="{{ route('place.delete-photo', ['photoId' => $userPhoto->id]) }}" method="POST">
                                                        
                                                        {{ csrf_field() }}

                                                        <button type="submit" class="img-box-item-delete-button" onclick="event.stopPropagation();"></button>

                                                    </form>

                                                </div>

                                            </div>
                                    
                                        </a>

                                    @endforeach
                            
                                </div>
                            @endif
                            
                            {{-- Show user photos grouped by users - not owner photos --}}
                            @foreach ($groupedUserPhotos as $userPhotos)
                                @php
                                    $userOfUserPhotos = $userPhotos->first()->user;
                                @endphp

                                <div class="section-visit">

                                    <div class="section-visit-content">

                                        <div class="section-visit-photo" style="background-image: url('{{ \App\Http\Helpers\FileHelper::getURL($userOfUserPhotos->avatar) }}')"></div>

                                        <h3 class="section-visit-text">{{ $userOfUserPhotos->name }}</h3>

                                    </div>

                                </div>

                                <div class="img-box-group">

                                {{-- Photos of one user --}}
                                @foreach ($userPhotos as $userPhoto)

                                    <a href="{{ \App\Http\Helpers\FileHelper::getURL($userPhoto->image) }}" data-lightbox="roadtrip" class="img-box-item">
                            
                                        <div class="img-box-item-content" style="background-image: url('{{ \App\Http\Helpers\FileHelper::getURL($userPhoto->image) }}')">  
                                        
                                        </div>
                                
                                    </a>

                                @endforeach
                            
                                </div>
                            @endforeach

                        </div>

                    @endif

                </div> 

            </div>


    </div>

@endsection