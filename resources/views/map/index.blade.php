
@extends("app")

@section('content')

    @if (Auth::check())
        @include('include/navigation')
    @else
        @include('include/public-navigation')
    @endif

    <div class="page-row section section-map section-gray">

        <div class="section-content-side-block js-side-block">

            <div class="section-filter">

                <p>@lang('map.map.texts.1')</p>

                <div class="section-filter-list-wrap">

                    <select class="section-filter-list js-place-categories">
                        <option class="section-filter-list-item js-item" value="">@lang('map.map.texts.2')</option>
                        @foreach ($categories as $category)
                            <option class="section-filter-list-item js-item" value="{{ $category->name }}">{{ $category->name }}</option>
                        @endforeach
                    </select>

                </div>

                <a class="main-button js-place-categories-button">@lang('map.map.buttons.1')</a>

            </div>

            <div class="box-group js-place-item-wrap">

              <div class="box-group-wrap js-place-item-list">
                <a href="{{route('place.detail',['placeId' => 'empty'])}}" class="box-photo-wrap is-hidden js-place-item">
            
                    <div class="box-item">
            
                        <div class="box-item-content">
            
                            <div class="box-item-content-photo js-place-item-image">

                            </div>

                            <div class="box-item-name">

                                <p class="js-place-item-name">
                                    Název místa
                                </p>

                            </div>

                            <div class="box-item-content-text js-place-item-checkin">
                                <i class="box-item-content-text-icon"></i>
                            </div>
            
                        </div>
            
                    </div>
            
                </a>
              </div>
            
            </div>

            <div class="section-content-side-block-button js-side-block-button"><i class="section-content-side-block-button-icon js-side-block-button-icon"></i> Místa</div>

        </div>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFFB_VGGA8SMwsff5AuHqMym3nvcYJP6o"></script>

        <div class="map js-map" data-map="places" data-public="{{ Auth::check() ? 'false' : 'true' }}">

        </div>
        
    </div>

@endsection