
@extends("app")

@section('content')

    @include('include/navigation')

    @include('include/second-navigation', [$historyBarType = 'flag', $historyBarPage = 'continentsDetail'])

    <div class="page-row section section-with-secondary-bar medals-detail section-gray">


                <div class="section-content-full-screen">

                    <div class="grid-wrap">

                        @foreach ($categories as $category)

                            <div class="grid-wrap-item grid-wrap-item-smaller small-one-of-one large-one-of-five">

                                <div class="card-content {{ $category->imageClass }}">

                                    <div class="card-label">

                                        <div class="card-text card-text-black">{{ mb_strtoupper($category->name) }}</div>

                                        <div class="card-img {{ ($category->iconImage == null && $category->userFlags->isEmpty()) ? 'silver-flag' : 'gold-flag' }}" @if($category->iconImage != null) style="background-image: url('{{ \App\Http\Helpers\FileHelper::getURL($category->iconImage) }}')" @endif></div>

                                        <a href="{{ route('flags.places', ['continentId' => $continentId, 'categoryId' => $category->id]) }}" class="main-button main-button-min">@lang('continents-detail.continents-detail.buttons.1')</a>

                                    </div>

                                </div>

                            </div>

                        @endforeach
                         
                    </div>

                </div> 

    </div>

@endsection