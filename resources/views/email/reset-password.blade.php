{{-- Markdown email template - cropped copy of notifications::email --}}
@component('mail::message')

# Map'n'paw

## Zapomenuté heslo

Poslali jste žádost o resetování hesla k vašemu účtu.

{{-- Action Button --}}
@isset($actionText)
@component('mail::button', ['url' => $actionUrl, 'color' => 'blue'])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}

@endforeach

{{-- Subcopy --}}
@isset($actionText)
@component('mail::subcopy')
Pokud vám nejde stisknout tlačítko "{{ $actionText }}", otevřete následující
adresu v prohlížeči: [{{ $actionUrl }}]({{ $actionUrl }})
@endcomponent
@endisset
@endcomponent