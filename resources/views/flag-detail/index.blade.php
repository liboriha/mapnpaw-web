
@extends("app")

@section('content')

    @include('include/navigation')

    @include('include/second-navigation', [$historyBarType = 'flag', $historyBarPage = 'detail'])


    <div class="page-row section section-with-secondary-bar medals-detail section-gray">

                <div class="section-content-full-screen">

                    <div class="grid-wrap">

                        @foreach ($continents as $continent)

                            <div class="grid-wrap-item grid-wrap-item-small small-one-of-one large-one-of-five">

                                <div class="card-content {{ $continent->imageClass }}">

                                    <div class="card-label">

                                        <div class="card-text card-text-black">{{ mb_strtoupper($continent->name) }}</div>

                                        <p>@lang('detail.detail.for-flag.1')</p>

                                        <div class="card-text card-text-green">{{ $continent->flagsCount }}</div>

                                        <a href="{{ route('flags.categories', ['continentId' => $continent->id]) }}" class="main-button main-button-min">@lang('detail.detail.buttons.1')</a>

                                    </div>

                                </div>

                            </div>

                        @endforeach
                         
                    </div>

                </div> 

    </div>

@endsection