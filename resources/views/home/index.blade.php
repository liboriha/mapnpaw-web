
@extends("app")

@section('content')

    @include('include/navigation-landing')
    @include('include/header-landing')


    <div class="page-row section-landing" id="how-it-function">

        <div class="section-landing-content">

            <div class="row">

                <div class="small-12 column">

                    <h2 class="h2-landing">@lang('section-how-it-function.how-it-function.1')</h2>

                </div>
            
            </div>

            <div class="row">

                <div class="small-12 medium-6 column">

                    <div class="section-landing-box-wrap section-landing-box-wrap-img">

                        <div class="section-landing-box-content-img screen-like"></div>

                    </div>

                </div>

                <div class="small-12 medium-6 column">

                    <div class="section-landing-box-wrap section-landing-box-wrap-text">

                        <div class="section-landing-box-content-text">

                            <h3 class="h3-landing">@lang('section-how-it-function.how-it-function.2')</h3>
                        
                            <p>@lang('section-how-it-function.how-it-function.3')</p>
                    
                        </div>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="small-12 medium-6 medium-push-6 column">

                    <div class="section-landing-box-wrap section-landing-box-wrap-img">

                        <div class="section-landing-box-content-img screen-medal"></div>

                    </div>

                </div>

                <div class="small-12 medium-6 medium-pull-6 column">

                    <div class="section-landing-box-wrap section-landing-box-wrap-text">

                        <div class="section-landing-box-content-text">

                            <h3 class="h3-landing h3-right">@lang('section-how-it-function.how-it-function.4')</h3>
                        
                            <p class="p-right">@lang('section-how-it-function.how-it-function.5')</p>
                    
                        </div>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="small-12 medium-6 column">

                    <div class="section-landing-box-wrap section-landing-box-wrap-img">

                        <div class="section-landing-box-content-img screen-flag"></div>

                    </div>

                </div>

                <div class="small-12 medium-6 column">

                    <div class="section-landing-box-wrap section-landing-box-wrap-text">

                        <div class="section-landing-box-content-text">

                            <h3 class="h3-landing">@lang('section-how-it-function.how-it-function.6')</h3>
                        
                            <p>@lang('section-how-it-function.how-it-function.7')</p>
                    
                        </div>

                    </div>

                </div>

            </div>

        </div>
        
    </div>

    <div class="page-row section-landing section-landing-download" id="download">

        <div class="section-landing-content-download">

            <div class="row">

                <div class="small-12 column">

                    <h4 class="h4-landing">@lang('section-download.section-download.1')</h4>

                    <div class="landing-button-group landing-button-group-center">

                        <a class="button-landing">
                            <i class="button-landing-img app-store"></i>@lang('buttons-landing.download-button.app-store')</a>
                
                        <a class="button-landing">
                            <i class="button-landing-img play-store"></i>@lang('buttons-landing.download-button.play-store')</a>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="page-row section-world">

        <div class="row">

            <div class="small-12 medium-6 medium-centered large-3 large-uncentered column">

                <div class="section-landing-box-one">

                    <h3 class="h3-landing">@lang('section-world.section-world.1')</h3>

                    <p>@lang('section-world.section-world.2')</p>

                </div>

            </div>

        </div>

        <div class="section-landing-world-img"></div>

    </div>

    @include('include/footer-landing')

@endsection

@section('popups')

    @include('include/popup/log-in')

    @include('include/popup/forgot-password')

    @if (isset($restoreToken))
        @include('include/popup/reset-password')
    @endif

    @include('include/popup/register')

@endsection