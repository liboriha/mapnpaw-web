
@extends("app")

@section('content')

    @include('include/navigation')


    <div class="page-row section profile">

        <div class="section-content">

            <div class="row">

                <div class="small-12 column">

                    <div class="section-profile">

                        <form action="{{ route('profile.update') }}" method="POST" enctype="multipart/form-data">

                        {{ csrf_field() }}

                        <div class="section-profile-photo {{ \App\BladeHelper::failClass('photo-file') }} @if($isFacebookUser != false) help @endif js-profile-image" style="background-image: url('{{ \App\Http\Helpers\FileHelper::getURL($user->avatar) }}')" @if($isFacebookUser != false) title="@lang('form.form.help.facebook-user')" @endif>

                            @if($isFacebookUser == false)

                            <label for="photo-file" class="section-profile-photo-content">

                            @lang('form.form.labels.profile-photo')

                            </label>

                            @endif

                        </div>
                                    
                        {!! \App\BladeHelper::addFailParagraph('photo-file') !!}

                        <input type="file" name="photo-file" id="photo-file" class="input-file js-profile-input"  @if($isFacebookUser != false) disabled @endif>

                        <div class="section-form">

                            <div class="input-row input-row-clear {{ \App\BladeHelper::failClass('name') }}">
                                
                                <label for="name" class="section-label">@lang('form.form.labels.name')</label>
                            
                                <input type="text" name="name" id="name" class="section-input" value="{{ \App\BladeHelper::old('name', $user->firstname) }}" @if($isFacebookUser != false) disabled title="@lang('form.form.help.facebook-user')" @endif>
                                    
                                {!! \App\BladeHelper::addFailParagraph('name') !!}
                        
                            </div>

                            <div class="input-row input-row-clear {{ \App\BladeHelper::failClass('last-name') }}">
                                
                                <label for="last-name" class="section-label">@lang('form.form.labels.last-name')</label>
                            
                                <input type="text" name="last-name" id="last-name" class="section-input" value="{{ \App\BladeHelper::old('last-name', $user->lastname) }}" @if($isFacebookUser != false) disabled title="@lang('form.form.help.facebook-user')" @endif>
                                    
                                {!! \App\BladeHelper::addFailParagraph('last-name') !!}
                        
                            </div>

                            <div class="input-row input-row-clear {{ \App\BladeHelper::failClass('nickname') }}">
                                
                                <label for="nickname" class="section-label">@lang('form.form.labels.nickname')</label>
                            
                                <input type="text" name="nickname" id="nickname" class="section-input" value="{{ \App\BladeHelper::old('nickname', $user->nickname) }}">
                                    
                                {!! \App\BladeHelper::addFailParagraph('nickname') !!}
                            
                            </div>



                            <div class="input-row input-row-clear {{ \App\BladeHelper::failClass('email') }}">
                                
                                <label for="email" class="section-label">@lang('form.form.labels.mail')</label>
                                
                                <input type="text" name="email" id="email" class="section-input" value="{{ \App\BladeHelper::old('email', $user->email) }}"  @if($isFacebookUser != false) disabled title="@lang('form.form.help.facebook-user')" @endif>
                                    
                                {!! \App\BladeHelper::addFailParagraph('email') !!}
                            
                            </div>

                          @if ($isFacebookUser == false)

                            <div class="input-row input-row-clear {{ \App\BladeHelper::failClass('password') }}">
                                
                                <label for="password" class="section-label">@lang('form.form.labels.change-password')</label>
                                
                                <input type="password" name="password" id="password" class="section-input" value="">
                                    
                                {!! \App\BladeHelper::addFailParagraph('password') !!}
                            
                            </div>

                            @endif

                            <div class="checkbox-group {{ \App\BladeHelper::failClass('name-visibility') }}">

                                <div class="checkbox-group-wrap">

                                      <input type="radio" name="name-visibility" value="{{ \App\User::VISIBILITY_NAME }}" id="name-is-visible" class="input-checkbox" {{ \App\BladeHelper::old('name-visibility', $user->visibility) != \App\User::VISIBILITY_NICKNAME ? 'checked' : '' }}>
                                    
                                    <label for="name-is-visible" class="checkbox-label">@lang('form.form.labels.name-visible')<i class="checkbox-icon"></i></label>
                                
                                </div>

                                <div class="checkbox-group-wrap">

                                    <input type="radio" name="name-visibility" value="{{ \App\User::VISIBILITY_NICKNAME }}" id="nick-is-visible" class="input-checkbox" {{ \App\BladeHelper::old('name-visibility', $user->visibility) == \App\User::VISIBILITY_NICKNAME ? 'checked' : '' }}>
                                    
                                    <label for="nick-is-visible" class="checkbox-label">@lang('form.form.labels.nickname-visible')<i class="checkbox-icon"></i></label>
                                
                                </div>
                                    
                                {!! \App\BladeHelper::addFailParagraph('name-visibility') !!}

                            </div>

                            <button class="main-button main-button-min">@lang('form.form.buttons.save')</button>

                        </div>

                        </form>

                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection