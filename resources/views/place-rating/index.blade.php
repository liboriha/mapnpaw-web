
@extends("app")

@section('content')

    @if (Auth::check())
        @include('include/navigation')
    @else
        @include('include/public-navigation')
    @endif

    @if (Route::currentRouteName() == 'medals.place.rating')
        @include('include/second-navigation', [$historyBarType = 'medals', $historyBarPage = 'placeRating'])
    @elseif (Route::currentRouteName() == 'flags.place.rating')
        @include('include/second-navigation', [$historyBarType = 'flag', $historyBarPage = 'placeRating'])
    @endif

    <div class="page-row section place-raiting">

        <div class="header" style="background-image: url('{{ \App\Http\Helpers\FileHelper::getURL($place->image) }}')"></div>

            <div class="row">

                <div class="small-12 column">

                    <div class="section-content" style="padding-bottom: 1rem">

                        <h1 class="h1 width-48 ">{{ $place->name }}</h1>

                        <p class="section-p width-48 ">{{ $place->address }}</p>

                    </div>

                    <div class="section-hr"></div>

                    <div class="section-content">

                        @foreach ($place->userRatings->reverse() as $userRating)

                            <div class="section-relative section-padding">

                                <div class="section-visit">

                                    <div class="section-visit-content">

                                        <div class="section-visit-photo" style="background-image: url('{{ \App\Http\Helpers\FileHelper::getURL($userRating->user->avatar) }}')"></div>

                                        <div class="section-visit-text-with-description">
                                            <h3>{{ $userRating->user->name }}</h3>
                                            <p>@lang('place-rating.place-rating.texts.1') {{ $userRating->created_at ? $userRating->created_at->format('j.n.Y') : '' }}</p>
                                        </div>

                                    </div>

                                </div>

                                <p class="section-p width-48">{{ $userRating->rating }}</p>

                                <div class="section-raiting">

                                    <div class="section-raiting-content">
                                    
                                        <div class="section-raiting-item">

                                            <div class="section-raiting-item-content {{ round($userRating->stars) >= 1 ? 'is-active' : '' }}">

                                            </div>
                                            
                                        </div>
                                        <div class="section-raiting-item">

                                            <div class="section-raiting-item-content {{ round($userRating->stars) >= 2 ? 'is-active' : '' }}">

                                            </div>
                                        </div>
                                        <div class="section-raiting-item">

                                            <div class="section-raiting-item-content {{ round($userRating->stars) >= 3 ? 'is-active' : '' }}">

                                            </div>
                                        </div>
                                        <div class="section-raiting-item">

                                            <div class="section-raiting-item-content {{ round($userRating->stars) >= 4 ? 'is-active' : '' }}">

                                            </div>
                                        </div>
                                        <div class="section-raiting-item">

                                            <div class="section-raiting-item-content {{ round($userRating->stars) >= 5 ? 'is-active' : '' }}">

                                            </div>
                                        </div>
                                    
                                    </div>

                                </div>

                                <div class="section-hr bottom-relative"></div>

                            </div>

                        @endforeach

                    </div>

                </div> 

            </div>

        </div>

    </div>

@endsection