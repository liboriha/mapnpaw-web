
@extends("app")

@section('content')

    @include('include/navigation')


    <div class="page-row section profile">

        <div class="section-content section-margin-top">

            <div class="row">

                <div class="small-12 column">

                    <div class="section-profile">

                        <div class="section-profile-photo" style="background-image: url('{{ \App\Http\Helpers\FileHelper::getURL($user->avatar) }}')"></div>

                        <div class="section-padding">

                            <h1>{{ trim($user->firstname . " " . $user->lastname) }}</h1>

                            <p>{{ $user->nickname }}</p>

                            <p><i class="email-icon"></i> {{ $user->email }}</p>

                        </div>

                        <div class="button-group-row">

                            <div class="button-group-row-wrap">
                                
                                <a href="{{ route('profile.edit') }}" class="main-button">@lang('form.form.buttons.profile-edit')</a>
                        
                            </div>
                            <div class="button-group-row-wrap">
                                
                                <a href="{{ route('logout') }}" class="main-button main-button-white">@lang('form.form.buttons.logout')</a>

                            </div>

                    </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@endsection