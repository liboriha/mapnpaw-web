
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="keywords">
        <meta name="description" content="description">
        <meta name="author" content="pixelmate.cz">
        @if (Auth::check())
            <meta name="jwt-token" content="{{ Tymon\JWTAuth\Facades\JWTAuth::fromUser(Auth::user()) }}">
        @endif
        <meta name="X-API-Key" content="{{ config('awapp.X-API-Key') }}">
        <meta name="API-locale" content="{{ App::getLocale() }}">

        <title>{{ config('app.name') }}</title>

        <link rel="shortcut icon" href="{{ asset('images/favicon/favicon.ico') }}" type="image/x-icon">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    </head>
    <body style="background-color: white;">

        <script>
            {{-- Show js popup window if needed. --}}
            @if (session('show-js-popup', null) != null)
                document.showPopup = '{{ session('show-js-popup') }}';
            @endif
        </script>

        @yield('content')
        @yield('popups')

        <script src="{{ asset('js/app.js') }}"></script>

        {{-- livereload --}}
        @if (getenv('APP_ENV') === 'local')

            <style>
                #__bs_notify__ {display: none !important;}
            </style>

            <script id="__bs_script__">
                document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.18.6'><\/script>".replace("HOST", location.hostname));
            </script>

        @endif

    </body>
</html>
