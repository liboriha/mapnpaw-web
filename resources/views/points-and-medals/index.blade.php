
@extends("app")

@section('content')

    @include('include/navigation')


    <div class="page-row section points-and-medals">

        <div class="section-content-full-screen">

            <div class="grid-wrap grid-wrap-reverse">

                <div class="grid-wrap-item grid-wrap-item-min-height small-one-of-one large-one-of-three">

                        <div class="card-content  card-content-min-height looking-man">

                            <div class="card-label">

                                <div class="card-img like"></div>

                                <div class="card-text">@lang('points-and-medals.points-and-medals.texts.1')</div>

                                <a class="main-button main-button-min main-button-transparent button-hover-green js-content-changer">@lang('points-and-medals.points-and-medals.buttons.1')</a>

                            </div>

                        </div>

                        <div class="card-content-hide card-content js-hide-content">

                            <div class="card-content-hide-top">

                                    <div class="card-label">

                                        <div class="card-img like-green"></div>
                                        
                                        <div class="card-text card-text-black">@lang('points-and-medals.points-and-medals.hide-box.1')</div>

                                        <div class="card-text card-text-black" style="margin-top:1rem; text-transform: uppercase">{{ trans_choice('points-and-medals.points-and-medals.hide-box.4', $pointsPhotos + $pointsCheckins, ['points' => $pointsPhotos + $pointsCheckins]) }}</div>

                                    </div>

                            </div>

                            <div class="card-content-hide-bottom-left">

                                <div class="card-label">

                                    <p>@lang('points-and-medals.points-and-medals.hide-box.2')</p>
                                    
                                    <div class="card-content-hide-bottom-img photo-and-plus"></div>
                                    
                                    <div class="card-text">{{ trans_choice('points-and-medals.points-and-medals.hide-box.4', $pointsPhotos, ['points' => $pointsPhotos]) }}</div>

                                </div>

                            </div>

                            <div class="card-content-hide-bottom-right">

                                <div class="card-label">

                                    <p>@lang('points-and-medals.points-and-medals.hide-box.3')</p>
                                    
                                    <div class="card-content-hide-bottom-img check"></div>
                                    
                                    <div class="card-text">{{ trans_choice('points-and-medals.points-and-medals.hide-box.4', $pointsCheckins, ['points' => $pointsCheckins]) }}</div>

                                </div>

                            </div>

                        </div>

                </div>

                <div id="medals" class="grid-wrap-item grid-wrap-item-min-height small-one-of-one large-one-of-three">

                        <div class="card-content  card-content-min-height man-and-river">

                            <div class="card-label">

                                <div class="card-img medal"></div>

                                <div class="card-text">@lang('points-and-medals.points-and-medals.texts.2')</div>

                                <a href="{{ route('medals') }}" class="main-button main-button-min main-button-transparent button-hover-orange">@lang('points-and-medals.points-and-medals.buttons.1')</a>

                            </div>    

                        </div>

                </div>

                <div id="flags" class="grid-wrap-item grid-wrap-item-min-height small-one-of-one large-one-of-three">

                        <div class="card-content  card-content-min-height tour-map">

                            <div class="card-label">

                                <div class="card-img flag"></div>

                                <div class="card-text">@lang('points-and-medals.points-and-medals.texts.3')</div>

                                <a href="{{ route('flags') }}" class="main-button main-button-min main-button-transparent button-hover-blue">@lang('points-and-medals.points-and-medals.buttons.1')</a>

                            </div>

                        </div>

                </div>

            </div>

        </div>

    </div>

@endsection