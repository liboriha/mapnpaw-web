
@extends("app")

@section('content')

    @include('include/navigation')

    <div class="page-row section section-map section-gray">

        <div class="section-content-side-block js-side-block">

            <div class="box-group without-filter js-place-item-list">
            
                <a href="{{route('place.detail',['placeId' => 'empty'])}}" class="box-photo-wrap is-hidden js-place-item">
            
                    <div class="box-item">
            
                        <div class="box-item-content">
            
                            <div class="box-item-content-photo js-place-item-image">

                            </div>
            
                            <div class="box-item-name">

                                <p class="js-place-item-name">
                                    Název místa
                                </p>

                            </div>

                            <div class="box-item-content-text js-place-item-checkin">
                                <i class="box-item-content-text-icon"></i>
                            </div>
            
                        </div>
            
                    </div>
            
                </a>
            
            </div>

            <div class="section-content-side-block-button js-side-block-button"><i class="section-content-side-block-button-icon js-side-block-button-icon"></i> Místa</div>

        </div>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBFFB_VGGA8SMwsff5AuHqMym3nvcYJP6o"></script>

        <div class="map js-map" data-map="places-history">

        </div>
        
    </div>

@endsection