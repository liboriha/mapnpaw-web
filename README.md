# Pixelmate Dev stack

## Backend installation

- `composer install`
- `cp .env.example .env`
- Configure `.env` (primarily database and also email, facebook and google maps)
- `php artisan package:discover`
- `php artisan key:generate`
- `php artisan storage:link`
- `php artisan migrate --seed`
- `bash post-install.sh`
- `php artisan jwt:secret`

### JWT token authentication
- For better JWT token authentication (delayed invalidation - due to async), set queue driver `QUEUE_DRIVER` different than `sync`.

### Adding testing data

- `php artisan db:seed --class=TestDataSeeder` (email: `user@test.cz`, password: `123456`)

### Build place clusters

- `php artisan placeClusters:build`
  - Parameters:
    - `--showLevelProgress` : Show detailed level progress

### Place deduplication

- `places:deduplication`
  - Parameters:
    - `--pretend` : Define if only do log without really deleting places}
    - `--noClosePlaces` : Define if not to deduplicate close places}
    - `--noSameNames` : Define if not to deduplicate places with same names and distance}
    - `--samePlaceCategories=3,13,23,33,43` : Selected categories IDs to deduplicate same names}
    - `--closePlacesDistance=0.00005` : Distance in sphere degrees to determine close points}
    - `--sameNamesDistance=4000` : Distance in meters to determine close points with same names}
    - `--latDivide=6` : Divide process of close places - divide latitude axis to X parts}
    - `--lngDivide=12` : Divide process of close places - divide longitude axis to X parts}';

## Frontend installation

- npm install

## Running application in development

### Backend

- `php artisan serve`

### Frontend

- `npm run watch`

## Building application for production

- `npm run prod`
